
https://sive.rs/ff
# digital garden

#knowledge #project

## Theory
![[dg_scheme.png]]

__________________________________

### [[people.AndyMatuschak]] about his digital garden

>These notes are mostly written for myself: they’re roughly my thinking environment ([Evergreen notes](https://notes.andymatuschak.org/z4SDCZQeRo4xFEQ8H4qrSqd68ucpgE6LU155C); [My morning writing practice](https://notes.andymatuschak.org/zVFGpprS64TzmKGNzGxq9FiCDnAnCPwRU5T)). But I’m sharing them publicly as an experiment ([Work with the garage door up](https://notes.andymatuschak.org/z21cgR9K3UcQ5a7yPsj2RUim3oM2TzdBByZu)). If a note seems confusing or under-explained, it’s probably because I didn’t write it for you! Sorry—that’s sort of an essential tension of this experiment ([Write notes for yourself by default, disregarding audience](https://notes.andymatuschak.org/z8AfCaQJdp852orumhXPxHb3r278FHA9xZN8J)).


### Garage doors open

[Szymon Kaliski on Twitter: "two things have been on my mind lately (or maybe I‘m only verbalizing them now): pixel space in digital tools (https://t.co/FUo2NUwHo5) and “fractality” of some things in software (https://t.co/t8OlhGOGPJ) sharing in an attempt to “work with the garage doors open”" / Twitter](https://mobile.twitter.com/szymon_k/status/1320040354117263362?s=20)


### Articles
- [A Brief History & Ethos of the Digital Garden](https://maggieappleton.com/garden-history)
- [Digital Gardening by Maggie Appleton](https://github.com/MaggieAppleton/digital-gardeners) !!!
- https://twitter.com/Mappletons/status/1250532315459194880
- [The Swale: Weaving between Garden and Stream](https://bonkerfield.org/2020/05/swale-garden-stream/)
- [NeoCyborgs](https://maggieappleton.com/neocyborgs)
- [...sharing in an attempt to “work with the garage doors open”" / Twitter](https://mobile.twitter.com/szymon_k/status/1320040354117263362?s=20)
- sidereal
	- [Matuschak - Enabling environment](https://notes.andymatuschak.org/z3DaBP4vN1dutjUgrk3jbEeNxScccvDCxDgXe)

### Gardens
- [Second-Brain](https://github.com/KasperZutterman/Second-Brain)
	- A curated list of awesome Public Zettelkastens 🗄️ / Second Brains 🧠 / Digital Gardens 🌱
- [maggieappleton](https://maggieappleton.com/garden/) 
- [Andy Matuschak](https://notes.andymatuschak.org)
- - [nikitavoloboev/knowledge: Everything I know](https://github.com/nikitavoloboev/knowledge)
- https://groktiddlywiki.com/read/

### [[concepts.knowledge managment]]

- [Every Page Is Page One](https://everypageispageone.com/the-book/)
- [PARA](https://fortelabs.co/blog/para/) #para 
- [Clean Up Your Mess - A Guide to Visual Design for Everyone](http://www.visualmess.com/)
- [Immersive mediums replace thought](https://notes.azlen.me/s3husuaw/)
- [nikitavoloboev/knowledge: Everything I know](https://github.com/nikitavoloboev/knowledge)
- [Memex Dreams ‒ Szymon Kaliski](https://szymonkaliski.com/writing/2020-04-19-memex-dreams)

## tools 
- https://www.notion.so/Artificial-Brain-Networked-notebook-app-a131b468fc6f43218fb8105430304709

### instruments

#### [tools.obsidian](tools.obsidian.md)

#### [[tools.dendron]]

#### [tools.markdown](tools.markdown.md)

[agora](https://github.com/flancian/agora)

### tools - wiki 
#### tiddlywiki
- https://nesslabs.com/digital-garden-tiddlywiki
- https://giffmex.org/stroll/stroll.html
	- https://giffmex.org/gifts/tiddlyblink.html#TiddlyBlink%20home
- saving
	-  https://tiddlywiki.com/#Saving%20via%20WebDAV
	- https://tiddlywiki.com/#Saving%20on%20TiddlySpot 

#### mediawiki

#### [Growi](https://growi.org/en/)
- 『Markdown × Wiki × Free』
- https://growi.org/en/#features
#hedgedoc


### Related internal
#### [areas.filetag](areas.filetag.md)

---------------------------------
---------------------------------

## To Sort
 - https://wiki.thingsandstuff.org/Main_Page
 - https://bonkerfield.org/tag/art/
 - http://gordonbrander.com/pattern/
 - https://tomcritchlow.com/wiki/media-theory/
 - https://busterbenson.com/piles/
 - http://gordonbrander.com/pattern/design-patterns/




https://www.are.na/ajmir-kandola/gardens-of-tomorrow