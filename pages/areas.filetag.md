# filetag
#knowledge #media

## 101

_How to organize and tag local files?_
_How to work with various media files?_

## To follow
- [[concepts.filesystem]]
	- [[concepts.flat-file]]
- [[concepts.archive]]
	- [[concepts.archives.art]]
- [[incubation.annotation.media]]
- [[incubation.audio.sample.managment]]
- [[audio.metadata]]
- [[projects.upend]]
### Cuts
- [[fulldocs.matuschak.ontologies.associative]]
- [[fulldocs.voit.files.managment]]

## Computer Tools

#### [tagspaces](https://www.tagspaces.org/)
- file manager + tagger ==> organizer
	- + digital asset managment	
- multiplatform

### [[concepts.filesystem]] related
#### [Everything](https://www.voidtools.com/)
- Windows file indexation + search
- linux alternative: fsearch, locate

#### [tmsu](https://tmsu.org/)
>TMSU is a tool for tagging your files. It provides a simple command-line tool for applying tags and a virtual filesystem so that you can get a tag-based view of your files from within any other program. 

### [[concepts.Digital Asset Managment]]

_Operations on a collection of digital assets require the use of a computer application implementing digital asset management to ensure that the owner, and possibly their delegates, can perform operations on the data files._


## Adjacent

- [incubation.annotation.media](incubation.annotation.media.md)
- [concepts.Algorithmic Editing](concepts.Algorithmic%20Editing.md)
- [[concepts.map]]
- [[concepts.trailblazers]]
- [[dga.flat.graph]]
- [[incubation.mediamateriality]]
- [[incubation.interface]]

## Unsorted
youtube - STOP! Don't Name That File Without First Watching This Video. - https://www.youtube.com/watch?v=Wu0CxdflECY



