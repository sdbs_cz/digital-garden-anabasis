# [[las]].future
[[las.roadmap]] 

## [[las.future.dokumentace]]


## [[las.future.technicalia]]

## Topicalia
+ [[las.missing]]
+ [[las.quotes.2022]] //main doc for now
- space bar signs 
-

## - [ ] Rename
- Line and Surface X Line of Flight [[1000p]]

---------------------------------

---------------------------------

---------------------------------

---------------------------------

---------------------------------

>Information is like a bank. Some of us are rich, some of us are poor with information. All of us can be rich. Our job - your job is to rob the bank, to kill the guard. We go out there to destroy everybody, who keeps and hides the whole information. 
 - Genesis P. Orridge


> Draw the line, says the accountant: but one can in fact draw it anywhere. 
 - Gilles Deleuze

> We live in capitalism. Its power seems inescapable. So did the divine right of kings. Any human power can be resisted and changed by human beings. Resistance and change often begin in art, and very often in our art, the art of words.
>  - Ursula K. Le Guin

-----------------------------------

- https://wiki.killuglyradio.com/wiki/Project/Object_concept [[people.FrankZappa]]