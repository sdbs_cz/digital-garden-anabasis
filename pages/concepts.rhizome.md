
# Rhizome

## Biology
>**Rhizome** Definition. A **rhizome** (also known as rootstocks) is a type of plant stem situated either at the soil surface or underground that contains nodes from which roots and shoots originate (shown below). **Rhizomes** are unique in that they grow perpendicular, permitting new shoots to grow up out of the ground.




![](https://upload.wikimedia.org/wikipedia/commons/b/b1/Euphorbia_rhizophora2_ies.jpg)

### also some roots cause tree structere
>![[Pasted image 20221125012911.png]]
> - https://images.wur.nl/digital/collection/coll13/search

## philosophy --> [[fulldocs.twitter.TaylorAdkins.rhizome]]

## Adjacent

- [[concepts.context]]
- [[concepts.map]]
- [[1000p]]
- [[incubation.puzzles]]
- [[concept.holon]]