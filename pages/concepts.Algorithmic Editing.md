# Algorithmic Editing
#chaostream 

## Articles
- [A Brief History of Algorithmic Editing](https://medium.com/janbot/a-brief-history-of-algorithmic-editing-732c3e19884b)
- [Why should machines make art?](https://medium.com/janbot/should-machines-b59af9c5c7ab)
	- An Experimental AI Film Director Calls it Quits https://hyperallergic.com/750870/jan-bot-amsterdam-filmmuseum-filmmaker-ai/​
		- ---> jan bot NFTed out // [[nft]] / #hellsbells
- https://link.medium.com/TrPYQzYVPbb
- https://subconscious.substack.com/p/hypertext-montage
	- great junction point toward [[concepts.trailblazers]]
- https://www.videomaker.com/how-to/editing/the-art-and-science-of-algorithmic-editing/
	- not really interesting

### Short
- [How Artificial Intelligence is Transforming Video Editing](https://www.intelligenthq.com/artificial-intelligence-transforming-video-editing/)
- [A new algorithm makes editing video as easy as editing text](https://engineering.stanford.edu/magazine/article/new-algorithm-makes-editing-video-easy-editing-text)
- [Immersive mediums replace thought](https://notes.azlen.me/s3husuaw/)

## Inspiring projects
- <https://www.jan.bot/>
	- <https://medium.com/janbot/jan-bots-step-by-step-822b831d0402>
	- <https://medium.com/janbot>
- [narra project AKA CAS in the past](https://narra.eu/)
- popcorn (check [[tools.EDL]])
- office vodoo
- [Writing a Program that Edits my Videos](https://www.youtube.com/watch?v=0ZeO0IQaJ-A) ?
- https://geocinema.network/
- http://owm.hek.ch/js !!




## #techmech 
- <https://analyticsindiamag.com/top-5-ai-powered-video-editing-tools/>

### Commercial
- Magisto 
	- https://www.magisto.com/
- Video Puppet
	- https://www.narakeet.com/news/2020/08/20/videopuppet-is-now-narakeet.html


### Video
- [AviSynth](http://avisynth.nl/index.php/Main_Page)
- [Auto-Editor](https://github.com/WyattBlue/auto-editor)
- https://github.com/DevonCrawford/Video-Editing-Automation
- https://pypi.org/project/moviepy/

### Audio
- https://github.com/delthas/libpaulstretch
- https://github.com/Singhak/MultimediaChanger
- https://github.com/Sciss/Eisenkraut

### Codecs
- https://forum.shotcut.org/t/suggested-codecs-file-formats-for-editing-speed-reduced-lagging/11756



# Adjacent
- [[concepts.hypertext]]
- [[Bottom up writing prevents confirmation bias and provide a shorter and iterative feedback loop]]
- [[incubation.concepts.DatabaseArt]]
- [[tools.EDL]]
- [[ai.image.processing]]
- [[ai.narration]]