# mermaid

- [ ] example file
	- [x] ef gearogram?
		- [x] https://hedgedoc.sdbs.cz/s/SkiwAiZPv#
	- [ ] [[tools.EDL]] ? 

## 101
- https://mermaid-js.github.io/mermaid/
- https://medium.com/better-programming/making-diagrams-fun-again-with-mermaid-8a2c9ea3e471
- https://www.diagrams.net/blog/mermaid-diagrams

## Mermaid usecase extended
- mermaid notation
	- --> 
		- [[tools.EDL]]
		- input list

## Mermaid friendly software
- [[tools.hedgedoc]]
- [[tools.obsidian]]


------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

# Mermaid 101
:::warning 
fucked
:::
## Diagrams that mermaid can render:

## Flowchart 

graph TD; A-->B; A-->C; B-->D; C-->D;

￼

## Sequence diagram

sequenceDiagram participant Alice participant Bob Alice->>John: Hello John, how are you? loop Healthcheck John->>John: Fight against hypochondria end Note right of John: Rational thoughts <br/>prevail! John-->>Alice: Great! John->>Bob: How about you? Bob-->>John: Jolly good!

￼

## Gantt diagram

gantt dateFormat YYYY-MM-DD title Adding GANTT diagram to mermaid excludes weekdays 2014-01-10 section A section Completed task :done, des1, 2014-01-06,2014-01-08 Active task :active, des2, 2014-01-09, 3d Future task : des3, after des2, 5d Future task2 : des4, after des3, 5d

￼

Class diagram - ￼ experimental

classDiagram Class01 <|-- AveryLongClass : Cool Class03 *-- Class04 Class05 o-- Class06 Class07 .. Class08 Class09 --> C2 : Where am i? Class09 --* C3 Class09 --|> Class07 Class07 : equals() Class07 : Object[] elementData Class01 : size() Class01 : int chimp Class01 : int gorilla Class08 <--> C2: Cool label

￼

Git graph - ￼ experimental

gitGraph: options { "nodeSpacing": 150, "nodeRadius": 10 } end commit branch newbranch checkout newbranch commit commit checkout master commit commit merge newbranch 

￼

Entity Relationship Diagram - ￼ experimental

erDiagram CUSTOMER ||--o{ ORDER : places ORDER ||--|{ LINE-ITEM : contains CUSTOMER }|..|{ DELIVERY-ADDRESS : uses 

￼

User Journey Diagram

journey title My working day section Go to work Make tea: 5: Me Go upstairs: 3: Me Do work: 1: Me, Cat section Go home Go downstairs: 5: Me Sit down: 5: Me

￼

