## Questions
### Interkonektivity via SLI
sli becomes NVlink 

##### https://en.wikipedia.org/wiki/Scalable_Link_Interface
>SLI allows two, three, or four graphics processing units (GPUs) to share the workload when rendering real-time 3D computer graphics.

>Not all motherboards with multiple PCI-Express x16 slots support SLI. 

##### SLI HB Bridge 
https://www.gpumag.com/nvidia-sli-and-compatible-cards/

>**High-Bandwidth Bridge or SLI HB Bridge** (650 MHz Pixel Clock and 2GB/s Bandwidth) – This is the fastest bridge and is sold exclusively by Nvidia. It’s recommended for monitors up to 5K and surround. SLI HB Bridges are only available in 2-way configurations.

##### resume?
- [SLI is not needed for 3d renders](https://linustechtips.com/topic/896397-is-sli-really-needed-for-rendering/)

- [SLI and VR](https://www.quora.com/Is-SLI-worth-it-for-VR?share=1)

- [ SLI in gamemachine](https://www.quora.com/Is-SLI-worth-it-in-a-gaming-machine)
- SLI and nvidia quadro?

- [ SLI and machine learning]
	- https://www.mathworks.com/matlabcentral/answers/482171-can-i-ues-nvidia-gpu-sli-bridges-for-better-deep-learning-performance
### VRam share?

- is it possible?
	- --> seems not possible ---> 11GB

### Machine Learning
- https://www.pugetsystems.com/labs/hpc/NVIDIA-GTX-1080Ti-Performance-for-Machine-Learning----as-Good-as-TitanX-913/

#### Hardware setup

##### !!
- https://timdettmers.com/2020/09/07/which-gpu-for-deep-learning/
- https://timdettmers.com/2018/12/16/deep-learning-hardware-guide/
- https://www.reddit.com/r/buildapc/comments/inqpo5/multigpu_seven_rtx_3090_workstation_possible/
##### unsorted
- https://blog.slavv.com/the-1700-great-deep-learning-box-assembly-setup-and-benchmarks-148c5ebe6415 !!!
- https://medium.com/the-mission/how-to-build-the-perfect-deep-learning-computer-and-save-thousands-of-dollars-9ec3b2eb4ce2
- http://guanghan.info/blog/en/my-works/building-our-personal-deep-learning-rig-gtx-1080-ubuntu-16-04-cuda-8-0rc-cudnn-7-tensorflowmxnetcaffedarknet/
- https://www.servethehome.com/deeplearning10-the-8x-nvidia-gtx-1080-ti-gpu-monster-part-1/
- multi gpus
	- https://hackerfall.com/story/which-gpus-to-get-for-deep-learning
	- https://medium.com/analytics-vidhya/setting-up-a-multi-gpu-machine-and-testing-with-a-tensorflow-deep-learning-model-c35ad76603cf
- cluster networking
	- https://timdettmers.com/2014/09/21/how-to-build-and-use-a-multi-gpu-system-for-deep-learning/ 



