# [[fulldocs]].twitter.zoomable\_ui

https://twitter.com/rsnous/status/1360841991383179266

> would it be right to say that "zoomable user interface" is 1. a complete success for maps and 2. a complete failure everywhere else? (if so, why?)


## links from thread
- https://www.youtube.com/watch?v=QOzmX2WpPZY
	- https://dasher.acecentre.net/
- https://blogs.harvard.edu/sj/2012/03/14/britannica-goes-out-of-print/





## fucked copy of thread




](https://twitter.com/rsnous)

would it be right to say that "zoomable user interface" is 1. a complete success for maps and 2. a complete failure everywhere else? (if so, why?)

[

![](https://pbs.twimg.com/profile_images/1037026925129035776/R9RFf7_a_bigger.jpg)







](https://twitter.com/gordonbrander)

One of my things is that the iPhone springboard is basically a simplified zoomable interface with two zoom levels. In the early days many of the icons intuitively read as mini cartoon versions of the app.

[

![](https://pbs.twimg.com/profile_images/646523069343793152/qXmq04Eq_bigger.png)







](https://twitter.com/rsnous)

!! it did immediately stick out to me that Table 1 in [library.tuit.uz/knigiPDF/Ebsco](https://t.co/JdDuTYYmMW?amp=1) used the iPhone app launcher as its example of ZUI on the iPhone -- it's certainly not what I think of when I think of zooming on the phone

[

![Image](https://pbs.twimg.com/media/EuM5fBjUcAEZVOK?format=jpg&name=small)







](https://twitter.com/rsnous/status/1360994830734659586/photo/1)

[

![](https://pbs.twimg.com/profile_images/629226127966531584/BVOAmw7y_bigger.jpg)







](https://twitter.com/pkerpedjiev)

Nope. The standard way to view genomes is through zoomable user interfaces. See e.g.

[

![](https://pbs.twimg.com/profile_images/2594815487/Headshot_bigger.jpg)







](https://twitter.com/andy_matuschak)

The only other interesting example I can think of is treemaps in data viz. I think you're generally right, and the other examples people are offering in this thread really aren't what was meant by "zoomable user interface." Decent bibliography/review here: [library.tuit.uz/knigiPDF/Ebsco](https://t.co/3ojIrpc5il?amp=1)

[

![](https://pbs.twimg.com/profile_images/1104555477197676544/x7vevGlG_bigger.jpg)







](https://twitter.com/morganherlocker)

Few other digital objects can be subdivided so many times and still provide a useful chunk of information, viewed in isolation. Hard to imagine something like a CAD or a wav file where a 1/68 millionth (z18 of map quadtree) slice of the content is useful. [wiki.openstreetmap.org/wiki/Zoom\_leve](https://t.co/IvEGybXq3E?amp=1)

[

![](https://pbs.twimg.com/profile_images/1143947140236009472/hFtcuuS1_bigger.jpg)







](https://twitter.com/TimSDobbs)

Maybe the way to think about it is that you zoom from factory>machine>part, or from discography>album>song>section>sample? We've parsed the fully zoomed-out version of maps, but not of other data

[

![](https://pbs.twimg.com/profile_images/652498614355365888/6QIrG4G3_bigger.jpg)







](https://twitter.com/Jermolene)

Maps have semantic zooming, the most important quality of ZUIs: the elision and simplification of information as you zoom out to maintain comprehension. I was very excited about ZUIs from following

’s work in early 2000s, feel disappointed we haven’t made more of them

[

![](https://pbs.twimg.com/profile_images/1258838853877747715/7DZue6ia_bigger.jpg)







](https://twitter.com/metasj)

Zooming through layered space, with compositing, tiling, and assigning a place + size + granularity to elements, is broadly useful but takes work to set up. Britannica Online attempted this for knowledge, but never launched.

[

![](https://pbs.twimg.com/profile_images/1303021509884043264/6bBn71ox_bigger.jpg)







](https://twitter.com/tylerangert)

Not sure if this was mentioned already but the iOS photos app is a pretty great ZUI imo (especially since it both zooms out to see more content and let’s you scroll through time at larger / smaller intervals).


---------------------------

[[concepts.map]]
[[incubation.interface]]