# Flucoma
> The Fluid Corpus Manipulation project (FluCoMa) instigates new musical ways of exploiting ever-growing banks of sound and gestures within the digital composition process, by bringing breakthroughs of signal decomposition DSP and machine learning to the toolset of techno-fluent computer composers, creative coders and digital artists.

## Download
64 bit - https://www.flucoma.org/download/
- supercollider - https://learn.flucoma.org/installation/sc/
- puredata
- max/msp

## Tutorials
- https://learn.flucoma.org/getting-started/
- https://learn.flucoma.org/learn/decomposition-overview/
	- https://learn.flucoma.org/reference/bufnmf/
		- **!** vocoder like but concatenative
- https://learn.flucoma.org/explore/roma/ hardware
- https://learn.flucoma.org/learn/2d-corpus-explorer/
- concatenative in sc - https://discourse.flucoma.org/t/concatenative-synthesis-sc/1850/5
	- https://discourse.flucoma.org/t/applications-of-flucoma-and-concatenative-synthesis/1855

### Commands
- https://learn.flucoma.org/reference/
- https://github.com/flucoma/flucoma-cli/blob/main/QuickStart.md
- !!!! https://discourse.flucoma.org/t/how-to-use-cli-slice-indice-output/883/2
## ONLY 32bit wavs!!!
### Patches
- pointzero - https://encanti.gumroad.com/l/kqtXF 
	- probably needs full max
- SP tools - https://rodrigoconstanzo.com/sp-tools/
- - https://rodrigoconstanzo.com/combine/ 
	- also M4L or runtime
- https://discourse.flucoma.org/t/soundscaper-soundscape-generation-patch-made-with-flucoma/1711/3
	- for MAx

#### ReaComa
*Reaper scripts integration
- https://discourse.flucoma.org/t/using-flucoma-tools-with-reaper/311/52 
- https://github.com/ReaCoMa/ReaCoMa-2.0 **!!!**
- https://phd.jamesbradbury.net/tech/reacoma/
- after installation
	- new action and add scripts from reacoma librayr

##### Notes
NMF - non negative matrix factorization
HPSS - harmonic percussive source separation
## Gadgets
- supports - https://www.ultraleap.com/product/leap-motion-controller/#pricingandlicensing cca 3000 czk

### raspberry pi 4
- https://discourse.flucoma.org/t/compiling-for-raspberry-pi-4/1652/15