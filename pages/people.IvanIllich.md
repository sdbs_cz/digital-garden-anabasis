
# Ivan Illich

![](https://hedgedoc.sdbs.cz/uploads/64206809-6db8-4c6f-bbf1-7f1180493962.png)
The Reverend Monsignor

## Tools for Conviviality
>Society can be destroyed when further growth of mass production renders the milieu hostile, when it extinguishes the free use of the natural abilities of society’s members, when it isolates people from each other and locks them into a man-made shell, when it undermines the texture of community by promoting extreme social polarization and splintering specialization, or when cancerous acceleration enforces social change at a rate that rules out legal, cultural, and political precedents as formal guidelines to present behavior. Corporate endeavors which thus threaten society cannot be tolerated. At this point it becomes irrelevant whether an enterprise is nominally owned by individuals, corporations, or the state, because no form of management can make such fundamental destruction serve a social purpose.

> In fact, however, the vision of new possibilities requires only the recognition that scientific discoveries can be used in at least two opposite ways. The first leads to specialization of functions, institutionalization of values and centralization of power and turns people into the accessories of bureaucracies or machines. The second enlarges the range of each person’s competence, control, and initiative, limited only by other individuals’ claims to an equal range of power and freedom.


>While evidence shows that more of the same leads to utter defeat, nothing less than more and more seems worthwhile in a society infected by the growth mania. The desperate plea is not only for more bombs and more police, more medical examinations and more teachers, but also for more information and research. The editor-in-chief of the Bulletin of Atomic Scientists claims that most of our present problems are the result of recently acquired knowledge badly applied, and concludes that the only remedy for the mess created by this information is more of it. It has become fashionable to say that where science and technology have created problems, it is only more scientific understandingand better technology that can carry us past them. The cure for bad management is more management. The cure for specialized research is more costly interdisciplinary research, just as the cure for polluted rivers is more costly nonpolluting detergents. The pooling of stores of information, the building up of a knowledge stock, the attempt to overwhelm present problems by the production of more science is the ultimate attempt to solve a crisis by escalation.

## Adjacent
- [[people.ClaudeBragdon]]
- [[people.VilemFlusser]]
- https://en.wikipedia.org/wiki/Eutrapelia
- https://en.wikipedia.org/wiki/Austerity