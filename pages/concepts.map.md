# Map
#chaostream 

## Mapping [[quotes]]

>what is the map is not a bug, it's a future 
>> delojza ([KSX byproduct](https://kunsaxan.sdbs.cz/)) [markov chain generator](https://en.wikipedia.org/wiki/Markov_chain)

-----------

>Because there is no limit to the number of possible map projections, there can be no comprehensive list.
>> https://en.wikipedia.org/wiki/List_of_map_projections

-----------

> a map is a (usually fixed) representation to help navigate a space—it wouldn’t necessarily contain the kind of qualitative inquiry you’re referring to.
>> https://forum.obsidian.md/t/lyt-kit-now-downloadable/390

-----------

> Unlike the graphic arts, drawing, or photography, unlike tracings, the rhizome pertains to a map that must be produced, constructed, a map that is always detachable, connectable, reversible, modifiable, and has multiple entryways and exits and its own lines of flight.
>>[\[17\]](https://contempaesthetics.org/newvolume/pages/article.php?articleID=812#FN17)]

-----------

> What distinguishes the map from the tracing is that it is entirely oriented toward an experimentation in contact with the real. The map does not reproduce an unconscious closed in upon itself; it constructs the unconscious. It fosters connections between fields, the removal of blockages on bodies without organs, the maximum opening of bodies without organs onto a plane of consistency. It is itself a part of the rhizome. The map is open and connectable in all of its dimensions; it is detachable, reversible, susceptible to constant modification. It can be torn, reversed, adapted to any kind of mounting, reworked by an individual, group, or social formation. It can be drawn on a wall, conceived of as a work of art, constructed as a political action or as a meditation. 
> - DnG - ATP [[1000p]]

-----------

> ![[JamesCorner_AgencyOfMapping2021-07-18 20-56-45.png.annotations]]> > [[JamesCorner_AgencyOfMapping2021-07-18 20-56-45.png.annotations]]


-----------

>  Indeed, Carter refers to the ‘inertia and levelling of possibilities’, even political impossibility, designers face in trying to realize projects out of the ordinary in a mass democracy. Because of the duality of a map – that is both analogous to reality yet abstracted from it – and hence its association with objectivity, truth and neutrality, the process of mapping offers a potent vehicle for the actualization of theory in the built environment, especially at the urban scale.
>  Half a century before Corner, in “On Exactitude in Science,” Jorge Luis Borges had presaged the uselessness of cartography as tracing. In Borges’s tale, a gigantic, over-detailed map is abandoned specifically because of its redundancy with the actual territory. Corner cites Borges to support his argument that mappings as replicas are worthless. Indeed, maps cannot be entirely objective, as representing spatiality entails some form of cultural situatedness. The techniques of ‘drift’, ‘layering’, ‘game-board’ and ‘rhizomes’ are described by Corner as practices in contemporary design and planning that open up new possibilities for mapping. Such thematic techinques share a personal approach to territorial representation, the contestation of dominant images of the city as well as the multiplicity and decentering of mappings. Challenging the traditional conception of static space, Corner aims for a return to the original exploratory character of mapping, not physically like the first explorers, but creatively through mind.
> - Camille Bédard
> -   https://architecturesofspatialjustice.wordpress.com/2012/10/09/agency_mapping/

-----------

>   Drift- This are the maps which are created using series of explanations and participatory acts with no rules to it. This helps in mapping some of the hidden topographies in the city.
>
>  Layering- In this the city or the place is divided in terms of layers which are superimposed on one another, each of the layer here shows a different mapped aspect of that place.
>
>  Game-board- Maps are kept as a game board with particular game rules and anyone can take part in this game, it’s like a competition which helps in negotiating some of the complex planning decisions.
>
>  Rhizome- It has a concept of open ended maps in which a point connects to any other point, this type of maps always be in the middle with no beginning or end to it. This is a continuous process and different aspects keep on adding to the map in terms of point connections.
> -   https://ideabatch2.wordpress.com/2017/01/25/the-agency-of-mapping-speculation-critique-and-invention-james-corner-edited-by-denis-cosgrove-1999-divyarajsinh-rana/

[[concepts.situationism]]


---------
----------
## Questions of perspective
![[Pasted image 20220223003021.png]]
![[Pasted image 20220226142108.png]]
![[Pasted image 20220725011331.png]] -  Jan Micker - Bird's Eye View of Amsterdam (ca. 1652)
### Space
- https://www.boredpanda.com/different-perspective-telephoto-lens-vs-wide-angle-philip-davali-olafur-steinar-ry/
### Time
- https://www.lancaster.ac.uk/chronotopic-cartographies/

### Projection
![[5857327792872601812_120.jpg]]
![[Pasted image 20250221133934.png]]
![[Pasted image 20221219001002.png]]
![[Pasted image 20220212150603.png]]

![[Pasted image 20220212150614.png]]

![[Pasted image 20220212150621.png]]
![[Pasted image 20220212150628.png]]
![[Pasted image 20220212150644.png]]

- https://www.jasondavies.com/maps/transition/
- https://www.jasondavies.com/maps/


---------------
![[Pasted image 20220614205311.png]]

![[Pasted image 20220624140211.png]]

![[Share_Media_20220806_214208.png]]
![[Pasted image 20230828233821.png]]
-----------------
-----------------

## Linking / visualising

![image.png](danger-of-visual-repre.png)

## Language
![[mapping_language.png]]
### mapping neuromancer
- [[people.WilliamGibson]]
- https://cindyjuheunlee.blogspot.com/2011/11/proposal-for-mapping.html


-------------------

-------------------
## Interesting online maps and map projection interpretations
- https://geodienst.github.io/lighthousemap/
- https://mapfight.xyz
- https://twitter.com/leviwesterveld/status/1506253203704356867
- https://www.loc.gov/resource/g4104c.ct003853/?st=image&r=0.069%2C0.39%2C0.348%2C0.188%2C0
- https://travelerdoor.com/2022/07/06/mapped/

-----------
-----------

## [[fulldocs]].at.garage

- [[fulldocs.twitter.booksasmaps]]
	- https://old.reddit.com/r/slatestarcodex/comments/mvs0vf/what_books_are_for_a_response_to_why_books_dont/
- [[fulldocs.mastodon.gis]]

## Adjacent

- [[1000p]]
- [[concepts.topology]]
- [[concepts.topography]]
- [[concepts.disambiguation]]
- [[incubation.areas.diagrammatic]]
- https://geocinema.network/ #geo #cinema
- https://www.pocitovemapy.cz/
-----------------
-----------------

![[Pasted image 20230131184939.png]]


https://www.youtube.com/watch?v=bCvpoHxSVIE - Why Google Maps Is The Most Important Tech Product Of The 2020s - maps --> AR