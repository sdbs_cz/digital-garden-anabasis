`The history of the universe… is the handwriting produced by a Minor god in order to communicate with a Demon.`
- Borges - [Tlön, Uqbar, Orbis Tertius](https://en.m.wikipedia.org/wiki/Tl%C3%B6n,_Uqbar,_Orbis_Tertius)

# 2022 addenums
## Rework
- [[las.reconcept.intro.2]]
## Add
### Blocks
### [[las.quotes.cities]]
### [[las.quotes.visionaries]]
### [[incubation.hermes]]
[[las.quotes.map]]


### Singles
#### Puzzles / [[people.GeorgesPerec]]
- [[fulldocs.GeorgesPereconPuzzles.quotes]]
### Codex twitter
- - [[fulldocs.twitter.codex.relations]] #map #relation

### Sauce

- WSB
	- Electronic revolution 
		- https://pile.sdbs.cz/item/61
		- #alphabet
		- //follow glosolalia

### ??? 

- temporality theme
	- //as proposed #fill
		- txt gifs 
	- +
- aarseth / ergodic literature 
- 1000 plates subcanvas
	- DNF 
		- existenalism X postlanguage


## To Re/Check
>Let us, then, recapitulate our argument, in order to try to suggest what form the new civilization might take. We have two alternatives before us.
>
First, there is the possibility that imaginal thinking will not succeed in incorporating conceptual thinking. This could lead to a generalized depolitization, deactivation, and alienation of humankind, to the victory of the consumer society, and to the totalitarianism of the mass media.
>
Such a development would look very much like the present mass culture, but in more exaggerated or gross form. The culture of the elite would disappear for good, thus bringing history to an end in any meaningful sense of that term. The second possibility is that imaginal thinking will succeed in incorporating conceptual thinking. This would lead to new types of communication in which man consciously assumes the structural position. Science would then be no longer merely discursive and conceptual, but would have recourse to imaginal models. Art would no longer work at things (“oeuvres”), but would propose models. Politics would no longer fight for the realizations of values, but would elaborate manipulable hierarchies of models of behavior. All this would mean, in short, that a new sense of reality
 - Vilem Flusser - line and surface

#fix 

----

>Imaginal thought was a translation of fact into image, and conceptual thought was a translation of image into
concept. (First there was the stone, then the image of the stone, then the explanation of that image.) In the future, the situation may become thus:
Imaginal thought will be a translation from concept into image, and conceptual thought a translation from image to concept. In such a feedback situation, an adequate model can finally be elaborated. **First there will be an image of something, then there will be an explanation of that image, and then there will be an image of that explanation. This will result in a model of something (this something having been, originally, a concept).
And this model may fit a stone (or some other fact, or nothing).** Thus a fact, or the absence of a fact, will have been disclosed. There would once more exist a criterion of distinction between fact and fiction (fit and
unfit models), and a sense of reality would have been recovered. What has just been said is not an epistemological or ontological
 - las 


## 2023 chaosstream
> Stop anthropomorphising billionaires. 
>  - Aral Balkal

In the September 1965 issue of Soviet Life, astronomer Iosif Shklovsky made the following statement regarding prospects for the future of humanity: «Profound crises lie in wait for a developing civilization and one of them may well prove fatal. We are already familiar with several such critical (situations):» [https://en.wikipedia.org/wiki/Iosif_Shklovsky#Quotes](https://en.wikipedia.org/wiki/Iosif_Shklovsky#Quotes) (a) Self-destruction as a result of a thermonuclear catastrophe or some other discovery which may have unpredictable and uncontrollable consequences. (b) Genetic danger. (c) Overproduction of information. (d) Restricted capacity of the individual's brain which can lead to excessive specialization, with consequent dangers of degeneration. (e) A crisis precipitated by the creation of artificial intelligent beings.

Ideology doesn´t arrive and say, "I am ideology". Ideology says: "I am nature, and this is how things are". (Mark Fisher - Capitalist Realism)