https://post.lurk.org/@crickxson/105888232700886216 crickxson@post.lurk.org - "Loosening the #grid: #topology as the basis for a more inclusive #GIS" #Westerveld #Knowles​
https://visionscarto.net/i-was-there​
"The past 25 years have seen a proliferation of interest in GIS among humanists and humanistic social scientists. Under various banners – historical GIS, qualitative and mixed methods GIS, spatial and digital history, digital humanities, spatial humanities or geohumanities – researchers have developed new ways to include qualitative data within the framework of GIS. At the same time, social and theoretical critiques of GIS as a tool wielded chiefly by and for those in power have increased awareness of its limitations, particularly for studying human perceptions, experiences, and the meanings of place. We agree with a growing chorus of geographers that the most common data structures in GIS are inadequate to capture or analyze the relational dimensions of subjectivity. As part of the solution, we propose an alternative hybrid framework that prioritizes topological relationships while retaining coordinate locations as points of connection between geographic places and experiential evidence. [...]"
#cartography​

![](https://post.lurk.org/system/media_attachments/files/105/888/221/126/888/464/original/e1ee70f9200dca2b.jpg?1615726030)

![](https://post.lurk.org/system/media_attachments/files/105/888/221/127/384/733/original/602ae761fdf49fd0.jpg?1615726030)

![](https://post.lurk.org/system/media_attachments/files/105/888/221/129/077/370/original/720715a54891566a.jpg?1615726030)

