## image morphing and pixel sorting

### image morph
#### autoimagemorph
- https://github.com/jankovicsandras/autoimagemorph

```
python autoimagemorph.py -inframes "['f0.jpg','f25.jpg','f50.jpg'`'f75.jpg','f100.jpg','f125.jpg','f150.jpg']" -outprefix f -framerate 25
```

```
python autoimagemorph.py -inframes "['f0.png','f50.png','f100.png','f150.png','f200.png','f250.png','f300.png','f350.png','f400.png','f450.png','f500.png','f550.png']" -outprefix f -framerate 50 -subpixel 4
```

```
python3 autoimagemorph.py -inframes "['f0.png','f50.png','f100.png','f150.png','f200.png']" -outprefix f -framerate 50 -subpixel 3
```

#### untested
- https://github.com/volotat/DiffMorph
- https://github.com/ryanfb/torch-warp
- https://github.com/C00reNUT/ImageMorphing
- https://github.com/patirasam/Face-Morphing-DelaunayTriangulation-
#### not-working
- https://github.com/malyvsen/reimage ???



### pixelsort


#### pixelsort by satyarth
- https://github.com/satyarth/pixelsort

for image in *.png ;  do python3 -m pixelsort "$image" -i edges -t 0.9 -o "${image%.*}_sorted.png" ; done

for image in *.png ;  do python3 -m pixelsort "$image" -i edges -t 0.9 -a 90 -o "${image%.*}_sorted.png" ; done

saturation is way to go

for image in *.png ;  do python3 -m pixelsort "$image" -i edges -t 0.5 -a 90 -s minimum -o "${image%.*}_sorted.png" ; done


##### Adjacent
- https://github.com/jankovicsandras/girihjs
- https://github.com/jankovicsandras/kalocsai
- https://github.com/jankovicsandras/primisynth

#### untested
- https://github.com/stepsal/block_sort
- https://github.com/rkargon/pixelsorter  
	- **!animate!**
- https://github.com/guimondmm/prism-sort-glitch
	- open-art
	- glitch
- https://github.com/kimasendorf/ASDFPixelSort
	- OG pixelsort  

## primitive
- https://github.com/fogleman/primitive
- https://github.com/chux0519/purr
- **might be interesting addition or mask generator for pixelsorting!**

## [[ffmpeg]]
- `ffmpeg -framerate 15 -i f%d.png f.gif`

## [[tools.imagemagick]]
- `mogrify -format png *.jpeg` imagemagick batch

## Datamosh
>calafou@systerserver.town - Hem fet algunes proves amb el #DatamosherPro en la seva versió python linuxera i sembla molt interessant
>
#VJ​
#Video​
#Datamosh​
>
>https://github.com/Akascape/Datamosher-Pro​

## Adjacent
- https://github.com/topics/generative-art
- https://github.com/terkelg/awesome-creative-coding


Potrace a hriby media to svg and back sh