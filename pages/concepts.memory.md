# Memory
#incubation 

:diamond_shape_with_a_dot_inside:

> Our praxis (manipulations) with electronic memories force us to admit that memory is not a thing but a process, whether that process involves computer hardware or our bodies. This praxis forces us to admit that there is no hard core within each of us which somehow mysteriously governs that process, but that the process of acquiring, storing and transmitting information flows through us and involves not only all of present and past society but also the whole of what we call 'the world'. We are but knots within a universal network of information flux that receive, process and transmit information. Our praxis with electronic memories forces us to admit that what each of us calls 'I' is a knot of relations that, when unraveled, reveals itself to have no hook on which those relations may hang (like the proverbial onion).
> - [[people.VilemFlusser]] - ["On Memory (Electronic or Otherwise)"]


## Articles, etc.

- Vilém Flusser - ["On Memory (Electronic or Otherwise)"](/images/4/4b/Flusser_Vilem_1990_On_Memory_Electronic_or_Otherwise.pdf "Flusser Vilem 1990 On Memory Electronic or Otherwise.pdf"), _Leonardo_ 23:4 (1990), pp 397-399. Adapted from a presentation at [Ars Electronica](/Ars_Electronica "Ars Electronica"), 14 Sep 1988. (English) 

- Memory in Motion Archives, Technology and the Social

- [AUGMENTING HUMAN INTELLECT : A Conceptual Framework. October 1962. By D. C. Engelbart](http://1962paper.org/web.html#)

---------------------------------
>"The best way to get the right answer on the Internet is not to ask a question; it's to post the wrong answer."
>  - [Cunningham's law](https://meta.wikimedia.org/wiki/Cunningham%27s_Law)

# Adjacent

- [[concepts.knowledge managment]]
- [[concepts.archive]]
- [[people.VannevarBush]]
- [[concepts.archives.art]]
- [[incubation.anarcheology]]
- [[concepts.bookmarking]]
---------------------------------
---------------------------------

---------------------------------
---------------------------------

---------------------------------
---------------------------------

## #incubation
- second brain
- https://www.youtube.com/watch?v=vfpN5TsaTtg
- https://getantarapp.com/
- [There is No Software - Friedrich Kittler](https://web.archive.org/web/20181016072632/http://www.ctheory.net/articles.aspx?id=74)

### [[people.Kiergaard]]
- memory x repetion
- https://philosophy.stackexchange.com/questions/47084/what-does-kierkegaard-mean-by-recollection-repetition-and-remembrance
- https://en.wikipedia.org/wiki/Repetition_(Kierkegaard_book)
- https://en.wikiquote.org/wiki/Repetition_(Kierkegaard)
- https://www.youtube.com/watch?v=RazVqpIoqM4