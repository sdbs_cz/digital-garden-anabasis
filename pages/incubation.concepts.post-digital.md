>**Postdigital,** in artistic practice, is an attitude that is more concerned with being human, than with being digital, similar to the concept of "undigital" introduced in 1995,[[1]](https://en.wikipedia.org/wiki/Postdigital#cite_note-1) where technology and society advances beyond digital limitations to achieve a totally fluid multimediated reality that is free from artefacts of digital computation (quantization noise, pixelation, etc.)[[2]](https://en.wikipedia.org/wiki/Postdigital#cite_note-2).
>
> Postdigital is concerned with our rapidly changed and changing relationships with digital technologies and art forms. If one examines the textual paradigm of consensus, one is faced with a choice: either the "postdigital" society has intrinsic meaning, or it is contextualised into a paradigm of consensus that includes art as a totality.
> - https://en.wikipedia.org/wiki/Postdigital


https://stones.computer/

[[people.BrianMassumi]]
[[post-analog]]
[[incubation.mediamateriality]]
[[DigitalGardenAnabasis/people.VilemFlusser]]
[[concepts.glitchart]]

------

![[Pasted image 20221101232358.png]]