# Workflows and toolsets for Obsidian

## workflows
_Obsidian focused, but general points are made._


- [Example Workflows in Obsidian - Share & showcase - Obsidian Forum](https://forum.obsidian.md/t/example-workflows-in-obsidian/1093)
- maps of content
	- https://forum.obsidian.md/t/on-the-process-of-making-mocs/1060
- [Personal Knowledge Management with Zettelkasten and Obsidian](https://yordi.me/personal-knowledge-management-with-zettelkasten-and-obsidian/)
-[obsidian and zettelkasten](https://forum.obsidian.md/t/obsidian-zettelkasten/1999)
- [The life-changing magic of refactoring notes](https://mobydiction.ca/blog/refactoring-notes)
- what about folders
	- https://www.reddit.com/r/ObsidianMD/comments/rnlcoz/seeking_inspiration_on_how_to_categorise_notes/

## video-making
- https://thebuccaneersbounty.wordpress.com/2021/08/22/tutorial-how-to-create-a-video-project-tracker-in-obsidian/

## kits
- LYT kit 
	- https://publish.obsidian.md/lyt-kit/_Start+Here
	- https://forum.obsidian.md/t/linking-your-thinking-resources/6177
- - https://forum.obsidian.md/t/para-starter-kit/223 #para
## terminal
- https://forum.obsidian.md/t/terminal-power-users-how-do-you-use-command-line-tools-in-conjunction-with-obsidian/14377/11
## planner, etc.
- https://www.reddit.com/r/ObsidianMD/comments/sfq78s/unpopular_opinion_obsidian_is_an_excellent_task/

--------------------------

- [Clean Up Your Mess - A Guide to Visual Design for Everyone](http://www.visualmess.com/)