# [[areas.audio]] sample managment
#fix #tosort #chaostream 

### Aka Audio library
#### see [[audio.metadata]]


- https://www.sweetwater.com/sweetcare/articles/file-management-for-music-production/
- https://linuxmusicians.com/viewtopic.php?p=145724&hilit=audiostellar#p145724

### list of tools

- see/ to-update / UCS compatible
	- soundQ
	- soundly
	- basehead
	- soundminer #paid
	- reaper media manager

#### Free or free version
- https://www.audiopluginsforfree.com/adsr-sample-manager/	
	- https://www.kvraudio.com/product/adsr-sample-manager-by-adsr
- https://getsoundly.com/
- https://resonic.at/product-comparison
- [SonicHive](https://gitlab.com/samplehive/sample-hive) - linux only
- [SampleCat](https://ayyi.github.io/samplecat/) linux only
-  https://github.com/coderofsalvation/usm commandline linux locate
#### paid 
- https://www.sononym.net/ #linux also
- https://www.loopcloud.com/cloud/subscriptions/plans
- http://www.ryaudio.com/ win7 
- https://algonaut.audio/ #maps #linux
- https://www.xlnaudio.com/products/xo #maps
- https://www.waves.com/plugins/cosmos-sample-finder#introducing-cosmos-sample-finder
#### ?
- https://baseheadinc.com/	 
- https://getsoundly.com/
- https://serato.com/sample
- ?
	- https://baseheadinc.com/	 
	- https://getsoundly.com/
	- https://serato.com/sample

- ehhh/aaah
	- https://medium.com/pragmatic-sound/how-to-manage-your-samples-and-sound-design-audio-files-1626b21ee6a
	- https://www.umlautaudio.com/blog/amplify-productivity-sample-management
- aah
	- https://www.elektronauts.com/t/looking-for-a-sample-management-software/131437/8 

### #misplaced 
- list of samplers
	- https://www.soundsnap.com/blog/the-top-software-samplers-and-sample-players/
- sample rate converter
	- https://www.voxengo.com/product/r8brain/

-----
## audio annotation
- https://github.com/midas-research/audino

------------------------------------
------------------------------------
------------------------------------


>tyldurd@framapiaf.org - Waves Cosmos: a free sample finder. It uses machine learning (AI, they say :-p) to auto-tag samples based on audio content. Really cool! 
>
> #music_production #audio #sampling #daw​

> paul@post.lurk.org - I've been using #novembeat as an opportunity to actually try and use my experimental zettelkasten for managing my sample library. Each sample I've used has been tagged and annotated with timestamped observations in my zet. To date, my zet tells me I've done this process to 80 unique samples in my novembeats.
>
> The zet is same technology that powers my static HTML wiki, so there a little bit of a wiki component too! I will be posting those bits to my website. The samples, for practical storage and legal reasons, can't be included. But things are definitely starting to come together!
>
#personalwiki #zettelkasten #sampling​


--------
>  I ended up accumulating about three thousand recordings of singing, piano, guitar, ambience, noise, and nature, without 'turning them into something', and this is for lack of some way to let the ideas mingle together.
> 	- [Applying note-taking reflexes to making music](https://rosano.hmm.garden/01fvmvwv1p0te9nh86z4r2h82x) 


## Adjacent
- [[concepts.Digital Asset Managment]]
- [[areas.filetag]]
- [[projects.upend]]
- [[incubation.audio.synthesis.concatenative]]
- [[concepts.digital garden]]