IPFS
====

omag#inform [[inform]][[areas.self-hosting]]\

What?
-----

<https://ipfs.io>

IPFS is a bit like torrents, but for web - the files aren\'t hosted at a
single place, but instead whoever has a copy and wants to host it, can
host it, and the links can exist forever.

(But it also means it\'s kind of slow, but **you too** can help /-\\
projects out - see below.)

### Super simplified technical stuff

<img src="//screenshot_2019-10-29_ipfs_is_the_distributed_web.png" width="480" alt="screenshot_2019-10-29_ipfs_is_the_distributed_web.png" />

More specifically, it works like this: for each file, there is a
[\"hash\"](https://en.wikipedia.org/wiki/Distributed_hash_table) - a
\"fingeprint\" of the file\'s contents. It\'s just 30 characters or so
(\"`QmTmrhGKN21S4xmiv13jJrmKXNfe2Nuk2zUfn6eieY8dYy`\", for example), but
it uniquely [^1] identifies that specific file.

Guy A, a \"node\", has the full file (a webpage, for example) - and
shouts out to everyone on the network that he has a file with hash that
is `QmTmrhblabla`\... Guy B, who wants to see the webpage, **only needs
to know the hash** - `QmTmrhblabla`\... So he connects to IPFS, and asks
everyone \"who has `QmTmrhblabla`?\" - and Guy A hears this, and sends
him the full file. Or - and this is the important part - whoever also
has this file on their computer, also hears this, and also can respond
with the file.

**TL;DR:** This means that as long as there is someone who has the file
- and someone who knows what they want (i.e. they have the hash) - they
can always access that file/webpage/image. Forever. With regular web, if
you\'ve got a link to a file, and the site goes down, there is nothing
you can do. But if you\'ve got an IPFS link, and anyone else has the
file and an internet connection, the link will [never]{.ul} go down.

### Public Gateways

And you technically don\'t need to install anything to use IPFS -
there\'s a \"public gateway\" - which you can use to access the files
with a normal browser. It works by putting the file\'s hash after
<https://ipfs.io/ipfs/>[^2] - and you will also get to the file. After
the file is accessed through the public gateway, it usually stays there
for some 20-30 minutes. This means the first loading is usually slow,
but all after that are much faster.

But you do need to download and install IPFS for\...

How to become a node and spread decentralized goodness
------------------------------------------------------

[IPFS Desktop](https://github.com/ipfs-shipyard/ipfs-desktop) - this is
by far the most comfortable way to do it (on Windows). [Direct link to
installer
exe](https://github.com/ipfs-shipyard/ipfs-desktop/releases/download/v0.9.7/ipfs-desktop-setup-0.9.7.exe)

### Adding files: Conventional version

Then you go to files, and select \"Add\" &gt; \"From IPFS\"

<img src="//2019-10-29_14_34_48-window.png" width="400" />

And insert the hash/address of whatever you want to host (i.e.
`/ipfs/Qmblablabla...`). This \"pins\" it[^3] so that you keep hosting
it.

### Adding files: Alternative version

> actually for me it seems to work much better when i look for the hash
> through \*explore\* and download, than through \*files\* and \*add
> files\*
>
> \- bgsl
>
> > updt 1 - ok this seems temporal. but it seems i am able to host
> > files much smoother after this. - bgsl

IPFS hosted /-\\ projects
-------------------------

(that could use some pinning)

| What                       | Hash                                                                                                                         | Public / Static Address (IPNS)[^4]                                                                 |
|----------------------------|------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------|
| Speculative Ecologies AR   | [/ipfs/QmS7NSBxfSM1sb4VTBsyRyJHk2HhsDXioCEGF47YcX1nZF](https://ipfs.io/ipfs/QmS7NSBxfSM1sb4VTBsyRyJHk2HhsDXioCEGF47YcX1nZF)  | N/A                                                                                                |
| GUNK 7 AR                  | [/ipfs/QmRJoxWdm4ZSHA4UCWyyiye7uQxfuSsuKkBt7ngo7CChfF](https://ipfs.io/ipfs/QmRJoxWdm4ZSHA4UCWyyiye7uQxfuSsuKkBt7ngo7CChfF)  | N/A                                                                                                |
| GUNKS *(as of 2020-10-10)* | [/ipfs/Qmb6WJzMereTNCMh1drjepq3wEn9r6HkBZKadc7CFwf98V](https://ipfs.io/ipfs/Qmb6WJzMereTNCMh1drjepq3wEn9r6HkBZKadc7CFwf98V/) | [/ipns/QmX3hGuNKRRogN12S\...](https://ipfs.io/ipns/QmX3hGuNKRRogN12S6Gjg7s3TWDzjHLh25P4da9nBrTxKh) |

(and in case you are interested in pinning, you might be interested in
[some seeding at klastr\|](https://klastr.sdbs.cz))

[^1]: not really \"uniquely\", but it\'s super unlikely that two files
    would have the same hash

[^2]: like
    <https://ipfs.io/ipfs/QmTmrhGKN21S4xmiv13jJrmKXNfe2Nuk2zUfn6eieY8dYy>

[^3]: because if you don\'t use the IPFS public gateway and have IPFS
    installed locally instead, and use that for browsing - you also host
    everything you visited, but only for some time, after that it gets
    deleted. Pinning means \"don\'t forget to host this\".

[^4]: This is the address you can bookmark, since it will always be \"up
    to date\". However, it\'s only possible to access/download files
    this way, for pinning you need the actual hash, more information on
    <https://docs.ipfs.io/guides/concepts/ipns/>
