![[DigitalGardenAnabasis/machupicchu.highway.png]]

We can compare Machu Picchu with the USA highway Route 66. It is a
public service. It has given birth around it to a successful economy
composed of hotels, restaurants, garages, industries, doctors, etc. in
many cities. Nobody owns Route 66; nobody owns Machu Picchu either.
With Machu Picchu, "data is a public service".
