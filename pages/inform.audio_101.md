Audio 101
=========
#crippled #tosort 
[[areas.audio]]
[[inform]]
[[]]


FIXME

Loudness
--------

<https://en.wikipedia.org/wiki/Equal-loudness_contour>

Sample Rates, etc.
------------------

-   [Understanding Digital Sound and Analog
    Sound](https://www.dummies.com/consumer-electronics/understanding-digital-sound-and-analog-sound/)
-   [24/192 Music Downloads \...and why they make no
    sense](https://people.xiph.org/~xiphmont/demo/neil-young.html)
    -   [Accompanying Video](https://xiph.org/video/vid2.shtml)
        (explains sampling, bit-depth, aliasing, dither\...)

### 16 bit vs 24 bit

*Quote: Originally Posted by stupeT Given my "real world poor man\'s
studio"\...Shall I print in 24 bit or is 16 bit enough and I will have
no lose what so ever, but better performance of my DAW? Cheers stupeT I
think it is unlikely that an otherwise reasonably capable DAW computer
would bottleneck due to recording at 24-bit instead of 16-bit. Reaper
and all modern DAWs use high-precision audio engines over 24-bit, so
your samples are being processed at high bit depths even if they are
low-resolution samples. A second fast hard drive is pretty cheap in the
scheme of things and almost a requirement for high-track-count audio, it
seems to me. Moreover, 24-bit is stupidly cheap and easy insurance
against the single biggest headache of digital recording, namely trying
to set the record levels high enough without clipping. With 16 bit, if
you need to leave 24dB headroom above the aver- age level for a singer
with no mic technique, then you\'re really only recording at about 12
bits resolution on average. The whole point of 24 bit is that you no
longer have to record close to zero, you could record with peak levels
of like -50 and still have CD-quality resolution. So you can leave
plenty of headroom and just turn down the input gain as low as you want
-- no fear of clipping, and no worries of lost resolution, no matter how
"wild" the singer. Sample rate is a whole different thing, OTOH. Working
at higher sample rates def- initely affects performance.* [^1]

Post-Production
---------------

### Loudness and gain staging

-   <https://www.sweetwater.com/insync/gain-staging/>
-   <https://www.izotope.com/en/learn/gain-staging-what-it-is-and-how-to-do-it.html>

### Dynamic Effects

### Spatial Effects

### Noise Removal

1.  with Audacity
    1.  <https://manual.audacityteam.org/man/noise_reduction.html>
    2.  <https://www.wikihow.tech/Remove-Background-Noise-in-Audacity>
2.  with RX7
3.  with [Reaper](inform.reaper.md)
    1.  <http://podcasternews.com/2014/04/04/noise-reduction-reaper-plugin/>

------------------------------------------------------------------------

<https://www.youtube.com/watch?v=TEjOdqZFvhY>

Mics 101
--------

FIXME

[^1]: <http://stash.reaper.fm/v/3107/wdyrsla_061709.pdf>
