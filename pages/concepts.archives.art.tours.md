#  Online tours

### Mnemosyne Atlas --> [[people.AbyWarburg]]
- https://warburg.sas.ac.uk/aby-warburgs-bilderatlas-mnemosyne-virtual-exhibition
- https://warburg.sas.ac.uk/library-collections/warburg-institute-archive/online-bilderatlas-mnemosyne
- [Cornell Uni - Ten panels from the Mnemosyne Atlas](https://warburg.library.cornell.edu/)


### Jan Patocka  -->[[people.JanPatocka]]
- https://my.matterport.com/show/?m=JguEFtshpUo

### +
- https://sketchfab.com/3d-models/calvin-hobbes-the-big-bang-dd484cd2dc9e477381e152b69d329506
