# digitalgarden.anabasis
- [[dga]]
- [[000_start-here]]
- [[concepts.memory]]
- [[concepts.backlinks]]
- [[concepts.digital garden]]
- [[concepts.knowledge managment]]

## crosslinked aka EXTERNAL

- [tml.notes](https://t.mldk.cz/notes/)kn


---------------------------------
---------------------------------

### Related - hyperlinks

#### [inform](https://inform.sdbs.cz)

#### [pile](https://pile.sdbs.cz)

#### [sdbs hedgedoc](https://hedgedoc.sdbs.cz)

## Praxis

### almost essential to sort out
- [ ] jak syncovat tento document
	- [x] syncthing
	- [x] markdown server/parser => [gardenserver](gardenserver.md)
- [ ] folders?
	- [ ] how to aproach 
		- [ ] folders x vaults
			- [ ] [[concepts.flat-file]]
		- [ ] vaults x domains
	- ![ ](folder_scheme.png) ?
	- ! [ ](screen.areas.png)
	- digital.garden.sdbs.cz x aleadub.garden.sdbs.cz x ksx.garden.sdbs.cz
- [x] webmentions?
---------------------------------