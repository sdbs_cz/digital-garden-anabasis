# Henry Flynt

>Flynt initiated a utopian critique of art from the stand-point of the absolute subjectivity of taste.
> - https://stewarthomesociety.org/interviews/flynt.htm

> American experimental violinist and philosopher, who produced much of his music from the period between 60s and 80s. A number of reissues of his old material introduced his work to larger American public. He also was a member of Velvet Underground for a short time.
>  - https://www.discogs.com/artist/215301-Henry-Flynt



>Rather than holding out utopian promises, it is better to give whoever can grasp it the realization that the experience beyond art already occurs in his or her life--but is totally suppressed by the general repressiveness of society.
>- Art or Brend


1/ concept art
2/  music
3/ Blueprint for higher civilization

-------------
## Blueprint for higher civilization
>![[Pasted image 20230314212226.png]]
>
> - https://threadreaderapp.com/thread/1070178530552475648.html

----------




- https://en.wikipedia.org/wiki/Henry_Flynt
- https://monoskop.org/Henry_Flynt

- https://www.e-flux.com/journal/115/374421/on-constitutive-dissociations-as-a-means-of-world-unmaking-henry-flynt-and-generative-aesthetics-redefined/
- http://www.henryflynt.org/

- https://www.youtube.com/watch?v=Risflr61D-4

## Quotes and notes
### Art or Brend
- https://www.ubu.com/papers/flynt_art-brend.html

> The artist or entertainer cannot exist without urging his or her product on other people. In fact, after developing his or her product, the artist goes out and tries to win public acceptance for it, to advertise and promote it, to sell it, to force it on people. If the public doesn't accept it as first, he or she is disappointed. He or she doesn't drop it, but repeatedly urges the project on them.

#las #visionaries



> People have every reason, then, to ask the artist: Is your product good for me even if I don't like or enjoy it? This question really lays art open. One of the distinguishing features of art has always been that it is very difficult to defend art without referring to people's liking or enjoying it. (Functions of art such as making money or glorifying the social order are real enough, but they are rarely cited in defense of art. Let us put them aside.) When one artist shows his latest production to another, all he can usually ask is "Do you like it?" Once the "scientific" justification of art is discredited, the artist usually has to admit: If you don't like or enjoy my product, there's no reason why you should "consume" it.


> But there is a fundamental contradiction here. Consider the object which one person produces for the liking, the enjoyment of another. The value of the object is supposed to be that you just like it. It supposedly has a value which is entirely subjective and entirely within you, is a part of you. Yet--the object can exist without you, is completely outside you, is not your or your valuing, and has no inherent connection with you or your valuing. The product is not personal to you.


> To be totally successful, the object would have to give you an experience in which the object is as personal to you as your valuing of it. Yet you remain aware that the object is another's product, separable from your liking of it. The artist tries to "be oneself" for other people, to "express oneself" for them.


> These just-likings are your "brend." Some of your dreams are brend; and some children's play is brend (but formal children's games aren't). In a sense, though, the attempt to give interpersonal examples of brend is futile, because the end result is neutral things or actions, cut off from the valuing which gives them their only significance; and because the end result suggests that brend is a deliberate activity like carrying out orders. The only examples for you are your just-likings, and you have to guess them by directly applying the abstract definition.


> Note: the avant-garde artist may raise a final question. Can't art or entertainment compensate for its impersonality by having sheer newness as a value? Can't the very foreignness of the impersonal object be entertaining? Doesn't this happen with my "Mock Risk Games," for example?
The answer is that entertainmental newness is also subjective. What is entertainingly strange to one person is incomprehensible, annoying or irrelevant to another. The only difference between foreignness and other entertainment values is that brend does not have more foreignness than conventional entertainment does.


### Home Interviews Flynt
- https://www.stewarthomesociety.org/interviews/flynt.htm

> FLYNT: My early work was philosophic, what would be called epistemology, I was convinced I'd dicredited cognition. When somebody says that all statements are false, the obvious problem is that as an assertion it's self-defeating. I had to find a way to frame this insight which was not self-defeating and that's in "Blueprint", the essay entitled "The Flaws Underlying Beliefs." One has to do what Wittgenstein claimed to do in the "Tractatus Logico-Philosophicus," which is to use the ladder and then throw it away. The way I devolved, moved out from, this position of strict cognitive nihilism, was with the idea of building a new culture which would depart profoundly from the scientific culture in which we live.


> At the same time I felt a tremendous disquiet about the avant-garde, there was something very inauthentic about it. There was the mystique of scientificity, Stockhausen was making claims which were actually false, that were philosophically discreditable.


> I have a lot of problems with modern European culture. I find European music to be very four-square, it really lends itself to computerisation. In classical oil painting, there seemed to be a radical turn to seeing things as the camera sees them, with that technological modification. I began to have a tremendous problem with all of this. At the same time I was listening to black music and I began to think that the best musicians were receiving the worst treatment. The people who were doing the greatest work were despised as lower class, with no dignity accorded to what they did, while the stuff being promoted as serious culture and performed in the Lincoln Centre was absolutely worthless.


> There was no real emotion in it, the possibility of ingenuous experience had been replaced by an ideology of science and scientism.


> I became very angry about the fact that I'd been talked into going to these Cage concerts when I was in college, that I'd sat and tried to make myself like that stuff and think in those terms. I felt I'd been brainwashed, that it was a kind of damage to my sensibilities. I'm still mad about this, I still feel I've not recovered from the experience.


> At that time I was initiating concept art. I was doing a lot of things, many of them imitative. The purpose of concept art as a genre is to unbrainwash our mathematical and logical faculties. At the same time it's bound up with aesthetic delectation. I think these two aspects are integral to concept art, it's not just an artificial pasting together of the two things, they actually change each other in the course of their interaction.


> From there I moved to an absolutely subjective position aesthetically, where each individual should become aware of their unformed taste. I used the term brend to signify this and thought that it would replace art. Basically, at this time, I viewed any work of art as an imposition of another persons taste and saw the individual making this imposition as a kind of dictator. I don't think there's any irony about the fact that I was beginning to dabble in political leftism at the very time I was inventing a theory in which art disappears and is replaced by a kind of absolute individualism. It's not strange if you understand what the final utopia of socialism was supposed to be. It's no different from talking about getting rid of money or the state.


> Another point I made was that black American music was a new language and I don't feel this was ever really acknowledged. What happened was that rock became an incredible commercial success, people just became bored with serious music and it was forgotten. It was not an intellectual battle or a battle of principle at all.


> HOME: To return to the point about confusion, to me that seems central to what you do. Before we started taping the conversation, you said your writing was a black hole which would suck people in and deconstruct their mode of thought.
                    FLYNT: That was in relation to cognition. I have a picture of an ideal consciousness which the writings are directed towards producing. It's not confused, I'm actually a great fan of lucidity.


> HOME: I wasn't implying that your formulations were confused, what I was trying to say was that the texts have a disorientating effect on the reader.
                  FLYNT: I associate lucidity with belieflessness. I'm trying to assemble materials for a different mode of life, but it's a completely open question about how they might connect up. The whole drive of western culture, the part of it which is serious, is towards an extreme objectification. It's carried to the point where the human subject is treated almost as if it's dirt in the works of a watch. I'm trying to go to the source of this insane aberration, so that I can dissolve it. I want to do this by integrating subjectivity and objectivity, by making these two things intrinsically interdependent.


----------
https://henryflynt.org/aesthetics/meaning_of_my_music.htm

> Given my views on art, why do I bother with music at all?  The reasons cannot be separated from the reasons for my commitment to ethnic music, for if the possibilities of music were limited to academic and modernist tone-play, then I would have abandoned music after my February 1961 concerts in New York.  It has become clear that the foremost consideration in my musical activity is my own satisfaction:  to assert my dignity, to elevate my morale.  (Hillbilly music began as the self-entertainment of isolated farmers.)  Here I refer to my practice of music, but equally to my continuing appreciation of figures who preceded me.  [Cf. my tribute to Pandit Pran Nath, completed 2002.]  The utopia in human relationships to which my philosophy is directed is unattainable in the foreseeable future.  Activities are worthy, then, whose contribution is to keep the dream alive.  To ennoble the cultural media of a non-privileged, autochthonous community is a way of ennobling the community itself.  As for sound art specifically, it physically inundates you while unfolding in time; it is visceral and kinesthetic; it bypasses verbal concept-recognition and belief-systems.

## Adjacent
- [[people.LaMonteYoung]]
- [[concepts.ConceptArt]]
