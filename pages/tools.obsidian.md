#knowledge #tool [[software.km]] #software


# obsidian

## https://obsidian.md/

_marketeering message_
>A second brain,
for you, forever.
>Obsidian is a powerful knowledge base that works on top of
a local folder of plain text Markdown files.


## tl;dr

All platforms (android too) friendly notetaking ---> knowledgebase app. AKA personal-wiki. See [[concepts.knowledge managment.methodologies]]

- filebased
- markdown

## 101

### keyboard
- **ctrl+o** - open file or create
- **ctrl+p** - select command
	- `split vertically` `split horizontally`
	- ...
- **ctrl+e** - edit vs. preview mode 
- **LMB + shift** open link 
- **LMB + ctrl + shift** open link in new pane
#### commands
- split

### syntax
``[[link]]`` - link
`![[translcusion]]` - transclusion [[tools.obsidian.transclusion]]

## 102
### Links
- [Free Course for Beginners | Obsidian Note-Taking App](https://www.youtube.com/playlist?list=PL3NaIVgSlAVLHty1-NuvPa9V0b0UwbzBd) #video
- https://www.youtube.com/watch?v=bM2PlwgPuIA
- [Productivity Guru](https://www.youtube.com/channel/UC_Xx2woqzPkcvG_ea5d276A)
- [Obsidian Keyboard Shortcuts](https://keycombiner.com/collections/obsidian/winlinux/)
- lists
	- https://github.com/kmaasrud/awesome-obsidian

### going online 
- https://t.co/TYdY3NmlB6

### ![[tools.obsidian.workflows]]

### ![[tools.markdown]]

## Features and plugins
### features
#### [[tools.mermaid]] - notation for flowcharts
- https://mermaid-js.github.io/mermaid/#/
#### knowledge graph
- [[dga.flat.graph]]
#### transclusion
![[tools.obsidian.transclusion]]

### Plugins

#### Major / interesting
##### Clipper / highlighter #update
- obmd clipper
	- https://www.reddit.com/r/ObsidianMD/comments/jhhp4r/obsidian_clipper_plugin/
	- https://jplattel.github.io/obsidian-clipper/ 
- roam clipper
	- https://github.com/GitMurf/roam-highlighter#roam-highlighter 
##### Note Refactor
> for extracting the selected portion of a notes into new note.

##### Taskbone OCR
- *OCR via cloud, local Tesseract possible*


##### limelight

#### Minor #tosort 
##### Paste URL into selected text
##### Advanced Tables
##### cMenu

##### Mind Map
- *markdown to mindmap*

##### Sliding Panes
 - *parallel textface aka Andy Matuschak mode*
	- [[people.AndyMatuschak]]	

##### Media Extended
- timestamps, subtitles/captions
- https://github.com/aidenlx/media-extended

##### Map of Content
- https://github.com/Robin-Haupt-1/Obsidian-Map-of-Content

##### Journey plugin
- https://github.com/akaalias/obsidian-journey-plugin

##### readitlater
- https://github.com/DominikPieper/obsidian-ReadItLater

##### review
- https://github.com/ryanjamurphy/review-obsidian

##### checklsit consolidate
- https://github.com/delashum/obsidian-checklist-plugin

##### dataview
- https://github.com/blacksmithgu/obsidian-dataview

##### todos
- https://github.com/tgrosinger/slated-obsidian
- https://github.com/schemar/obsidian-tasks

##### adjacency matrix maker 
- https://github.com/SkepticMystic/adjacency-matrix-maker

##### breadcrumbs
- https://github.com/SkepticMystic/breadcrumbs

##### juggl [graphs]
- https://juggl.io/Juggl

##### vantage
- https://github.com/ryanjamurphy/vantage-obsidian

##### admonition
- https://github.com/valentine195/obsidian-admonition/releases/tag/6.1.1
#### community plugins references
- ![[Screenshot from 2021-12-26 02-11-46.png]]
- https://papierlos-studieren.net/en/2021/07/28/my-8-favourite-obsidian-plugins-1/
- https://deskoflawyer.com/obsidian-community-plugins-2/

### other tools + unsorted

- autolink 
	- https://forum.obsidian.md/t/auto-linking-tool/2218/3
	- https://github.com/perkinsben/obs_tools/tree/master/forward_linker
- obs utils
	- https://github.com/mm53bar/obsidian_utils 
- folder note / flatfile retarded
	- https://forum.obsidian.md/t/folder-note-plugin-add-description-note-to-folder/12038 
- publish / obsidian-zola 
	- https://www.reddit.com/r/ObsidianMD/comments/u3irg6/a_free_simple_good_looking_alternative_to/

#### kindle #update
- [fyodor](https://github.com/rccavalcanti/fyodor) #1 :+1:
- **kindle highlighter community plugin **
- [web clipper and kindle high-lighter] https://forum.obsidian.md/t/web-clipper-highlighter-and-kindle-highlights-notes-extraction-extension/852 #kindle
- [klipbook](https://github.com/grassdog/klipbook)
- [kindlehighlights](https://box.matto.nl/kindlehighlights2wiki.html)

#### [[tools.markdown]]

### themes
- ?
	- https://github.com/jdanielmourao/obsidian-sanctum
	- https://github.com/chetachiezikeuzor/Yin-and-Yang-Theme

### CSS
[Notice blocks - warning, info, success, danger blocks - Share & showcase - Obsidian Forum](https://forum.obsidian.md/t/notice-blocks-warning-info-success-danger-blocks/4216)
tag pills




## alternatives
_Obsidian is open source, but stil commercial project. Here is list of open source alternatives:TOREADTHEMARKDOWNS_
- [Dendron](https://www.dendron.so/notes/4bb85c39-d8ac-48ad-a765-3f2a071f7bc9.html)
	- supports export
- [joplin](https://github.com/rccavalcanti/joplin)
- [zettlr](https://zettlr.com/)
- [bangle](https://github.com/bangle-io/bangle-io)
- [tangentnotes](http://tangentnotes.com/)

## Obsidian issues
- [Nested vaults](https://forum.obsidian.md/t/nested-vaults-usage-and-risks/6360)