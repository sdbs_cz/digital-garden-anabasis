# Artificial Intelligence
#chaosstream #incubation #tosort


>“It's a synthetic party, so many of the policies can be contradictory to one another," Staunæs said. "Modern machine learning systems are not based on biological and symbolic rules of old fashioned artificial intelligence, where you could uphold a principle of noncontradiction as you can in traditional logic. When you synthesize, it's about amplifying certain tendencies and expressions within a large, large pool of opinions. And if it contradicts itself, maybe they could do so in an interesting way and expand our imagination about what is possible.”
> - https://www.vice.com/en/article/jgpb3p/this-danish-political-party-is-led-by-an-ai

## Follow

- [projects.feedfarm](projects.feedfarm.md)

- [INFORM>AVG>AI](https://inform.sdbs.cz/doku.php?id=avg#ai_deeplearning_etc)


- image / video
	- [[ai.image.on-line]]
	- [[ai.image.processing]]
	- [[ai.image.generation]]
- toolsets
	- [[tools.ai.magenta]]
	- [[incubation.openAI]]
- audio **_rooted in different garden_ #fix**
	- [[ai.music]]
	- [[ai.narration]]
	- [[audio.ai]]
	- [[hudba.mix.ai]] 
	- [[audio.ai.spleeter]] 
- chaosstream
	- [[areas.AI.tech]]
- text
	- https://www.bedtimestory.ai/
	- chatgpt / bard / ...

## articles
[Anatomy of an AI System](https://anatomyof.ai/)
- https://pluralistic.net/2023/02/16/tweedledumber/#easily-spooked chatbots etc

### [[people.NoamChomsky]]
- https://www.openculture.com/2023/02/noam-chomsky-on-chatgpt.html
- https://www.youtube.com/watch?v=axuGfh4UR9Q
- https://whimsical.com/mlst-chomsky-transcript-WgFJLguL7JhzyNhsdgwATy
- https://www.youtube.com/watch?v=PBdZi_JtV4c




## model 
- model issuses
- language issues
- language model issues
- [[concepts.language]]

## artsy fartsy
- [Creativity x Machine Learning](https://mlart.co/)
	- 👋I'm Emil, a resident at the Google Arts & Culture Lab. I've curated a collection of Machine Learning experiments.


------------
https://twitter.com/jschauma/status/1626443725533577221

------
------
BOOM.  
  
**Pluralistic: Google’s chatbot panic (16 Feb 2023) – Pluralistic: Daily links from Cory Doctorow**  
[https://pluralistic.net/2023/02/16/tweedledumber/#easily-spooked](https://pluralistic.net/2023/02/16/tweedledumber/#easily-spooked)  
  
ChatGPT and its imitators have all the hallmarks of a tech fad, and are truly the successor to last season's web3 and cryptocurrency pump-and-dumps. One of the clearest and most inspiring critiques of chatbots comes from science fiction writer Ted Chiang, whose instant-classsic critique was called "ChatGPT Is a Blurry JPEG of the Web":  
  
[https://www.newyorker.com/tech/annals-of-technology/chatgpt-is-a-blurry-jpeg-of-the-web](https://www.newyorker.com/tech/annals-of-technology/chatgpt-is-a-blurry-jpeg-of-the-web)  
  
Chiang points out a key difference between the output of ChatGPT and human authors: a human author's first draft is often an original idea, badly expressed, while the best ChatGPT can hope for is a competently expressed, unoriginal idea. ChatGPT is perfectly poised to improve on the SEO copypasta that legions of low-paid workers pump out in a bid to climb the Google search results.  
  
Speaking of Chiang's essay in this week's episode of the This Machine Kills podcast, Jathan Sadowski expertly punctures the ChatGPT4 hype bubble, which holds that the next version of the chatbot will be so amazing that any critiques of the current technology will be rendered obsolete:  
  
**232. 400 Hundred Years of Capitalism Led Directly to Microsoft Viva Sales**  
[https://soundcloud.com/thismachinekillspod/232-400-hundred-years-of-capitalism-led-directly-to-microsoft-viva-sales](https://soundcloud.com/thismachinekillspod/232-400-hundred-years-of-capitalism-led-directly-to-microsoft-viva-sales)  

![](https://nyx.cz/images/play.png)![](https://wave.sndcdn.com/NEE9bDdpKDv2_s.png)

  
  
Sadowski notes that OpenAI's engineers are going to enormous lengths to ensure that the next version won't be trained on any of the output from ChatGPT3. This is a tell: if a large language model can produce materials that are as good as human-produced text, then why can't the output of ChatGPT3 be used to create ChatGPT4?  
  
Sadowski has a great term to describe this problem: "Hapsburg AI." Just as royal inbreeding produced a generation of supposed supermen who were incapable of reproducing themselves, so too will feeding a new model on the exhaust stream of the last one produce an ever-worsening gyre of tightly spiraling nonsense that eventually disappears up its own asshole.

------
------

[[areas.AI.tech]]

------------

