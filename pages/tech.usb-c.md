# USB-C

## power delivery
- https://help.dockingdrawer.com/kb/getting-started/what-is-usb-c-with-power-delivery-pd

```
clarfonthey@toot.cat - the idea of having a global standard for power delivery is extremely good because it means that instead of literally everything coming with their own power brick, we can just plug everything into one of the common USB ports available all around us. but I hate that the way we're doing it is via an extremely complicated, difficult-to-manufacture connector that is also much more likely to break.

it's worth taking a slight tangent to explain some of the absolute basics of power delivery, and how we got here. first, we measure power in watts (W), which is a rate of raw energy over time. if we want to measure energy, we multiply power by time and get units like watt-hours (Wh). most people get billed for electricity in kilowatt-hours (kWh) which are 1000 watt-hours.

now, electrical power is further split into voltage and current, measured in volts (V) and amps (A) respectively. mathematically, you multiply these two together to get power. one important bit about this is that, without simplifying too much, you can tweak the voltage and current of some power going down a line using specialised circuits to bump one up and lower the other, or vice/versa, without changing the resulting power. this isn't a perfect conversion and some is loss, but at a glance that's basically what happens.

so, when we actually send power from a station down wires to people's homes, we crank up the voltage to reduce the current, which retains the same amount of power but reduces losses through the wires as they travel for large distances.

the other thing we do is send the power as AC, but for the sake of explanation, this is something that's done to make the voltage conversions easier and it helps further reduce losses through wires. this AC can sometimes be used directly, but especially in the case of computers, it needs to be converted to DC before being used.

the point is, a lot of power delivery is all about tweaking the voltage up and down all over the place until eventually it gets to something that needs power, at which point we convert it to the voltage the device actually needs and convert it from AC to DC.

now, USB alone solves one of these problems: the power given by USB is always DC, and it's particularly low voltage. while the voltage coming out of outlets is 120V or 240V which is highly dangerous, the voltage coming out of USB is 5V and will essentially not hurt you even if you try. (don't try)

however, USB initially only allowed around 0.5A of current before being bumped to 1A, meaning that the power from USB initially was a measly 5W at best. although that can definitely power a keyboard, it'll take forever to charge your phone.

QuickCharge (QC), a proprietary protocol, helped alleviate much of this problem by allowing up to 2A, then later 3A. this effectively cut the time to charge your phone in half and then in thirds, which is not a small thing.

however, nowadays, we have USB-PD, and one of the biggest things it adds is the ability to control the voltage, which can further increase the power. USB-PD does this by adding extra connections in the USB C connector that allow sending signals to request higher voltage. the default 5W power is sent to the device to control the circuits that send this signal, after which the higher voltage is sent down a separate line to properly power the device.

the power bricks that used to come with most applicances were the ones that controlled these conversions, since there was no other way to convert the power in a useful way. and so, not only did you have to have a plug that varied by country to get this power, you also had to have extra electronics specific to the device to convert the voltage. USB helps do away with all of that; people have their own power bricks built into their outlets, wire strips, computers, whatever, and we save a lot of e-waste and headaches. right?

at this point, it's worth mentioning that ordinary USB A had four connections, with wires going in and out for power, and wires going in and out for data. later versions of USB added in five extra wires for more data, and USB C added in three more for power delivery and configuration alongside... duplicating all the wires to allow flipping it upside down!? that's six times the wires for UBC-C, which all has to fit into a connector that's under half the size of the original.

so, USB A had four wires in this big ol connector that us scientists call chonky. we could actually see them and fix them if they broke. power delivery only used 2/4, but that's only 2 wires wasted.

now, USB-C exists and we're only using 5/12 wires for power delivery, or 10/24 if you really need to flip it upside down. in addition to being very complicated, they also don't have any mechanism for holding them in place (see: the holes in the sides of a USB-A connector) and are held in by friction alone. they break easily and cost a lot to make.

it's great that now we can get just one battery pack or power brick that can charge or power all sorts of devices. but to do it, we have this overly complicated connector that breaks and can't be fixed easily. it's a mess.

like, having USB-C is still nice for phones and such, which absolutely cannot rely on separate ports for power and data. but if you need it to power a clock or lamp or whatever, there's no reason to require such a complicated connector. and there's no reason why we couldn't have a simpler connector that can be wired into USB C, so we have USB-C to whatever wires.
ugh.
```
