# BT
- BT5 is must-have
	- https://www.howtogeek.com/343718/whats-different-in-bluetooth-5.0/
- APTX low latency also
# APTX
## windows

- https://www.howtogeek.com/354321/why-bluetooth-headsets-are-terrible-on-windows-pcs/
-  https://mdp-lab.com/aptx-win10/

### how to disable headset [pairing]
![[bluetooth.20211204142557.png]]
## linux
- https://medium.com/@multi.flexi/use-aptx-on-linux-3f3c4d597dcd
- https://github.com/EHfive/pulseaudio-modules-bt
### debian 
- https://blog.shanock.com/compiling-pulseaudio-aptx-support-on-debian-10/
- https://github.com/lagerimsi-ds/debian-buster_bluetooth_aptX-LDAC/
- https://packages.debian.org/bullseye/libopenaptx-dev
### ubuntu
- https://askubuntu.com/questions/1071469/how-to-enable-aptx-for-bluetooth-devices
- - https://github.com/EHfive/pulseaudio-modules-bt/wiki/Packages#ubuntu-1804-1810-1904

## apple
#weirdshit
- https://www.cnet.com/tech/mobile/what-is-apple-w1-bluetooth-chip-headphones-explained/

## future
- https://www.howtogeek.com/558579/what-is-bluetooth-le-audio-and-why-will-you-want-it/