# markdown

## #101
>Markdown is intended to be as easy-to-read and easy-to-write as is feasible.
>
>Readability, however, is emphasized above all else. A Markdown-formatted document should be publishable as-is, as plain text, without looking like it’s been marked up with tags or formatting instructions. 
>
>It makes it riddiculously easy to write a text document without having to take your fingers off the keyboard and use the mouse or look up some complex html tag. It is also really easy to read for someone who doesn’t even know markdown.

## 102 
- if file --> file extension `.md`
- editors
	- multios
		- [typora](https://github.com/typora/)
	- *ANDROID*
		- [markor] [https://play.google.com/store/apps/details?id=net.gsantner.markor&hl=en_US&gl=US] 
		
### #tosort 
- https://gist.github.com/Almeeida/41a664d8d5f3a8855591c2f1e0e07b19
- https://daringfireball.net/projects/markdown/syntax
- https://churchm.ag/markdown-101/
- https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
		
#### markdown emojis
- https://3os.org/markdownCheatSheet/emoji/
- https://gist.github.com/rxaviers/7360908

## Tools and #tosort 
- [fyodor - My Clippings to markdown](https://github.com/rccavalcanti/fyodor)  :+1:
	- my clippings to markdown files converter #kindle
- [MarkDownload](https://addons.mozilla.org/en-GB/firefox/addon/markdownload/) :+1:
	- Markdown Web Clipper #firefox
- [MarkMap](https://markmap.js.org/)
	- markdown + mindmap
- [markdown-table-editor kernel](https://github.com/susisu/mte-kernel) 
-  [pdf2md](https://pdf2md.morethan.io/) - in browser but local
-  [Docs to Markdown - G Suite Marketplace](https://gsuite.google.com/marketplace/app/docs_to_markdown/700168918607?pli=1)
	- gdocs to markdown converter

 

### going online
- https://sleeplessbeastie.eu/2016/12/12/how-to-publish-markdown-files-effortlessly/
###### [[concepts.digital garden]]
- inform vs. informal
- [gardenserver](gardenserver.md) //our own
- [madness](https://madness.dannyb.co/)
- [digital garden with Obsidian](https://forum.obsidian.md/t/understanding-digital-garden-creation-with-obsidian/2725/2)
- [markdown-folder-to-html - npm](https://www.npmjs.com/package/markdown-folder-to-html)
- [allmark - the markdown server](https://allmark.io/)
- [Eleventy Garden](https://github.com/binyamin/eleventy-garden)
- [Obsidian to HTML converter](https://github.com/kmaasrud/obsidian-html)
	- [Publish your Obsidian Vault to your Digital Garden](https://yordi.me/publish-your-obsidian-vault-to-your-digital-garden/)


#### examples
[bryan-jenks/INDEX](https://publish.obsidian.md/bryan-jenks/INDEX)

### going pdf
#### [Pandoc](https://pandoc.org/) 
- If you need to convert files from one markup format into another, pandoc is your swiss-army knife. 
- [pandoc-markdown-to-pdf](https://www.mscharhag.com/software-development/pandoc-markdown-to-pdf)


## android 


---

https://subconscious.substack.com/p/subtext-markup-for-note-taking


#split
