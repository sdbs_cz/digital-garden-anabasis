# Archives in art history

--------------------------
>Art history is not linear; although it is often taught as such. Culture is a multi-dimensional network that feeds and builds upon itself in a mashup that transcends time.
>- [Ryan McGinnes - Art History Is Not Linear](http://www.ryanmcginness.com/sites/default/files/user-uploads/%2B%20McGinness%20Art%20History%20Is%20Not%20Linear.pdf)
---------------------------


## Bilderatlas Mnemosyne
![https://2.bp.blogspot.com/_xxCI1ErSIb0/SxPHUar7cTI/AAAAAAAAAJE/bhygx-Zv5_8/s1600/full.jpg](https://2.bp.blogspot.com/_xxCI1ErSIb0/SxPHUar7cTI/AAAAAAAAAJE/bhygx-Zv5_8/s1600/full.jpg)

>In Greek mythology, Atlas was the name of a titan, brother of Prometheus, who confronted the Gods of Olympia, to take over their power and give it to humankind. The story goes that he was punished in the same measure of his force. While a vulture ripped Prometheus’ liver in the borders of the East, Atlas in the West (between Andalusia and Morocco) was compelled to hold the whole celestial dome on his shoulders. So more tells the story, this burden gave him in surmountable knowledge and desperate wisdom. He was the ancestor of astronomers, geographers, and some say he was the first philosopher. A mountain (Atlas), an ocean (Atlantic) and an anthropomorphic architectural support (Atlant) got their name after him.
>> FROM THE ATLAS » MNEMOSYNE«

>More recent Warburg research has shown that it is plausible that Aby Warburg’s historiography was formed in conjunction with technical media. So the structure of Warburg’s “Thinking in Pictures” (“Denken in Bildern”) was modelled by means of, among other things, imaging and image transmission processes such as cinematography, and the materiality of these media extends deep into Warburg’s historiographical and epistemological designs.
>> https://zkm.de/en/event/2016/10/aby-warburg-mnemosyne-bilderatlas-kolloquium/abstracts

>As can be seen, the _Mnemosyne Atlas_ is not properly a book or atlas in the traditional sense. It is rather a deconstructive space, a milieu for contrast and dialogue, and a battleground of images and mutable concepts that proceeds according to connections and disjunctions. As is well known, Warburg called such mechanism the "law of the good neighbor." For his part, Georges Didi-Huberman, who has carried out exhaustive research into the _Atlas_ of Aby Warburg, alludes to a dialectical montage aimed at dealing with discontinuities and partial knowledge. In Didi-Huberman’s view, Warburg shows a destructive behavior that paradoxically makes room for the appearance of creative relations. Thus, thanks to this anti-method, Warburg promotes the arousal of unpredictable events within the epistemological realm. In this context, it is highly significant that Warburg’s disorganized procedure bears a strong resemblance to Gilles Deleuze and Félix Guattari’s rhizomatic proposal.[\[8\]](https://contempaesthetics.org/newvolume/pages/article.php?articleID=812#FN8) As it is put by those authors, a rhizome is not a root and neither a tree, both of which grow vertically. On the contrary, the rhizome grows horizontally, connecting and disconnecting diverse points.

### Online tours --> [[concepts.archives.art.tours]]
- https://warburg.sas.ac.uk/aby-warburgs-bilderatlas-mnemosyne-virtual-exhibition
- https://warburg.sas.ac.uk/library-collections/warburg-institute-archive/online-bilderatlas-mnemosyne
- [Cornell Uni - Ten panels from the Mnemosyne Atlas](https://warburg.library.cornell.edu/)

### Links
-  gombrich
	-  http://www.columbia.edu/cu/arthistory/faculty/Freedberg/Gombrich_and_Warburg_Making_and_Matching.pdf
- Warburg institute
	- https://warburg.sas.ac.uk/library-collections/warburg-institute-archive/online-bilderatlas-mnemosyne  
	- https://warburg.blogs.sas.ac.uk/2020/09/23/bilderatlas-mnemosyne-exhibition-assistant-lorenza-gay/
- ZKM Warburg project
	- https://zkm.de/en/search/site/warburg
	- https://web.archive.org/web/20171113093551/http://zkm.de/en/blog/2016/10/what-can-be-done-with-images 
	- https://markamerika.com/news/from-the-zkm-collection-writing-the-history-of-the-future
- [From rhizome perspective?](https://contempaesthetics.org/newvolume/pages/article.php?articleID=812#FN3) 
	- [[concepts.rhizome]] 

### [[people.AbyWarburg]]
>There were other problems that made the work on the Atlas an infinite endeavour. Warburg was a technophile. He was interested in telecommunication, the press and travelling; all these new technologies enabled new forms of travelling, but also prolonged the old idea of migration that connected civilizations from the beginning. _Technology_, for example in the form of printing,was also the direct link between Dürer’s engravings and the 28 telephones in his avant-garde library building. He had already written an article entitled „Airship and submarine in medieval imagination“ that suggested that former societies had anticipated what he called “vehicles of thought” and imagination that we dispose of today. Images were their vehicles.
> - http://www.educ.fc.ul.pt/hyper/resources/mbruhn/

### [[people.GeorgesDidiHuberman]]
- I of Hermes
- 
## Arcades Project
- [Arcades Project@Wikipedia
- ](https://en.wikipedia.org/wiki/Arcades_Project)
- [[people.WalterBenjamin]]
## Mundaneum
>The **Mundaneum** was an institution which aimed to gather together all the world's [knowledge](https://en.wikipedia.org/wiki/Knowledge "Knowledge") and classify it according to a system called the [Universal Decimal Classification](https://en.wikipedia.org/wiki/Universal_Decimal_Classification "Universal Decimal Classification"). It was developed at the turn of the 20th century by Belgian lawyers [Paul Otlet](https://en.wikipedia.org/wiki/Paul_Otlet "Paul Otlet") and [Henri La Fontaine](https://en.wikipedia.org/wiki/Henri_La_Fontaine "Henri La Fontaine"). The Mundaneum has been identified as a milestone in the history of data collection and management,[[1]](https://en.wikipedia.org/wiki/Mundaneum#cite_note-1) and (somewhat more tenuously) as a [precursor to the Internet](https://en.wikipedia.org/wiki/History_of_the_Internet "History of the Internet").[[2]](https://en.wikipedia.org/wiki/Mundaneum#cite_note-2)
>-https://en.wikipedia.org/wiki/Mundaneum
- _Art or Archive? “The Mundaneum’s photographic heritage put to the test of time”__
	- https://artsandculture.google.com/story/0AXRA53tkh4A8A
		- https://europe.googleblog.com/2015/08/google-pays-tribute-to-belgiums.html
		- 
	- [[concepts.trailblazers]]
	- 




## media archeology

   - [[concepts.linearity]]
   - [[incubation.anarcheology]]
   - [[incubation.anarcheology]]
   - https://ctrl-c.club/~lettuce/trash.html
