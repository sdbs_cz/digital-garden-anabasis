# [[las]].future
[[las.roadmap]] 

## [[las.future.dokumentace]]
	
_**“Everything is related to everything else, but near things are more related than distant things.”** — W. Tobler_

## Footage / Motion_capture /  Archive
### Interaction -->---> Animation

## Technicalia / conceplallia
- endframes links to opposite side of canvas
	- how
		- hyperlink? like hyperdrive-anti-anchor
	- endless vs. not endless
- more hyperlinks
- more SVGs and/or zoom-svg-visuals overall
- more videos
- fix soundsauce for seemless situation
	- content-vice, no coding [yet?]
- space bar signs
	- safety space bar signs to remind you to press spacebar in case of emergency 
- more anchors
- cleanup of 101

### User Interface / interaction
- joystick
- ddr
- bigscreen - with webcam control?
- [[fulldocs.twitter.zoomable_ui]]]

![[Pasted image 20211012234618.png]] #update 

## Topicalia
+ [[las.missing]]
- Line and Surface X Line of Flight [[1000p]]
- WSB
	- Electronic revolution
		- https://pile.sdbs.cz/item/61
- perec 
	- puzzles
		- https://www.thenation.com/article/archive/georges-perec-puzzles/ 
- serpent theme
	- Wilson 
	- Warburg
- Cities theme [[las.quotes.cities]]
- temporality theme
	- //as proposed
		- txt gifs 
	- +
- aarseth / ergodic literature 
- 1000 plates subcanvas
	- DNF 
		- existenalism X postlanguage
- visionaries (aka [[concepts.trailblazers]])
	- [alan kay - tribute to ted nelson](https://www.youtube.com/watch?v=AnrlSqtpOkw) - konec
- space bar signs 
- [[fulldocs.twitter.codex.relations]]


## - [ ] Rename
- Line and Surface X Line of Flight [[1000p]]

---------------------------------

---------------------------------

---------------------------------

---------------------------------

---------------------------------

**Information is like a bank. Some of us are rich, some of us are poor with information. All of us can be rich. Our job - your job is to rob the bank, to kill the guard. We go out there to destroy everybody, who keeps and hides the whole information. Genesis P. Orridge **


**Draw the line, says the accountant: but one can in fact draw it anywhere. Gilles Deleuze**



Once we cast architecture into cyberspace, these concerns take on both theoretical and practical urgency. The architect must now take into active interest not only the motion of the user through the environment, but also account for the fact that the environment itself, unencumbered by gravity and other common constraints, may itself change position, attitude, or attribute. This new choreographic consideration is already a profound extension of responsibilities and opportunities, but it still corresponds only to “movement-image”. Far more interesting and difficult is the next step, in which the environment is understood not only to move, but also to breathe and transform, to be cast into the wind not like a stone but like a bird. What this requires is the design of mechanisms and algorithms of animation and interactivity for every act of architecture. Mathematically, this means that time must now be added to the long list of parameters of which architecture is a function.


-----------------------------------

- https://wiki.killuglyradio.com/wiki/Project/Object_concept frank zappa