# Self-hosting

## Essential
>Self-hosting is the use of a computer program as part of the toolchain or operating system that produces new versions of that same program—for example, a that can compile its own source code. Self-hosting software is commonplace on personal computers and larger systems. Other programs that are typically self-hosting include kernels, assemblers, command-line interpreters and revision control software.
> - https://en.wikipedia.org/wiki/Self-hosting

### Abstract Pragma
- personal computer as 
	- server
	- bridge to server
- [[anabasis]] computers 

### 101s
- https://wiki.r-selfhosted.com/
- https://wiki.selfhosted.show/


## Tools
### [syncthing](AREAS/DigitalGardenAnabasis/syncthing)
### [[AREAS/DigitalGardenAnabasis/_INFORM/ipfs]]
### [[torrent]]
[[linux.server]]
[[tools.nextcloud]]
[[tools.syncthing]]
## Relative
[sdbs_selfhosting](https://inform.sdbs.cz/doku.php?id=sdbs_selfhosting)

-----------