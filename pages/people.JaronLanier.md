He writes "If we start to believe that the Internet itself is an entity that has something to say, we're devaluing those people [creating the content] and making ourselves into idiots."[[25]](https://en.wikipedia.org/wiki/Jaron_Lanier#cite_note-Edge2-25)

[[concepts.playlisting]]