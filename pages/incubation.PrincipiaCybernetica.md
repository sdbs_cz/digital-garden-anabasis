> **Principia Cybernetica** is an international cooperation of scientists in the field of [cybernetics](https://en.wikipedia.org/wiki/Cybernetics "Cybernetics") and [systems science](https://en.wikipedia.org/wiki/Systems_science "Systems science"), especially known for their website, Principia Cybernetica. They have dedicated their organization to what they call "a computer-supported evolutionary-systemic philosophy, in the context of the transdisciplinary academic fields of Systems Science and Cybernetics".
>  -- https://en.wikipedia.org/wiki/Principia_Cybernetica

## Adjacent #unsorted 
- https://en.wikipedia.org/wiki/Red_Queen_hypothesis
	- https://en.wikipedia.org/wiki/Red_Queen%27s_race
- https://en.wikipedia.org/wiki/Global_brain
- https://en.wikipedia.org/wiki/Project_Cybersyn
- [[concepts.trailblazers]]
- [[incubation.wikipedia]]
- [[areas.philosophy]]