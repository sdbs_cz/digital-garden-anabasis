---
created: 2022-07-24T21:56:24 (UTC +02:00)
tags: 
source: https://www.thenation.com/article/archive/georges-perec-puzzles/
author: By Joshua Kosman and Henri Picciotto

September 14, 2012
---
[[people.GeorgesPerec]][[fulldocs.GeorgesPereconPuzzles.quotes]]

# Georges Perec on Puzzles | The Nation

> ## Excerpt
> Constraints, literary and ludic

---
## Georges Perec on Puzzles

## Georges Perec on Puzzles

## 

Constraints, literary and ludic

September 14, 2012

\[First, three links:  
• The [current puzzle  
](http://www.thenation.com/article/puzzle-no-3252)• Our puzzle-solving [guidelines  
](http://www.thenation.com/article/solving-nations-cryptic-crosswords)• A Nation puzzle solver’s [blog](http://thenationcryptic.blogspot.com/) where you can ask for and offer hints.\]

Writing cryptic clues is a form of constrained writing. To begin with, for each entry, one has to include both definition and wordplay—but not every sort of wordplay works for every entry! Most words, for example, do not yield anything intelligible if read backwards, many do not make for suitable anagrams, and so on. Then, once some form of wordplay has been found, one still needs to come up with a way to combine it with the definition in a way that has a plausible surface reading. These and other constraints are what make clue writing an interesting puzzle for the constructor.

Some of the most sustained exploration of the use of constraints in literature has come from the writers’ group Oulipo (the name is a bigram acronym for _Ouvroir de littérature potentielle,_ or “workshop of potential literature”). The group counts among its members the Italian novelist Italo Calvino and the American writer Harry Mathews, but most participants have been French. One prominent Oulipian was the French novelist Georges Perec (1936–82), who in addition to his literary work was a prolific constructor of crossword puzzles (about which we will have more to say in a future post). Perec wrote a lengthy novel (_La Disparition_, translated into English by Gilbert Adair as _A Void_) that never used the letter E—then followed up with a novella (_Les Revenentes_, translated by Ian Monk as _The Exeter Text: Jewels, Secrets, Sex_), in which the _only_ vowel used was E. He also constructed a palindrome of 1247 words (5,566 letters) which, unsurprisingly, has not been translated.

Perec’s masterwork, _Life A User’s Manual_, is built upon a massively intricate formal framework using multiple constraining schemes, and tells many interlocking stories. The backbone of the main narrative involves jigsaw puzzles. Perec writes: “The art of jigsaw puzzling begins with wooden puzzles cut by hand, whose maker undertakes to ask himself all the questions the player will have to solve, and, instead of allowing chance to cover his tracks, aims to replace it with cunning, trickery, and subterfuge. All the elements occurring in the image to be reassembled—this armchair covered in gold brocade, that three-pointed black hat with its rather ruined black plume, or that silver-braided bright yellow livery—serve by design as points of departure for trails that lead to false information.… From this, one can make a deduction which is quite certainly the ultimate truth of jigsaw puzzles: despite appearances, puzzling is not a solitary game: every move the puzzler makes, the puzzle-maker has made before; every piece the puzzler picks up, and picks up again, and studies and strokes, every combination he tries, and tries a second time, every blunder and every insight, each hope and each discouragement have all been designed, calculated, and decided by the other.”

This is of course true of puzzles of all types. When you solve our crossword, we are there with you, engaged in a sort of dialogue. When writing clues, we imagine how you might respond to a particular word or phrase, and lay traps accordingly. When you see through our schemes, you get beneath the surface of the clue, and unpack the way our minds work. Paradoxically, the friendly struggle we are engaged in is a collaboration!

Please share your thoughts on literary and cruciverbal constraints below, along with comments, questions, kudos or complaints about the current [puzzle](http://www.thenation.com/article/puzzle-no-3252) or any previous puzzle.
