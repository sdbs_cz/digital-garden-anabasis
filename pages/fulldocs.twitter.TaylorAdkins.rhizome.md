---
created: 2021-08-07T13:53:29 (UTC +02:00)
tags: []
source: https://twitter.com/tadkins613/status/1247359268585639936
author: 
---

# (1) Taylor Adkins on Twitter: "What's Deleuze and Guattari's concept of rhizome? THREAD 1/" / Twitter

> ## Excerpt
> What's Deleuze and Guattari's concept of rhizome? THREAD 1/

---
What's Deleuze and Guattari's concept of rhizome? THREAD 1/


Guattari defines the rhizome as a “lattice” and provides the aspects of its functioning on pg. 19 of The Machinic Unconscious (also cf. the first plateau of A Thousand Plateaus, pg. 21 in particular has a summary of the characteristics of the rhizome). Let's go through both 2/

In MU, we have a list of aspects: A) Unlike Chomskyan trees which proceed via dichotomy from a point S (abbreviation of Sentence), any point of a rhizome is connectable with any other point 3/

B) Not every trait of the rhizome will refer back to a linguistic trait. Every semiotic chain will connect with a variety of encoding modes (biological, political, economic…); this involves regimes of signs but also non-signs 4/

\[This point is important for the notion of signs-particles: beneath contents and expressions the plane of consistency (or the abstract machine) emits and combines particles-signs that set the most asignifying of signs to functioning in the most deterritorialized of particles\] 5/

C) Relations between levels of segmentarity within each semiotic stratum will differentate inter-stratic relations based on lines of flight of deterritorialization (for the notion of segmentarity, strata, and inter-strata, cf. plateau 3 of A Thousand Plateaus) 6/

D) A pragmatics of rhizomes renounces the idea of underlying structures; unlike the unconscious of psychoanalysis, the machinic unconscious is not representational, crystallized in codified complexes/redivided on a genetic axis; it is to be built like a map 7/

E) The map will be detachable, reversible, connectable, and modifiable 8/

F) Trees may exist in a rhizome; branches of a tree may bud in a rhizome-form 9/

We should also consider the fact that ATP deepens the notion of the rhizome by adding certain principles behind it 10/

A) Principle of connection and heterogeneity: every point of a rhizome can be connected to any other, and must be (indicated above, albeit the imperative “must” is interestingly not indicated in MU) 11/

B) Principle of multiplicity: when the multiple is treated substantively as “multiplicity”, we dismantle the dialectic of the One and the multiple 12/

(This seems to be Deleuze’s addition, insofar as it resonates directly with the history of philosophy); what’s important is the undermining of the notion of unity and centrality. Later in the plateau on the rhizome, this will be said in terms of acentered multiplicities, etc. 13/

One last thing to note is that from MU to ATP, the notion of “points” of a rhizome is complicated by the fact that these points are not so much points at all (as we would find in a tree-form), but actually lines, only lines 14/

If there are points in a rhizome they are fused with the line, under the line. This will be important for arguing that a rhizome cannot be overcoded, insofar as it lacks a supplementary dimension above and beyond its dimensions of lines. Multiplicities are in this sense flat 15/

C) Principle of asignifying rupture: this could also be called the principle of “aparallel evolution”, and it stands in opposition to any principle of oversignifying breaks 16/

although rhizomes contain lines of segmentarity that stratify it, it also flees down lines of deterritorialization; this principle correlates with the second aspect described in MU, arguing that semiotic chains are open to all sorts of modes of encoding 17/

D) Principle of cartography and decalcomania: this principle reiterates the notion that the rhizome concerns neither genetic axes nor deep structures, but the construction of a map, thereby avoiding representational models of the unconscious that function as tracings 18/
