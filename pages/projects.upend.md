
#chaostream 
# UpEnd

# --> https://tmladek.substack.com/
# --> https://t.mldk.cz/notes/883493cb-d722-45e6-bb1c-391ab523ac8b.html

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

# Essential
- alternativni pristup k filesystemu
- file storage jakozto database; database jakozto file storage
- fs ⇋ db ⟹ allmighty library
- ahierarchickej // polyhierarchicky pristup...

> Cut word lines Cut music lines Smash the control images Smash the control machine Burn the books Kill the priests Kill! Kill! Kill!
> - William S. Burroughs - The Soft Machine (1961) 

# Summary
#czech

> v podstate moje idea je :

## Base structure
> - databaze kde mas dva druhy objektu: data, a meta
>   - (jedinej rozdil je, ze data actually odkazujou [[hash]]em na nejakej file, a meta neodkazujou nikam, jenom slouzej jako "anchor")
> - a ke kazdymu objektu muzes mit arbitrarni keys a values
>   - kazdy takovy key/value tvori jeden `E/A/V` triplet (see [Entity Attribute Value model (Wikipedia)](https://en.wikipedia.org/wiki/Entity%E2%80%93attribute%E2%80%93value_model)):
>     - `E`ntity = objekt (`neconeco.mp3`, resp. jeho [[hash]])
>     - `A`ttribute = klic (`year`)
>     - `V`alue = hodnota klice (`2020`)
>   - struktura filesystemu samotna je taky `E/A/V` triplet: `FILE_shaiusdhuijhngsoyuhsdf BELONGS_TO_DIRECTORY shaoidsuhjaoijoiasjdioj`
>   - (kazdej `E/A/V` je taky objekt na kterej se da odkazovat, kdyby sis chtel anotovat anotace)

## Schemas
> - objekty maj kategorie, ktery znamenaj ze se muzes spolehnout ze objekt co je otagovanej "radio" ma: cas pridani, slozku, heaviness, etc.; co je otagovanej "public" ma: "projekt", "filetype", "rok", etc.
>   - to je taky `E/A/V` triplet: `Entita` je ten objekt, `Attribut` je neco jako `IS_A` a `Value` je odkaz na kategorii (ktera je taky objekt slozeny z `E/A/V` tripletu - znacici co musi objekt splnovat)

## Input
> - pridavas soubory bud pres web rovnou do databaze, nebo [[AREAS/DigitalGardenAnabasis/syncthing]]oidne do filetree, a pak zarazujes

## Output
> - output je bud ten, ze je ven exposly jakysi APIcko, ktery ti dava jenom pristup do jedny z moznejch hierarchii, a s tim pracuje treba radio, nebo photo uploader, nebo [[OCR]]ko
>   - nebo ten, ze to proste pouzivas misto file browseru, ale kdykoli mas moznost se vratit zpatky na baseline [[concepts.filesystem]] kdyz najdes, co hledas


## Geekmiscellanea
> [extremne zmateny notes z prubehu konceptualizace](projects.upend.upend_notes_tmp.pdf)
> (pravdepodobne useless pro kohokoli jinyho)


> (clarification - neni to teda jenom key/value, zjistil jsem ze se tomu rika entity/attribute/value, kdy ten key/value je jenom ta posledni dvojice, a "entity" je nejakej abstraktni anchor ke kterymu se pojej ruzny key/values; moje pointa je, ze entity nemusi bejt enom IDcko na ktery vazes, ale i primo data, ktery muzou existovat tim padem na vic mistech / jako vic filu zaroven (nebo taky zadny), a i ty samotny vztahy mezi entitama, cimz ti vznika moznost i nejak znackovat co vlastne o tech datech rikas)

# Goals

- ~~democratic user interface model a la Syncthing~~
- ~~Content-Addressability~~
- ~~K/V metadata store~~
- schemas / constraints on metadata
    - (auto-admin?)
- Links between objects
    - 1-way = Tags
    - 2-way = Relations
    - ~~3-way = ???~~
- 1st order tags
- 1st order links (?)
- kompozice souboru
  - skrz schemata! (sub-objekty pojici se k jedne ci vice entitam)
- Modes
    - R/O only
    - Hybrid
    - Full
- FS revisions / metadata only backups
- staging area, queues...

# Design

## Schemas

>Databases that are “schema-less” give you great flexibility and power when putting data in, and are extremely unhelpful when getting it out. A key-value store is a more technical version of “free text”, with the same drawbacks — it is pretty unhelpful when you want to extract info or do anything with the data, since you cannot guarantee that any specific keys will be there.

[Luke Plant - *We need less powerful languages*](https://lukeplant.me.uk/blog/posts/less-powerful-languages/)


# Resources // Inspiration

## Whole systems

### Design docs

❗[https://www.nayuki.io/page/designing-better-file-organization-around-tags-not-hierarchies](https://www.nayuki.io/page/designing-better-file-organization-around-tags-not-hierarchies)

### Similar projects

❗ [Perkeep](https://perkeep.org)

#### Greglook

[greglook/vault](https://github.com/greglook/vault)

[greglook/blocks](https://github.com/greglook/blocks)

[greglook/merkledag-core](https://github.com/greglook/merkledag-core)

## Criticisms, filesystems

[Files, Formats and Byte Arrays](https://shalabh.com/programmable-systems/files-and-file-formats.html)

❗ [Programátorova kritika chybějící struktury operačních systémů](https://blog.rfox.eu/cz/Programatorova_kritika_chybejici_struktury_operacnich_systemu.html) 

❗ [There is only one OS, and it's been obsolete for decades](https://programmingmadecomplicated.wordpress.com/2017/08/12/there-is-only-one-os-and-its-been-obsolete-for-decades/)

## Highly adjacent projects!

### Datalog

v podstate identicky E/A/V model

[Learn Datalog Today!](http://www.learndatalogtoday.org/chapter/1)

### BeOS / HaikuOS

<https://www.howtogeek.com/696193/what-was-beos-and-why-did-people-love-it/>

Idea s [[concepts.filesystem]]em jakozto databazi s metadaty:

<https://www.haiku-os.org/docs/userguide/en/workshop-filetypes+attributes.html>

# Information enters, is processed
<https://blog.owulveryck.info/2020/11/13/ontology-graphs-and-turtles-part-i.html>

![[mapping_language.png]]

<details>
<summary>Random (and outdated) points</summary>

- content-addressability
- hidden structure within filetypes
- breaking apart from a hirearchy
- key/value store protocol // backend
- VFS compat layer
- config keys as files
- FS as a rich API
- syncthing model, sidecar files
- S3 API ?

</details>

-----------------------------
# #incubation
- https://github.com/crypto5000/celody