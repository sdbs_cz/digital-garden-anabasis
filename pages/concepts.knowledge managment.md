# knowledge managment

## normcore.essential
>Knowledge management (KM) is the process of creating, sharing, using and managing the knowledge and information of an organization.[1] It refers to a multidisciplinary approach to achieve organisational objectives by making the best use of knowledge.[2] 
>>
>Personal knowledge management (PKM) is a process of collecting information that a person uses to gather, classify, store, search, retrieve and share knowledge in their daily activities (Grundspenkis 2007) and the way in which these processes support work activities (Wright 2005). It is a response to the idea that knowledge workers need to be responsible for their own growth and learning (Smedley 2009). It is a bottom-up approach to knowledge management (KM) (Pollard 2008). 
>>https://en.wikipedia.org/wiki/Personal_knowledge_management

### Memex & [[people.VannevarBush]] & [[concepts.trailblazers]] 
>The memex (originally coined "at random",[1] though sometimes said to be a portmanteau of "memory" and "index"[2]) is the name of the hypothetical proto-hypertext system that Vannevar Bush described in his 1945 The Atlantic Monthly article "As We May Think". Bush envisioned the memex as a device in which individuals would compress and store all of their books, records, and communications, "mechanized so that it may be consulted with exceeding speed and flexibility". The memex would provide an "enlarged intimate supplement to one's memory".[3] The concept of the memex influenced the development of early hypertext systems (eventually leading to the creation of the World Wide Web) and personal knowledge base software.[4] The hypothetical implementation depicted by Bush for the purpose of concrete illustration was based upon a document bookmark list of static microfilm pages and lacked a true hypertext system, where parts of pages would have internal structure beyond the common textual format. Early electronic hypertext systems were thus inspired by memex rather than modeled directly upon it. 
>>https://en.wikipedia.org/wiki/Memex

- https://learningmetonymy.wordpress.com/2008/12/26/bush-more-memex/
- https://www.theatlantic.com/magazine/archive/1945/07/as-we-may-think/303881/

## [[concepts.knowledge managment.methodologies]]

## [[tools.knowledge managment]]

## other
- [Michel Foucault's Conception of Discourse as Knowledge and Power](https://www.youtube.com/watch?v=mb02e2SYdGg) #video
- https://www.notion.so/tools-and-craft/03-ted-nelson #video [[people.TedNelson]]
- [Immersive mediums replace thought](https://notes.azlen.me/s3husuaw/) also see [[incubation.attention.economy]]

## Adjacent

- [[fulldocs.matuschak.ontologies.associative]] by [[people.AndyMatuschak]]

### [Memory](concepts.memory.md)
### [[concepts.archive]]
### [[concepts.archives.art]]
### [[concepts.Digital Asset Managment]]
### [[concepts.digital garden]]
### [[incubation.audio.sample.managment]]
### [[incubation.infinitecanvas]]
-----------

![](data_wisdom_panel.png)

-----------
![](bicameral_idea.png)

---


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------

# #tosort 


https://numinous.productions/ttft/

https://subconscious.substack.com/p/second-subconscious

https://christine.website/blog/gtd-on-paper-2021-06-13



--------------------------------------


- [nikitavoloboev/knowledge: Everything I know](https://github.com/nikitavoloboev/knowledge)
- [Memex Dreams ‒ Szymon Kaliski](https://szymonkaliski.com/writing/2020-04-19-memex-dreams)

