## Quotes
>“We are all burnt by ultraviolet rays. We all contain water in about the same ratio as Earth does, and salt water in the same ratio that the oceans do. We are poems about the hyperobject Earth.”  
― Timothy Morton, [Hyperobjects: Philosophy and Ecology after the End of the World](https://www.goodreads.com/work/quotes/24903561)

>“I think that this music could liquefy my internal organs, make my ears bleed (this has actually occurred), send me into seizures. Perhaps it could kill me. To be killed by intensed beauty, what a Keatsian way to die.”  
― Timothy Morton, [Hyperobjects: Philosophy and Ecology after the End of the World](https://www.goodreads.com/work/quotes/24903561)

> «Art is a pretty good tactic, because instead of holding up a poster and complaining outside a bank, this is much more like the anarchist tactic of occupy – you just model a different way of inhabiting social space.» [⤴️](https://omnivore.app/me/timothy-morton-ecology-without-nature-cccb-lab-1875185972b#5795568f-bd96-4a40-bca3-ecc82135e435)

## Keywords
hyperobject of global warming
ecology without nature
dark ecology

## Hyperlinks
- https://ecologywithoutnature.blogspot.com/
- https://www.changingweathers.net/en/episodes/48/what-is-dark-ecology/
	- https://www.changingweathers.net/en/episodes/43/dark-ecological-chocolate/
- https://lab.cccb.org/en/tim-morton-ecology-without-nature/
- the other crap?? - https://blogs.ubc.ca/crap/

## Also see
- [[people.SlavojZizek]] Ecology as new opium - https://www.youtube.com/watch?v=xGznfVmb5Xg&list=PLqNswU0qSP6An_1U4XHt92uO9k1VUz03L
- [[people.WernerHerzog]] - Harmony and universe - https://www.youtube.com/watch?v=pF5xBtaL3YI