# [[las]].reconcept

- [ ] summary of reconcept aka V0.1
- [ ] cleunps
- [ ] [[las.future]]
- [ ] funding

-----------
## 100 == Map Of Content
### Tasks 
- [[las.reconcept.tasks]]

### Principles + sauce
- [[las.reconcept.canvas]]
- [[las.quotes]]

### Theme - anchors
- [[las.reconcept.frontpage]]
- [[las.reconcept.intro]]




## 101
- reconcept ==> Proof of Concept
	- without social network context
	- videos / motion graphics as
		- still frames
		- gifs
- technically
	- [ infinite canvas](https://www.infinite-canvas.org/)

### schema
- https://excalidraw.com/#room=570c7bcfdd8d1b1a1467,RvDFVKOHWt3DpJxu4RPWOA

![las-reconcept-schema.png](las-reconcept-schema.png)

### intro anchor
![[intro_V1b.png]]

### stone anchor
![[EXP_1_stone-japanprometheus.png]]

## Topics
- 0/ ABOUT == FRONTPAGE
- 1/ INTRO
	- intro #intro
	- prophet / visionary
		- ==> #dimension ==> **one-dimensional man is disappearing** ==> other [3rd] axis
- 2/ image x line = #image #reading 
- 3/ Time - #temporality

### redux version
- line 
- surface
- visionaries
- prophets



-----------

## principles
### infos

anchor pages

### hypertext 
- text i jako anotace svg / video
- jak resit anotace, zdroje hypertext
	- ---> PARALLEL PAGES VISIBLY CONNECTED [[concepts.parallel textface]]

### visuals
- infinites zoom 
	- text
	- 3d objects rerendered  as svgs
- motion image aka video
	- asociativni videa 
	- text rerenders az abstrakce 
	- asociativni stills
- gifs ?? 

### sound 
- as spaces within canvas 


## videomaterial needs
### videos for stills
- 3 * 1-3 mins
	-  #associations #vertov #chrismarker #infostorm
	-  videos will presented only as slides / series of still through zoomable interface 
- topics ? #incubation 
	- __
	- serpents
	- dna
	- machine motion
	- libraries
	- sand castles
	- color explosion

### motion graphics
- txt ---> in motion
- 3 * 1min + permutace?



-------------------------
-------------------------
-------------------------

![[panels_using_trails.png]]

-------------------------
-------------------------
-------------------------

## #chaosstream 
- 3d objekt jako root
	- bud skutecne 3d
	- nebo animace == serie stils
- texty [[las.quotes]] jako vodic
	- ---> animace textu
	- -----> texty jako docs / hypertext
- texty jako markmap / map
- zvuky 
	- jako soundscape 
	- k videu jako synchra motivy
- video jen jako outside linky
- about sekce

-- media
-- operating alien mythologies


