# concepts.trailblazers
![[trailblazers.bambam.png]]
![[image_trail_zoom.png]]
![[panels_using_trails.png]]
![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Modra_turisticka_trasa_zna%C4%8Dka-%C5%A1ipka_doleva.jpg/800px-Modra_turisticka_trasa_zna%C4%8Dka-%C5%A1ipka_doleva.jpg)

------------------------

- [[people.VannevarBush]]
- [[people.AbyWarburg]]
- [[people.TedNelson]]
- [[people.VilemFlusser]]
- [[people.JaronLanier]]


-------------------------

- [[concepts.knowledge managment]]
- [[concepts.digital garden]]
- [[concepts.archive]]
- [[concepts.archives.art]]]
- [[people.AndyMatuschak]]
- [[concepts.situationism]]

----
[[people.WalterBenjamin]]
“To live means to **leave traces**. In the interior, these are accentuated.” I
- https://www.academia.edu/41294842/_Erase_the_traces_urban_experience_in_Walter_Benjamins_commentary_on_Brechts_lyric_poetry
- https://crosspollenblog.wordpress.com/2014/06/15/thinking-outside-the-box-walter-benjamins-critique-of-dwelling/
The interior is not just the universe but also the etui of the private in-  
dividuaL To dwell means to leave traces. In the interior, these are accen-  
tuated. Coverlets and antimacassars, cases and containers are devised in  
abundance; in these, the traces of the most ordinary objects of use are im-  
printed. In just the same way, the traces of the inhabitant are imprinted in  
the interior. Enter the detective story, which pursues these traces. Poe, in  
his "Philosophy of Furniture" as well as in his detective fiction, shows  
himself to be the first physiognomist of the domestic interior. The crimi-  
nals in early detective novels are neither gentlemen nor apaches, but pri-  
vate citizens of the middle class.