# How tos

## monitoring
### graphic cards

- https://www.maketecheasier.com/monitor-nvidia-gpu-linux/
	- [nvtop](https://github.com/Syllo/nvtop)
- nvidia-smi
	- official
	- https://developer.download.nvidia.com/compute/DCGM/docs/nvidia-smi-367.38.pdf

### powerstats
[powertop](https://manpages.ubuntu.com/manpages/xenial/man8/powertop.8.html)

- https://manpages.ubuntu.com/manpages/xenial/man8/powerstat.8.html
	- https://www.hecticgeek.com/powerstat-power-calculator-ubuntu-linux/
- https://github.com/rouvoy/powerapi
- https://software.intel.com/content/www/us/en/develop/articles/intel-power-gadget.html

## various 
### system to ssd?
https://askubuntu.com/questions/40372/how-to-move-ubuntu-to-an-ssd

### various rams
https://www.quora.com/Can-I-use-8GB-RAM-together-with-4GB-RAM

### PCIe expansion cables
http://compuny.cz

## Overclocking
- https://askubuntu.com/questions/938770/how-do-i-overclock-my-gtx-1080-on-ubuntu-16-04
- https://gist.github.com/bsodmike/369f8a202c5a5c97cfbd481264d549e9 !!!
- https://askubuntu.com/questions/859218/nvidia-1070-overclocking-help-needed
- https://medium.com/@TaneSavoya/step-to-step-guide-how-to-mine-with-nvidia-gpus-in-windows-10-more-quiet-cooler-and-energy-bf3b63f40919

## Window managment
https://www.faqforge.com/linux/easily-split-screen-space-fit-two-windows-ubuntu-16-04lts/

## bash
https://stackoverflow.com/questions/3215742/how-to-log-output-in-bash-and-see-it-in-the-terminal-at-the-same-time
https://unix.stackexchange.com/questions/458227/how-to-run-bash-script-on-startup-on-linux