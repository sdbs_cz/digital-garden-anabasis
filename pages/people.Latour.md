### from:  NEGOTIATING THE VALUE(S) OF DESIGN(ING)

#surface #las

>As Latour (2008) points out:
From a surface feature in the hands of a not-so-serious-profession
that added features in the purview of much-more-serious-
professionals (engineers, scientists, accountants), design has been
spreading continuously so that it increasingly matters to the very
substance of production. (p. 2)



> Latour (1993a) shows that Pasteur—who is essentially
remembered as a great scientist—was a combination of heterogeneous
elements, such as notebooks, statistics, bacteria, sheep, laboratories;
Pasteur-the-great-scientist, the argument goes, did not exist outside of this
network—he was a network.

>Commenting on the variety of misinterprations in relation to ‘actor-network theory’,
Latour (1999a) concedes that a more accurate descriptor for ANT could have been
‘actant-rhizome ontology.’ Indeed, the ontology of ANT is strongly influenced by the
work of Gilles Deleuze, whose version of distributed materialism is foundational to the
notion of ‘network’ as an assemblage of heterogeneous components. For a detailed
treatment of Deleuze’s assemblage theory see DeLanda (2006; 2016).