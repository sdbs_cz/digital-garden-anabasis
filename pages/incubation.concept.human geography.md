>Human geography or anthropogeography is the branch of geography that is associated and deals with humans and their relationships with communities, cultures, economies, and interactions with the environment by studying their relations with and across locations.[Wikipedia](https://en.wikipedia.org/wiki/Human_geography "Wikipedia")


## #unsorted 
- http://geography.ruhosting.nl/geography/index.php?title=Space_vs._place
- https://ses.library.usyd.edu.au/bitstream/handle/2123/9743/communitymapping.pdf;jsessionid=335D3B9D21F39F6E55CEC4F4E2B0AF9F?sequence=1
- https://edit.place/@philippschmitt/introduction

>As Tuan (1977, p. 3) says: _"space is freedom, place is security"._

## Adjacent
- [[concepts.map]]
- [[concepts.topography]]
- [[concepts.situationism]]


-------------
SUN RA / Space Is The Place https://www.youtube.com/watch?v=dokLwszdUgY