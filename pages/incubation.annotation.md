# Annotation


>An annotation is extra information associated with a particular point in a document or other piece of information. It can be a note that includes a comment or explanation. Annotations are sometimes presented in the margin of book pages. For annotations of different digital media, see web annotation and text annotation.
>>Wikipedia


## #incubation #techmech

- https://teachingcenter.wustl.edu/2018/06/online-annotation-tools/
- https://iteachu.uaf.edu/annotate-collaboratively/
- https://www.goodannotations.com/tools/draw-on-pdf

- bit off
	- https://github.com/writefreely/text-pic




### Tools
- https://screenotate.com/
- https://docdrop.org/
	- !!!!


## Notes by [[people.AndyMatuschak]]
- [Cognitive scaffolding](https://notes.andymatuschak.org/z8aiVRywvJYDB9gvpCDxa4KUBcKr8R4geNAiJ) 

- [Dynamic mediums usually lack an authored time dimension](https://notes.andymatuschak.org/z8ZWYXFwXV38qiCgRx7zf2ySy9WCxWvcizNVr)

------------
------------
------------

>![[discuss_sum.png]]
> - https://arxiv.org/pdf/2009.07446.pdf



