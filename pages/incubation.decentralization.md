#incubation #crippled 

https://decentraland.org/
https://fediverse.party/

---
https://post.lurk.org/@lidia_p/105119506416515486 lidia_p@post.lurk.org - Tomorrow, my desktop essay 'Feditation', commissioned by The International Center for Knowledge in the Arts, will be screened as part of the programme of their event Coherency, Diversity and Networks in Artistic Research and its session on "Alternatives & Interdependencies in Constructing Future Networks". You can find more info and a link to the webinar here:

https://artisticresearch.dk/en/activities/link-for-symposium-coherence-diversity-and-networks-in-artistic-research​

#fediverse #federation​


- [[tools.mastodon]]
- [[tools.fediverse]]
- [[areas.self-hosting]]