#media #annotation #inform #avg

## [[inform]]
## [[incubation.annotation.media]] /// [[concepts.Digital Asset Managment]]
## [[tools]]
## #techmech  #fix #tosort 

# Media [[AREAS/DigitalGardenAnabasis/annotation]], referencing

*   Anvil
    
    *   It offers multi-layered annotation based on a user-defined coding scheme. During coding the user can see color-coded elements on multiple tracks in time-alignment. Some special features are cross-level links, non-temporal objects, timepoint tracks, coding agreement analysis, 3D viewing of motion capture data and a project tool for managing whole corpora of annotation files. Originally developed for gesture research in 2000, ANVIL is now being used in many research areas including human-computer interaction, linguistics, ethology, anthropology, psychotherapy, embodied agents, computer animation and oceanography.
        
    *   [https://www.anvil-software.org/](https://www.anvil-software.org/ "https://www.anvil-software.org/")
        
*   ANT
    
    *   VideoAnt is a web-based video annotation tool for mobile and desktop devices. Use VideoAnt to add annotations, or comments, to web-hosted videos. VideoAnt-annotated videos are called “Ants”. VideoAnt is a web-based video annotation tool for mobile and desktop devices. Use VideoAnt to add annotations, or comments, to web-hosted videos. VideoAnt-annotated videos are called “Ants”. Export your annotations in a variety of data formats. You can even embed your Ants on a personal website, learning management system, or anywhere HTML is allowed.
        
    *   [https://ant.umn.edu/](https://ant.umn.edu/ "https://ant.umn.edu/")
        
*   PureRef
    
    *   PureRef is a stand-alone program for Windows, Mac and Linux that keeps track of your images. Whether you're gathering inspiration, making mood boards or need reference images for your painting or 3D model, PureRef is there so you can focus on creating.
        
    *   [https://www.pureref.com/index.php](https://www.pureref.com/index.php "https://www.pureref.com/index.php")
        
*   Quixel bridge
    
    *   With its custom import, channel packing, and automated export features, Bridge allows you to manage your assets in one place like never before. The days of manual shader setup, image conversion or sensitive folder structures are over; welcome to the future of asset management.
        
    *   [https://quixel.com/bridge](https://quixel.com/bridge "https://quixel.com/bridge")
        
*   Lignes de Temps
    
    *   The Lignes de Temps software takes advantage of the analysis and synthesis possibilities offered by digital media. Inspired by the « timelines » commonly used on digital editing benches, Lignes de Temps offers a graphic representation of a film, revealing from the outset, and in extenso, its cuttings. Lignes de Temps offers in this a new access to the film, substituting for the logic of constrained scrolling that constitutes the experience of any movie viewer, and for the purposes of analysis, the « mapping » of a temporal object. Also, by selecting a segment of a timeline, the user has direct access to the corresponding clip or sequence in the movie, a sequence that can be described and analyzed by textual, audio, video, or documented by images or internet links.
        
    *   [https://www.iri.centrepompidou.fr/outils/lignes-de-temps-2/](https://www.iri.centrepompidou.fr/outils/lignes-de-temps-2/ "https://www.iri.centrepompidou.fr/outils/lignes-de-temps-2/")
        

#### Online #unsorted

*   [https://wevu.video/](https://wevu.video/ "https://wevu.video/")
    
*   [https://www.reclipped.com/](https://www.reclipped.com/ "https://www.reclipped.com/")
    
*   [https://www.hippovideo.io/](https://www.hippovideo.io/ "https://www.hippovideo.io/")
    
*   [https://heraw.com/en/register](https://heraw.com/en/register "https://heraw.com/en/register")



-----------