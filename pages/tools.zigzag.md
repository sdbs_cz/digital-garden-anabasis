# ZigZag 
## by [[people.TedNelson]]

## Text
- https://web.archive.org/web/20120722092427/http://journals.tdl.org/jodi/article/view/131/129
- https://patents.google.com/patent/US6262736
- http://www.dgp.utoronto.ca/~mjmcguff/research/zigzag-and-mSpace/zigzag-and-mSpace.pdf

## Videos
- https://www.youtube.com/watch?v=WEj9vqVvHPc
- https://www.youtube.com/watch?v=si1EJ584foA

## Adjacent
- [[fulldocs.inferior.outliner]]