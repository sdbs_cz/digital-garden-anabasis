# people.VilemFlusser

> ![[Caminos_Ferrari_1982.png]] 
>>  “Caminos” by Léon Ferrari_1982.png
>> https://aphelis.net/lines-tim-ingold-vilem-flusser/
## Towards a Philosophy of Photography
>“The task of a philosophy of photography is to reflect upon this possibility of freedom - and thus its significance - in a world dominated by apparatuses; to reflect upon the way in which, despite everything, it is possible for human beings to give significance to their lives in the face of the chance necessity of death. Such a philosophy is necessary because it is the only form of revolution left open to us.”  

> “Our thoughts, feelings, desires and actions are being robotized; 'life' is coming to mean feeding apparatuses and being fed by them. In short: Everything is becoming absurd. So where is there room for human freedom?”

> “Communication is an artificial, intentional, dialogic, collective act of freedom, aiming at creating codes that help us forget our inevitable death and the fundamental senselessness of our absorb existence.”

> “Traditional images signify phenomena whereas technical (produced by an apparatus) images signify concepts.”

## [The City as Wave-Trough in the Image Flood](https://pile.sdbs.cz/item/105)

> Kabely is a bit of word play here because it can mean both “wire(s)” and “cable” in the sense of “cable TV.” I have chosen “cable” here to give the sense of multiple lines of transmission that bring the public sphere into the private. Public space pushes into the private thanks to cable (as in the case of television). Private space pushes into public thanks to machines (like cars).

> Everything is divisible, and there can be no individual. Not only can atoms be split into particles but so can all mental objects; actions become “aktomes,” decisions become “dezidemes,” perceptions become stimulations, representations become pixels.

> The cathedral are seen as surface phenomena, as congealed, materialized masks, as a kind of archaeological kitchen trash. 


## Essays +
### [Line and Surface](https://pile.sdbs.cz/item/101)
#### ref
- https://kangyy1.wordpress.com/2014/01/02/vilem-flusser-writings-line-and-surface/
- https://thathasbeen.wordpress.com/2013/01/01/line-and-surface/
#### quotes
[[[las.quotes]]

### [Codified World](https://pile.sdbs.cz/item/100)
#### ref
- https://thathasbeen.wordpress.com/2013/01/06/the-codified-world/

#### quotes
[[[las.quotes]]

## [[incubation.codes]]

## Related
- [[fulldocs.twitter.codex.relations]]
- [[las]]
	- [[las.quotes]]
	
	
--------------------
--------------------
--------------------
--------------------
--------------------

# Unsorted

## online
- http://eaas.uni-freiburg.de/demo-flusser.html !!!
- https://aaaaarg.fail/ref/b394ff0944244fa201d8fe8621f1a0a2#0.01
- https://www.gamu.cz/en/playing-against-the-apparatus-vilem-flussers-media-and-culture-philosophy/
- https://www.flusserstudies.net/flusser
	- https://www.flusserstudies.net/flusseriana
	- https://www.udk-berlin.de/forschung/permanente-forschungseinrichtungen/vilem-flusser-archiv/

## video
- https://www.imdb.com/title/tt0360961/
- http://www.flusserdvd.c3.hu/index_en.html

----

https://adelheidmers.org/aweb/home.htm
https://aphelis.net/lines-tim-ingold-vilem-flusser/
