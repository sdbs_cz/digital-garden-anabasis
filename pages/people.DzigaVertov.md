# Dziga Vertov

>[Everybody who cares for his art seeks the essence of his own technique.](https://www.azquotes.com/quote/930809)
> - [Dziga Vertov](https://www.azquotes.com/author/45194-Dziga_Vertov)

#film
Kino-Eye uses every possible means in montage, comparing and linking all points of the universe in any temporal order, breaking, when necessary, all the laws and conventions of film construction.

----

#film #temporality 
Freed from the boundaries of time and space, I co-ordinate any and all points of the universe, wherever I want them to be. My way leads towards the creation of a fresh perception of the world. Thus I explain in a new way the world unknown to you.