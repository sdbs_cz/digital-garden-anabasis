# Aby Warburg

>When Aby Warburg was twenty years old, he traded his birthright as the firstborn son in exchange for his brother’s promise to buy him books for the compiling of a library.
>>https://contempaesthetics.org/newvolume/pages/article.php?articleID=812#FN3

## [[concepts.archives.art]]

## Links - eng
- https://warburg.library.cornell.edu/
- https://www.academia.edu/6760946/Aby_Warburg_s_Serpent_Ritual_Towards_a_new_understanding_of_the_origin_of_Religion
- https://warburg.sas.ac.uk/archive/archive-collections/verkn%C3%BCpfungszwang-exhibition/mnemosyne-materials

## Links - cs
- https://artalk.cz/2020/11/25/clovek-a-patos-v-case-prostoru-a-obraze/

## whoa
- https://contempaesthetics.org/newvolume/pages/journal.php?search=true
