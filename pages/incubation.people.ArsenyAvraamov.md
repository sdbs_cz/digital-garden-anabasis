> Shortly after the October Revolution he proposed to the Commissar of Public Enlightenment, Anatoly Lunacharsky, a project to burn all pianos — symbols of the despised twelve-tone, octave-based ‘well-tempered’ scale, which he believed had adversely affected human hearing for several hundred years. 


> "The timbre is the soul of a musical sound. To build abstract harmonic schemes and then ‘orchestrate’ them is not creative any more; in this way it is possible to reach a full decomposition of the process of musical creation down to the sequence of compositional exercises: to invent a sequence of tones, to incorporate any rhythm, to harmonize the melody obtained and, finally, to start its colouring, using an historically readymade palette... In the act of true creativity each sound should be born already incarnated... 


> The factory sirens were the most popular sound sources in the early 1920-s, considered as a substitution of the former ‘bourgeois’ church bells and popular in construction of the new sound machines.
> 
> Heconducted the symphony himself from a specially built tower, using signallingflags directed simultaneously toward the oil flotilla, the trains at the station,the shipyards, the transport vehicles and the workers' choirs . Avraamov didnot want spectators, but intended the active participation of everybody in thedevelopment of the work through their exclamations and singing, all united withthe same revolutionary will. Avraamov reflected on the potential of music, andthe influence of the sounds that define our environment - their importance andthe role they had to fulfil after the October Revolution - an aspect of his thinkingwhich helps us to understand the ultimate meaning of the composition of the Symphony of Sirens

https://monoskop.org/Arseny_Avraamov

---

[[projects.lalar]]
[[people.LaMonteYoung]]