# Jace Clayton
aka DJ Rupture

- https://www.theguardian.com/music/2004/jun/29/popandrock1


## Uproot

### Quotes

> In a so-called attention economy, what better way to prove that you've paid attention to something than to remix it? Meet the folk music of the twenty-first century.

> As is, the bulk of humanity's audiovisual cultural records lies in the grip of places such as YouTube. Before any platform can stream, it must first warehouse all relevant data. To put it delicately, streaming services are our default music libraries, and they are crap. Strangely, public conversation around streaming boils down to "Hey, can companies make money off this? Look how little they give the artists! Economic questions elbow their way in front of a cultural debate. Every time I see an article like that, I want to yell out, "Were inside the infinite library! Let's start showing a sense of wonder and respect!"

> Recorded sound vibrates between history and pleasure. Live Sound exists only in the present. It cannot linger. This is one of the reasons why sound defines public space even more than architecture. Kids jamming that week's hit, neighbors fucking bchind a thin wall, the call to prayers divine layer competing with traffic's blare, the loud low boom of something blowing up-and its opposite, hilltop garden quiet. #las  

> Architecture real or imagined-prompts memory so etfficiently because our brains are hardwired to recall things-in-places and unusual or -disturbing scenes. Memory-quiz champion envision fantastic images that correspond to what they want to remember and "place" each icon in some specific nook of their mental building. These mnemonic aids are called memory palaces. Their effectiveness speaks to how when a place is destroyed or erased, all the personal histories linked to it slip further into the past. #las 

> Let whoever cares distinguish between the durable and the disposable. Right-click what you love, save as. 

> Once millions of songs are available at a click and your listening patterns hop around with a whimsy borm of erratic search terms and opaque algorithms, timelines explode. Youth of the tribal generation access a world of music freed trom chronological restraints. 

> The power of collage goes beyond proving that the dominant image or narrative can be redirectedit : it also wreaks havoc on perspective. Muliple points of view piled up in Höch's photo montages,...

> Songs circulate online among fans for whom an MP3 player set to Shuffle trumps conventional genre as an organizing principle. Refined tastes intertwine with semi-random surf trails to become indistinguishable from each other. Timelines fray, genealogies wander. These under-the-radar exchanges generally happen outside commercial spheres, adding to the fertile mess. You must sift through a lot of junky MP3s to uncover the great ones, but in the end, all the world's sonic secrets are out there, clumped irregularly across the Internet's flat and mighty sprawl. A catchy genre name or evocative creation myth can make the output of a few friends appear as a bustling scene to outside eyes, and the online hype can turn into a selffulfilling prophecy if global excitement trickles down into actual gigs.