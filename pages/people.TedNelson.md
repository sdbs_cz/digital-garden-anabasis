
# Ted Nelson

## 101
### Videos
- [Ted's 70th Birthday Lecture: "Intertwingularity: When Ideas Collide"](https://www.youtube.com/watch?v=tyD2l3rGcIU)
- [Ted Nelson in Herzog's "Lo and Behold"](https://www.youtube.com/watch?v=Bqx6li5dbEY)
- [MY YEAR AT JOHN LILLY'S DOLPHIN LAB](https://youtu.be/ONhnEmoSRfk)
	- [[incubation.miamilab]], [[people.JohnCLilly]]

### [[concepts.parallel textface]]
### [[concepts.backlinks]]
### [[tools.EDL]]

## Visual Snippets


![[nelson-xanadu.png]]
![[nelson-hypercomics.png]]
![[nelson-ttomuchtosay.png]]
![[nelson_intertwingled.png]]
![[nelson-literarymachines-reading.png]]
![[nelson-literarymachines-reorganization.png]]








## literary machines
>![[global system nelson.jpg]]

>![[reorganization nelson.jpg]]

>![[howtoread_literaymachines_nelson.jpg]]
>how to read it

>![[Pasted image 20210102223835.png]]

>![[Pasted image 20210103161225.png]]
> Bingo | OOPS

----
http://fed.wiki.org/xanadu-basics