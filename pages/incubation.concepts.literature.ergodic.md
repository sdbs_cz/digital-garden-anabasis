# Ergodic Literature

> Ergodic literature is literature that relies on or requires the active engagement of a readership. Espen Aarseth writes _Cybertext_ that the term "ergodic" comes "from the Greek words ergonand hodos, meaning, ‘work’ and ‘path’" and that:
>
 >> In ergodic literature, nontrivial effort is required to allow the reader to traverse the text. If ergodic literature is to make sense as a concept, there must also be nonergodic literature, where the effort to traverse the text is trivial, with no extranoematic responsibilities placed on the reader except (for example) eye movement and the periodic or arbitrary turning of pages.
>  
> - https://directory.eliterature.org/glossary/5018


## Adjacent
- [[areas.text]]
- [[concepts.hypertext]]