# RENOISE:Generation Z 
>#### We are very sorry: since 24 February 2022 the letter Z became a simbol of Russian fascism and national disgrace...
>- http://asmir.info/generation_z_e.htm

Generation Z: Experiments in Sound and Electronic Music in Early 20th Century Russia is exhibition and book. This is place for interesting quotes and related context.

## Sources
- https://www.ctm-festival.de/magazine/generation-z-renoise
- https://archive.org/details/smirnov-andrey-sound-in-z-experiments-in-sound-and-electronic-music-in-early-20th-century-russia/page/n13/mode/2up
- https://monoskop.org/Andrey_Smirnov
- http://asmir.info/generation_z_e.htm
    - http://asmir.info/articles/GenerationZ_ReNoise_CTM14.pdf  **THIS IS GOOD**
- https://techpeterburg.wixsite.com/mysite/post/sound-in-z-forgotten-experiments-in-sound-and-electronic-music-in-early-20th-century-russia
- https://www.randform.org/blog/?p=5531
------------------------
## Quotes and screenshots
### Generation:Z

>  «Russian philosopher Lev Gumilev has described the people who drive mankind into the future as ‘passionaries’. Many of the figures included in this publication might be described as constituting ‘Generation Z’—ingenious, creative people trying to move forward during a period of great transformation» [
 
 #las  #visionaries  

>«Russian Futurist Velimir Hlebnikov asserted: ‘Radio has solved a problem that the Temple as such has not solved... the problem of joining the uniform soul of mankind to a uniform daily wave... This problem is solved by means of lightning.’» 

#radio [[concepts.playlisting]]

> «Projectionism (from the latin projectus, thrown forward) was intended to reflect the urge to rush ahead, or more accurately, to rush into the future. Solomon Nikritin applied this term not only to new approaches in painting and methods of art criticism, but also to the methodology of constructing a new society, to which it was considered necessary to aspire. In 1919 Nikritin developed his fundamental theory of Projectionism. According to his philosophy, the rational essence of nature is the highest goal of technology and culture» 
> «Nikritin developed, for example, biomechanical temperaments and scales for body movements. As a basis for the metrics he introduced the notion of the octave. In this case it defines the maximum area mechanically reachable by a dancer with their feet in a fixed position»

[[concepts.accelerationism]]

>![[Pasted image 20230317114006.png]]  
Pic. 4 - Solomon Nikritin. Draft manuscript. 1922. The manuscript illustrates an attempt to develop  a typology and classification system for human movements according to the principles and terms of biomechanics and acoustics. RGALI. 

> «In 1924 he went so far as to attempt to chart the process of the evolution of consciousness and the creative energy of society from simple, primitive states to the perfection of a future classless society. Based on a creative human network, this projected society would function without any central authority» 

> «One of the paradoxes within the early history of Russian sound art is that only a few professional, academically educated composers were involved in it. To discover its roots researchers have to explore the activities of avantgarde artists, actors, painters, poets, scholars and inventors» 

> «Many cross-disciplinary artists from the 1910s were self-taught amateurs, combining knowledge in music, acoustics, literature, arts, psychology, physics, mathematics and other disciplines. The same was true worldwide. As the Hungarian painter and photographer, pioneering avantgarde polymath László Moholy-Nagy stated in an article of 1928-30: ‘Contemporary “musicians” have so far not even attempted to develop the potential resources of the gramophone record, not to mention the wireless or ether-waves.’» 

> «Anticipating the upcoming musical trends he wrote: ‘The music of nature is free in its choice of notes—light, thunder, the whistling of wind, the rippling of water, the singing of birds. The nightingale sings not only the notes of contemporary music, but the notes of all the music it likes. Free music follows the same laws of nature as does music and the whole art of nature. Like the nightingale, the artist of free music is not restricted by tones and halftones.’» 

> «Kulbin developed his art theory on the basis of his physiological and neurological studies. For him, the physical action of the universal movement of colour or sound served as outer stimuli that caused psychical effects in the spectator’s brain. Kulbin declared harmony and dissonance to be basic principles of art» 

> «The same year during the summer vacation he began his first experiments with sound, which he called the Laboratory of Hearing: .’On vacation, near Lake Ilmen. There was a lumber-mill which belonged to a landowner called Slavjaninov. At this lumber-mill I arranged a rendezvous with my girlfriend… I had to wait hours for her. These hours were devoted to listening to the lumber-mill. I tried to describe the audio impression of the lumber-mill in the way a blind person would perceive it.’»

> «As Sholpo noted: We were sure that by knowing this data we could get an analytical insight into the secrets of creativity (at least in performance) and, armed with mathematical formulae, break mystical and idealistic tendencies with an explanation of the phenomena of music creation» 

#### Cyborganization
#cyber #cyborg [[las.quotes.cyber]] [[incubation.miamilab]]

>
>![](https://hedgedoc.sdbs.cz/uploads/6609749a-0991-4145-a70b-3cb044d95eca.png)
> Sound in Z - chapter: machine worshippers p.131



>THE CENTRAL INSTITUTE OF LABOUR ( CIT )
>
The Central Institute of Labour (Tsentralny Institut Truda – CIT) was founded
by Alexey Gastev in Moscow in 1920 and supported by Lenin.
>
In his institute, Gastev investigated the functions of certain operational com-
plexes that encompass both worker and machine in a single, unbroken chain:
»These machine-human complexes also produce the synthesis between bi-
ology and engineering that we are constantly cultivating. And the integrated,
calculated incorporation of determinate human masses into a system of
mechanisms will be nothing other than social engineering.«
>
According to CIT methodology, every physical motion of cadets was precisely
planned and assessed so that by the end of training, full automatism could
be achieved. The human body was to become a machine. Gastev declared:
»We start from the most primitive, the most elementary movements and pro-
duce the mechanization of man himself […] The perfect mastery of a given
movement implies the maximum degree of automaticity. If this maximum
increases […] nervous energy would be freed for new initiating stimuli, and
the power of an individual would grow indefinitely.«
>
CIT was an unusual institution that was frequented by fanatical old inven-
tors and fascinated teenagers alike. Alongside the physiological laboratory,
there were labs for sensorics, psychotechnics and education. A variety of
multimedia tools and interactive gadgets were devised, including instruments
for photography and film, systems for monitoring musical performances and
instructorless simulation apparatus for cars and planes. It was scientific
research with an interdisciplinary and broad-ranging agenda.
>
In the mid-1920s, one of the CIT departments was Solomon Nikritin’s Pro-
jection Theatre, a testing ground for the development of the ideal »Man of
the Future.« In 1928, Gastev organized the Ustanovka (Setup) joint-stock
company, which audited the work of industrial enterprises and provided
recommendations on the efficient organization of their work processes on
a commercial basis, which led to complete financial independence of CIT
from the state. Although by the late 1930s, CIT had produced over 500,000
qualified workers in 200 professions and 20,000 industrial trainers in 1,700
educational centres, the totalitarian State was not interested in the creation
of a network of socially engineered Cyborgs with liberated minds. In 1938,
the institute was finally closed.

>ORDER 05
>
Funeral rites at the cemetery of planets.
A howl in the catacomb of worlds.
Millions, into the manhole of the future.
Billions, weapons stronger.
Labour camp of the mind.
Chains of the heart.
Engineer Everyman.
Drive geometry into their necks.
Logarithms into their gestures.
Defile their romanticism.
Tons of indignation.
Normalize the word from pole to pole.
Phrases on the decimal system.
A boiler company for speech.
Annihilate verbality.
Make the tunnels resound.
Turn the sky red for arousal.
Gears—at superspeed.
Brain machines—high load.
Cinema eyes—fix.
Electric nerves—to work.
Arterial pumps, activate.
>
> - Alexei Gastev 192122

> The totalitarian State of the 1930s was opposed to the creation of an anarchical network of socially engineered Cyborgs with liberated minds. In 1938 Alexei Gastev was arrested on false charges of ‘counter-revolutionary terrorist activity’ and sentenced to death by a speedy trial; his institute was closed. On 15 April 1939 Gastev was shot to death in the suburbs of Moscow.

- additional sauce
	- + https://publikationen.bibliothek.kit.edu/1000141304/136596123
	- also biomechanics @ monoskop


--------------------------------------
--------------------------------------
--------------------------------------


### Music, class and party in 1920s Russia • International Socialism

- http://isj.org.uk/music-class-and-party

> On May Day 1918 in Moscow, both the “Marseillaise” and the “Internationale” were played and sung.1 But there was also a need to remember the dead of the ­revolution. The revolutionary funeral dirge “You Fell Victim”, sung by mass choirs, was a staple at these events, having already replaced “God Save the Tsar” as the tune played by the bells of the Kremlin. Meanwhile set-piece performances such as the one in Petrograd on May Day 1918 often included Mozart’s Requiem.

> The music for these performances was chosen for its stock connotations but set a precedent for attitudes towards different kinds of music in subsequent years. It was common for the privileged and exploiting classes to be accompanied by commercial “gypsy music”, the Can-Can or Viennese waltzes. On one occasion the “Marseillaise” was gradually drowned out by the “Internationale” to symbolise the displacement of the provisional government by the soviets. 

> Proletkult’s musical goal was to involve workers in musical activity, setting up studios across the country where workers could practise a range of art forms. The movement comprised a spectrum of different perspectives. At one end were those who primarily wanted to allow Russian workers the access to art and culture they had been denied under Tsarism. A Proletkult document of 1918 explains that in the Moscow music studios, of which there were 17,5 workers studied musical literacy and attended courses on music appreciation as well as learning folk and revolutionary songs and engaging in choral singing and composition.6 This work of “cultural enlightenment” overlapped with the efforts of the new government’s Commissariat of Enlightenment (Narkompros) headed by Anatoly Lunacharsky.7

> Music should be an expression of the highly collective and cooperative labour of modern society. One result was the creation of “noise orchestras” which used industrial engines, turbines, dynamos, sirens, hooters and bells to generate performances within factories, sometimes accompanied by a gymnastic “ballet” on the shopfloor. 

> Lenin’s view was partly shaped by his own conservative tastes in art, but its real basis was a materialist understanding of culture that was profoundly at odds with the thinking of the revolutionary artists. In Lenin’s view, culture is not something that can be engineered at will, but is shaped over long periods by the material conditions that underpin it. Speaking at the Tenth Party Congress in 1921, he argued:
The cultural problem cannot be settled as swiftly as political and military problems. One must understand that the conditions for movement ahead are now lacking. It is possible to conquer politically in several weeks, and in war in several months; but to be victorious culturally in such a time is impossible. A much longer period is required.18 


### How the Russian Avant-Garde came to serve the Revolution

- https://www.rbth.com/arts/2017/06/23/how-the-russian-avant-garde-came-to-serve-the-revolution_788467

>Many avant-garde artists embraced the revolution. It suddenly turned out that their artistic ideas were resonant with political slogans, such as Kazimir Malevich's proposal to burn all paintings and exhibit their ashes in museums, because - he asserted - there would be no art after Suprematism. Tragically, this idea was implemented in the first months of the 1917 revolution.

>On the first anniversary of the revolution, artist Nathan Altman wrapped the wings the Hermitage museum in Petrograd in scarlet fabric (nearly 80 years later, the artist Christo would wrap the Reichstag building in a similar way, producing an international sensation).

![[Pasted image 20230318152735.png]]
Varvara Stepanova. Design for textile, 1924. / Private collection

>![[Pasted image 20230318152818.png]]
The most renowned of these is the project to build a monument to the Third International (an association of international Communist parties) created by Vladimir Tatlin in 1919. The tilting steel frame was to contain three objects made of glass: A cube, a cylinder, and a cone. These were designed to rotate at different rates, completing one circle within one year, one month, and one day, thus counting the time passed since the beginning of the new era. Had the project been realized, it would have been taller than the Eiffel Tower.

## Adjacent and other
- [[incubation.people.ArsenyAvraamov]]
- [[incubation.areas.diagrammatic]]
- [[incubation.people.HenryFlynt]]
- [[people.DzigaVertov]]

### Outside
- https://monoskop.org/Aleksei_Gastev
- https://monoskop.org/Computing_and_cybernetics_in_CEE#former_Soviet_Union
- https://monoskop.org/Central_Institute_of_Labour *not working?*


