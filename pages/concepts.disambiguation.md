> _Disambiguate_ has been a somewhat obscure term for ‘specify’ for ages.  And the noun form, _disambiguation_, has been used even more sparingly.  At some point in the last century, perhaps in the 1950s, it became a popular term in computational linguistics.   And before that it was basically only used by one person, writing about logic and semantics in the early 19th century.  All of this sprang to my mind because of the tremendous popularity of the word in and through Wikipedia.  In the encyclopedia, it is the canonical way to describe the clarification of an ambiguous term, the indication of type used to specify the context of an article title.
> - https://blogs.harvard.edu/sj/2009/06/25/on-disambiguation-and-the-atomization-of-meaning/


### [[concepts.MOC]]
###  adjacent