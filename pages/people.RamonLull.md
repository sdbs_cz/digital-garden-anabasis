> Trees Llull structured many of his works around trees. In some, like the Book of the Gentile and the Three Wise Men, the "leaves" of the trees stand for the combinatorial elements (principles) of the Art. In other works a series of trees shows how the Art generates all ("encyclopedic") knowledge. The Tree of Science (1295-6) comprises sixteen trees ranging from earthly and moral to divine and pedagogical.[35] Each tree is divided into seven parts (roots, trunk, branches, twigs, leaves, flowers, fruits). The roots always consist of the Lullian divine principles and from there the tree grows into the differentiated aspects of its respective category of reality.[36]

[[concepts.trailblazers]]

[[concepts.rhizome]]

[[concepts.archives.art]]