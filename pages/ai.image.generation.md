# ai.image.generation
>Quasimondo - In case you didn't know: prompting is just one step away from the devil's work. But I do find the "bringing to light" angle very charming. Lucinate? Lucination? #lucifer
>
>https://mastodon.social/@Quasimondo/108448450639037019

- https://twitter.com/supercomposite/status/1567162288087470081

## text to 
### image
#### online
- https://hypnogram.xyz/
- dall-e mini / craiyon
	- https://www.craiyon.com/
	- https://huggingface.co/spaces/dalle-mini/dalle-mini
- https://huggingface.co/spaces/awacke1/Art-from-Text-and-Images
- https://huggingface.co/spaces/Sakil/image_generator !
- https://huggingface.co/spaces/OFA-Sys/OFA-Text2Image_Generation !! 
- https://huggingface.co/spaces/multimodalart/latentdiffusion
- https://huggingface.co/spaces/multimodalart/mindseye-lite
	- https://multimodal.art/mindseye !!
- https://huggingface.co/spaces/multimodalart/vqgan
- https://huggingface.co/spaces/BernardoOlisan/vqganclip

##### abstract
- https://huggingface.co/spaces/Joshagibby/Samila-Visual-Waves

- Stable Diffusion in Blender 3.3 to generate AI images
	- https://www.youtube.com/watch?v=k2rsRmJr9lI
	- https://www.reddit.com/r/blender/comments/x75rn7/i_wrote_a_plugin_that_lets_you_use_stable/

#### other / commercial

##### midjourney AI
- https://www.youtube.com/watch?v=9Z7ALQrRq_k
- remix function
	- https://ckovalev.com/midjourney-ai/weekly/how-to-use-midjourney-remix-mode

##### dall-e
- https://openai.com/blog/dall-e/
- https://openai.com/blog/dall-e-introducing-outpainting/

[[ai.image.processing]]

##### others
- blue willow - https://www.bluewillow.ai/
- leonardo.ai - https://leonardo.ai/ - game asssets

#### online abstract
- https://huggingface.co/spaces/Joshagibby/Samila-Visual-Waves

#### searchj
- https://huggingface.co/spaces/LayBraid/SpaceVector_v0

### video
- https://app.pictory.ai/textinput
- https://makeavideo.studio/

- https://www.youtube.com/watch?v=w_sxuDMt_V0&list=WL&index=6
	- hows us how to make amazing AI animations with Stable Diffusion and Cloud Deforum in just a few minutes!

#### deforum animation 
- https://colab.research.google.com/github/deforum/stable-diffusion/blob/main/Deforum_Stable_Diffusion.ipynb
- https://www.youtube.com/watch?v=bolW97Mrcc4

#### VQGAN-CLIP-GENERATOR
- `This is a package (with available notebook) for running VQGAN+CLIP locally, with a focus on ease of use, good documentation, and generating smooth style transfer videos.`
- https://github.com/rkhamilton/vqgan-clip-generator

#### stable diffusion for gimp
- https://80.lv/articles/a-new-stable-diffusion-plug-in-for-gimp-krita/

#### stable difussion frontends
- https://github.com/AUTOMATIC1111/stable-diffusion-webui/wiki/Features

#### DiscoDiffusion
![[fcnmod7zff191.webp]]
- https://www.reddit.com/r/DiscoDiffusion/comments/uwr1l8/watercolor_city/

- `A frankensteinian amalgamation of notebooks, models and techniques for the generation of AI Art and Animations.`
- https://github.com/alembics/disco-diffusion
- https://www.reddit.com/r/deepdream/comments/sa5q9p/march_15_44bc_the_roman_senate_proclaims_death_to/
- https://colab.research.google.com/drive/1sHfRn5Y0YKYKi1k-ifUSBFRNJ8_1sa39?authuser=2#scrollTo=BGBzhk3dpcGO
- https://www.reddit.com/r/MediaSynthesis/comments/t4q8iv/journey_into_the_other_disco_diffusion/
- https://colab.research.google.com/drive/1lx9AGsrh7MlyJhK9UrNTK8pYpARnx457?usp=sharing
- https://colab.research.google.com/github/alembics/disco-diffusion/blob/main/Disco_Diffusion.ipynb#scrollTo=1YwMUyt9LHG1
##### # New Technique: Adding Text While Processing
- https://www.reddit.com/r/DiscoDiffusion/comments/uaem27/new_technique_adding_text_while_processing/

## Generating motion
- https://dreamoving.github.io/dreamoving/
- https://huggingface.co/spaces/zcxu-eric/magicanimate **!**
## Generating new views (or POVs)
`NeRF(Neural Radiance Fields) is a method that achieves state-of-the-art results for synthesizing novel views of complex scenes.`

### examples
- https://www.reddit.com/r/MediaSynthesis/comments/uw5nqu/some_additional_nerf_madness/
- (http://www.matthewtancik.com/nerf) 

### Tools
- https://github.com/yenchenlin/nerf-pytorch
- https://github.com/delldu/NeRF
- https://github.com/NVlabs/instant-ngp

## 3d objects
- https://cdm.link/2022/09/train-ai-on-images-and-get3d-populates-your-world-with-3d-objects-and-characters/

## annotations
https://huggingface.co/spaces/awacke1/Video-Summary
https://huggingface.co/spaces/awacke1/VideoSummary2

------
##### lexica stable diffusion search
- https://lexica.art/

----
dreambooth - github