# projects.feedfarm.hardware.now

## [[tech.grub.issues]]

## [Asus PRIME Z270-A](https://www.asus.com/us/Motherboards/PRIME-Z270-A/)

### bios
- https://blockoperations.com/motherboard-bios-settings-for-asus-z270-a-and-z270-p/

### SLI
- SLI HB bridge (2-WAY-M)

[[projects.feedfarm.questions#SLI HB Bridge]]

### practical
#### bios
- https://blockoperations.com/motherboard-bios-settings-for-asus-z270-a-and-z270-p/

#### PCIe
Revision: 3.0
Max Lanes: 16
Configs: 1x16, 2x8, 1x8+2x4

## CPU 
### i7 7700K
- https://www.tomshardware.com/reviews/intel-kaby-lake-core-i7-7700k-i7-7700-i5-7600k-i5-7600,4870-8.html

### past [Pentium G4400 - Intel](https://en.wikichip.org/wiki/intel/pentium_(2009)/g4400)



## [[feedfarm.hardware.1080TI]]

##