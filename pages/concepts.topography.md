# Topography
#chaosstream 

># Topography - Wikipedia
> ## Excerpt
> This article is about the study of Earth's surface shape and features. For discussion of land surfaces themselves, see Terrain. For other uses, see Topography (disambiguation).
>
>**Topography** is the study of the forms and features of [land surfaces](https://en.wikipedia.org/wiki/Land_surface "Land surface"). The topography of an area could refer to the surface forms and features themselves, or a description (especially their [depiction](https://en.wikipedia.org/wiki/Depiction "Depiction") in maps).
>
>Topography is a field of [geoscience](https://en.wikipedia.org/wiki/Geoscience "Geoscience") and [planetary science](https://en.wikipedia.org/wiki/Planetary_science "Planetary science") and is concerned with local detail in general, including not only [relief](https://en.wikipedia.org/wiki/Terrain#Relief "Terrain"), but also [natural](https://en.wikipedia.org/wiki/Natural_environment "Natural environment") and [artificial](https://en.wikipedia.org/wiki/Built_environment "Built environment") features, and even [local history](https://en.wikipedia.org/wiki/Local_history "Local history") and [culture](https://en.wikipedia.org/wiki/Culture "Culture"). This meaning is less common in the [United States](https://en.wikipedia.org/wiki/United_States "United States"), where topographic maps with [elevation](https://en.wikipedia.org/wiki/Elevation "Elevation") contours have made _topography_ synonymous with _relief_.
>
>Topography in a narrow sense involves the recording of relief or [terrain](https://en.wikipedia.org/wiki/Terrain "Terrain"), the three-dimensional quality of the surface, and the identification of specific [landforms](https://en.wikipedia.org/wiki/Landform); this is also known as [geomorphometry](https://en.wikipedia.org/wiki/Geomorphometry "Geomorphometry"). In modern usage, this involves generation of elevation data in digital form ([DEM](https://en.wikipedia.org/wiki/Digital_Elevation_Model "Digital Elevation Model")). It is often considered to include the graphic representation of the landform on a [map](https://en.wikipedia.org/wiki/Map "Map") by a variety of [cartographic relief depiction](https://en.wikipedia.org/wiki/Cartographic_relief_depiction "Cartographic relief depiction") techniques, including [contour lines](https://en.wikipedia.org/wiki/Contour_line "Contour line"), [hypsometric tints](https://en.wikipedia.org/wiki/Hypsometric_tints "Hypsometric tints"), and [relief shading](https://en.wikipedia.org/wiki/Shaded_relief "Shaded relief").[\[1\]](https://en.wikipedia.org/wiki/Topography#cite_note-1)[\[2\]](https://en.wikipedia.org/wiki/Topography#cite_note-2)[\[3\]](https://en.wikipedia.org/wiki/Topography#cite_note-3)

---------------


![https://www2.jpl.nasa.gov/srtm/images/bin/interf_fig4.gif](https://www2.jpl.nasa.gov/srtm/images/bin/interf_fig4.gif)


![https://earth.esa.int/image/image_gallery?uuid=e1d04769-870e-4ca9-bc93-1f98491d8efe&groupId=163813&t=1340037552729](https://earth.esa.int/image/image_gallery?uuid=e1d04769-870e-4ca9-bc93-1f98491d8efe&groupId=163813&t=1340037552729)

>![[Pasted image 20210512234350.png]]
>> Guattari - The Three Ecologies