Reaper
======

[[audio.tools]]

Essential
---------

FIXME

<https://www.reaper.fm/download.php>

<https://wiki.cockos.com/wiki/index.php/ReaperKbd>

### Shortcuts - interface

### Shortcuts - editing

|                                   |             |
|-----------------------------------|-------------|
| Automation point                  | shift + LMB |
| Marker                            | M           |
| Normalize                         |             |
| Split                             | S           |
| [\#Dynamic Split](#Dynamic Split) | D           |
| Create Region                     | shift + R   |

#### action: "item:select all items in current time selection" 
, give it a shortcut. [**ALT+S**]

You can make a custom action from this one with a lot more custom functionality if you need, or assign it to mouse buttons,assign it to dubbelclick on the trackpanel,make a custom toolbar button,... whatever you want.

- https://forums.cockos.com/showthread.php?p=1043880

Useful
------

### remove gaps
- https://www.reddit.com/r/Reaper/comments/l4o5s9/how_to_remove_empty_space_between_lots_of_items/

### ReaPack

> ReaPack is a package manager for REAPER, the Digital Audio
> Workstation. Discover, install and keep up to date your REAPER
> resources including ReaScripts, JS effects, extensions, themes,
> language packs, templates, web interfaces and more. [^1]

<https://reapack.com/user-guide>

### Ninjam

[Ninjam](AREAS/DigitalGardenAnabasis/_INFORM/ninjam)

### Dynamic Split

**shortcut D**

     - Remove Silent Passages 
       - is to clean up a track and remove those passages that should be silent.
     -  Split at transients 
       - If you want to quantize audio-items, this is the the first tool you need to use.  
     - more at https://wiki.cockos.com/wiki/index.php/Dynamic_split

### File-Managment

1.  REAPER 101: File Management
    1.  <https://reaperblog.net/2013/07/reaper-101-file-management/>
2.  File Management in REAPER DAW Tutorial
    1.  <https://reaperblog.net/2019/11/file-management-video/>

Semi-Obscure
------------

### ReaRender

<https://github.com/YatingMusic/ReaRender> A python toolkit for
automatic audio/MIDI rendering using REAPER

## ReaThon
https://github.com/jamesb93/reathon `reathon` is a python package for constructing REAPER session with native python constructs. The majority of the interface is a reflection of the `.rpp` file structure which itself is very similar to `.xml` with tags and elements (except each element is called a 'chunk').
#python

Courses and tutorials
---------------------

1.  tips and tricks forum thread -
    <https://forum.cockos.com/showthread.php?t=1184>
2.  \#REAPER WEEK -- 100 tips on using REAPER -
    <https://reaperblog.net/2011/10/reaper-week-100-tips-on-using-reaper/>

### Text

10 Essential Reaper TIPS and SHORTCUTS!
<https://thehomerecordings.com/reaper-tips-and-shortcuts/>

Power Arranging In Reaper
<https://www.soundonsound.com/techniques/power-arranging-reaper>

### Video

![](https://www.youtube.com/watch)

![](https://www.youtube.com/watch)

REAPER Mixer Shortcuts -
<https://reaperblog.net/2020/01/reaper-mixer-shortcuts/>

How to Motion Track Video in REAPER -- Moving Camera Effect -
<https://reaperblog.net/2020/02/motion-track-video/>

Mastering Field Recordings in REAPER -
<https://www.youtube.com/watch?v=7BfWqRapF5E>

[^1]: <https://reapack.com/>
