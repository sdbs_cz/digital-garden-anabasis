# concepts.structure

## Bill Viola three structures
>Viola felt as if there are 3 different structures to describe patterns of data structures. There is the branching structure, matrix structure, and schizo structure.[[19]](https://en.wikipedia.org/wiki/Bill_Viola#cite_note-19)
>
"The most common structure is called branching. In this structure, the viewer proceeds from the top to bottom in time."[[20]](https://en.wikipedia.org/wiki/Bill_Viola#cite_note-ReferenceA-20) The branching structure of presenting data is the typical narrative and linear structure. The viewer proceeds from a set point A to point B by taking an exact path, the same path any other reader would take. An example of this is [Google](https://en.wikipedia.org/wiki/Google "Google") because users go into this website with a certain mindset of what they want to search for, and they get a certain result as they branch off and end at another website.
>
>The second structure is the Matrix structure. This structure describes media when it follows nonlinear progression through information. The viewer could enter at any point, move in any direction, at any speed, pop in and out at any place.[[20]](https://en.wikipedia.org/wiki/Bill_Viola#cite_note-ReferenceA-20) Like the branching structure, this also has its set perimeters. However, the exact path that is followed is up to the user. The user has the option of participating in decision-making that affect the viewing experience of the media. An example of this is Public Secrets, a website that reveals secrets of the justice and the incarceration system within the US for women. There is a set boundary of what users can and can't do while presenting them with different themes and subjects users are able to view. Different users will find themselves taking different paths, using flash cues to guide themselves through the website. This vast selection of paths presents many users with a unique viewing experience (in relation to that of the previous persons). As well, they have the choice to read the excerpts from these women or hear it out loud. This connects to Borges' "The Garden of Forking Paths"[[21]](https://en.wikipedia.org/wiki/Bill_Viola#cite_note-21) where the participant has a variety of choices on how they see a story unfold before them. Each time, they can create a different path.
>
>The last structure is called the schizo, or the spaghetti model. This form of data structure pertains to pure or mostly [randomness](https://en.wikipedia.org/wiki/Randomness "Randomness"). "Everything is irrelevant and significant at the same time. Viewers may become lost in this structure and never find their way out."[[20]](https://en.wikipedia.org/wiki/Bill_Viola#cite_note-ReferenceA-20)>
>
> - https://en.wikipedia.org/wiki/Bill_Viola#Viola's_Three_Structures

## Adjacent
- [[concepts.linearity]]
- [[concepts.map]]
- [[people.BillViola]]
- [[areas.architecture]]
- [[concepts.rhizome]]
- [[incubation.pattern]]