# tools.inkscape.[[las]].authoring
 [[tools.svg]]

- objects menu openable from main toolbar

## 101
- circle - audio
	- blue - general
	- yellow-orange - voices _?_
- rectangle - red - anchor ?
- square - start

## links
### anchors
- object ---> right click ---> create link
- id without hashtag

- anchor <---description <---#anchor_id <---rectangle

### hyperlink
- object ---> right click ---> create link
- href >>> url

### intro / start
- square
- object properties
--> id `start`

## images
while importing **link** is needed **not embedded**
## image sequences
- image file --> object properties
- ---> description
- `down / up /... / up left / right down /....`
- new line
- `motion_source/sutr1/files.lst`
- ---> ! set button !
- `down
motion_source/sutr1/files.lst
`
	
## sound
- circle (NO ELLIPSE NO)
- object properties ---> desription
- sound_source/xxx.mp3
- ---> ! set button !


## Interaction manual
double click - fullscreen
click with middle button - grab
spacebar - anchor //intro
mouse pointer --> edge - edge scrolling [fullscreen must be on]


## image workflows
### lists
#### windows list
The method is the same as for all versions of Windows, starting with Windows 3.1 (from 1985!):

1.  Open File Explorer.
2.  Navigate to the folder under scrutiny.
3.  Press Ctrl+L
4.  Type this command (or use copy/paste) and press Enter:  
    cmd  /c  dir  /b  > "%temp%\\Dir.txt"  & notepad  "%temp%\\Dir.txt"

### windows 11
- open in terminal
- `` dir >filename.txt ``
- also see kinda misleading https://www.makeuseof.com/windows-11-copy-folders-file-list-into-text-file/
#### linux list
ls *.png > files.lst

### optimization
#### png
`parallel --lb --tag optipng -o 5 -i 1 ::: **/*.png`

- `parallel` = process in [parallel](https://www.gnu.org/software/parallel/)
- `-o 5` = ridiculously high optimization
- `-i 1` = turn on [interlacing](https://blog.codinghorror.com/progressive-image-rendering/)

#### jpeg
`parallel --lb --tag jpegoptim --all-progressive --force ::: **/*.jpg`