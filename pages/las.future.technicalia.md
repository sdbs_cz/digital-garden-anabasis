## Technicalia / conceplallia
- endframes links to opposite side of canvas
	- how
		- hyperlink? like hyperdrive-anti-anchor
	- endless vs. not endless
- more hyperlinks
- more SVGs and/or zoom-svg-visuals overall
- more videos
- fix soundsauce for seemless situation
	- content-vice, no coding [yet?]
- space bar signs
	- safety space bar signs to remind you to press spacebar in case of emergency 
- more anchors
- cleanup of 101

### User Interface / interaction
- joystick
- ddr
- bigscreen - with webcam control?
- [[fulldocs.twitter.zoomable_ui]]]

![[Pasted image 20211012234618.png]] #update 