# linux server
##### adjacent
[[NAS]][[NAS - albedo]] [[linux]] [[raspberry pi]] [[linux.bash]]
[[inform.linux_tips]]
## Distros
### debian
- https://linustechtips.com/topic/6398-howto-create-your-own-linux-home-server-using-debian/
- https://www.linuxquestions.org/questions/debian-26/use-debian-for-my-home-server-4175415233/

### #unsorted
- https://thishosting.rocks/best-lightweight-linux-distros/
- https://www.truenas.com/truenas-core/
- https://perfectmediaserver.com/tech-stack/proxmox.html
- https://www.lions-wing.net/lessons/servers/home-server.html

### preimaged
- manjaro based https://github.com/zilexa/Homeserver

## software
- https://pi-hole.net/
- [[tools.archivebox]]
- samba
- qbittorrent
- syncthing
- deja dup?
- ipfs [[ipfs.selfhosting]]
- wallabag
- netdata - healtchecks.io

- https://www.addictivetips.com/ubuntu-linux-tips/best-linux-home-server-apps/
- https://github.com/zilexa/Homeserver

- https://github.com/awesome-selfhosted/awesome-selfhosted

### ?
- https://filerun.com/

### cli
- https://twitter.com/amilajack/status/1479328649820000256?s=20
- `exa`, `htop` (samozrejme), a pouzivaval jsem `duf` . `zoxide`
- https://github.com/ibraheemdev/modern-unix
- https://www.geeksforgeeks.org/tree-command-unixlinux/

### media and streaming
- plex - https://www.plex.tv/
- https://radarr.video/
- jellyfin - https://jellyfin.org/
## desktop enviroment
[[linux.i3wm]]

## 101 
- https://linuxhint.com/run_linux_command_script_sys_reboot/

## threads #unsorted
- https://www.reddit.com/r/HomeServer/comments/rsaj81/doing_a_lot_with_old_cheap_hardware/
- https://www.reddit.com/r/DataHoarder/comments/6sab9c/debian_or_ubuntu_for_home_server/
- https://www.reddit.com/r/DistroHopping/comments/neb5l1/lightweight_distro_for_little_home_server/
- https://www.partitionwizard.com/clone-disk/lightweight-linux-distro.html


- https://www.linuxserver.io/blog/2015-03-24-setting-up-a-linux-home-server-using-the-hp-proliant-microserver-gen8-g1610t-3

- https://linustechtips.com/topic/1158827-best-linux-os-for-multi-purpose-home-server/?__cf_chl_tk=q2G._0ZAgY0ijRfJ2hnaP_e3q9uwx1fUYFbFHT0Om1k-1641223192-0-gaNycGzNB6U

- https://www.pcworld.com/article/406209/how-to-have-a-linux-home-server-on-the-cheap.html
- https://www.reddit.com/r/selfhosted/comments/sdcfk9/my_dashboard_after_learning_about_selfhosting/
	- https://www.fuzzygrim.com/posts/selfhosted-setup
## thin
- https://boratory.net/things-to-do-with-a-thin-client/
- https://linustechtips.com/topic/1199977-thin-client-as-home-server-filesmedia/?__cf_chl_tk=o2TufiRAN307O015vk35aU6N6aC9UUi5iOc4afI3gJQ-1642071753-0-gaNycGzNBqU

## Adjacent
- [[linux.bash]]
- [[incubation.tech.screen]]