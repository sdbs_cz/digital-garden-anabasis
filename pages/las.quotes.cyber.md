# las.cyber 
> The word _cybernetics_ comes from [Greek](https://en.wikipedia.org/wiki/Ancient_Greek "Ancient Greek") κυβερνητική (_kybernētikḗ_), meaning "governance", i.e., all that are pertinent to κυβερνάω (_kybernáō_), the latter meaning "to steer, navigate or govern", hence κυβέρνησις (_kybérnēsis_), meaning "government", is the government while κυβερνήτης (_kybernḗtēs_) is the governor, pilot, or "helmsperson" of the "ship".
>![[Pasted image 20220807215733.png]]

> "The Definition of a Net: Anything made with interstitial vacuities."
 >
>- Dr. Samuel Johnson

## Intertwingle
-------
- #cyber - [[las.quotes.cities]]
- #connection 
### [[incubation.hermes]]
>“Where are you?” “What place are you talking about?” I don’t know, since Hermes is continually moving on. Rather, ask him, “What roadmap are you in the process of drawing up, what networks are you weaving together?” No single word, neither substantive nor verb, no domain or specialty alone characterizes, at least for the moment, the nature of my work. I only describe relationships. For the moment, let’s be content with saying it’s “a general theory of relations.” Or “a philosophy of prepositions."[\[31\]](https://contempaesthetics.org/newvolume/pages/article.php?articleID=812#FN31)
> - Michel Serres Passe-Partout
> 	- also Rhizomatic Mnemosyne: Warburg, Serres, and the Atlas of Hermes

#connection
> Pia and Pantope, Michel Serres (1995)
paints an interesting picture of our post-industrial times:
“So you see angels everywhere…?”
“Their lot, with all the august title of subject! The light that comes
from the sun and stars brings messages, which are decoded by
optical or astrophysical instruments; a radio aerial emits, transmits
and receives; humans do not need to intervene here. As they say,
when something’s working, leave well alone.”
Pantope continues, as determinedly as Pia: “If we become angels,
will we still work?”
”Probably never again in the same way as yesterday, when our
forefathers were out there toiling on the land, or laboring over a
piece of iron, forming it, reforming it, transforming it with their
hands, using tools and machines.”
”We exchange information with objects that appear more as
relations, tokens, codes and transmitters.”
”What’s more,” says Pia, seriously, “in this new world of increasing
interconnectedness, the old kinds of work are fast becomingcounter-productive. They pollute, they produce crises and
unemployment for the societies organized around them; they are
allowed to outlive their usefulness, and become dangerous,
wasteful. As a core activity, they enlist and mobilize the whole of
society in the same way that religion once did, or, more recently,
war. Disasters always seem to derive from things which had an
initial usefulness, but which, even though they have outlived their
time, we then continue to operate, despite their enormous costs in
terms of death and catastrophe.” (pp. 52–53)

#cyber #newton //--> follow flusser
> ”Newtown industrialises signs, manufactures things with information, constructs the universe with wind, does not remain obtusely materialist within matter, but goes beyond and carries materialism into software.” 
> (Serres, 1995, p. 71)

#cyber #visionaries [[las.reconcept.intro.2]]
>**Everything is deeply intertwingled.** In an important sense there are no "subjects" at all; there is only all knowledge, ...
> - Nelson
## Adjacent
- [[incubation.areas.diagrammatic]]
- [[concepts.trailblazers]]
- [[incubation.PrincipiaCybernetica]]
- [[fulldocs.Speed and Information Cyberspace Alarm]]
- 