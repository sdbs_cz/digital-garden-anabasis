# [[fulldocs]].twitter.selforganizing-system

- [Tweet](https://twitter.com/i/status/1366402601789448196) by [@gordonbrander](https://twitter.com/gordonbrander) on [[March 1st, 2021]]:
- “Self-organizing system” becomes meaningless unless the system is in close contact with an environment... [pic.twitter.com/zJkIVMKkTR](https://twitter.com/gordonbrander/status/1366402601789448196/photo/1)
- ![Image](https://pbs.twimg.com/media/EvZv7KJVEAI1ASw?format=jpg&name=small)	
- Living organisms can’t survive in their own waste products.
  - Alan Kay
- Very little goes to waste in a rainforest. [twitter.com/startuployalis…](https://twitter.com/startuployalist/status/990845067492388864)
- “A forest has no weeds.”
  - “Any sufficiently advanced technology is indistinguishable from nature.”
  - “Pollution is nothing but the resources we are not harvesting. We allow them to be dispersed because we've been ignorant of their value.” (Buckminster Fuller.)
- There’s a kind of fractal challenge to recycling, caused by the Anna Karenina problem “all happy resources are alike; each unhappy waste product is unhappy in its own way.”
- Because of the fractal shape of the problem, it’s hard to imagine how to get to a circular economy past the low-hanging fruit.
  - OTOH markets, like all evolutionary systems, are good at evolving fractal niches. Perhaps if there were incentives to close loops... 🤔
- What an organism feeds on is negative entropy. To put it less paradoxically, the essential thing in metabolism is that the organism succeeds in freeing itself from all the entropy it cannot help producing while alive.
  - Schroedinger
- There are local and temporary islands of decreasing entropy in a world in which the entropy as a whole tends to increase, and the existence of these islands enables some of us to assert the existence of progress.
  - Norbert Wiener
- Our pale blue soap bubble island of decreasing entropy, continually finding new creative ways to soak up that wash of warm sunlight.

## Adjacent
- [[people.AlanKay]]
- [[people.BuckminsterFuller]]
- [[people.NorbertWiener]]