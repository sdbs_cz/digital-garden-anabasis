# Infinite Canvas

>The **infinite canvas** refers to the potentially limitless space that is available to [webcomics](https://en.wikipedia.org/wiki/Webcomics "Webcomics") presented on the [World Wide Web](https://en.wikipedia.org/wiki/World_Wide_Web "World Wide Web"). The term was introduced by [Scott McCloud](https://en.wikipedia.org/wiki/Scott_McCloud "Scott McCloud") in his 2000 book _[Reinventing Comics](https://en.wikipedia.org/wiki/Reinventing_Comics "Reinventing Comics")_, in which he suggested that webcomic creators could make a [web page](https://en.wikipedia.org/wiki/Web_page "Web page") as large as needed to contain a comic page of any conceivable size. This infinite canvas would create an endless amount of storytelling benefits and would allow creators much more freedom in how they present their artwork.
> -  https://en.wikipedia.org/wiki/Infinite_canvas

## Tools
- https://dragonman225.js.org/jade
- https://gitlab.com/tmladek/line-and-surface/

## Adjacent
- [[las.inspirations]] and [[las]]
- [[incubation.interface]]
- [[concepts.map]] and [[concepts.MOC]]