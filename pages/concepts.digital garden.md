# digital garden

#knowledge #project

## Theory
![[dg_scheme.png]]

__________________________________

### [[people.AndyMatuschak]] about his digital garden

>These notes are mostly written for myself: they’re roughly my thinking environment ([Evergreen notes](https://notes.andymatuschak.org/z4SDCZQeRo4xFEQ8H4qrSqd68ucpgE6LU155C); [My morning writing practice](https://notes.andymatuschak.org/zVFGpprS64TzmKGNzGxq9FiCDnAnCPwRU5T)). But I’m sharing them publicly as an experiment ([Work with the garage door up](https://notes.andymatuschak.org/z21cgR9K3UcQ5a7yPsj2RUim3oM2TzdBByZu)). If a note seems confusing or under-explained, it’s probably because I didn’t write it for you! Sorry—that’s sort of an essential tension of this experiment ([Write notes for yourself by default, disregarding audience](https://notes.andymatuschak.org/z8AfCaQJdp852orumhXPxHb3r278FHA9xZN8J)).

### << 101 >>
- blogging vs. [[concepts.knowledge managment]]
- notetaking vs. publication vs. piling

### Garage doors open

[Szymon Kaliski on Twitter: "two things have been on my mind lately (or maybe I‘m only verbalizing them now): pixel space in digital tools (https://t.co/FUo2NUwHo5) and “fractality” of some things in software (https://t.co/t8OlhGOGPJ) sharing in an attempt to “work with the garage doors open”" / Twitter](https://mobile.twitter.com/szymon_k/status/1320040354117263362?s=20)



### Gardens
- [Second-Brain](https://github.com/KasperZutterman/Second-Brain)
	- A curated list of awesome Public Zettelkastens 🗄️ / Second Brains 🧠 / Digital Gardens 🌱
- [maggieappleton](https://maggieappleton.com/garden/) 
- [Andy Matuschak](https://notes.andymatuschak.org)
- - [nikitavoloboev/knowledge: Everything I know](https://github.com/nikitavoloboev/knowledge)
- https://groktiddlywiki.com/read/
- https://anagora.org/index
	- social network X digital garden


## tools 
- https://www.notion.so/Artificial-Brain-Networked-notebook-app-a131b468fc6f43218fb8105430304709

### instruments

#### [tools.obsidian](tools.obsidian.md)

#### [[tools.dendron]]

#### [tools.markdown](tools.markdown.md)

#### [agora](https://github.com/flancian/agora)

### tools - wiki 
#### [[tools.tiddlywiki]]
#### mediawiki

#### [Growi](https://growi.org/en/)
- 『Markdown × Wiki × Free』
- https://growi.org/en/#features
#hedgedoc


### Articles
- [A Brief History & Ethos of the Digital Garden](https://maggieappleton.com/garden-history)
- [Digital Gardening by Maggie Appleton](https://github.com/MaggieAppleton/digital-gardeners) !!!
- https://twitter.com/Mappletons/status/1250532315459194880
- [The Swale: Weaving between Garden and Stream](https://bonkerfield.org/2020/05/swale-garden-stream/)
- [NeoCyborgs](https://maggieappleton.com/neocyborgs)
- [...sharing in an attempt to “work with the garage doors open”" / Twitter](https://mobile.twitter.com/szymon_k/status/1320040354117263362?s=20)
- sidereal
	- [Matuschak - Enabling environment](https://notes.andymatuschak.org/z3DaBP4vN1dutjUgrk3jbEeNxScccvDCxDgXe)

### Adjacent
#### [areas.filetag](areas.filetag.md)
#### **[[projects.pile]]**
#### [[concepts.playlisting]]

#### [[concepts.knowledge managment]]

#### [[tools.markdown]]


---------------------------------
---------------------------------

---------------------------------
---------------------------------

---------------------------------
---------------------------------

# #tosort 
 - https://wiki.thingsandstuff.org/Main_Page
 - https://bonkerfield.org/tag/art/
 - http://gordonbrander.com/pattern/ #patterns 
 - https://tomcritchlow.com/wiki/media-theory/
 - https://busterbenson.com/piles/
 - http://gordonbrander.com/pattern/design-patterns/




https://www.are.na/ajmir-kandola/gardens-of-tomorrow



https://sive.rs/ff


> ![Image](https://pbs.twimg.com/media/EmfKmwSUcAAOFwG?format=jpg&name=4096x4096)
> - https://twitter.com/sariazout/status/1326253159447097344?s=20
>
> - https://mobile.twitter.com/sariazout/status/1326253159447097344/photo/1


> “Sometimes when you're in a dark place you think you've been buried, but you've actually been planted.”
>
>― Christine CaineCaine


https://massive.wiki/~~~~


