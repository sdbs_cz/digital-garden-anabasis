# [[incubation.annotation]] .music notation / [annotation](incubation.annotation.md)

# #incubation
## concepts of notation
### western tradition etc.

## #techmech 

### musicXML
>MusicXML is an XML-based file format for representing Western musical notation. The format is open, fully documented, and can be freely used 

- https://www.musicxml.com/
- https://www.soundslice.com/musicxml-viewer/

## [[areas.AI]]

## ??? #tosort 
- https://www.vexflow.com/
- https://lilypond.org/
- https://www.coreymwamba.co.uk/musify/

## Adjacent
- [[incubation.audio.sample.managment]]
- [[people.JaceClayton]]
- [[people.JeanDubuffet]]