# EDL
>An edit decision list or EDL is used in the post-production process of film editing and video editing. The list contains an ordered list of reel and timecode data representing where each video clip can be obtained in order to conform the final cut.

>EDLs are created by offline editing systems, or can be paper documents constructed by hand such as shot logging. These days, linear video editing systems have been superseded by non-linear editing (NLE) systems which can output EDLs electronically to allow autoconform on an online editing system – the recreation of an edited programme from the original sources (usually video tapes) and the editing decisions in the EDL.



## Basic formats
- https://en.wikipedia.org/wiki/Edit_Decision_List
	- https://www.niwa.nu/2013/05/how-to-read-an-edl/
- https://en.wikipedia.org/wiki/Advanced_Authoring_Format

## EDL tools
- http://edlmax.com/MAXGUIDE.HTML
- https://github.com/sbidoul/edledit
- https://github.com/rdp/sensible-cinema
	- EDL player for DVDs, etc. maybe online

https://en.wikipedia.org/wiki/AviSynth

## Alternatives
### OTIO
- https://github.com/PixarAnimationStudios/OpenTimelineIO
	- https://opentimelineio.readthedocs.io/en/latest/

>OTIO viewer
>![](opio_viewer.png)

-----
## Mozilla Popcorn maker
>![[popcornmaker.png]]

- https://archive.org/details/popcornproject
	- https://archive.org/details/Popcornarchive-introToMediaVideoEssay
- https://github.com/mozilla/butter
	- https://github.com/mozilla/popcorn-js
		- https://github.com/menismu/popcorn-js

# Related
- https://donottrack-doc.com/en/
- https://youtu.be/1gPM3GqjMR4
- also [[people.TedNelson]] used the term

### VRML
- ....
- https://en.wikipedia.org/wiki/3D_Construction_Kit