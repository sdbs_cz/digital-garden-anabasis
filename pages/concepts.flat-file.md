# Flat-file database

# Essential
>A flat-file database is a database stored in a file called a flat file. Records follow a uniform format, and there are no structures for indexing or recognizing relationships between records. The file is simple. A flat file can be a plain text file, or a binary file. Relationships can be inferred from the data in the database, but the database format itself does not make those relationships explicit.
>The term has generally implied a small database, but very large databases can also be flat. 
> - https://en.wikipedia.org/wiki/Flat-file_database