# Blender - [[tools.svg]] import
## Before import
### inkscape
- before loading to inkscape, fix contrast ratios
- export as plain svg
- https://inkscape.org/doc/tutorials/tracing/tutorial-tracing.html

## Blender
### curve / path
#### Shape
- 3D - clumsy, but more depth
- 2D - boring, but sharper readable
#### Extrude
- prefered
- 
#### Bevel
-