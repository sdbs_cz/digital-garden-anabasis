--------------------------------------------
//INCUBATIONINCUBATIONINCUBATIONINCUBATION

--------------------------------------------

_This vault can be opened through regular [markdown](tools.markdown) editors, or [knowledge management](concepts.knowledge%20managment) tools like [obsidian](tools.obsidian). This vault is being shared through [syncthing](tools.syncthing)_.

**It is available at [garden.sdbs.cz](https://garden.sdbs.cz).**  
**Its history resides at [gitlab.com/sdbs_cz/digital-garden-anabasis](https://gitlab.com/sdbs_cz/digital-garden-anabasis)**  
It is being served by gardenserver: [gitlab.com/tmladek/gardenserver](https://gitlab.com/tmladek/gardenserver)

--------------------------------------------

- Relevant [[projects.pile]] docs: https://pile.sdbs.cz/tag/19
- Public [hypothes.is](https://hypothes.is) group: <https://hypothes.is/groups/28G37dwe/hypertext-blazers>

--------------------------------------------

# DigitalGardenAnabasis starts here

Focus of this vault is collecting notes in these:

## Areas

- [[concepts.knowledge managment]] and [concepts.digital garden](concepts.digital%20garden.md)s
- [File tagging](areas.filetag.md), [[concepts.Digital Asset Managment]] and [[incubation.annotation.media]], [[incubation.audio.sample.managment]]
- [[areas.philosophy]] & [[incubation.areas.diagrammatic]] 
- [[areas.video]]
- [[areas.audio]]
- [areas.self-hosting](areas.self-hosting.md)
- [[incubation.concepts.DatabaseArt]]


## Projects
- [[dga]] - digital garden anabasis - this site
- [[projects.upend]] >>>
- [ALEADUB](projects.ALEADUB.md)
- [Line and Surface](las.md)
- sdbs [[concepts.digital garden]]
	- [gardenserver](gardenserver.md)- markdown parser
	- [inform](https://inform.sdbs.cz) development
	- [pile](https://pile.sdbs.cz) inclusion
	- [gunk](https://ipfs.io/ipns/QmX3hGuNKRRogN12S6Gjg7s3TWDzjHLh25P4da9nBrTxKh/) gunking
- [projects.portfolio generator](projects.portfolio%20generator.md)
- [[projects.feedfarm]] aka growing through [artificial_intelligence](areas.AI.md), [[feedfarm.mining.rendering]] and [[rendering]]
	
- lalalalala
	- ![](lalalala.png)
	- [[projects.lalar]]


--------------------------------------------

> A [complex](complexity) system that works is invariably found to have evolved from a simple system that worked. A complex system designed from scratch never works and cannot be patched up to make it work. You have to start over, beginning with a working simple system.
>> [John Gall](https://en.wikipedia.org/wiki/John_Gall_(author))

--------------------------------------------

> ![dg_scheme](dg_scheme.png)
 >>[[people.MaggieAppleton]]

 -------------------------------------------

-  https://web.archive.org/web/20190607172020/http://people.umass.edu/klement/tlp/tlp.html
-  [[people.VilemFlusser]]
 
-------------------------------------------
 
 >![[nelson-ttomuchtosay.png]]
 >>[[people.TedNelson]]

-------------------------------------------
 
 >![Image](https://pbs.twimg.com/media/Eu31xSMUYAMbObz?format=jpg&name=small)
 >> NASA #update

----------------------------
> ![[img_layers_How buildings learn Steward Brand 1994.png]]
>> How buildings learn, Steward Brand, 1994

![[Share_Media_20220513_230411.png]]