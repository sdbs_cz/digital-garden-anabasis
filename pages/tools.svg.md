- cleaning svgs
	- oh my svg (sonny piers)

## SVG tools and workflows
- [[tools.blender.svg]]
- [[tools.potrace]]
- [[tools.inkscape]]
- ...
- https://www.smashingmagazine.com/2021/03/svg-generators/
- https://www.smashingmagazine.com/2022/05/magical-svg-techniques/


### Generative SVG

- https://georgefrancis.dev/writing/a-generative-svg-starter-kit/
	- https://dev.to/georgedoescode/a-generative-svg-starter-kit-5cm1
- https://frontend.horse/articles/generative-grids/
	
------
bleeptrack@chaos.social - RT @algoritmic@twitter.com

Smoothly interpolate between variations of SVG paths https://github.com/Minibrams/svg-path-morph​

🐦🔗: https://twitter.com/algoritmic/status/1541786470482149379​


-----

mairin - You got a spreadsheet of 100 conference talk names and presenters and need to generate a banner graphic for each. What to do? Use Inkscape!

:) Here is a tutorial on how to automate this and generate them in seconds: https://peertube.linuxrocks.online/w/sf8Vqgg3aRkPKpb7KMsHgH Example files link in vid details!

------

danslerush@fosstodon.org - For #makers : #Flatterer is a #b3d add-on by @sybren to export shapes to laser-cuttable .SVG files /> https://stuvel.eu/post/2022-02-09-flatterer-v1.1/​

humanetech - @imakefoss there are so many wonderful #FOSS tools in this space.

I hope that soon someone will volunteer to curate a #delightful list on the topic and join the https://delightful.club​

Am already maintaining too many lists to do so myself. For info, see: https://codeberg.org/teaserbot-labs/delightful/src/branch/main/delightful.md​

If someone is interested, just ping me, or create an issue in the #codeberg repo.

danslerush@fosstodon.org - For #makers : #Flatterer is a #b3d add-on by @sybren to export shapes to laser-cuttable .SVG files /> https://stuvel.eu/post/2022-02-09-flatterer-v1.1/​

----
https://github.com/re-path/studio