# audio.metadata
## Semantics
- freesound
	- https://freesound.org/help/faq/#i-have-many-sounds-to-upload-is-there-a-way-to-describe-many-sounds-in-bulk
	- https://freesound.org/help/moderators/#good-description-standards
- schema.org
	- https://schema.org/AudioObject
	- https://schema.org/Clip
	- https://schema.org/ArchiveComponent
	- https://schema.org/Dataset
- also semantics of XMP
	- https://en.wikipedia.org/wiki/Extensible_Metadata_Platform
- vocabulary
	- https://dictionary.cambridge.org/topics/senses-and-sounds/sounds-made-by-objects-movement-or-impact/

## Metadata solutions
- id3tag 
	- *used by consumers and more?*
- bwf - https://en.wikipedia.org/wiki/Broadcast_Wave_Format 
	- *this seems to be liked by profesionals*
		- https://mediaarea.net/BWFMetaEdit/xml_chunks
		- https://www2.archivists.org/sites/all/files/Andrec,%20Michael.pdf fulltext? where?
- XMP - https://en.wikipedia.org/wiki/Extensible_Metadata_Platform
- dolby - https://professionalsupport.dolby.com/s/topic/0TO4u000000f1pFGAQ/audio-metadata?language=en_US
	- probably interesting just for dolby 


## Tagging
- https://sourceforge.net/projects/kid3/ #multiplatform
- https://onetagger.github.io/ also spotify analysis / genre analysis
	- spotify genres - https://docs.google.com/spreadsheets/d/1wYokScjoS5Xb1IvqFMXbSbknrXJ7bySLLihTucOS4qY/edit#gid=0
- https://docs.mp3tag.de/mapping/
-  [[inform.reaper]] / [[audio.software.reaper]]
	- https://reaper.blog/2021/11/metadata-editing-in-reaper/
		- https://www.youtube.com/watch?v=bDfuMIeqawc
	- https://reaper.blog/2018/03/sound-library-metadata/https://www.stephenschappler.com/2020/05/11/reaper-quick-tip-exporting-audio-with-embedded-metadata/

## Articles
- https://www.production-expert.com/production-expert-1/metadata ac3
- https://www.creativefieldrecording.com/2014/06/17/an-introduction-to-sound-fx-metadata-apps-2-comparing-apps/#Metadata_App_Chart 2014
- https://archaeologydataservice.ac.uk/help-guidance/guides-to-good-practice/basic-components/digital-audio/preserving-digital-audio/deciding-how-to-archive/
## Adjacent
- [[concepts.Digital Asset Managment]]
- [[incubation.audio.sample.managment]]
- [[areas.filetag]]
- [[audio.datasets]]
- [[incubation.concepts.DatabaseArt]]
- [[areas.audio]]
- [[concepts.archive]]
- [[concepts.archives.art]]