# Archivebox

[[areas.self-hosting]]

https://github.com/ArchiveBox/ArchiveBox

## How-tos
- https://github.com/ArchiveBox/ArchiveBox/wiki/Scheduled-Archiving
- https://github.com/ArchiveBox/ArchiveBox/wiki/Publishing-Your-Archive
- https://computingforgeeks.com/install-use-archivebox-self-hosted-internet-archiving/
- https://sleeplessbeastie.eu/2019/06/19/how-to-install-archivebox-to-preserve-websites-you-care-about/
### rss feed
- https://www.reddit.com/r/DataHoarder/comments/ioupbk/archivebox_question_how_do_i_import_links_from_a/
	- https://github.com/RSS-Bridge/rss-bridge
	- https://ssl.reddit.com/prefs/feeds/

## Usecased
- http://webermartin.net/blog/web-archiv-teil-8-wallabag-und-archivebox/
- https://manfred.life/archivebox

___________
https://github.com/dbeley/archiveboxmatic


----
- [[tools.zim]]
- [[tools.kiwix]]
- [[tools.XOWA]] - Offline wikipedia
