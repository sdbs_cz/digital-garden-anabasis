# Line and Surface

## Live here >>> https://sdbs.cz/las

## Interesting positions
- https://sdbs.cz/las/#-8146,-14585,0.136z
- https://sdbs.cz/las/#8924,-4263,0.892z
- https://sdbs.cz/las/#-8146,-14585,0.136z
- https://sdbs.cz/las/#39597,-12623,34.547z
- https://sdbs.cz/las/#-26761,-12554,0.969z
- https://sdbs.cz/las/#-7718,1186,2.257z

![[Screenshot from 2021-03-28 01-22-25.png]]

## Crossroad	
- [[las.reconcept]] notes on conceptualization through canvas
- [[las.credits]]
- [[las.quotes]] - pool of quotes
- [[las.inspirations]]
- @ [[projects.pile]]  >>> https://pile.sdbs.cz/tag/20

## tek
- https://gitlab.com/tmladek/line-and-surface/