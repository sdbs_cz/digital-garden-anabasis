# Stream
#OUTPOSTS #backbone #theory #techmech #chaosstream 

>While it is still true that everything on the Web is
[digitally] preprogrammed, the notion of a
dictatorship of the [hyper]link carries less weight.
… The open architecture of the Web lends itself
to the accumulation of analog effects. The
increase in image and sound content alongside
text provides more opportunities for resonance
and interference between thought, sensation, and
perception. A crucial point is that all the sense
modalities are active in even the most apparently
monosensual activity. … Given the meagerness of
the constituent links on the level of formal
inventiveness or uniqueness of content, what
makes surfing the Web compelling can only be
attributed to an accumulation of effect, or …
momentum, continuing across the linkages.
>-([[people.BrianMassumi]], 2002: 140-141)

>History decays into images, not into stories.
> - [[people.WalterBenjamin]], The Arcades Project, 476.

------------------------
## conflicts
### Live vs. Background
- stream as live performance and/or background soap opera
### Active layer vs. passive perception
- active layers of stream... participation? vs. perception?
## toolsets
### [[incubation.annotation]]
![](docdrop_screenshot.png)
#OUTPOSTS 

https://docdrop.org/

### chat
[projects.sermon](projects.sermon.md)
https://github.com/gatteo/react-jitsi

## Related
- [[concepts.hypertext]]
- [[DigitalGardenAnabasis/annotation]]
- [[incubation.decentralization]]
- [[concepts.archives.art.tours]]
- [[incubation.attention.economy]]
- [[concepts.web.doomed]]
- [[streaming.software]]
- [[tool.stream.tech.audio]]
- [[tech.stream.jamming]]
- [[audio.stream.local]]
- [[audio.stream.sonobus]]

________________________________________________________
________________________________________________________

________________________________________________________
________________________________________________________

________________________________________________________
________________________________________________________

# #chaostream 
------------------------
https://live.sdbs.cz

-----------------------
https://upstage.org.nz/

-------------------------------------------
[CatalogOFFDigitalDiscomfort.pdf](CatalogOFFDigitalDiscomfort.pdf)

--------------------------------------

![[meme.signal.20211028120621.png]]


## #techmech 

- https://nageru.sesse.net/
- https://github.com/owncast/owncast
- https://stagetimer.io/
- https://framablog.org/2021/01/07/peertube-v3-its-a-live-a-liiiiive/
- http://websdr.org/


### Android

IP CAM

### profi tools
#### NDI
[[tech.NDI]]
#### SRT 
- https://www.srtalliance.org/about-srt-technology/

