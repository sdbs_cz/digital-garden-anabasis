# Glitch Art
#chaosstream #list
## algo-glitch demented
formulation by [Daniel Temkin](http://danieltemkin.com)
- [There's Not Much 'Glitch' In Glitch Art](https://www.vice.com/en/article/wnj5aq/theres-not-much-glitch-in-glitch-art)
- [Notes on Glitch](http://worldpicturejournal.com/WP_6/Manon.html)
- [Glitch && Human/Computer Interaction](https://nooart.org/post/73353953758/temkin-glitchhumancomputerinteraction)

## poor image 

https://www.e-flux.com/journal/10/61362/in-defense-of-the-poor-image/

## sound

### ryoji ikeda
https://www.youtube.com/watch?v=k3J4d4RbeWc
https://www.youtube.com/watch?v=XwjlYpJCBgk

#### C4I
https://www.youtube.com/watch?v=Nk1O9VkG4Qs

### oval
- https://www.discogs.com/Oval-Wohnton/master/1641915

## materialita digitalita
https://www.vice.com/en/article/gvwv7y/8-artists-are-bringing-glitch-art-into-the-real-world


## JODI
#games
http://sod.jodi.org/
http://aaaan.net/world-wide-wrong-jodi/

## memory of broken dimension
#games
http://brokendimension.com/ 


## blender
https://www.youtube.com/watch?v=_YbvXgls1Yw

## kanye west
https://www.youtube.com/watch?v=wMH0e8kIZtE

## diakur - 
https://www.youtube.com/watch?v=Q2VMWR8xlls

## faces
#games
https://www.youtube.com/watch?v=Qr5EuMxsOic


------
McLaren - [Pas de deux](https://www.youtube.com/watch?v=WopqmACy5XI) 

----
![](glitch-wardrobe.png)