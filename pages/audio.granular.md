# Granular synthesis & adjacent



- !! list 
	- https://wiki.thingsandstuff.org/Granular_synthesis
	- https://hiphopmakers.com/best-free-granular-vst-plugins
	- https://vstwarehouse.com/granulizer/
- **https://github.com/enrike/slicer** 
	- http://www.ixi-audio.net/content/software.html 

## windows
- https://www.kvraudio.com/product/emergence-by-daniel-gergely
- https://2mgt.st8.ru/Nasca_2.html

### ableton
- https://www.edmprod.com/granular-synthesis/

### borderlands
- [[borderlands-controls]]
- also there is sexy & paid iphone version


## linux
- https://linuxmusicians.com/viewtopic.php?t=24099
- https://linuxmusicians.com/viewtopic.php?t=18464

### chaolist
- https://github.com/sjaehn/BHarvestr
- https://github.com/linuxmao-org/Frontieres ??
### Soundgrain
also win
- https://github.com/belangeo/soundgrain
	- http://ajaxsoundstudio.com/software/soundgrain/
	- RTAudio issues?
		- https://www.youtube.com/watch?v=tJ72-yEJgmU

### Spectmorph
- morphing between 2 sounds
- https://www.spectmorph.org/downloads/

### emission control
- https://github.com/EmissionControl2/EmissionControl2
- ## #multiplatform 

### oi granddad
- multiplatform
- https://github.com/publicsamples/Oi-Grandad

## paid
- https://www.tracktion.com/products/spacecraft
- apple
	- https://apps.apple.com/nl/app/fluss-granular-playground/id6443472888?l=en
## other synths
- https://www.tunefish-synth.com/
- http://www.hrastprogrammer.com/hrastwerk/tranzistow.htm

## unsorted
- https://www.audiopluginsforfree.com/procedural-ambient-noise-orchestra-pano/
- https://2mgt.st8.ru/Nasca.html
- https://www.noisemakers.fr/downloads/#1450094621447-c1c781d1-21d4=
- https://notam.no/prosjekter/ceres/
- https://github.com/ssj71/infamousPlugins
- **# gRainbow**
	- https://plugins4free.com/plugin/3941/
	- **g rainbow multiplatform synth** 
	- **!!!!!!*
	- https://bboettcher3.github.io/grainbow 

### ai music
- https://www.youtube.com/watch?v=ry5eCasj2S4
- https://www.amazon.com/power-sequencer/s?k=power+sequencer
## in pure data
- http://www.pd-tutorial.com/english/ch03s07.html



---------------------------------
[[[incubation.audio.synthesis.concatenative]]]