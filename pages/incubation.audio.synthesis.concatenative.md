# Concatenative synthesis

## 101
> **Concatenative synthesis** is a technique for synthesising sounds by concatenating short samples of recorded sound (called _units_).
>  - https://en.wikipedia.org/wiki/Concatenative_synthesis

>synonyms for concatenation
>> 
> -   [chain](https://www.thesaurus.com/browse/chain)
> -   [continuity](https://www.thesaurus.com/browse/continuity)
> -   [integration](https://www.thesaurus.com/browse/integration)
> -   [interlocking](https://www.thesaurus.com/browse/interlocking)
> -   [link](https://www.thesaurus.com/browse/link)
> -   [nexus](https://www.thesaurus.com/browse/nexus)
> -   [series](https://www.thesaurus.com/browse/series)
> -   [succession](https://www.thesaurus.com/browse/succession)
> -   [uniting](https://www.thesaurus.com/browse/uniting)

## 102

- types
	- audio mosaicing
		- https://github.com/danmackinlay/mosaicing_omp_ismir_2019/

## Audio and databases
- samplers and smart samplers
- GAN [[incubation.ai.audio]]
	- for GAN vs. diffusion see: https://cyberneticforests.substack.com/p/what-was-the-gan
- [[incubation.audio.sample.managment]]
- See also: [[incubation.concepts.DatabaseArt]]
- audio source separation
	- https://danmackinlay.name/notebook/source_separation.html
	- non-matrix factorization aka nmf

## Tools

- https://vi-control.net/community/threads/are-there-any-free-standalone-concatenative-synthesis-programs.148552/

### Paid
- FactorSynth - https://anemond.net/
- ReformerPro - https://www.krotosaudio.com/products/

### Promising
- https://colab.research.google.com/github/stevetjoa/musicinformationretrieval.com/blob/gh-pages/nmf_audio_mosaic.ipynb
- https://soundlab.cs.princeton.edu/research/mosievius/
- http://imtr.ircam.fr/imtr/Corpus_Based_Synthesis
	- max/msp
	- http://imtr.ircam.fr/imtr/Diemo_Schwarz
- https://github.com/benhackbarth/audioguide
	- OSX but promising
- https://rodrigoconstanzo.com/combine/ 
	- also M4L or runtime
- https://neutone.space/
	- probably more [[DigitalGardenAnabasis/ai.music]]
	
#### Audiostellar [8/10]
- https://audiostellar.xyz/
	- https://www.arj.no/tag/sox/
		- cataRT
	- ableton link incoming 



#### timbreIDLib Pure Data
https://github.com/wbrent/timbreIDLib
>timbreIDLib is a library of audio analysis externals for Pure Data. The classification external [timbreID] accepts arbitrary lists of audio features and attempts to find the best match between an input feature and previously stored training instances. The library can be used for a variety of real-time and non-real-time applications, including sound classification, sound searching, sound visualization, automatic segmenting, ordering of sounds by timbre, key and tempo estimation, and concatenative synthesis.

- https://forum.pdpatchrepo.info/topic/11876/scrambled-hackz-how-did-he-do-it

#### flucoma
[[audio.soft.flucoma]]

https://www.flucoma.org/
>TheFluidCorpusManipulationproject(FluCoMa)instigatesnewmusicalwaysofexploitingever-growingbanksofsoundandgestureswithinthedigitalcompositionprocess,bybringingbreakthroughsofsignaldecompositionDSPandmachinelearningtothetoolsetoftechno-fluentcomputercomposers,creativecodersanddigitalartists.

- PD / Max / supercollider also
- reaper also [[reacoma]]


#### samplebrain [4/10]
https://gitlab.com/then-try-this/samplebrain

#### cataRT
- https://ircam-ismm.github.io/max-msp/catart.html#videos
- it is backed by https://ircam-ismm.github.io/max-msp/mubu.html

#### AudioGuide
- mac os / offline / to reaper export
- https://github.com/benhackbarth/audioguide

#### rhytmcat
- https://github.com/carthach/rhythmCAT

#### scripts #see #asap
- python https://github.com/Pezz89/PySoundConcat
- super collider https://github.com/olafklingt/SPList 

#### redtimbre plugins
not so expensive - windows
	- https://www.redtimbreaudio.com/product-page/graphiti
	- https://www.redtimbreaudio.com/product-page/delaycat
### Unsorted


- https://discourse.flucoma.org/t/flucoma-vs-librosa-nmf/2172

- https://en.wikipedia.org/wiki/Festival_Speech_Synthesis_System
- https://en.wikipedia.org/wiki/ESpeak

	- - https://www.isi.edu/~carte/e-speech/synth/index.html


- https://en.wikipedia.org/wiki/Sinsy
	- > **Sinsy** (**Sin**ging Voice **Sy**nthesis System) (しぃんしぃ) is an online [Hidden Markov model](https://en.wikipedia.org/wiki/Hidden_Markov_model "Hidden Markov model") (HMM)-based singing voice synthesis system by the [Nagoya Institute of Technology](https://en.wikipedia.org/wiki/Nagoya_Institute_of_Technology "Nagoya Institute of Technology") that was created under the [Modified BSD license](https://en.wikipedia.org/wiki/BSD_licenses "BSD licenses").[](https://en.wikipedia.org/wiki/Sinsy#cite_note-1)
	- http://www.sinsy.jp/


- https://www.audiolabs-erlangen.de/resources/MIR/2015-ISMIR-LetItBee
	- http://labrosa.ee.columbia.edu/hamr_ismir2014/proceedings/doku.php?id=audio_mosaicing

- https://www.danieleghisi.com/phd/PHDThesis_20180118.pdf
- https://musicinformationretrieval.com/

Also there were mogees - https://www.youtube.com/watch?v=G_hBhORGE6Y
- [[]]

- python concatenative
	- https://github.com/carthach/PyConcat

AudioFlux
- https://libaudioflux.github.io/audioFlux/
- **[`audioFlux`](https://github.com/libAudioFlux/audioFlux)** is a deep learning tool library for audio and music analysis, feature extraction. It supports dozens of time-frequency analysis transformation methods and hundreds of corresponding time-domain and frequency-domain feature combinations. It can be provided to deep learning networks for training, and is used to study various tasks in the audio field such as Classification, Separation, Music Information Retrieval(MIR) and ASR etc.
- [[audio.ai]]


-------------
## Articles
- https://direct.mit.edu/comj/article/41/2/21/94580/Rhythmic-Concatenative-Synthesis-for-Electronic
- https://danmackinlay.name/notebook/concatenative_synthesis.html

## See Also
- http://diemo.free.fr/wp/?page_id=2
## Adjacent
- [[incubation.audio.sample.managment]]
- [[tools.tts]]
- [[incubation.ai.audio]]
- [[ai.music]]
- [[ai.narration]]
- [[incubation.ai.audio]]
- [[incubation.concepts.DatabaseArt]]
- [[fulldocs.Speed and Information Cyberspace Alarm]]
- [[fulldocs.voit.files.managment]]
- [[concepts.playlisting]]