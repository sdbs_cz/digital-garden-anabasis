# [[las]].inspirations

This selection is made through technical and conceptual premises.Content pool is placed @ [[las.quotes]].

## Tech
### Context / Focus screens
- https://szymonkaliski.com/notes/pixel-space-and-tools/
- https://patrickbaudisch.com/projects/focuspluscontextscreens/index.html

### Zooming
#### [Pixels @ xkcd](https://xkcd.com/1416/)
![[xkcd-zooming.png]]

#### [Eagle Mode](http://eaglemode.sourceforge.net/)

#### [Zooming user interface @ Wikipedia](https://en.wikipedia.org/wiki/Zooming_user_interface)

### Infinite Canvas
Now included also in [[tools.obsidian]] and others...

#### Muse App 
https://museapp.com/


## People
### [[people.TedNelson]]
- [wiki](https://en.wikipedia.org/wiki/Ted_Nelson)


![[nelson_intertwingled.png]]
![[nelson-hypercomics.png]]

#### Literary Machines
>![[nelson-literarymachines-reorganization.png]]

>Ways of reading
>![[nelson-literarymachines-reading.png]]


#### Xanadu
![[nelson-xanadu.png]]
- https://maggieappleton.com/xanadu-patterns/ /[[people.MaggieAppleton]]
- [[concepts.parallel textface]]
- [[concepts.hypertext]]

### [Aby Warburg](https://en.wikipedia.org/wiki/Aby_Warburg)
- [[people.AbyWarburg]]
- [[concepts.archives.art]]
- [Atlas Mnemosyne Online](https://warburg.library.cornell.edu)

### Lev Manovich
- http://manovich.net/index.php/exhibitions

### [Jorge Luis Borges](https://en.wikipedia.org/wiki/Jorge_Luis_Borges "Jorge Luis Borges")
#### The Library of Babel
- https://en.wikipedia.org/wiki/The_Library_of_Babel
- https://libraryofbabel.info/

### [[people.VilemFlusser]]
- [wikipedia](https://en.wikipedia.org/wiki/Vil%C3%A9m_Flusser)
- https://sites.rhizome.org/flusserPEB/
- https://hybridspacelab.net/project/villem-flusser/




------------------------------
>![[Pasted image 20230608211854.png]]
> - https://www.researchgate.net/publication/224285687_Visualizing_uncertainty_for_improved_decision_making