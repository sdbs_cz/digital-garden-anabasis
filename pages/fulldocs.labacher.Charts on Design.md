---
created: 2022-01-23T10:08:26 (UTC +01:00)
tags: []
source: https://www.christophlabacher.com/notes/charts-on-design
author: 
---

# Charts on Design — Christoph Labacher

> ## Excerpt
> I have a soft spot for the aesthetics of charts scanned from old books. This is an ongoing, eclectic collection of my favorite charts on design.

---
20\. December 2021

I have a soft spot for the aesthetics of charts scanned from old books. **This is an ongoing, eclectic collection of my favorite charts on design.**

___

## Designer’s Knowledge

![[designers-knowledge.png]]

“Designer’s Knowledge” from Eder, 1966: Definitions and Methodologies. In Gregory, The Design Method \[p. 20\]

___

## Ideal of Education

![[ideal-of-education.png]]

“Ideal of Education” from Eder, 1966: Definitions and Methodologies. In Gregory, The Design Method \[p. 19\]

“Before any work of human skill can be produced it must be imagined. \[…\] The basis of much human activity lies in a full and broad education, consisting of a balanced view of the sciences, technologies, the arts, and the crafts (Figure 3.1). Two dangers of a lacking balance in education are obvious: firstly of overlooking possible connections, analogies, etc., and secondly of overlooking or ignoring the fellow human being.” (1)Eder, 1966

___

## Schematic of the Ulm School of Design’s Curriculum

![[hfg-ulm.png]]

“A diagramme from a 1951 concept script illustrates the school’s universal approach \[Scholl 1951\]” from Oswald, 2012: The Information Department at the Ulm School of Design

“The time for exclusive professional specialization is over. Politics, science, art, and economics must be viewed in their integral relationship. Education for knowledge must be replaced by education towards unprejudiced universal thinking.” (2)Scholl, 1950; via Oswald, 2012

___

## Graphical Representation of Design Method

![[design-method.png]]

“Graphical Representation of Design Method” from McCrory, 1966: The Design Method in Practice. In Gregory, The Design Method \[p. 16\]

“Although the design method is similar to the scientific method, it has not been as carefully defined nor historically as well established. Nevertheless, the design method is as inherent to the design process as the scientific method is to scientific exploration. \[…\] The starting point of the design method is more comprehensive than that of the scientific method. Unlike fundamental scientific research, design is motivated by need rather than by curiosity. Therefore, in addition to requiring knowledge of the state of the technical art, the design method requires recognition of a need which warrants an investment of effort and funds.” (3)McCrory, 1966

___

## The Design Problem

![[design-problem.png]]

“The Design Problem” from Papanek, 1972: Design for the real world \[p. 68\]

“The designer-planner shares responsibility for nearly all of our products and tools and hence nearly all of our environmental mistakes. He is responsible either through bad design or by default: by having thrown away his responsible creative abilities, by ‘not getting involved,’ or by ‘muddling through.’” (4)Papanek, 1972

___

## Victor Papanek’s Function Complex

![[function-complex.png]]

“Function Complex” from Papanek, 1972: Design for the real world \[p. 26\]

![[function-complex-2.png]]

“Function Complex” \[second edition\] from Papanek, 2011: Design for the real world \[p. 7\]

___

## A Rhizomatic Map of Visual Communication

![[rhizomatic-map-of-visual-communication.png]]

“A Rhizomatic Map of Visual Communication” from Moriarty & Barbatsis, 2004: From an Oak to a Stand of Aspen: Visual Communication Theory Mapped as Rhizome Analysis. In Smith et al., Handbook of Visual Communication: Theory, Methods, and Media \[p. xx\]

“\[T\]he rhizome is a dynamic, decentered, system or network. It is a structure without any controlling center of hierarchy, a kind of self-reproducing multiplicity that cannot be understood as a single organization or localized in a particular territory. It’s also generative in that the offshoots or runners become freestanding plants.” (5)Moriarty & Barbatsis, 2004

___

## Typographic Tokens

![[typographic-tokens.png]]

“Typographic Tokens” from Bonsiepe, 1968: A method of quantifying order in typographic design. In ulm, 21 \[p. 33\]

“There is a platitude which might be very well applied to the activities of the designer, namely that a state of order is preferable to a state of disorder. For, among many other things, designing means creating order, means putting structure into an array of objects or signs, means reducing disorder and arranging elements into a whole that makes sense.” (6)Bonsiepe, 1968

___

## Triangulating Lost Knowledge

This chart is not about design, but about knowledge in general:

![[feynman.png]]

“Forgotten facts can be recreated by triangulating from known facts” from Feynman et al., 2013: Feynman’s tips on physics \[p. 59\]

“Now, suppose that something happened to your mind, that somehow all the material in some region was erased, and there was a little spot of missing goo in there. The relations of nature are so nice that it is possible, by logic, to ‘triangulate’ from what is known to what’s in the hole. \[…\] **And you can re-create the things that you’ve forgotten perpetually — if you don’t forget too much, and if you know enough. In other words, there comes a time \[…\] where you’ll know so many things that as you forget them, you can reconstruct them from the pieces that you can still remember.** It is therefore of first-rate importance that you know how to ‘triangulate’ — that is, to know how to figure something out from what you already know.” (7)Feynman et al., 2013

___

## Jencks’ theory of evolution — an overview of 20th Century architecture

![[jencks-theory-of-evolution.png]]

“Jencks’s ‘The Century is Over, Evolutionary Tree of Twentieth-Century Architecture’ with its attractor basins” from Jencks, 2000: Jenck’s theory of evolution: An overview of twentieth-century architecture

“Now that the century is over it is time to ask what it meant for architecture. \[…\] The main narrative does not belong to any building type, movement, individual or sector. **Rather, it belongs to a competitive drama, a dynamic and turbulent flow of ideas, social movements, technical forces and individuals all jockeying for position.** Sometimes, a movement or an individual may be momentarily in the public eye and enjoy media power, but such notoriety rarely lasts for more than five years and usually for not more than two.” (8)Jencks, 2000

___

### References

-   Bonsiepe, G. (1968). [A method of quantifying order in typographic design. Ulm, 21](https://monoskop.org/images/a/ae/Ulm_21.pdf#page=26), 24–31.
-   Eder, W. E. (1966). [Definitions and Methodologies](https://doi.org/10.1007/978-1-4899-6331-4_3). In S. A. Gregory (Ed.), The Design Method (pp. 19–31). Springer US.
-   Feynman, R. P., Gottlieb, M. A., Leighton, R. & Sands, M. L. (2013). Feynman’s tips on physics: Reflections, advice, insights, practice: a problem-solving supplement to the Feynman lectures on physics. Basic Books.
-   Jencks, C. (2000). [Jenck’s theory of evolution: An overview of twentieth-century architecture](https://www.architectural-review.com/archive/jencks-theory-of-evolution-an-overview-of-20th-century-architecture). Architectural Review, 208(1241), 76–79.
-   McCrory, R. J. (1966). [The Design Method in Practice](https://doi.org/10.1007/978-1-4899-6331-4_2). In S. A. Gregory (Ed.), The Design Method (pp. 11–18). Springer US.
-   Moriarty, S., & Barbatsis, G. (2004). From an Oak to a Stand of Aspen: Visual Communication Theory Mapped as Rhizome Analysis. In K. L. Smith, S. Moriarty, K. Kenney, & G. Barbatsis (Eds.), [Handbook of Visual Communication: Theory, Methods, and Media](https://doi.org/10.4324/9781410611581) (pp. xi–xxii). Routledge.
-   Oswald, D. (2012). [The Information Department at the Ulm School of Design](https://www.proceedings.blucher.com.br/article-details/the-information-department-at-the-ulm-school-of-design-8554). Blucher Design Proceedings, 1(1), 46–50.
-   Papanek, V. J. (2011). Design for the real world: Human ecology and social change (2nd. ed. compl. rev., repr). Thames and Hudson.
-   Papanek, V. J. (1972). Design for the real world: Human ecology and social change (1st American ed.). Pantheon Books.

Eder, 1966

Scholl, 1950; via Oswald, 2012

McCrory, 1966

Papanek, 1972

Moriarty & Barbatsis, 2004

Bonsiepe, 1968

Feynman et al., 2013

Jencks, 2000
