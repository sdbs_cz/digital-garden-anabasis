# [[tools.obsidian]].transclusion

## transclusion
==> [[people.TedNelson]]

## header link
`[[page#section]]`
>You can also link to specific headers in files. Start typing a link like you would normally. When the note you want is highlighted, press `#` instead of `Enter` and you'll see a list of headings in that file. Continue typing or navigate with arrow keys as before, press `#`again at each subheading you want to add, and `Enter` to complete the link.
> - https://help.obsidian.md/How+to/Internal+link

### reference / transclusion
`![[page#section]]` as with images

## block level link / reference
block level transclusion since Obsidian 0.9.5, [link to the docs](https://publish.obsidian.md/help/How+to/Link+to+blocks). An example
`![[filename#^dcf64c]]`.

### caret == ^

- ‸ ⁁ ⎀  ^
- https://en.wikipedia.org/wiki/Caret

- `shift + 6` 
	- 6 not on numpad
- 