# [[fulldocs]].twitter.booksasmaps
- [Tweet](https://twitter.com/i/status/1378550004680249348) by [@andy_matuschak](https://twitter.com/andy_matuschak) on [[April 4th, 2021]]:
- Books as maps! "Today the book is already… an outdated mediation between two different filing systems. For everything that matters is to be found in the card box of the researcher who wrote it, and the scholar studying it assimilates it into his own card index." —Walter Benjamin [pic.twitter.com/6l8zaGver6](https://twitter.com/andy_matuschak/status/1378550004680249348/photo/1)
- (In Reflections, p. 78; via Noah Wardrip-Fruin's interesting foreword to Engelbart's Augmenting Human Intellect in The New Media Reader)
- The odd thing about this framing is that the book doesn't really perform the mediation! It's a sort of serialization of the card box of the researcher who wrote it. The reader has to "come to terms with the author" and then "bring the author to terms" to map to their own.
- Also of course this framing, taking too seriously, ignores the role of narrative—a common issue with hypertext-dreamer interpretations of this kind of comment. Prose is not just a structured sequence of claim-atoms!
- But of course, Benjamin knows this. From the preceding page:
  - "CAUTION: STEPS
  - Work on good prose has three steps: a musical stage when it is composed, an architectonic one when it is built, and a textile one when it is woven."

[[people.WalterBenjamin]]
[[people.AndyMatuschak]]
[[concepts.map]]

# [[fulldocs]].twitter.selforganizing-system

- [Tweet](https://twitter.com/i/status/1366402601789448196) by [@gordonbrander](https://twitter.com/gordonbrander) on [[March 1st, 2021]]:
- “Self-organizing system” becomes meaningless unless the system is in close contact with an environment... [pic.twitter.com/zJkIVMKkTR](https://twitter.com/gordonbrander/status/1366402601789448196/photo/1)
- ![Image](https://pbs.twimg.com/media/EvZv7KJVEAI1ASw?format=jpg&name=small)	
- Living organisms can’t survive in their own waste products.
  - Alan Kay
- Very little goes to waste in a rainforest. [twitter.com/startuployalis…](https://twitter.com/startuployalist/status/990845067492388864)
- “A forest has no weeds.”
  - “Any sufficiently advanced technology is indistinguishable from nature.”
  - “Pollution is nothing but the resources we are not harvesting. We allow them to be dispersed because we've been ignorant of their value.” (Buckminster Fuller.)
- There’s a kind of fractal challenge to recycling, caused by the Anna Karenina problem “all happy resources are alike; each unhappy waste product is unhappy in its own way.”
- Because of the fractal shape of the problem, it’s hard to imagine how to get to a circular economy past the low-hanging fruit.
  - OTOH markets, like all evolutionary systems, are good at evolving fractal niches. Perhaps if there were incentives to close loops... 🤔
- What an organism feeds on is negative entropy. To put it less paradoxically, the essential thing in metabolism is that the organism succeeds in freeing itself from all the entropy it cannot help producing while alive.
  - Schroedinger
- There are local and temporary islands of decreasing entropy in a world in which the entropy as a whole tends to increase, and the existence of these islands enables some of us to assert the existence of progress.
  - Norbert Wiener
- Our pale blue soap bubble island of decreasing entropy, continually finding new creative ways to soak up that wash of warm sunlight.
