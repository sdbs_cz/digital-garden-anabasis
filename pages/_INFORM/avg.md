======= AUDIO VIDEO GRAPHICS =======

1.  This is confusing list!
2.  Use Table of Content on the right and guide yourself. Fix stuff.

Digital - essential
-------------------

-   digital foundations - FLOSS guide -
    <http://write.flossmanuals.net/digital-foundations/introduction/>

AUDIO
-----

-   [Audio 101](Audio 101)
-   Handbooks and tips
    -   <https://ubuntustudio.org/audio-handbook/>
    -   <https://vintageking.com/blog/2019/12/mastering-engineers-checklist-for-mix-engineer/>
    -   <http://openmusictheory.com/contents.html>
-   Lists
    -   <https://github.com/ad-si/awesome-music-production>
    -   <https://github.com/faroit/awesome-python-scientific-audio>
    -   <https://github.com/olilarkin/awesome-musicdsp>


-   <http://www.codingwoman.com/generative-music/>
-   <https://zrna.org/>
-   <http://theaudioarchive.com/TAA_Resources_File_Size.htm>


-   VCV rack
    -   <https://cdm.link/2020/06/free-modular-recipesin-vcv-rack-with-jw-modules/>

### crucial

-   audacity
    -   <http://write.flossmanuals.net/audacity/introduction/>
-   foobar
-   [inform.reaper](inform.reaper.md) \[advanced\]

### semi-bizzare

-   MSDP -<https://www.musicsdp.com/>
    -   a free and open source program for Multimedia Synthesis, Design,
        and Performance
-   Borderlands -
    <https://ccrma.stanford.edu/~carlsonc/256a/Borderlands/#Downloads>
    -   The instrument may be performed by creating and destroying new
        clouds and dynamically moving them around the landscape. The
        landscape itself may also be shifted.
-   Grainstation-C -
    <https://github.com/micah-frank-studio/Grainstation-C>
    -    is an open-source, granular performance workstation designed to
        build realtime, evolving sound sculptures with optional
        ambisonics. It seamlessly integrates with a Novation
        LaunchControl XL Mark 2 (easily modifiable for any other
        controller) and can processes 4 disk tracks and 3 live input
        streams.
-   Audio DiffMaker - <https://www.libinst.com/Audio%20DiffMaker.htm>
    -   signal difference extraction software

### bizzare

-   Traktion - <https://www.tracktion.com/develop/tracktion-engine>
    -   we've been refactoring our DAW engine to make it more modular,
        composable, customizable and simpler to use for a wide range of
        audio applications. Our aim is to take care of all the difficult
        and time consuming aspects of developing audio apps so you can
        focus on building great features and user interfaces people will
        love.
-   Raven Lite -<http://ravensoundsoftware.com>
    -    is a free software program that lets users record, save, and
        visualize sounds as spectrograms and waveforms.
-   <https://www.reddit.com/r/sounddesign/comments/fozts6/what_is_a_good_technique_to_fake_3d_audio_in/>
-   pyAudioAnalysis - <https://github.com/tyiannak/pyAudioAnalysis>
    -   A Python library for audio feature extraction, classification,
        segmentation and applications
-   thid dx7 cartridge doesnt exist -
    <https://www.thisdx7cartdoesnotexist.com/>

### Ableton

#### ABL/M4L

-   <https://gumroad.com/l/websamplr>
-   <http://www.maxforlive.com/library/device/445/cc-sender-8>
-   instant afro -
    <https://www.beatlabacademy.com/instant-afro-make-clave-beats-like-an-expert-afrobeat-reggaeton-dancehall/>

#### ABL tutorials

-   Two Minute Tutorials - Ableton Live
    -   https://www.youtube.com/playlist?list=PLaGozABWWWKhCU6NMazewwz5-8MbHrk6\_

### Music Production + VSTs

-   lists
    -   <https://github.com/smyth64/music-production-links>
    -   <https://blog.landr.com/best-free-vst-plugins/>
    -   <https://www.soundshockaudio.com/10-best-free-reverb-vst-plugins/>
-   VSTi x64
    -   Labs
    -   Ribs - granulator
        -<https://www.kvraudio.com/forum/viewtopic.php?f=1&t=486995>
-   VSTe x64
    -   pitchproof
-   ??
    -   <https://www.tokyodawn.net/tokyo-dawn-labs-discontinued-products/>

### audio files

     * https://bedroomproducersblog.com/2020/02/14/free-sound-effects/
     * Anechoic Data - a collection of audio recordings captured in anechoic conditions. The collection includes recodings of early musical instruments, singing, speech and more. 
       * https://www.openair.hosted.york.ac.uk/?page_id=310
     * GDC free sound banks, four volumes
       * https://sonniss.com/gameaudiogdc18  

### IR data

-   <https://www.openair.hosted.york.ac.uk/?page_id=36>
-   <http://www.morevox.com/web/freebbie.html>
-   <http://www.echothief.com/>

VIDEO
-----

[Video 101](Video 101)

-   <https://ebsynth.com/>
-   <https://handmadecinema.com/>

<https://github.com/yemount/pose-animator/>
<https://github.com/yemount/pose-animator/> Pose Animator takes a 2D
vector illustration and animates its containing curves in real-time
based on the recognition result from PoseNet and FaceMesh. It borrows
the idea of skeleton-based animation from computer graphics and applies
it to vector characters

### Live Video // Broadcasting

-   HapMap - <https://gethapmap.com/en/> - Ultra-Lightweight Projection
    Mapping Software
-   Openmapper - <https://www.openmapper.ch/> - tool to make video
    mapping,every platform, scaleable, syncable, opensource
-   resolume plugins
    -   resolume ctrl - <https://github.com/onomuta/resolumeCtrl> -
        chaser
-   smode tutorials
    -   Smode 8.5 Tutorial : Emit particles from a video -
        <https://www.youtube.com/watch?v=1tzQv0wwdQ0>
    -   Smode Station 8 5 tutorial : Wireframe renderer -
        <https://www.youtube.com/watch?v=ghVcHiXGlyc>
-   Hedron - <https://github.com/nudibranchrecords/hedron> - Perform
    live shows with your three.js creations.
-   Larix - <https://softvelum.com/larix/> Larix mobile streaming
-   <https://wmspanel.com/nimble/p2p_streaming> Nimble Streamer
    capabilities for peer-to-peer (P2P) streaming
-   <https://nageru.sesse.net/>

### Dead Video

-   <https://github.com/leandromoreira/digital_video_introduction>
    -   <https://github.com/leandromoreira/digital_video_introduction#how-does-a-video-codec-work>
-   Synopsis - suite of open source software for computational
    cinematography
    -   <https://synopsis.video/cinemanet/>
-   txs - Generate and compare x264 test encodings with different
    settings
    -   <https://github.com/plotski/txs>
-   OpenCV tutorials
    -   
        <https://www.learnopencv.com/face-morph-using-opencv-cpp-python/>

### NDI tools

-   <https://github.com/rezonant/grandiose>
-   <http://www.zenvideo.co.uk/ndi.htm>
-   <https://www.medialooks.com/video-transport>

<https://cineshader.com/about>

3D
--

### blender

-   bone-studio blender fork


-   <http://write.flossmanuals.net/blender/introduction/>
-   <http://peca.jinak.cz/2020/05/blender-3d-kurzor>


-   We Taught an AI To Synthesize Materials \| Two Minute Papers \#251 -
    <https://www.youtube.com/watch?v=cnquEovq1I4>
-   Generative Modeling with Modifiers (Blender 2.8) -
    <https://www.youtube.com/watch?v=qC5eX_WZU2E>
-   <https://www.blendernation.com/2020/01/21/command-line-rendering-made-easy/>
-   <http://sinestesia.co/blog/art/crosshatching-with-eevee/>
-   <https://render.otoy.com/forum/viewtopic.php?f=7&t=72241>
-   <https://blender-addons.org/sorcar-addon/>
-   <https://www.youtube.com/watch?v=_9dEqM3H31g>
-   [blender lipsync](https://youtu.be/tCxKbaUKbg0)


-   [PIFuHD: Multi-Level Pixel-Aligned Implicit Function for
    High-Resolution 3D Human
    Digitization](https://shunsukesaito.github.io/PIFuHD/)
-   <https://www.youtube.com/watch?v=icqLFiwpNlM>
-   [Mixer is a Blender addon developed at Ubisoft Animation Studio for
    Real Time Collaboration in 3D edition. It allows multiple Blender
    users to work on the same scene at the same time. Thanks to a
    broadcasting server that is independent from Blender, it is also
    possible to implement a connection for other 3D editing
    softwares.](https://github.com/ubisoft/mixer)

### 3d printing optimilizations

-   <https://blender.stackexchange.com/questions/18916/how-to-remove-interior-faces-while-keeping-exterior-faces-untouched>
-   <https://blender.stackexchange.com/questions/31467/how-to-reduce-vertex-count-on-a-mesh>
-   3D Print Toolbox (Addon)

### blender addons

-   MB-Lab - <https://mb-lab-community.github.io/MB-Lab.github.io/>
    -   MB-Lab is a Blender addon that creates humanoid characters
-   RandoMesh - <https://github.com/mantissa-/RandoMesh>
    -   A Blender addon to add randomized geometry to any mesh. Supports
        Blender 2.80.
-   PolyQuilt - <https://blender-addons.org/polyquilt-addon/>
    -   edit mode like creating faces, knife tool, fan, edge-loops, etc.
        It does this with ONLY the left-mouse-button.
-   Loop Tools - <https://www.youtube.com/watch?v=W2MjvKy1yCo>
    -   really useful for editing meshes quickly and easily.
-   ByGen - <https://gumroad.com/l/BY-GEN>
    -   BY-GEN is an addon designed to simplify the process of creating
        non-destructive, generative modeling styles
        <https://youtu.be/PwfSlPpBfbw>
-   Edge Flow - <https://github.com/BenjaminSauder/EdgeFlow>
    -   This tool has two modes, the first makes each selected edge loop
        linear, the later works on edge rings and straightens them and
        adjusts each edge length.
-   GrowthNodes - <https://github.com/hsab/GrowthNodes>
    -   GrowthNodes is a Blender plugin for generative content creation
        and simulation of organic growth processes on polygonal
        surfaces. It can be utilized for both destructive and
        non-destructive content generation.
-   Pulverize - <https://github.com/sciactive/pulverize>
    -   Pulverize is a script for rendering video from Blender using
        multiple processes.
-   Tissue - Tissue -
    <http://www.co-de-it.com/wordpress/code/blender-tissue> \#tessalate
    -   Blender\'s add-on for computational design by Co-de-iT
    -   <https://www.youtube.com/watch?v=pVNYyJeLGZI> \#tutorial
-   Render to Print 0.3 - <https://github.com/zebus3d/Render_to_Print>
    -    With Render to print you can set the size of the render for a
        print!!.
-   QBlocker - <https://gumroad.com/l/gOEV>
    -   Qblocker is a great blender add-on for creating geometry
        interactive
-   Anti - Seam
    -   <https://blendermarket.com/products/anti-seam>
-   AddRoutes - <http://www.jpfep.net/pages/addroutes/>
    -    gathering AddMIDI and AddOSC in the same package. As a novelty
        an application for smartphones and tablets (Blemote)

### Blender - geodata, CAD

-   BlenderGIS - <https://github.com/domlysz/BlenderGIS>
    -   Blender addons to make the bridge between Blender and geographic
        data
        -   <https://www.reddit.com/r/blender/comments/8ydfpd/texas_population_density_by_county_2017_projection/>
-   CAD -&gt; to blender conversions
    -   
        <https://www.archvizblender.com/blog/how-to-import-dwg-files-into-blender>
    -   ODA File Converter
        -   Convert between .dwg and .dxf file formats with this Windows
            application. With wildcard support for input file
            specification, it allows .dxf files to be used as input.
-   ??
    -   The best way to import CAD file in Blender -
        <https://www.youtube.com/watch?v=OX-EWAndFeY>
    -   BOOK -
        <https://www.blender3darchitect.com/blender-2-8-for-architecture/>


    * 

### others 3D

-   <https://www.voxedit.io>
-   meshlab
-   smode synth
-   <https://nunustudio.org/>
-   3d and archeology
    -   <https://computationalarchaeology.wordpress.com/2013/01/20/3d-modelling-in-archaeology/>

### Textures

<https://hdrihaven.com/> Hdri 20k -- Texturas 8k (FREE PBR)

<https://cc0textures.com/> 8k (FREE )

<https://texturefun.com/> 4k (FREE )

<https://www.textures.com/> Gratuito y paga

<https://www.cgbookcase.com/> 1k - 2k - 3k - 4k (FREE )

<https://freestocktextures.com/> Texturas en general (FREE )

<http://texturelib.com/> (FREE )

<http://texturify.com/> Texturas en general (FREE )

AI, deeplearning, etc.
----------------------

### General, toolsets

-   magenta
    -   <https://magenta.tensorflow.org/>
    -   <https://www.youtube.com/watch?v=2FAjQ6R_bf0>
    -   <https://sandcobainer.github.io/audioblog-/max/2019/02/04/Google-Magenta.html>
    -   <https://www.twilio.com/blog/generate-music-python-neural-networks-magenta-tensorflow>
-   visual AI
    -   Artificial-Art
        -   <https://github.com/pearsonkyle/Artificial-Art>
    -   DeepFaceLab
        -   is a tool that utilizes machine learning to replace faces in
            videos.
        -   <https://github.com/iperov/DeepFaceLab>
    -   DeepPrivacy
        -   DeepPrivacy is a fully automatic anonymization technique for
            images.
        -   <https://github.com/hukkelas/DeepPrivacy>


-   Audio AI
    -   Spleeter


          * Spleeter is the Deezer source separation library with pretrained models written in Python and uses Tensorflow.
          * https://github.com/deezer/spleeter
      * Jukebox
        * https://colab.research.google.com/github/openai/jukebox/blob/master/jukebox/Interacting_with_Jukebox.ipynb  
    * semi-bizzare
      * Melodrive
        * https://assetstore.unity.com/packages/audio/music/melodrive-lite-beta-129271
      * Wekitrainer
        * The Wekinator is free, open source software originally created in 2009 by Rebecca Fiebrink. It allows anyone to use machine learning to build new musical instruments, gestural game controllers, computer vision or computer listening systems, and more.
        * http://www.wekinator.org/ 
        * https://maxforlive.com/library/device/5628/wekitrainer #m4l  
     * Lists
       * https://github.com/ybayle/awesome-deep-learning-music
       * https://www.reddit.com/r/AI_Music/

### Online tools

-   <https://www.remove.bg/> - Remove Image Background: 100%
    automatically -- in 5 seconds -- without a single click -- for free.
-   <https://letsenhance.io/> - Neural network image super-resolution
    and enhancement
-   <https://www.designwithai.com/> - to create beautiful logo packages
    for your brand.
-   <https://deepai.org/machine-learning-model/text2img> - TXT 2 IMG
-   <https://dreamscopeapp.com> -
-   <https://colourise.sg/#colorize>

### Tutorials, workflows, \...

-   <https://www.jackalope.tech/how-artists-can-set-up-their-own-neural-network-part-2-neural-network-install/>
    \#deepstyle
-   <https://www.reddit.com/r/deepdream/comments/954h2w/voltax3_script_release_the_best_hq_neuralstyle>
-   <https://www.reddit.com/r/deepdream/comments/c5wjet/powerplant_painted_sky_first_use_of_volta_3/>
-   <https://mc.ai/complete-guide-to-setup-magenta-tensorflow-on-windows/>
    \#styletransfer
-   upscaling
    -   <https://www.reddit.com/r/GameUpscale/comments/a7msxo/getting_started/>
-   lists
    -   <https://www.reddit.com/r/AIfreakout/comments/c2qh10/useful_links_to_generate_your_own_ai_freakouts/>
    -   <https://news.ycombinator.com/item?id=20514446>
-   StyleGAN2 - mapping music to facial expressions in real time -
    <https://www.youtube.com/watch?v=A6bo_mIOto0&list=WL&index=2>

AV Utilities
------------

### diagrams

-   <https://www.graphviz.org/> - online
-   <http://dia-installer.de/> - install

### Expansion - lights - projections

-   <http://fulldomevjs.com/fdvj/> Freeframe plugins
-   <https://discourse.vvvv.org/t/schema-streamlined-visual-programming-for-physical-devices/17545>

### Media anotation, referencing

-   Anvil
    -    It offers multi-layered annotation based on a user-defined
        coding scheme. During coding the user can see color-coded
        elements on multiple tracks in time-alignment. Some special
        features are cross-level links, non-temporal objects, timepoint
        tracks, coding agreement analysis, 3D viewing of motion capture
        data and a project tool for managing whole corpora of annotation
        files. Originally developed for gesture research in 2000, ANVIL
        is now being used in many research areas including
        human-computer interaction, linguistics, ethology, anthropology,
        psychotherapy, embodied agents, computer animation and
        oceanography.
    -   <https://www.anvil-software.org/>
-   ANT
    -   VideoAnt is a web-based video annotation tool for mobile and
        desktop devices. Use VideoAnt to add annotations, or comments,
        to web-hosted videos. VideoAnt-annotated videos are called
        "Ants". VideoAnt is a web-based video annotation tool for mobile
        and desktop devices. Use VideoAnt to add annotations, or
        comments, to web-hosted videos. VideoAnt-annotated videos are
        called "Ants". Export your annotations in a variety of data
        formats. You can even embed your Ants on a personal website,
        learning management system, or anywhere HTML is allowed.
    -   <https://ant.umn.edu/>
-   PureRef


      *PureRef is a stand-alone program for Windows, Mac and Linux that keeps track of your images. Whether you're gathering inspiration, making mood boards or need reference images for your painting or 3D model, PureRef is there so you can focus on creating.
      * https://www.pureref.com/index.php 
    * Quixel bridge
      * With its custom import, channel packing, and automated export features, Bridge allows you to manage your assets in one place like never before. The days of manual shader setup, image conversion or sensitive folder structures are over; welcome to the future of asset management.
      * https://quixel.com/bridge
    * Lignes de Temps
      * The Lignes de Temps software takes advantage of the analysis and synthesis possibilities offered by digital media. Inspired by the « timelines » commonly used on digital editing benches, Lignes de Temps offers a graphic representation of a film, revealing from the outset, and in extenso, its cuttings. Lignes de Temps offers in this a new access to the film, substituting for the logic of constrained scrolling that constitutes the experience of any movie viewer, and for the purposes of analysis, the « mapping » of a temporal object. Also, by selecting a segment of a timeline, the user has direct access to the corresponding clip or sequence in the movie, a sequence that can be described and analyzed by textual, audio, video, or documented by images or internet links.
      * https://www.iri.centrepompidou.fr/outils/lignes-de-temps-2/

#### Online unsorted

      * https://wevu.video/
      * https://www.reclipped.com/
      * https://www.hippovideo.io/
      * https://heraw.com/en/register

Creative Coding and Livecoding
------------------------------

-   <https://github.com/toplap/awesome-livecoding>
-   <https://github.com/pjagielski/awesome-live-coding-music>

<https://nodered.org/>

### Languages

-   [SuperCollider](supercollider)
    -   Overtone, FoxDot, \...
-   Tidal <https://tidalcycles.org/index.php/Welcome>
    -   patterns, patterns, patterns
-   Sonic Pi


        * https://sonic-pi.net/
    * Orca
      * is an esoteric programming language designed to quickly create procedural sequencers, in which every letter of the alphabet is an operation, where lowercase letters operate on bang, uppercase letters operate each frame.
      * https://github.com/hundredrabbits/Orca
    * alive - livecoding with persistent expressions
      * https://alive.s-ol.nu/index.html

### Collaboration tools

-   Troop - <https://github.com/Qirky/Troop>
    -   Troop is a real-time collaborative tool that enables group live
        coding within the same document across multiple computers.
        Hypothetically Troop can talk to any interpreter that can take
        input as a string from the command line but it is already
        configured to work with live coding languages FoxDot,
        TidalCycles, and SuperCollider.
-   Tidal-bridge -
    <https://gitlab.com/colectivo-de-livecoders/tidal-bridge>

### Online

-   <https://livecodelab.net/>
-   <https://github.com/ojack/hydra>
    -   Livecoding networked visuals in the browser
        <https://hydra-editor.glitch.me/>

Graphics
--------

-   caesium
-   gimp
-   inkscape
-   darktable
-   stacking and hugin
    -   <https://patdavid.net/2013/01/focus-stacking-macro-photos-enfuse.html>
    -   <http://dossy.org/2007/08/what-is-gimps-equivalent-of-photoshops-median-filter/>
    -   <https://pixls.us/articles/aligning-images-with-hugin/>
    -   <https://www.reddit.com/r/FOSSPhotography/comments/1e0bnz/noise_removal_with_median_stacks_gimpgmic/>
    -   <http://www.microscopy-uk.org.uk/mag/artjun09/rp-stack.html>
    -   <http://hugin.sourceforge.net/tutorials/surveying/en.shtml>

------------------------------------------------------------------------

------------------------------------------------------------------------

------------------------------------------------------------------------

------------------------------------------------------------------------

======= RNDM ==========

<https://www.microsoft.com/en-us/research/blog/microsoft-airsim-now-available-on-unity/?ocid=msr_blog_airsimunity_rd>
AI vs. Unity

<https://medium.com/@derwiki/time-collapsing-photos-with-opencv-a4042163dfb5>
\#openCV \#python

blender game engine -&gt; godot <https://godotengine.org/>

<https://www.jthorborg.com/index.html?ipage=signalizer> - osciloscope

KSX friendly
------------

<https://www.liquidsoap.info/> - Audio & Video Streaming Language

<https://hundredrabbits.itch.io/>

<http://www.lord-enki.net/>

------------------------------------------------------------------------

![](/46652379_2111130242242159_7330182098097012736_n.jpg)
