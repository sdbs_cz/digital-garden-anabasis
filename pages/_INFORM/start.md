:ŇĐ0¤4´u dĺvúő©:k}đ?u %%-% "´" RfŇP iJSHM%yéayéa **LATE ALPHA
STAGE** %ť¨ ¤Ą¦š!Ą¤ 4ÓN¤4ŔJJ( AIKI@„¦šq¦š J(˘€ő¦šSHh4ôĄ4"JJZJ
)´¦Đ1))i)‰i†"q×µ{ p:QAăŠ'Ň°ÓËKH(ÎbÏ4˜ÝéŒë1
cÕ·lÞ¤8G8ùƒBÞN06%/ìSà°yéa'ØCïÑÿôÕh
ŒãD\#¸VÕDU¢sP§úüÚÚœEk&lt;V\*Rç\'™Dd­=þ¡­MÇyéayéayéa

------------------------------------------------------------------------

[internal subpages](/internal/index)

[sdbs digital garden](https://garden.sdbs.cz/)

Resources
=========

Software Guides
---------------

-   [DigitalGardenAnabasis/_INFORM/ffmpeg](DigitalGardenAnabasis/_INFORM/ffmpeg.md)
-   [ipfs](AREAS/DigitalGardenAnabasis/_INFORM/ipfs)
-   [syncthing](AREAS/DigitalGardenAnabasis/_INFORM/syncthing) (work in progress)
-   [ninjam](AREAS/DigitalGardenAnabasis/_INFORM/ninjam) (work in progress)
-   [firefox](firefox) (work in progress)

Scrolls of Guided Chaos
-----------------------

-   [manuals](manuals)
-   [KPZ](KPZ) - (system upkeep and media processing toolbox)
-   [audio video graphics](avg)
-   [documents, text, literature](docs)
-   [web tools list](online toolbox)
-   [digital consumer habits good](consumer habits good)
-   [PRG-WT](prg-wt) (printing resources)
-   [sdbs selfhosting](sdbs selfhosting)

Fractions
=========

-   Self-hosted
    -   [hedgedoc](https://hedgedoc.sdbs.cz)
    -   [etherpad](https://etherpad.sdbs.cz/)
    -   [full crypto pastebin](https://paste.sdbs.cz/)
-   [tools.piratebox](tools.piratebox.md)

------------------------------------------------------------------------

^
`` @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@VpSpheOPUpW6ZWNPOQN0W##6h6NN#EU6#@@@@ @@8SjP66nhpRemWpZpPbpZbpJW6O0Op6mWWVi@@@ @@UeUPUZzwmpz6JWNmZeJEVlhbheQ66ROUjwm@@@ @@}Zejnn6pSbmjWmhOeJjbw=EmzWZ806Ejznm@@@ @@UPpP*6RWpBnpUhejWUb0w zmpS]jSWPSjjh@@@ @@Qmnz~6QOJO(OQn6UUWSmp,bW00R0pN8W8EV@@@ @@8wzJ_neJiz^pE]OSR8O06n88Q8#Zg#ghBWS@@@ @@6nmn ^r<ri,^r}pvN###Q^6#N##zbbVQE?B@@@ @@ZJ]v`==!^rimz_JjmbQB#B#QQ#Qn6n]eir@@@@ @@n?i=(leVUEbgORB@#Q#p8QhJPUjhm6pOW0@@@@ @@^nwJ}]]]JnNOh#@8B#@06B86Qgmp6ehU}N#@@@ @@@bpzinpVlZBOz#6S@##@#@##@Bj#NzWRinN@@@ @@@@B}?JwU(E#w0@wQ#Q#@#SE#@66#jZQen}=@@@ @@@@E<(?J}^np]0Bw6helOe}6ZmV8#zRP]mlv@@@ @@@@lJz?v?vmz]ZjnrVj6WWE8^]n^,}v?(-~v@@@ @@Q]]Jjn]lv(hVVPJmUlVJv]E}}6VVWwV6PPO@@@ @@S<(nlUVzmzejewzjSemev??]iinhmnnz]]m@@@ @@P!=^}l]n}n](vrvOZSZS^:^~!=>r??rr??n@@@ @@#eeVnSn}wennj}JW0g0RJllwVne}vi]BS}g@@@ @@@6O88RbUQOSJPVzQ8WEgphphn]nRB#8w@#@@@@ @@@@@@@@#@@@@#B##@@#@@@@@@##@@@@@@@@@@@@ ``  
^
