Telegram
========

FIXME

Useful
------

### workmode

> If you\'re using multiplatform Telegram Desktop app, try opening your
> settings and typing `workmode`.
>
> A pop-up should show up asking if you\'re willing to enable workmode.
> Submit and enjoy being able to hide the muted chats in your chats
> list.

### Mute

FIXME

### Schedule messages

> Open the Telegram app updated to the version 5.11 or higher.
>
> Enter in the individual or group chat that you want to schedule a
> message for.
>
> Write the message and hold down the send button.
>
> Tap Schedule Message and set a date and time for the content to be
> sent.
>
> Tap the lower button to leave the message scheduled. [^1]

### Archive

FIXME

### Grouping

FIXME

[^1]: <https://blog.en.uptodown.com/schedule-messages-telegram-android/>
