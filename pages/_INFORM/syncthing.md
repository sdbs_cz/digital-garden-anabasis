Syncthing
=========

Essentials
----------

> Syncthing is a **continuous file synchronization program**. It
> synchronizes files between **two or more computers** in real time,
> safely protected from prying eyes.[^1]

Official page: http://syncthing.net  
Windows download: <https://github.com/canton7/SyncTrayzor/releases/>
(You want `SyncTrayzorSetup-x64.exe`)

### What? Why?

Syncthing is basically like Dropbox, MEGA, or Google Drive - except that
there is no central server, and all synchronization happens between the
computers themselves.

Primarily, this means there\'s no capacity limits (except your HDDs), no
big brother spying, no possible shutdown involved. But it also means
there is no way to link Syncthing stuff to the outside, and that the
computers need to be online at the same time to sync.

Its *primary* use-case is when a single person has more computers, and
wants to keep some folders synchronized on all of them.

#### /-\\ use-case

What we use it for, however, is general data sharing - there\'s
semi-public Syncthing folders (listed below). It works well for common
(audio, video) files and projects, for sharing essential software
packages, and for community-powered backups as well. Also for local
(LAN) sync - there is no need for internet, local network is enough.

**WARNING: Because Syncthing wasn\'t really meant for this sort of use,
it requires a bit of carefulness on part of everyone involved. There\'s
no authentication, no permissions, and by default no [](AREAS/DigitalGardenAnabasis/_INFORM/syncthing#versioning). It\'s not too easy to mess up
accidentally, but if you do, it\'s not pretty.**

How?
----

<https://docs.syncthing.net/intro/getting-started.html>

Advanced stuff
--------------

### Send / Receive Only

Send Only & Receive Only limit the folder/file changes either received
or sent from the current machine.

#### Send Only Folder

1.  All changes from other devices in the cluster are ignored.
    1.  If the folder gets out of sync (because of changes made
        elsewhere), a red "Override Changes" button will override **any
        changes made to files through whole cluster**, which will be
        overwritten by the version on this host (any files that don't
        exist on this host will be deleted, and so on).
2.  Good as master for backups.

#### Receive Only Folder

1.  Local changes are not sent to other devices.
2.  Good as slave for backups.

### Versioning

The default behavior is that Syncthing keeps no history of synced files.

This is generally what you want, as it saves space on your machine, but
it can cause data loss, in case someone else accidentally
deletes/modified a file on their machine.

Syncthing, in typical unix neckbeard fashion, has several different
modes of file versioning:

#### Trash Can File Versioning

> This versioning strategy emulates the common "trash can" approach.
> When a file is deleted or replaced due to a change on a remote device,
> it is a moved to the trash can in the `.stversions` folder. If a file
> with the same name was already in the trash can it is replaced.
>
> A configuration option is available to clean the trash can from files
> older than a specified number of days.

#### Simple File Versioning

> With "Simple File Versioning" files are moved to the `.stversions`
> folder (inside your shared folder) when replaced or deleted on a
> remote device. This option also takes a value in an input titled "Keep
> Versions" which tells Syncthing how many old versions of the file it
> should keep.

#### Other modes and more detailed info

<https://docs.syncthing.net/users/versioning.html>

### External HDDs

You can use syncthing with external harddrives. That is good for backup
clusters, huge projects, etc.

On windows it helps to set fixed drive letter to your hardrive, so there
are no mismatches.

> When assigning your drive letter, make sure to choose a letter toward
> the end of the alphabet list. For example, X, Y, or Z --- otherwise
> Windows has the tendency to eventually assign a different letter.
> Also, keep in mind that it will only be the assigned letter on the
> computer you changed it on. If you take your external drive to a
> different PC, Windows will give it a different dynamic letter.[^2]

<https://www.groovypost.com/howto/assign-permanent-letter-removable-usb-drive-windows/>

QnA
---

### Common Gotchas

#### folder marker missing // .stfolder blues

Syncthing marks each synced folder with an empty, hidden directory in
that folder, called `.stfolder`. This tells Syncthing everything is in
order, and this is really the folder which it should be
modifying/uploading changes from.

If that (empty) folder is deleted, Syncthing will vomit with a similar
error message:

    2020-04-02 21:12:50: Error on folder "KSX_OUT" (ksx-out): folder marker missing (this indicates potential data loss, search docs/forum to get information about how to proceed)

This can happen for a couple of reasons:

-   Actual potential data loss - you should [](harddrive.SMART.md) and remember where you keep your backups.
-   Some sort of \"empty folder cleanup\" thing ran on the folder and
    deleted `.stfolder`.
    -   If that\'s the case, and it\'s really the only thing that
        happened, it\'s safe to simply re-create `.stfolder` (by
        right-clicking and selecting \"New Folder\") in that directory.
    -   If not, and you still do this, you\'ve just successfully
        corrupted everyone else\'s Syncthing folders with your broken
        data.
-   Entire folder is missing. See [](AREAS/DigitalGardenAnabasis/_INFORM/syncthing#moving_syncthing_folders)

### CPU

> Hashing, compression and encryption cost CPU time. Also, using the GUI
> causes a certain amount of extra CPU usage to calculate the summary
> data it presents. Note however that once things are in sync CPU usage
> should be negligible.

> To minimize the impact of this, Syncthing attempts to lower the
> process priority when starting up.

> To further limit the amount of CPU used when syncing and scanning, set
> the environment variable GOMAXPROCS to the maximum number of CPU cores
> Syncthing should use at any given moment. For example, GOMAXPROCS=2 on
> a machine with four cores will limit Syncthing to no more than half
> the system's CPU power.

### Whitelisting files

FIXME

### Moving syncthing folders?

FIXME

Semi-public Syncthing folders
-----------------------------

See [syncthing](/internal/syncthing)

[^1]: From <https://syncthing.net/>

[^2]: From
    <https://www.groovypost.com/howto/assign-permanent-letter-removable-usb-drive-windows/>
