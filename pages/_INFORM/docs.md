DOCUMENTS
=========

PILE
----

<https://pile.sdbs.cz>

FIXME

Document post-processing
------------------------

-   [BRISS](https://sourceforge.net/projects/briss/) - PDF cropping
    utility
-   [ScanTailor](http://scantailor.org/) - Complete scan post-processing
    workstation

OCR
---

FIXME

[ocr.space](https://ocr.space//)online ocr -

CZ
--

-   [CZACCENT](https://nlp.fi.muni.cz/cz_accent/) - oháčkování aka
    ohákování
