Manuals
=======

Software
--------

-   Communications
    -   [Firefox](firefox) FIXME
    -   [Telegram](telegram) FIXME
-   Files
    -   [Syncthing](AREAS/DigitalGardenAnabasis/_INFORM/syncthing)
    -   [IPFS](AREAS/DigitalGardenAnabasis/_INFORM/ipfs)
-   Media
    -   [DigitalGardenAnabasis/_INFORM/ffmpeg](DigitalGardenAnabasis/_INFORM/ffmpeg.md)
    -   [OBS Studio](obs) FIXME
    -   [Reaper](inform.reaper.md)
    -   [SuperCollider](supercollider)
-   Systems
    -   [Linux tips](Linux tips)
    -   [harddrive.SMART](harddrive.SMART.md) FIXME

Tech Theory
-----------

-   [Audio 101](Audio 101) \[work in progres\]

Workflows
---------

> wpws proposal

    >> * [[streaming - partner]]
    >> * [[streaming - host]]
    >> * [[moderator - routines]]
