SuperCollider
=============

[avg\#audio](avg#audio)

[avg\#creative\_coding\_and\_livecoding](avg#creative_coding_and_livecoding)

Essential
---------

<https://github.com/supercollider/supercollider>

Granulation
-----------

<http://danielnouri.org/docs/SuperColliderHelp/UGens/Playback%20and%20Recording/TGrains.html>

<http://pustota.basislager.org/_/sc37/Help/Tutorials/Buffer_Granulation.html>

<http://pustota.basislager.org/_/sc-help/Help/Tutorials/Live_Granulation.html>

<https://github.com/Vasileios/Simple_Sc_Granulator_GUI>

<https://lukaprincic.si/development-log/supercollider-granulator-a-gui>

Patterns
--------

<https://doc.sccode.org/Tutorials/Getting-Started/16-Sequencing-with-Patterns.html>

<https://doc.sccode.org/Tutorials/A-Practical-Guide/PG_01_Introduction.html>

<http://distractionandnonsense.com/sc/A_Practical_Guide_to_Patterns.pdf>

<https://github.com/lvm/Repetition.sc>

\+ tidal

To Check
--------

<https://github.com/dkmayer/miSCellaneous_lib>

-   SonoTexto
    -   A SuperCollider class to record and play Buffers. I write this
        Class to improvise with musicians that play acoustic
        instruments, so I can record small Buffers of the instruments in
        the moment of the improvisation. Also it can be used to record
        small fragments of sound environment in order to perform a live
        coding session with sound as material.
    -   <https://github.com/hvillase/sonotexto>

Tools based on SC
-----------------

-   LNX studio
    -   <http://lnxstudio.sourceforge.net/>
    -   <https://cdm.link/2016/04/a-totally-free-daw-and-live-environment-built-in-supercollider-lnx_studio/>
-   TX Modular
    -   <http://www.palemoonrising.co.uk/Downloads.html>
    -   <https://cdm.link/2018/02/tx-modular-vast-free-set-sound-tools-supercollider/>
