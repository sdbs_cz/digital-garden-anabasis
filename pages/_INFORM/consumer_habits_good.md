consumer\_habits\_good
======================

Lists
-----

-   <https://github.com/lowwebtech/list-save-data>
-   <https://humanetech.com/resources/take-control/>
    -   <https://github.com/humanetech-community/awesome-humane-tech>
-   <https://prism-break.org/en/>
-   remotework
    -   <https://wiki.fsfe.org/Activities/FreeSoftware4RemoteWorking>

Android
-------

-   Blokada - adblocker - <https://blokada.org/>
-   odysea - music player
-   f-droid - opensource app repo
-   p!n
-   usb tether ??
-   qr scanner ??

PC / WIN
--------

-   chat/ IM
    -   telegram - <https://telegram.org/>
    -   riot.im - <https://riot.im>
        -   <https://matrix.to/#/!CWuqkPrDOGczUiqsaN:matrix.org> -
            browser client sometimes fails use desktop client
-   [firefox](firefox)


        * Firefox Profile Maker (nice defaults) - https://ffprofile.com/
        * Firefox about:config privacy / control settings - https://gist.github.com/0XDE57/fbd302cef7693e62c769
        * clearURLs - https://addons.mozilla.org/en-US/firefox/addon/clearurls/
        * tabs
          * ctrl+tab behaviour - https://superuser.com/questions/18609/changing-firefox-tab-cycle-order
          * https://addons.mozilla.org/en-US/firefox/addon/always-right/
        * privacy
          * prism-break - http://prism-break.org/en/categories/windows/#web-browser-addons 
          * simply - ublock, https everywhere 
        * youtube
          * https://addons.mozilla.org/cs/firefox/addon/youtube-classic/
            * speeds up stuff; is better
          * https://addons.mozilla.org/en-US/firefox/addon/h264ify/
            * better formats
          * https://addons.mozilla.org/en-US/firefox/addon/sponsorblock/ 
    * utils
      * screenshots
        * green shot - http://getgreenshot.org/
      * players
        * audio foobar - https://www.foobar2000.org/
          * theme / preset  - 
          * https://github.com/Ottodix/Eole-foobar-theme
      * hotspots/tethering
      * notes 
        * workflowy
        * ownnotes
        * roam
      * sync
        * harddrive - http://www.freefilesync.org/
        * network / internet sync - https://syncthing.net/
      * computer hygiene
        * dupe cleaners
          * dupeGure
          * duplicatecleaner free 
        * ccleaner
        * remove empty directories - http://www.jonasjohn.de/red.htm
          * ! pay attention with syncthing folders ! 
        * recovery+ https://www.cgsecurity.org/wiki/TestDisk 
      * image batch
        * https://saerasoft.com/caesium/
      * video conversions
        * mpeg streamclip - http://www.squared5.com/
        * handbrake - https://handbrake.fr/ 
        * mediacoder
        * file converter   

linux distros
-------------

      * AV
        * **Ubuntu Studio**
        * AV linux
        * https://gitlab.com/giuseppetorre/bodhilinuxmedia
        * https://librazik.tuxfamily.org/base-site-LZK/english.php #audio  
      * privacy
        * https://prism-break.org/en/categories/windows/#operating-systems-live   
