sdbs\_selfhosting
=================

TMP
---

1.  sdbs backups [syncthing](AREAS/DigitalGardenAnabasis/_INFORM/syncthing)
2.  [ipfs](AREAS/DigitalGardenAnabasis/_INFORM/ipfs)
3.  [torrent](torrent)
4.  [pile](https://pile.sdbs.cz/)
5.  [klastr](https://sdbs.cz/klastr/)

Structure proposal
------------------

    ===== Intro =====
    ==== Project list ====
    ===== Essential =====
    ==== Tool list ====
    ===== Advanced =====

CHAOS
-----

### IPFS

The script for autopinning:

    set -ex
    cd
    pwd
    mkdir -p ipfs
    cd ipfs

    echo '
    #!/bin/bash
    DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

    cd "${DIR}"

    for url in "https://pile.sdbs.cz/api/ipfs_cids" "https://inform.sdbs.cz/ipfs_cids.php";do
            echo "Loading ${url}..."
            for cid in $(curl -s $url|sort -R);do
                    ipfs get "${cid}" && ipfs pin add --progress "${cid}"
            done
    done
    ' > update_pins.sh
    chmod +x update_pins.sh

    echo "30 */6 * * *  $(whoami)   ipfs name publish --key=gunk Qmb6WJzMereTNCMh1drjepq3wEn9r6HkBZKadc7CFwf98V" |sudo tee /etc/cron.d/ipfs

    set +x
    echo "Set-up complete. To update ipfs pins now, run ./update_pins.sh"

Copy and paste this into some file, then run it with `bash`. It will set
up periodic autopinning.
