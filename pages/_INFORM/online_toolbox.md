-   lists
       * https://libreprojects.net/#favs=wikipedia,joindiaspora-com,nextcloud,openstreetmap,jamendo,plos 
    * basic
      * https://www.worldtimebuddy.com/est-to-czech-republic-prague
    * text
      * [[https://ocr.space/|ocr.space]]
      * [[https://www.textfixer.com/tools/capitalize-sentences.php|Capitalize Sentences ]]
    * sharing - torrent
      * https://magnetlinkgenerator.com/
      * https://github.com/ngosang/trackerslist

**artsy fartsy**

-   **[tml tools](https://t.mldk.cz/tools/)**
-   [dummy image generator](https://dummyimage.com/)
-   [pixel sorting by Feiss](https://feiss.github.io/pixelsorting/)
-   [image to audio by nsspot](https://nsspot.herokuapp.com/imagetoaudio/)
-   [Edge detection](https://pinetools.com/image-edge-detection) Online
    by PineTools
-   <https://inform.sdbs.cz/doku.php?id=avg#online_tools>

**WebPlotDigitizer**

-   Web based tool to extract data from plots, images, and maps
-   <https://automeris.io/WebPlotDigitizer/>

**cut-up**

-   [Cut-ups & the Text Mixing Desk](http://www.lazaruscorporation.co.uk/cutup)

**music**
-   [pianoshow.me](https://pianoshow.me/)
