Ninjam
======

> NINJAM is open source (GPL) software to allow people to make real
> music together via the Internet. Every participant can hear every
> other participant. Each user can also tweak their personal mix to his
> or her liking. NINJAM is cross-platform, with clients available for
> Mac OS X, Linux, and Windows. [REAPER](inform.reaper.md) (our digital audio
> workstation software for Windows and OS X) also includes NINJAM
> support (ReaNINJAM plug-in). [^1]

Essentials
----------

> The NINJAM client records and streams synchronized intervals of music
> between participants. Just as the interval finishes recording, it
> begins playing on everyone else\'s client. So when you play through an
> interval, you\'re playing along with the previous interval of
> everybody else, and they\'re playing along with your previous
> interval. If this sounds pretty bizarre, it sort of is, until you get
> used to it, then it becomes pretty natural. In many ways, it can be
> more forgiving than a normal jam, because mistakes propagate
> differently. [^2]

> While the tempo in Reaper might be set to 100bpm and the tempo in
> Ninjam is also set to 100bpm, without manually pressing play in reaper
> at the exact moment of a beat in ninjam, the sequences won\'t be in
> sync. [^3]

![](/ninjamrouting.jpg)

-   <https://wiki.cockos.com/wiki/index.php/NINJAM_Documentation>
-   <https://www.freewebs.com/arxeia/Ninjam_User_Guide.pdf>
-   <https://forum.cockos.com/showthread.php?t=12578>
-   <https://forum.cockos.com/showpost.php?p=2230659&postcount=64>

### reaNinjam

Official client by Cockos for Reaper.

<https://www.cockos.com/ninjam/#Downloads>

reaNinjam w/ Ableton
--------------------

1.  Start Ableton.
2.  Use ASIO/reaRoute as asio output to get audio from Ableton Live (or
    other) to Reaper.
3.  In Repear
    1.  get reaNinjam runnig in master FX slot.
    2.  create stereotrack with reaRoute inputs
    3.  check soundcard samplerate. It should be 44.1kHz.
4.  Connect to Outpost NINJAM server.
5.  With help of ninjam metronome trigger play in Ableton Live or follow
    midi sync help.

**OR:**
<https://wiki.cockos.com/wiki/index.php/Using_NINJAM_with_Live_via_REAPER>

reaNinjam w/ other
------------------

You can use Virtual Audio Cable to get sound from or to other apps
to/from Reaper. If that is the case use VAC within a reaper WASAPI
drivers intstead of ASIO.

<https://vac.muzychenko.net/en/>

### Audio routing REA/ABL

1.  reaRoute - is not installed by default, but it is available in every
    Reaper installation file
    1.  <https://wiki.cockos.com/wiki/index.php/ReaRoute>
2.  Reaper will try to push for highest samplerate of your soundcard.
    Check that.

> 5.1. This extra step if for people who want to connect programs like
> Reason, Ableton, Cubase and similar directly to REAPER.
>
> > When in step 5 you select the input you will also see ReaRoute
> > Channel 1, 2, 3 and so on in mono input or ReaRoute Channel 1 /
> > ReaRoute Channel 2 and so on in stereo input. Select Stereo Input
> > then select ReaRoute Channel 1 / ReaRoute Channel 2.
> >
> > > Now goto your music software and to its audio setup, select the
> > > ASIO Driver as ReaRoute ASIO then select ReaRoute 1 and 2 for left
> > > and right if it gives u the extra option.
> > >
> > > > If you run 2 or more pieces of software then set one on ReaRoute
> > > > 1/2 then another on 3/4 and so on, then make a track for each in
> > > > REAPER and set the stereo input to ReaRoute Channel 1 / ReaRoute
> > > > Channel 2 then ReaRoute Channel 3 / ReaRoute Channel 4 for the
> > > > next track and so on.
>
> 6\. Almost done, at the bottom of REAPER there is a MASTER Volume
> slider, on this small panel there are also a few others things and one
> is a FX, click this FX to bring up the Master FX Chain window. In this
> window the Add button, this will bring up the Add FX to MASTER window.
> In this window select Cockos in the left panel, then in the right
> panel select ReaNINJAM, you may see VST: ReaNINJAM (Cockos), if so
> select that insted.
>
> > Next Click OK, the Add FX to MASTER window will now vanish and you
> > will be back on the Master FX Chain window. In the left Panel you
> > will see the ReaNINJAM plugin selected, If the square box to the
> > left of the ReaNINJAM is not ticked then click and tick it.
> >
> > > If by now the ReaNINJAM client window has not popped up and
> > > showing on the screen then to the right of the Master FX Chain
> > > window click Show ReaNINJAM Console, if you do not see this then
> > > click the ReaNINJAM once to highlight it then you should see the
> > > Show ReaNINJAM Console button.
> > >
> > > > You can now close the Master FX Chain window by clicking the x
> > > > at the top right of the Master FX Chain window. [^4]

### midiclock

**NINJAM have sync function now!!! This should help to sync anything to
Reaper, which can sync to ninjam BPM.**

You can sync ableton to reaper tempo through any midi loopback (virtual
midi cabel) utility.

<img src="/reaper_midi.png" width="400" />
<img src="/midi_abl.png" width="400" />
<img src="/sync_abl.png" width="400" />

1.  Ableton
    1.  Press play with EXT on (Nothing will play)
2.  ReaNinjam sync option
    1.  Set project tempo
    2.  Start Reaper playback on next loop
3.  ABL will play

#### midi loopback

WIN - <https://www.tobias-erichsen.de/software/loopmidi.html>

#### midi support links

-   <https://forum.cockos.com/showthread.php?t=21638>
-   <https://help.ableton.com/hc/en-us/articles/209071149-Synchronizing-Live-via-MIDI>
-   <http://untidymusic.com/js-plugins/reaper-midi-clock-sync-success> ?

UnEssential aka R&D
-------------------

### Importing Ninjam Session recordings to Reaper

<https://wiki.cockos.com/wiki/index.php/Importing_Ninjam_Sessions>

### Modes

Session mode -
<https://forum.cockos.com/showpost.php?p=79131&postcount=2>

### Alternative Clients

#### ninjam-js

> The Ninjam music collaboration client reimplemented using JavaScript.

<https://github.com/BHSPitMonkey/ninjam-js>

#### LinJam

> The highest goal of the LinJam project (and it\'s predecessor
> TeamStream) is to flatten the pro-audio learning-curve; easing the
> intimidation that most non-technical musicians experience when they
> are first introduced to NINJAM. [^5]

<https://github.com/linjam/linjam>

#### Ninjam plugin by Expert Sleepers

\[MAC OS only\]

> By having the Ninjam client in a plug-in, it makes it very easy to
> combine Ninjam into your normal computer-based music setup. It also
> gives you many more creative options for routing signals in and out of
> Ninjam, even without multichannel audio hardware attached to your
> computer.[^6]

<https://www.expert-sleepers.co.uk/ninjamplugin.html>

### Midi over net

-   <https://www.midi.org/articles/rtp-midi-or-midi-over-networks>
-   <https://www.nerds.de/en/ipmidi.html>

### Links

-   <https://github.com/teamikl/ninjam-chat>
-   <https://github.com/ayvan/ninjam-chatbot>
-   <https://github.com/ayvan/ninjam-dj-bot>

[^1]: <https://www.cockos.com/ninjam/>

[^2]: <https://www.cockos.com/ninjam/>

[^3]: <https://forum.cockos.com/showpost.php?p=127916&postcount=15>

[^4]: <https://forum.cockos.com/showthread.php?t=12578>

[^5]: <https://github.com/linjam/linjam>

[^6]: <https://www.expert-sleepers.co.uk/ninjamplugin.html>
