Firefox
=======

TLDR firefox is more than browser. And browsers are more than browsers
were before.

Tips & Tricks
-------------

[inform - legacy
links](https://inform.sdbs.cz/doku.php?id=consumer_habits_good#pc_win)

[Prevent Firefox from starting
automatically](https://support.mozilla.org/en-US/kb/firefox-starts-automatically-when-i-restart-window)

Plugins
-------

### Security and usability 101

-   [HTTPS
    Everywhere](https://prism-break.org/en/projects/https-everywhere)
-   [uBlock Origin](https://prism-break.org/en/projects/ublock-origin)
-   [clearURLs](https://addons.mozilla.org/en-US/firefox/addon/clearurls/)

### Unsorted

-   dark mode for dark hipsters\' eyes
    -   [DarkReader](https://github.com/darkreader/darkreader) -
        universal \"dark theme on every site\" addon
    -   [reddit // how do you make new tab page also
        dark](https://old.reddit.com/r/firefox/comments/ijvhuo/how_do_you_make_this_new_tab_page_also_dark/) -
        various ways to make new tab page also dark
-   tabs
    -   [ctrl+tab
        behaviour](https://superuser.com/questions/18609/changing-firefox-tab-cycle-order)
    -   [always-right](https://addons.mozilla.org/en-US/firefox/addon/always-right/)
-   youtube
    -   [youtube-classic](https://addons.mozilla.org/cs/firefox/addon/youtube-classic/) -
        speeds up stuff; is better
    -   [h264ify](https://addons.mozilla.org/en-US/firefox/addon/h264ify/) -
        better formats
    -   [sponsorblock](https://addons.mozilla.org/en-US/firefox/addon/sponsorblock/) -
        sponsorblock
-   downloading
    -   [download helper](https://www.downloadhelper.net/)

Account
-------

Firefox Account offers few [interesting
services](https://www.mozilla.org/en-US/firefox/accounts/):

### Send \[OFFLINE\]

<https://send.firefox.com/>

> Firefox Send is a free-to-use, online file-sending service, able to
> handle files up to 2.5 GB
>
> > All files are encrypted before being uploaded and decrypted on the
> > client after downloading. The encryption key is never sent to the
> > server.[^1]

There is also cool sidebar plugin -
<https://addons.mozilla.org/en-US/firefox/addon/send-to-fx/>

### Sync

<https://support.mozilla.org/en-US/products/firefox/sync-and-save>

<https://support.mozilla.org/en-US/kb/send-tab-firefox-desktop-other-devices>

> Firefox Sync, originally branded Mozilla Weave,\[2\] is a browser
> synchronization feature for Firefox web browsers. It allows users **to
> partially synchronize bookmarks, browsing history, preferences,
> passwords, filled forms, add-ons, and the last 25 opened tabs** across
> multiple computers.\[3\] The feature is included in the desktop
> **Firefox browser, Firefox for iOS, and Firefox for Android**.It keeps
> user data on Mozilla servers, but according to Mozilla **the data is
> encrypted** in such a way that no third party, not even Mozilla, can
> access user information.\[4\] It is also possible for the user to host
> their own Firefox Sync servers, or indeed, for any entity to do
> so.\[5\][^2]

### Monitor

<https://monitor.firefox.com/>

> It informs users if their email address and passwords used has been
> leaked in data breaches, using the database provided by Have I been
> Pwned? (HIBP)[^3]

### Lockwise

> Firefox Lockwise is **the password manager** of the Firefox web
> browser.\[5\]\[6\] On desktop, Lockwise is simply part of
> Firefox;\[7\] whereas on iOS and Android, it is also available as a
> standalone app. [^4]

Security
--------

FIXME

<https://support.mozilla.org/en-US/products/firefox/protect-your-privacy>

<https://support.mozilla.org/en-US/kb/manage-passwords-firefox-desktop-firefox-lockwise>

Profiles
--------

FIXME

-   How to?
    -   <https://support.mozilla.org/en-US/kb/profile-manager-create-remove-switch-firefox-profiles>
-   Creating desktop shortcut to specific profile
    -   <http://kb.mozillazine.org/Shortcut_to_a_specific_profile>
-   Plugin - Firefox Profile Maker (nice defaults) -
    -   <https://ffprofile.com>

<about:profiles>

[^1]: <https://en.wikipedia.org/wiki/Firefox_Send>

[^2]: <https://en.wikipedia.org/wiki/Firefox_Sync>

[^3]: <https://en.wikipedia.org/wiki/Firefox_Monitor>

[^4]: <https://en.wikipedia.org/wiki/Firefox_Lockwise>
