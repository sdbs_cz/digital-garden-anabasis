S.M.A.R.T.
==========

> S.M.A.R.T. (Self-Monitoring, Analysis and Reporting Technology; often
> written as SMART) is a monitoring system included in computer hard
> disk drives (HDDs), solid-state drives (SSDs), and eMMC drives. Its
> primary function is to detect and report various indicators of drive
> reliability with the intent of anticipating imminent hardware
> failures.

FIXME

<https://gsmartcontrol.sourceforge.io/>

<https://forums.tomshardware.com/faq/hdd-s-m-a-r-t-test-with-crystaldiskinfo.1730641/>
