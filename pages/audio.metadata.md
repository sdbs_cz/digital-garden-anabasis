# audio.metadata
## Semantics
- freesound
	- https://freesound.org/help/faq/#i-have-many-sounds-to-upload-is-there-a-way-to-describe-many-sounds-in-bulk
	- https://freesound.org/help/moderators/#good-description-standards
- schema.org
	- https://schema.org/AudioObject
	- https://schema.org/Clip
	- https://schema.org/ArchiveComponent
	- https://schema.org/Dataset
- also semantics of XMP
	- https://en.wikipedia.org/wiki/Extensible_Metadata_Platform
- vocabulary
	- https://dictionary.cambridge.org/topics/senses-and-sounds/sounds-made-by-objects-movement-or-impact/

#### Universal Category System
- https://universalcategorysystem.com/ ***!!!***
- voluntary filename standart
- synonym search possible
- [[audio.software.reaper]] - https://www.aaroncendan.me/side-projects/ucs

### articles
- https://www.creativefieldrecording.com/2014/06/04/an-introduction-to-sound-fx-metadata-1-the-basics/
- https://www.asoundeffect.com/metadata-style-guide/
- reddit
	- https://www.reddit.com/r/LocationSound/comments/13ijzq0/efficient_metadata_edit_software/
	- https://www.reddit.com/r/fieldrecording/comments/wk8lke/sound_database_software/
	- https://www.reddit.com/r/fieldrecording/comments/wk8lke/sound_database_software/
	- https://www.reddit.com/r/fieldrecording/comments/y784qg/comment/isv2w8r/

## Metadata standarts
- http://www.theaudioarchive.com/TAA_Resources_Metadata.htm
### id3tag 
- *used by consumers and more?*
### bwf 
- https://en.wikipedia.org/wiki/Broadcast_Wave_Format 
	- *this seems to be liked by profesionals*
		- https://mediaarea.net/BWFMetaEdit/xml_chunks
		- https://www2.archivists.org/sites/all/files/Andrec,%20Michael.pdf fulltext? where?
	- https://wavmetadata.blogspot.com/

### Others
- XMP - https://en.wikipedia.org/wiki/Extensible_Metadata_Platform
- dolby - https://professionalsupport.dolby.com/s/topic/0TO4u000000f1pFGAQ/audio-metadata?language=en_US
	- probably interesting just for dolby 


## Tagging software
- https://sourceforge.net/projects/kid3/ #multiplatform
- https://onetagger.github.io/ also spotify analysis / genre analysis
	- spotify genres - https://docs.google.com/spreadsheets/d/1wYokScjoS5Xb1IvqFMXbSbknrXJ7bySLLihTucOS4qY/edit#gid=0
- https://docs.mp3tag.de/mapping/
-  [[inform.reaper]] / [[audio.software.reaper]] **!!!**
	- https://reaper.blog/2021/11/metadata-editing-in-reaper/
		- https://www.youtube.com/watch?v=bDfuMIeqawc
	- https://reaper.blog/2018/03/sound-library-metadata/
	- https://www.stephenschappler.com/2020/05/11/reaper-quick-tip-exporting-audio-with-embedded-metadata/
	- 

## Articles
- https://www.production-expert.com/production-expert-1/metadata ac3
- https://www.creativefieldrecording.com/2014/06/17/an-introduction-to-sound-fx-metadata-apps-2-comparing-apps/#Metadata_App_Chart 2014
- https://archaeologydataservice.ac.uk/help-guidance/guides-to-good-practice/basic-components/digital-audio/preserving-digital-audio/deciding-how-to-archive/
## Adjacent
- [[concepts.Digital Asset Managment]]
- [[incubation.audio.sample.managment]]
- [[areas.filetag]]
- [[audio.datasets]]
- [[incubation.concepts.DatabaseArt]]
- [[areas.audio]]
- [[concepts.archive]]
- [[concepts.archives.art]]



-----------------

## Unsorted
- https://www.aes.org/publications/standards/search.cfm?docID=85
- https://tech.ebu.ch/metadata/ebucore
- https://old.ndk.cz/standardy-digitalizace/standardy-pro-zvukova-data
- https://www.niso.org/standards-committees/video-audio-metadata-guidelines
	- https://groups.niso.org/higherlogic/ws/public/download/28273/RP-41-2023_Video_Audio_Metadata_Guidelines.pdf **!!!**
- https://www.researchgate.net/publication/373364694_Metadata_Standards_in_Digital_Audio
- https://tagteamanalysis.com/2022/04/tta-guides-tagging-for-sound-design/
- https://github.com/Borewit/music-metadata
- https://audio-tag-analyzer.netlify.app/
- dead
	- https://www.ala.org/sites/default/files/alcts/content/resources/preserv/audio_metadata.pdf
	- https://www.ala.org/sites/default/files/alcts/content/resources/preserv/audio_2011.pdf
- 