# Database Art

#incubation #chaosstream

> The computer age brought with it a new cultural algorithm: reality → media → data → database” (224). By developing Manovich’s cultural algorithm further, I suggest expanding this diagram to: reality → media → data → database → algorithmic editing → new forms of narrative. To Manovich, cinema “is the intersection between database and narrative,” therefore, the expansion of the database must lead to more innovative and complex narratives (237).
> - [A Brief History of Algorithmic Editing](https://medium.com/janbot/a-brief-history-of-algorithmic-editing-732c3e19884b)\


>Indeed, if after the death of God (Nietzche), the end of grand Narratives of Enlightenment (Lyotard) and the arrival of the Web (Tim Berners-Lee)  
the world appears to us as an endless and unstructured collection of images, texts, and  other data records, it is only appropriate that we will be moved to model it as a database.  But it is also appropriate that we would want to develops poetics, aesthetics, and ethics of this database.
> - http://manovich.net/content/04-projects/022-database-as-a-symbolic-form/19_article_1998.pdf


## See Also
- https://web.archive.org/web/20211130004317/http://cas.famu.cz/wiki/doku.php?id=database_cinema
- https://web.archive.org/web/20181214141255/http://cas.famu.cz/wiki/doku.php?id=dejiny_audiovizualnich_teorii
- https://web.archive.org/web/20181214141345/http://cas.famu.cz/wiki/doku.php?id=dejiny_novych_medii_new_media_history

--------------------
## Adjacent
- [[people.DzigaVertov]]
- [[people.LevManovich]]
- [[concepts.Algorithmic Editing]]
- [[areas.filetag]]
- [[concepts.archives.art]]
- [[concepts.linearity]]
- [[concepts.hypertext]]
- [[projects.upend]]
- [[people.AbyWarburg]]
- [[concepts.playlisting]]

====
https://github.com/jblsmith/street-view-movie-maker