journalctl -x -b -1

Bss linux\_tips
===============

Commandline
-----------

sudo shutdown -h +60

Shortcuts
---------

ctrl+H - show hidden files

ctrl+alt+horizontal arrows - workspace switch

Hardidisk + datamanagment
-------------------------

\- GSmartControl - FSlint

guides
------

automount -
<https://askubuntu.com/questions/164926/how-to-make-partitions-mount-at-startup>
?

autostart

-   <https://askubuntu.com/questions/48321/how-do-i-start-applications-automatically-on-login>
    -   run command ?
-   <https://askubuntu.com/questions/172862/autostart-application-in-new-workspace-in-gnome-shell>

conky -

wildcards - <https://ryanstutorials.net/linuxtutorial/wildcards.php>

commandline -
<https://www.lifewire.com/linux-commands-for-navigating-file-system-4027320>

File Permissions - <https://linuxhandbook.com/linux-file-permissions/>

KPZ - gui
---------

\* greenshot alternative - <https://github.com/lupoDharkael/flameshot> ?
\* conky - \* fsearch - <https://github.com/cboxdoerfer/fsearch> -
everything alternative

KPZ - commandline
-----------------

\* youtubedl - \* spleeter- mhmmm \* internet archive client

KPZ - ppa list
--------------

Multimedia
----------

### Lists

-   <https://github.com/nodiscc/awesome-linuxaudio/blob/master/README.md>
-   <http://linux4vjs.net/>

### Audio

#### Essential

-   <https://help.ubuntu.com/community/UbuntuStudio/UbuntuStudioControls>

#### interesting

-   <https://audionodes.com/>

#### Slicers

-   <https://rock-hopper.github.io/shuriken/>
-   <https://sourceforge.net/projects/smasher/>

#### Streaming

-   butt - broadcast using this tool LIN/MAC/WIN
    -   it is in ubuntu repo
    -   <https://danielnoethen.de/butt/>

### Photo - collage

<https://askubuntu.com/questions/196293/applications-to-make-photo-collages>
<https://linuxaria.com/article/photo-collage-mosaic-linux>

HFS+
----

JUST FUCK IT

sudo mount -t hfsplus -o force,rw /dev/sdc /media/sdc

Interesting Distros
===================

[leeenux](https://leeenux-linux.com/) - linux for netbooks

[bodhi linux](https://www.bodhilinux.com/) - light linux
