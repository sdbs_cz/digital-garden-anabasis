# [las].credits
## References for V0.1
### William S. Burroughs
The Soft Machine (1961) 

### Vilém Flusser
Codified World (1978)
Line and Surface (1978)

### Hans-Georg Gadamer
Man and Language (1966)

### William Gibson
Pattern Recognition (2003)

### Theodor Nelson

### Andrej Tarkovsky

### Dziga Vertov

### Edward O. Wilson
Consilience (1998)

## Credits
### Visuals
Jakub Jirka
Kilian Kuděla
Pavel Osvald
Petr Zábrodský

### Sound
PZA
Dreamflowers
Saiga [IIkharus remix]
Zlom

### Programming
Tomáš Mládek

### Conception
Petr Zábrodský
Tomáš Mládek



