# TTS
aka text to speech [[areas.text]][[areas.audio]]

## Software
### Speech
- balabolka
- https://askubuntu.com/questions/53896/natural-sounding-text-to-speech

### Singing
- https://www.musicradar.com/news/5-of-the-best-speech-synthesizers-for-robotic-vocal-sounds
- https://dreamtonics.com/synthesizerv/


#### VSTs


- Alter/EGO
- https://vocasphere.net/2020/02/neutrino-neural-singing-synthesizer-available-for-download/


##### Paid
https://emvoiceapp.com/

## AI
- https://www.descript.com/overdub?lyrebird=true
- https://adamloving.com/2021/01/08/text-to-singing/
- https://ai.googleblog.com/2017/12/tacotron-2-generating-human-like-speech.html

https://www.automated-systems.com/moonbase-alpha-best-text-to-speech-text-to-speech-programs/

boilingsteam@mastodon.cloud - Mozilla Text to Speech (free to use, with pretrained models for 20 languages): https://github.com/mozilla/TTS #linux #mozilla #tts #speech #text​

adnan360@mas.to - Just learned about #Flite #TTS, which is similar to #Festival but written in C and supposed to be much faster.

http://www.festvox.org/flite/​

Also has voices for INDIC languages, such as Bengali, Gujrati, Hindi, Marathi, Tamil, Telegu, Punjabi: http://www.festvox.org/flite/packed/flite-2.1/voices/​

en@jlelse.blog - Text-to-speech is becoming more popular

https://jlelse.blog/thoughts/2022/03/tts​