# Audio Stellar

Audio Stellar is AI powered experimental sampler using librosa python library for audio analysis. This analysis than provide map of sounds and depending on their similiarity it groups sounds into clusters. Sound can be played through different types of units as particle, sequence, morph.
## 101
https://audiostellar.xyz/lang/en/index.html

- - https://linuxmusicians.com/viewtopic.php?p=145724&hilit=audiostellar#p145724


## Notes
eps - closeness to be cluster  
min - number which can make group

- accepts only mono ?
- is sensitive on samplerate of audio device, it should match that one of the file

**when recording internaly do not go over buffer 1024, otherwise the recording will be glitched**

## Background
analysis thru [https://librosa.org/doc/latest/index.html](https://librosa.org/doc/latest/index.html)

## Examples
- https://www.youtube.com/watch?v=2YxONrfA6po

## Adjacent
[[incubation.audio.synthesis.concatenative]]
[[incubation.audio.sample.managment]]
[[concepts.map]]