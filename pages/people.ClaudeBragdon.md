# Claude Bragdon

## 101
- architect
- theosophist

>series of Festivals of Song and Light that Bragdon staged with community music reformers from 1915 to 1918 in Rochester, Buffalo, Syracuse, and New York. These nocturnal community chorus festivals incorporated ornamental lamps and decorations into massive public events that drew tens of thousands of spectator-participants.[[3]](https://en.wikipedia.org/wiki/Claude_Fayette_Bragdon#cite_note-3) Through its role in both civic architecture and print media, projective ornament began to integrate these distinct realms into a single public sphere visually unified by geometric pattern.[[4]](https://en.wikipedia.org/wiki/Claude_Fayette_Bragdon#cite_note-4)
### -->
- https://en.wikipedia.org/wiki/Claude_Fayette_Bragdon
- https://theosophyart.org/2018/03/30/claude-fayette-bragdon/

## Literature
- pdfs
	- https://surface.syr.edu/cgi/viewcontent.cgi?article=1135&context=arc
	- https://pdxscholar.library.pdx.edu/cgi/viewcontent.cgi?article=1036&context=arch_design
	- https://bauarchitecture.com/docs/GEOmantic%20Math.pdf
- https://www.sacred-texts.com/eso/to/to00.htm
- [archive.org]( https://archive.org/search.php?query=%28%28subject%3A%22Bragdon%2C%20Claude%20Fayette%22%20OR%20subject%3A%22Bragdon%2C%20Claude%20F%2E%22%20OR%20subject%3A%22Bragdon%2C%20C%2E%20F%2E%22%20OR%20subject%3A%22Claude%20Fayette%20Bragdon%22%20OR%20subject%3A%22Claude%20F%2E%20Bragdon%22%20OR%20subject%3A%22C%2E%20F%2E%20Bragdon%22%20OR%20subject%3A%22Bragdon%2C%20Claude%22%20OR%20subject%3A%22Claude%20Bragdon%22%20OR%20creator%3A%22Claude%20Fayette%20Bragdon%22%20OR%20creator%3A%22Claude%20F%2E%20Bragdon%22%20OR%20creator%3A%22C%2E%20F%2E%20Bragdon%22%20OR%20creator%3A%22C%2E%20Fayette%20Bragdon%22%20OR%20creator%3A%22Bragdon%2C%20Claude%20Fayette%22%20OR%20creator%3A%22Bragdon%2C%20Claude%20F%2E%22%20OR%20creator%3A%22Bragdon%2C%20C%2E%20F%2E%22%20OR%20creator%3A%22Bragdon%2C%20C%2E%20Fayette%22%20OR%20creator%3A%22Claude%20Bragdon%22%20OR%20creator%3A%22Bragdon%2C%20Claude%22%20OR%20title%3A%22Claude%20Fayette%20Bragdon%22%20OR%20title%3A%22Claude%20F%2E%20Bragdon%22%20OR%20title%3A%22C%2E%20F%2E%20Bragdon%22%20OR%20title%3A%22Claude%20Bragdon%22%20OR%20description%3A%22Claude%20Fayette%20Bragdon%22%20OR%20description%3A%22Claude%20F%2E%20Bragdon%22%20OR%20description%3A%22C%2E%20F%2E%20Bragdon%22%20OR%20description%3A%22Bragdon%2C%20Claude%20Fayette%22%20OR%20description%3A%22Bragdon%2C%20Claude%20F%2E%22%20OR%20description%3A%22Claude%20Bragdon%22%20OR%20description%3A%22Bragdon%2C%20Claude%22%29%20OR%20%28%221866-1946%22%20AND%20Bragdon%29%29%20AND%20%28-mediatype:software%29 )
	- https://archive.org/details/aprimerhighersp00braggoog
	- https://archive.org/details/fourdimensional00braggoog/page/n146/mode/2up
	- 
- locked
	- https://www.tandfonline.com/doi/abs/10.1080/13602365.2013.821666

## Screencaps
![[Pasted image 20230314210430.png]]
![[Pasted image 20230314210436.png]]
![[Pasted image 20211211204326.png]]
![[Pasted image 20211211211556.png]]

![[Pasted image 20230314210449.png]]

![[Pasted image 20230314210501.png]]
![[Pasted image 20230314210508.png]]
![[Pasted image 20230314210517.png]]

## Texts
### MAN THE SPACE-EATER

> Man has been called the thinking animal. Space-eater would be a more appropriate title, since he so dauntlessly and persistently addresses himself to overcoming the limitations of his space. To realize his success in this, compare, for example, the voyage of Columbus' caravels with that of an ocean liner; or traveling by stage coach with train de luxe. Consider the telephone, the phonograph, the cinematograph, from the standpoint of space-conquest—and wireless telegraphy which sends forth messages in every direction, over sea and land. Most impressive of all are the achievements in the domain of astronomy. One by one the sky has yielded its amazing secrets, till the mind roams free among the stars. The reason why there are to-day so many men braving death in the air is because the conquest of the third dimension is the task to which the Zeit-Geist has for the moment addressed itself, and these intrepid aviators are its chosen instruments—sacrificial pawns in the dimension-gaining game.
>
> All these things are only the outward and visible signs of the angel, incarnate in a world of three dimensions, striving to realize higher spatial, or heavenly, conditions. This spectacle, for example, of a millionaire hurled across a continent in a special train to be present at the bedside of a stricken dear one, may be interpreted as the endeavor of an incarnate soul to achieve, with the aid of human ingenuity applied to space annihilation, that which, discarnate, it could compass without delay or effort. 

### Architecture and Democracy
>He takes us through the cities with large concrete buildings like the ones found in New York which he considers them to be power structures meant to carry on financial business. He is of the opinion that America is built on materialism and people have loss site of comradeship except soldiers when deprived of all but essentil materials, they find strengh in fellow soldiers
> - https://www.amazon.com/gp/customer-reviews/R2ED0CFDTAZWRN?ASIN=1171830947

# Adjacent
- [[people.OliverLeslieReiser]]
- [[incubation.people.AndreasGoppold]]
-  [[incubation.areas.diagrammatic]]
- [[people.EdwardOWilson]]
- [[people.Martinus]]
- [[incubation.generationZ]]
- [[incubation.people.PaulLaffoley]]
- [[incubation.people.HenryFlynt]]
- [[people.IvanIllich]]
- [[concept.ornament]]
- [[projects.lalar]]
- [[areas.architecture]]