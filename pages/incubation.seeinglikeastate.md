https://theanarchistlibrary.org/library/james-c-scott-seeing-like-a-state

### chapter 9

> The planned “scientific city,” laid out according to a small number of rational principles, was experienced as a social failure by most of its inhabitants. Paradoxically, the failure of the designed city was often averted, as was the case in Brasília, by practical improvisations and illegal acts that were entirely outside the plan. Just as the stripped-down logic behind the scientific forest was an inadequate recipe for a healthy, “successful” forest, so were the thin urban-planning schemata of Le Corbusier an inadequate recipe for a satisfactory human community.

>The thin simplifications of agricultural collectivization and centrally planned production have met a comparable fate, whether on the collective farms of the former Soviet Union or in the ujamaa villages of Nyerere’s Tanzania. Here again, the schemes that did not collapse altogether managed to survive thanks largely to desperate measures either not envisaged or else expressly prohibited by the plan. Thus an informal economy developed in Russian agriculture, operating on tiny private plots and the “theft” of time, equipment, and commodities from the state sector and supplying most of the dairy products, fruit, vegetables, and meat in the Russian diet.[[787]](https://theanarchistlibrary.org/library/james-c-scott-seeing-like-a-state#fn787) Thus the forcibly resettled Tanzanians successfully resisted collective production and drifted back to sites more suitable for grazing and cultivation. At times, the price of an unyielding imposition of state simplifications on agrarian life and production—Stalin’s forced collectivization or China’s Great Leap Forward—was famine. As often as not, however, state officials recoiled before the abyss and came to tolerate, if not condone, a host of informal practices that in fact underwrote the survival of the official scheme.
>
>
>These rather extreme instances of massive, state-imposed social engineering illustrate, I think, a larger point about formally organized social action. In each case, the necessarily thin, schematic model of social organization and production animating the planning was inadequate as a set of instructions for creating a successful social order. By themselves, the simplified rules can never generate a functioning community, city, or economy. Formal order, to be more explicit, is always and to some considerable degree parasitic on informal processes, which the formal scheme does not recognize, without which it could not exist, and which it alone cannot create or maintain.


## Adjacent
- [[people.LeCorbusier]]
- [[1000p]]