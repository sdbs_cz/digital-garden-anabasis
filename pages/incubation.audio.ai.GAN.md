- https://magenta.tensorflow.org/gansynth
- https://www.mathworks.com/help/audio/ug/train-gan-for-sound-synthesis.html
- https://daps.cs.princeton.edu/projects/HiFi-GAN/index.php?env-pairs=DAPS&speaker=f10&src-env=all
- https://github.com/chrisdonahue/wavegan
	- https://www.arxiv-vanity.com/papers/1802.04208/
- https://github.com/luickk/gan-audio-generator


- pdfs
	- https://www.cs.umd.edu/sites/default/files/scholarly_papers/Yang%2C%20Fan_MS%20scholarly%20paper.pdf
	- https://www.isca-speech.org/archive/pdfs/interspeech_2021/kim21f_interspeech.pdf
	- https://www.researchgate.net/publication/323142038_Synthesizing_Audio_with_Generative_Adversarial_Networks
	- https://theses.hal.science/tel-03640610/document
		- !!!!
	- https://acris.aalto.fi/ws/portalfiles/portal/103531495/BEHM_GAN_Bandwidth_Extension_of_Historical_Music_Using_Generative_Adversarial_Networks.pdf
	- https://speech.ee.ntu.edu.tw/~tlkagk/paper/audio2image.pdf
		- TOWARDS AUDIO TO SCENE IMAGE SYNTHESIS USING GENERATIVE ADVERSARIAL NETWORK
	- https://rawalvarun.github.io/files/project_reports/10_701_FINAL_PROJECT_REPORT_vrawal.pdf
		- https://github.com/rawalvarun?tab=repositories
	- cars
		- https://www.hindawi.com/journals/sv/2020/8888578/
	- ?? 
		- https://www.semanticscholar.org/paper/Audio-inpainting-with-generative-adversarial-Ebner-Eltelt/bbd9ea6aa8db09aa9f4f2c20ff9755fc74c3771f
		- https://minds.wisconsin.edu/handle/1793/82595 audioSinGAN
- online
	- https://www.leewayhertz.com/how-to-create-a-generative-audio-model/
	- https://stackoverflow.com/questions/74504746/how-to-create-an-audio-conditional-gan-pytorch pytorch
	- https://ieeexplore.ieee.org/document/9430540
		- Towards an End-to-End Visual-to-Raw-Audio Generation With GAN


- speeach
	- https://pytorch.org/hub/nvidia_deeplearningexamples_hifigan/
		- others
			- https://pytorch.org/hub/research-models
	- https://journals.sagepub.com/doi/full/10.1177/1550147720923529