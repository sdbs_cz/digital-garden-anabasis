## Methodologies and theory
[Knowledge Managment](concepts.digital%20garden.md#Knowledge%20Managment)

## Terms
### [[concepts.digital garden]]

### Permanent / Evergreen notes
- [Evergreen notes](https://notes.andymatuschak.org/Evergreen_notes)
- https://www.reddit.com/r/ObsidianMD/comments/s1blja/is_the_feynman_technique_the_perfect_method_for/

### [[concepts.MOC]]

## Methodologies

### Zettelkasten
>A zettelkasten consists of many individual notes with ideas and other short pieces of information that are taken down as they occur or are acquired. The notes are numbered hierarchically, so that new notes may be inserted at the appropriate place, and contain metadata to allow the note-taker to associate notes with each other. For example, notes may contain tags that describe key aspects of the note, and they may reference other notes. The numbering, metadata, format and structure of the notes is subject to variation depending on the specific method employed. 
>> https://en.wikipedia.org/wiki/Zettelkasten

- https://leananki.com/zettelkasten-method-smart-notes/

### PARA
> ![[para.desc.20210802171947.png]]
> > https://medium.com/praxis-blog/the-p-a-r-a-method-a-universal-system-for-organizing-digital-information-75a9da8bfb37

> ![](https://i2.wp.com/cdn-images-1.medium.com/max/800/1*i6I0M5kaZUOwIfq5q5W4mQ.jpeg?w=900&ssl=1)
> > https://fortelabs.co/blog/para/

### other
- [Every Page Is Page One](https://everypageispageone.com/the-book/)
- [Clean Up Your Mess - A Guide to Visual Design for Everyone](http://www.visualmess.com/)
- https://subconscious.substack.com/p/the-knowledge-ecology

### flat-file




## Libraries and history
### [[concepts.archives.art]]
### [[concepts.archive]]
### UDC
>The Universal Decimal Classification (UDC) is a bibliographic and library classification representing the systematic arrangement of all branches of human knowledge organized as a coherent system in which knowledge fields are related and inter-linked.
>> - https://www.reddit.com/r/datacurator/comments/5sj1g2/an_introduction_to_universal_decimal/


