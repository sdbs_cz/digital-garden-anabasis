# [[tools]].[[ai]].magenta

## 101
- https://magenta.tensorflow.org/research
- https://magenta.tensorflow.org/blog

## Articles
- 


## Toolsets

### NSynth
- https://colab.research.google.com/notebooks/magenta/gansynth/gansynth_demo.ipynb


### [ToneTransfer](https://sites.research.google/tonetransfer)
- press
	- https://thenextweb.com/neural/2020/10/01/googles-new-machine-learning-tool-turns-your-awful-humming-into-a-beautiful-violin-solo/
	- https://www.youtube.com/watch?v=f8CdZxWj--A
- https://sites.research.google/tonetransfer
- https://colab.research.google.com/github/magenta/ddsp/blob/master/ddsp/colab/demos/timbre_transfer.ipynb
- https://magenta.tensorflow.org/ddsp

>j_u_s_t_i_n
>>The model we use for pitch detection (SPICE: https://blog.tensorflow.org/2020/06/estimating-pitch-with-spice-and-tensorflow-hub.html) extracts the dominant pitch. The known limitation at this point is that it doesn't work well with polyphonic audio, and if you try to extract pitch from audio with multiple concurrent pitches (including microtones), you'll find that the estimation will bounce around between pitches. There are some configuration options in the colab (https://colab.sandbox.google.com/github/magenta/ddsp/blob/master/ddsp/colab/demos/timbre_transfer.ipynb#scrollTo=SLwg1WkHCXQO) that you can use to filter out pitches by amplitude.


#### Training
> This notebook demonstrates how to install the DDSP library and train it for synthesis based on your own data using our command-line scripts. If run inside of Colab, it will automatically use a free Google Cloud GPU.
> At the end, you'll have a custom-trained checkpoint that you can download to use with the DDSP Timbre Transfer Colab. 

- https://github.com/magenta/ddsp/blob/master/ddsp/colab/demos/train_autoencoder.ipynb
	- https://nbviewer.jupyter.org/url/github.com/magenta/ddsp/blob/master/ddsp/colab/demos/train_autoencoder.ipynb


### [Drums Transcription](https://magenta.tensorflow.org/oaf-drums)

### [Making an Album with Music Transformer](https://magenta.tensorflow.org/nobodys-songs)



## TPU
- https://en.wikipedia.org/wiki/Tensor_Processing_Unit

-------------

# #incubation 
https://github.com/Catsvilles/lofigen-magenta