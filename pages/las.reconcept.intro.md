#fiction #intro #facts #video #vertov
We must therefore ask how the various symbols of the world of fiction relate to their meanings. This shifts our problem to the structure of the media. If we take advantage of what was said in the first paragraph, we may answer the question as follows: Written lines relate their []()symbols to their meanings point by point (they “conceive” the facts they mean), while surfaces relate their symbols to their meanings by two-dimensional contexts (they “imagine” the facts they mean — if they truly mean facts and are not empty symbols). Thus, our situation provides us with two sorts of fiction: the conceptual and the imaginal; their relation to fact depends on the structure of the medium.

#intro #dimension 
Written lines relate their symbols to their meanings point by point (they “conceive” the facts they mean), while surfaces relate their symbols to their meanings by two-dimensional contexts (they “imagine” the facts they mean—if they truly mean facts and are not empty symbols). 

#intro #fiction #concept
When we translate image into concept, we decompose the image—we analyze it. We throw, so to speak,a conceptual point-net over the image, and capture only such meaning as did not escape through the meshes of the net. Therefore, the meaning of conceptual fiction is much narrower than the meaning of imaginal fiction, although it is far more clear and distinct. Facts are represented more fully by imaginal thought, more clearly by conceptual thought. The messages of imaginal media are richer, and the messages of conceptual media are sharper.

#intro #surface #image #temporality 
The fact that humankind is being programmed by surfaces (images) should not be considered a revolutionary piece of news. On the contrary, it apparently signifies a return to a primitive origin. Before the invention of writing, images were a decisive means of communication. Because most codes are ephemeral, such as the spoken word, gestures, and song, we are dependent on images to decipher the meaning that man has given both his deeds and his suffering from the time of Lascaux to the time of Mesopotamian tiles.

-----

> Cut word lines Cut music lines Smash the control images Smash the control machine Burn the books Kill the priests Kill! Kill! Kill!
> - William S. Burroughs - The Soft Machine (1961) 