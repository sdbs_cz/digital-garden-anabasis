# anarcheology
#chaosstream #incubation 

>
Foucault’s anarcheology proposes “refusal of universals — anti-humanist position — technological analysis of mechanisms of power and, instead of reform program: further extend points of non-acceptance” (OGL, 80). So, once again, the critical project proceeds through a dissociative historical analysis that has the effect of extending “points of non- acceptance”; or, as Deleuze would say, furthering the process of decodification; or, as Derrida would say, affirming an absolute break and difference.
>> http://terracritica.net/wp-content/uploads/OLeary.pdf

- [Anarchive as technique in the Media Archaeology Lab | building a one Laptop Per Child mesh network](https://link.springer.com/article/10.1007/s42803-019-00005-9?link_id=15)
- https://tiranalajm.com/the-new-moma-promises-a-turbo-charged-non-linear-art-history/
- http://rexcurry.net/anarchaeology.html
- https://www.academia.edu/36072709/Guerrilla_Networks_An_Anarchaeology_of_1970s_Radical_Media_Ecologies_TOC_and_Introduction
- https://jussiparikka.net/2014/01/09/archaeology/


https://www.pnas.org/content/115/37/E8585

[RADICAL MEDIA ARCHAEOLOGY (its epistemology, aesthetics and case studies). Wolfgang Ernst](https://www.youtube.com/watch?v=hshcw0TRmFw)

--------------------------
[Hito Steyerl: In Defense of the Poor Image](https://www.e-flux.com/journal/10/61362/in-defense-of-the-poor-image/)
- algo glitch dementedeeeeee [[concepts.glitchart]]

---------------------
---------------------
---------------------

https://www.softwarepreservationnetwork.org/

media ecology 
https://monoskop.org/Media_ecology

https://monoskop.org/Postmedia

https://monoskop.org/Vil%C3%A9m_Flusser

https://monoskop.org/Jussi_Parikka

http://www.advojka.cz/archiv/2014/22/pestovani-prachu

https://github.com/CAS-FAMU

------------------------

https://unite.openworlds.info/Open-Media-Network