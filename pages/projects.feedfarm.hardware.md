# [[projects.feedfarm]].hardware
## Hardware
### PCIe speeds

![[pcie - lanes speed.png]]
![](http://compuny.cz/images/PCIe.jpg)

https://www.guru3d.com/articles-pages/amd-ryzen-3-3100-and-3300x-review,3.html

## CPUs
https://en.wikichip.org/wiki/WikiChip
https://www.logicalincrements.com/best-cpu-gaming-pc

### motherbords
#### new
- 2 way sli? ? ? ?
	- https://www.alza.cz/msi-mpg-z390-gaming-pro-carbon-d5471041.htm ? 


## with quad SLI //VR solution 
- [reference](https://pcpartpicker.com/products/motherboard/#h=3,8&sort=price&l=4)
- [reference bez zdurazneni SLI](https://pcpartpicker.com/products/motherboard/#h=3,8&sort=price&L=4&m=8)

##### sorted by SLI capabilites
- 16/16/8/8
	- https://www.gigabyte.com/Motherboard/X399-DESIGNARE-EX-rev-10/sp#sp
		- CZ
			- 12000,-
	- https://www.asrock.com/MB/AMD/Fatal1ty%20X399%20Professional%20Gaming/index.asp#Specification
- 16/8/8/8 - socket LGA 2011
	- 
	- ` 4 PCI-E 3.0 x16  
2 PCI-E 2.0 x1 (in x4 slot),  
,  
\*\*\*4 PCI-E3.0x16 slots are running at 16/16/NA/8 or 16/8/8/8\*\*\*  
\*\*\*PCI-E slot #1 supports (x4) and slot #4 (x16) is disabled when Intel Core i7-5820K , i7-6800K or any processors with only 28 lanes is installed\*\*\*\*\*\*PCI-E slot #1 supports (x4) and slot #4 (x16) is disabled when Intel Core i7-5820K , i7-6800K or any processors with only 28 lanes is installed\*\*\* `
	-  https://www.senetic.cz/product/MBD-C7X99-OCE-O	
		- 8000,-
- 16/16/16/16 - socket 2099
	- https://www.asus.com/Commercial-Servers-Workstations/Pro-WS-X299-SAGE-II/	
		- https://www.bhphotovideo.com/c/product/1558809-REG/asus_pro_ws_x299_sage.html
			- 13000,-
	- https://www.asus.com/us/Motherboards/WS-X299-SAGE/
		-  https://www.zbozi.cz/hledani/?q=ASUS%20WS%20X299%20SAGE&razeni=nejlevnejsi
			- 12000,- 
	??? / !!!
	https://www.asrockrack.com/general/productdetail.asp?Model=X470D4U
	
	
	?
## Summary
Speed of PCI connections is important. Speed of 8x seems like minimum for rendering and machine learning. PCI lanes need to offered by motherboard and also CPU. 

The pci slots may not operate full bandwith even in case they offer the right hardware slot. Also motheboard dependend. Asus seems to be greatly specific with their motherboards.

### ==> machinelearning
- per card
	- 8x PCIe lanes
	- at least 2 cores 

## Schema of machines?
#### MachineFarm
3-4* 1080 heavy duty media machine

#### FeedFarm
2-3 1080 render, mining machine




---------------
https://www.alza.cz/gaming/amd-ryzen-5-3600-d5634213.htm?o=2
https://xevos.store/produkt/asus-prime-x570-pro/


https://www.asus.com/us/Motherboards/PRIME-X570-PRO/