I'm looking for a way to do a non-temporal only slit-scan. This may go completely against physics, but.. if a slit-scan is composed of moving images offset by time and space, I want to bend those pixels while they are playing back. Like being able to warp a glass filter in 3D while the footage plays back, or a camera films a scene. Essentially doing what a RGB version of a Rutt/Etra might be able to do, but with tactile controllers for the warping to make the output full of beautiful chance distortion.

The how-to below is not what I want to do, because it uses pre-determined displacement maps; I want to guide the displacement by feeding the motion from what you might do if you were smearing an original over the scanner head of a photocopier or scanner.

[

![How to Create the ‘2001’ Slit Scanning Effect with Digital Tools](https://external-prg1-1.xx.fbcdn.net/safe_image.php?d=AQGPcEPcgYKMswzl&w=500&h=261&url=https%3A%2F%2Fnofilmschool.com%2Fsites%2Fdefault%2Ffiles%2Fstyles%2Ffacebook%2Fpublic%2Fslit-scan.jpg%3Fitok%3DAILBibgU&cfs=1&sx=0&sy=32&sw=2400&sh=1253&ext=jpg&_nc_cb=1&_nc_hash=AQH_TxrXoxmnzIih)

](https://nofilmschool.com/2016/11/how-to-create-2001-slit-scanning-effect?fbclid=IwAR3Lf3FNJtEKvSF992tgYM5LvOTJXEtS8YFzGhCpPrc1SloGUSYq1HtCC3Y)

[

nofilmschool.com

How to Create the ‘2001’ Slit Scanning Effect with Digital Tools

This hands-on guide shows you how to distort your images like Kubrick.





](https://nofilmschool.com/2016/11/how-to-create-2001-slit-scanning-effect?fbclid=IwAR3xjKd8c6YA30qRUapf550jhjJZ8II2FwxAUjV5x8F_8tSqOq2_VNL3Ih8)

30 comments

1 share

### Comments

[Mark Landers](https://www.facebook.com/groups/526421244079223/user/100000850903926/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

I want to do this as well but with a digital camera.

![](data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%2318AFFF'/%3e%3cstop offset='100%25' stop-color='%230062DF'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0 0 0 0 0 0.299356041 0 0 0 0 0.681187726 0 0 0 0.3495684 0'/%3e%3c/filter%3e%3cpath id='b' d='M8 0a8 8 0 00-8 8 8 8 0 1016 0 8 8 0 00-8-8z'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='white' d='M12.162 7.338c.176.123.338.245.338.674 0 .43-.229.604-.474.725a.73.73 0 01.089.546c-.077.344-.392.611-.672.69.121.194.159.385.015.62-.185.295-.346.407-1.058.407H7.5c-.988 0-1.5-.546-1.5-1V7.665c0-1.23 1.467-2.275 1.467-3.13L7.361 3.47c-.005-.065.008-.224.058-.27.08-.079.301-.2.635-.2.218 0 .363.041.534.123.581.277.732.978.732 1.542 0 .271-.414 1.083-.47 1.364 0 0 .867-.192 1.879-.199 1.061-.006 1.749.19 1.749.842 0 .261-.219.523-.316.666zM3.6 7h.8a.6.6 0 01.6.6v3.8a.6.6 0 01-.6.6h-.8a.6.6 0 01-.6-.6V7.6a.6.6 0 01.6-.6z'/%3e%3c/g%3e%3c/svg%3e)

1

-    · 
    
    Reply
    
-    · 
    
    Share
    
-    · [1 d](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3641451799242803&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

[Todd Sines](https://www.facebook.com/groups/526421244079223/user/504820890/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

[

Mark

](https://www.facebook.com/groups/526421244079223/user/100000850903926/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)same here — but aside from a grid of controllable hot air guns melting optical quality plexiglass, I can’t wrap my head around how.

-   -    · 
        
        Reply
        
    -    · 
        
        Share
        
    -    · [1 d](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3641451799242803&reply_comment_id=3641493082572008&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)
    

[Mark Landers](https://www.facebook.com/groups/526421244079223/user/100000850903926/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

[

Todd Sines

](https://www.facebook.com/groups/526421244079223/user/504820890/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)I would look up Douglas Trumbal and his original design and try to duplicate it.

-   -    · 
        
        Reply
        
    -    · 
        
        Share
        
    -    · [1 d](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3641451799242803&reply_comment_id=3641496605904989&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)
    

Write a public reply…

  

[Ryan Legge](https://www.facebook.com/groups/526421244079223/user/1182886387/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

I think video culture has an app for that

![](data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%2318AFFF'/%3e%3cstop offset='100%25' stop-color='%230062DF'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0 0 0 0 0 0.299356041 0 0 0 0 0.681187726 0 0 0 0.3495684 0'/%3e%3c/filter%3e%3cpath id='b' d='M8 0a8 8 0 00-8 8 8 8 0 1016 0 8 8 0 00-8-8z'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='white' d='M12.162 7.338c.176.123.338.245.338.674 0 .43-.229.604-.474.725a.73.73 0 01.089.546c-.077.344-.392.611-.672.69.121.194.159.385.015.62-.185.295-.346.407-1.058.407H7.5c-.988 0-1.5-.546-1.5-1V7.665c0-1.23 1.467-2.275 1.467-3.13L7.361 3.47c-.005-.065.008-.224.058-.27.08-.079.301-.2.635-.2.218 0 .363.041.534.123.581.277.732.978.732 1.542 0 .271-.414 1.083-.47 1.364 0 0 .867-.192 1.879-.199 1.061-.006 1.749.19 1.749.842 0 .261-.219.523-.316.666zM3.6 7h.8a.6.6 0 01.6.6v3.8a.6.6 0 01-.6.6h-.8a.6.6 0 01-.6-.6V7.6a.6.6 0 01.6-.6z'/%3e%3c/g%3e%3c/svg%3e)

3

-    · 
    
    Reply
    
-    · 
    
    Share
    
-    · [1 d](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3641463575908292&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

[Matt Zog](https://www.facebook.com/groups/526421244079223/user/781402785/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

SSSScan

![](data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%2318AFFF'/%3e%3cstop offset='100%25' stop-color='%230062DF'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0 0 0 0 0 0.299356041 0 0 0 0 0.681187726 0 0 0 0.3495684 0'/%3e%3c/filter%3e%3cpath id='b' d='M8 0a8 8 0 00-8 8 8 8 0 1016 0 8 8 0 00-8-8z'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='white' d='M12.162 7.338c.176.123.338.245.338.674 0 .43-.229.604-.474.725a.73.73 0 01.089.546c-.077.344-.392.611-.672.69.121.194.159.385.015.62-.185.295-.346.407-1.058.407H7.5c-.988 0-1.5-.546-1.5-1V7.665c0-1.23 1.467-2.275 1.467-3.13L7.361 3.47c-.005-.065.008-.224.058-.27.08-.079.301-.2.635-.2.218 0 .363.041.534.123.581.277.732.978.732 1.542 0 .271-.414 1.083-.47 1.364 0 0 .867-.192 1.879-.199 1.061-.006 1.749.19 1.749.842 0 .261-.219.523-.316.666zM3.6 7h.8a.6.6 0 01.6.6v3.8a.6.6 0 01-.6.6h-.8a.6.6 0 01-.6-.6V7.6a.6.6 0 01.6-.6z'/%3e%3c/g%3e%3c/svg%3e)

2

-   -    · 
        
        Reply
        
    -    · 
        
        Share
        
    -    · [1 d](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3641463575908292&reply_comment_id=3641540739233909&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)
    

[Matt Zog](https://www.facebook.com/groups/526421244079223/user/781402785/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

is it's name

![](data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%2318AFFF'/%3e%3cstop offset='100%25' stop-color='%230062DF'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0 0 0 0 0 0.299356041 0 0 0 0 0.681187726 0 0 0 0.3495684 0'/%3e%3c/filter%3e%3cpath id='b' d='M8 0a8 8 0 00-8 8 8 8 0 1016 0 8 8 0 00-8-8z'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='white' d='M12.162 7.338c.176.123.338.245.338.674 0 .43-.229.604-.474.725a.73.73 0 01.089.546c-.077.344-.392.611-.672.69.121.194.159.385.015.62-.185.295-.346.407-1.058.407H7.5c-.988 0-1.5-.546-1.5-1V7.665c0-1.23 1.467-2.275 1.467-3.13L7.361 3.47c-.005-.065.008-.224.058-.27.08-.079.301-.2.635-.2.218 0 .363.041.534.123.581.277.732.978.732 1.542 0 .271-.414 1.083-.47 1.364 0 0 .867-.192 1.879-.199 1.061-.006 1.749.19 1.749.842 0 .261-.219.523-.316.666zM3.6 7h.8a.6.6 0 01.6.6v3.8a.6.6 0 01-.6.6h-.8a.6.6 0 01-.6-.6V7.6a.6.6 0 01.6-.6z'/%3e%3c/g%3e%3c/svg%3e)

2

-   -    · 
        
        Reply
        
    -    · 
        
        Share
        
    -    · [1 d](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3641463575908292&reply_comment_id=3641540825900567&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)
    

Write a public reply…

  

[Gilbert Sinnott](https://www.facebook.com/groups/526421244079223/user/696085524/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

this is something I’ve been experimenting with via C++ … my approach has been to not focus so much on “slits” but in having N amount of frames to combine and play with on the fly (RAM permitting):

[https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3573786506009333](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3573786506009333/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

![](data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%2318AFFF'/%3e%3cstop offset='100%25' stop-color='%230062DF'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0 0 0 0 0 0.299356041 0 0 0 0 0.681187726 0 0 0 0.3495684 0'/%3e%3c/filter%3e%3cpath id='b' d='M8 0a8 8 0 00-8 8 8 8 0 1016 0 8 8 0 00-8-8z'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='white' d='M12.162 7.338c.176.123.338.245.338.674 0 .43-.229.604-.474.725a.73.73 0 01.089.546c-.077.344-.392.611-.672.69.121.194.159.385.015.62-.185.295-.346.407-1.058.407H7.5c-.988 0-1.5-.546-1.5-1V7.665c0-1.23 1.467-2.275 1.467-3.13L7.361 3.47c-.005-.065.008-.224.058-.27.08-.079.301-.2.635-.2.218 0 .363.041.534.123.581.277.732.978.732 1.542 0 .271-.414 1.083-.47 1.364 0 0 .867-.192 1.879-.199 1.061-.006 1.749.19 1.749.842 0 .261-.219.523-.316.666zM3.6 7h.8a.6.6 0 01.6.6v3.8a.6.6 0 01-.6.6h-.8a.6.6 0 01-.6-.6V7.6a.6.6 0 01.6-.6z'/%3e%3c/g%3e%3c/svg%3e)

5

-   -    · 
        
        Reply
        
    -    · 
        
        Share
        
    -    · [1 d](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3641476215907028&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)
    

[Marinko Marinkov](https://www.facebook.com/groups/526421244079223/user/524215728/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

someone did a tutorial of video tracking with datamoshing - i'm wondering if that technique might work with a bit of refining... here is their tutorial [https://www.youtube.com/watch?v=VLGKFk3wIL4](https://www.youtube.com/watch?v=VLGKFk3wIL4&fbclid=IwAR3xjKd8c6YA30qRUapf550jhjJZ8II2FwxAUjV5x8F_8tSqOq2_VNL3Ih8)

![Blender 2.8 Tracking with data moshing tutorial](https://external-prg1-1.xx.fbcdn.net/safe_image.php?d=AQGxa3rKOLyC53E2&w=98&h=98&url=https%3A%2F%2Fi.ytimg.com%2Fvi%2FVLGKFk3wIL4%2Fmaxresdefault.jpg&cfs=1&_nc_cb=1&_nc_hash=AQHrrsuPuuS4Gc6W)

YOUTUBE.COM

Blender 2.8 Tracking with data moshing tutorial

[Blender 2.8 Tracking with data moshing tutorial](https://www.youtube.com/watch?v=VLGKFk3wIL4&fbclid=IwAR2pFFqiw2uOZQCtA1xtJo4ksPVHiWcp4YtPv_w-IwGuTgsIwvRm_49L3pc)

![](data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%2318AFFF'/%3e%3cstop offset='100%25' stop-color='%230062DF'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0 0 0 0 0 0.299356041 0 0 0 0 0.681187726 0 0 0 0.3495684 0'/%3e%3c/filter%3e%3cpath id='b' d='M8 0a8 8 0 00-8 8 8 8 0 1016 0 8 8 0 00-8-8z'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='white' d='M12.162 7.338c.176.123.338.245.338.674 0 .43-.229.604-.474.725a.73.73 0 01.089.546c-.077.344-.392.611-.672.69.121.194.159.385.015.62-.185.295-.346.407-1.058.407H7.5c-.988 0-1.5-.546-1.5-1V7.665c0-1.23 1.467-2.275 1.467-3.13L7.361 3.47c-.005-.065.008-.224.058-.27.08-.079.301-.2.635-.2.218 0 .363.041.534.123.581.277.732.978.732 1.542 0 .271-.414 1.083-.47 1.364 0 0 .867-.192 1.879-.199 1.061-.006 1.749.19 1.749.842 0 .261-.219.523-.316.666zM3.6 7h.8a.6.6 0 01.6.6v3.8a.6.6 0 01-.6.6h-.8a.6.6 0 01-.6-.6V7.6a.6.6 0 01.6-.6z'/%3e%3c/g%3e%3c/svg%3e)

1

-   -    · 
        
        Reply
        
    -    · 
        
        Share
        
    -    · [1 d](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3641485692572747&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)
    -    · 
        
        Edited
        
    

[Marc Sciglimpaglia](https://www.facebook.com/groups/526421244079223/user/1594452611/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

Highly doable in the digital domain, I’ve also done some openFrameworks and Jitter experiments but lost the code years ago. The trick is to think less in terms of scan lines and more of a circular frame buffer of a predetermined size. Then your strategy for composing any single frame is just to map each pixel to a buffer index, which can be as simple as a vertical or horizontal gradient for the classic slitscan effect, or any number of other mappings which you can change on the fly. So you’re not distorting time, you’re distorting a per-pixel window into a moving block of time.

![](data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%2318AFFF'/%3e%3cstop offset='100%25' stop-color='%230062DF'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0 0 0 0 0 0.299356041 0 0 0 0 0.681187726 0 0 0 0.3495684 0'/%3e%3c/filter%3e%3cpath id='b' d='M8 0a8 8 0 00-8 8 8 8 0 1016 0 8 8 0 00-8-8z'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='white' d='M12.162 7.338c.176.123.338.245.338.674 0 .43-.229.604-.474.725a.73.73 0 01.089.546c-.077.344-.392.611-.672.69.121.194.159.385.015.62-.185.295-.346.407-1.058.407H7.5c-.988 0-1.5-.546-1.5-1V7.665c0-1.23 1.467-2.275 1.467-3.13L7.361 3.47c-.005-.065.008-.224.058-.27.08-.079.301-.2.635-.2.218 0 .363.041.534.123.581.277.732.978.732 1.542 0 .271-.414 1.083-.47 1.364 0 0 .867-.192 1.879-.199 1.061-.006 1.749.19 1.749.842 0 .261-.219.523-.316.666zM3.6 7h.8a.6.6 0 01.6.6v3.8a.6.6 0 01-.6.6h-.8a.6.6 0 01-.6-.6V7.6a.6.6 0 01.6-.6z'/%3e%3c/g%3e%3c/svg%3e)

12

-    · 
    
    Reply
    
-    · 
    
    Share
    
-    · [1 d](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3641498105904839&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

[Anton Marini](https://www.facebook.com/groups/526421244079223/user/100000511725247/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

This is the way. I’ve made exactly this digital filter and composing bw masks to feed the buffer offset results in really cool effects, especially if you feed the output of the buffer back into it.

![](data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%2318AFFF'/%3e%3cstop offset='100%25' stop-color='%230062DF'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0 0 0 0 0 0.299356041 0 0 0 0 0.681187726 0 0 0 0.3495684 0'/%3e%3c/filter%3e%3cpath id='b' d='M8 0a8 8 0 00-8 8 8 8 0 1016 0 8 8 0 00-8-8z'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='white' d='M12.162 7.338c.176.123.338.245.338.674 0 .43-.229.604-.474.725a.73.73 0 01.089.546c-.077.344-.392.611-.672.69.121.194.159.385.015.62-.185.295-.346.407-1.058.407H7.5c-.988 0-1.5-.546-1.5-1V7.665c0-1.23 1.467-2.275 1.467-3.13L7.361 3.47c-.005-.065.008-.224.058-.27.08-.079.301-.2.635-.2.218 0 .363.041.534.123.581.277.732.978.732 1.542 0 .271-.414 1.083-.47 1.364 0 0 .867-.192 1.879-.199 1.061-.006 1.749.19 1.749.842 0 .261-.219.523-.316.666zM3.6 7h.8a.6.6 0 01.6.6v3.8a.6.6 0 01-.6.6h-.8a.6.6 0 01-.6-.6V7.6a.6.6 0 01.6-.6z'/%3e%3c/g%3e%3c/svg%3e)

6

-   -    · 
        
        Reply
        
    -    · 
        
        Share
        
    -    · [1 d](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3641498105904839&reply_comment_id=3641576335897016&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)
    

[Todd Sines](https://www.facebook.com/groups/526421244079223/user/504820890/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

[

Marc

](https://www.facebook.com/groups/526421244079223/user/1594452611/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)&[

Anton

](https://www.facebook.com/groups/526421244079223/user/100000511725247/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)— do you mind showing any examples?

-   -    · 
        
        Reply
        
    -    · 
        
        Share
        
    -    · [1 d](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3641498105904839&reply_comment_id=3641785032542813&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)
    

Write a public reply…

  

[Chris King](https://www.facebook.com/groups/526421244079223/user/286500765/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

![badge icon](https://static.xx.fbcdn.net/rsrc.php/v3/yi/r/oJUaDQAiBQe.png)

[

Todd Sines

](https://www.facebook.com/groups/526421244079223/user/504820890/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)in this group we obey the laws of thermodynamics![😡](https://static.xx.fbcdn.net/images/emoji.php/v9/te6/1.5/16/1f621.png)

![](data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 180 180' %3e %3cdefs%3e %3cradialGradient cx='50.001%25' cy='50%25' fx='50.001%25' fy='50%25' r='50%25' id='c'%3e %3cstop stop-color='%23F08423' stop-opacity='0' offset='0%25'/%3e %3cstop stop-color='%23F08423' stop-opacity='.34' offset='100%25'/%3e %3c/radialGradient%3e %3cradialGradient cx='50%25' cy='44.086%25' fx='50%25' fy='44.086%25' r='57.412%25' gradientTransform='matrix(-1 0 0 -.83877 1 .81)' id='d'%3e %3cstop stop-color='%23FFE874' offset='0%25'/%3e %3cstop stop-color='%23FFE368' stop-opacity='0' offset='100%25'/%3e %3c/radialGradient%3e %3cradialGradient cx='10.82%25' cy='52.019%25' fx='10.82%25' fy='52.019%25' r='10.077%25' gradientTransform='matrix(.91249 .4091 -.31644 .7058 .174 .109)' id='e'%3e %3cstop stop-color='%23F28A2D' stop-opacity='.5' offset='0%25'/%3e %3cstop stop-color='%23F28A2D' stop-opacity='0' offset='100%25'/%3e %3c/radialGradient%3e %3cradialGradient cx='74.131%25' cy='76.545%25' fx='74.131%25' fy='76.545%25' r='28.284%25' gradientTransform='rotate(-38.243 1.4 .537) scale(1 .40312)' id='f'%3e %3cstop stop-color='%23F28A2D' stop-opacity='.5' offset='0%25'/%3e %3cstop stop-color='%23F28A2D' stop-opacity='0' offset='100%25'/%3e %3c/radialGradient%3e %3cradialGradient cx='31.849%25' cy='12.675%25' fx='31.849%25' fy='12.675%25' r='10.743%25' gradientTransform='matrix(.98371 -.17976 .03575 .19562 0 .16)' id='g'%3e %3cstop stop-color='%23D45F00' stop-opacity='.25' offset='0%25'/%3e %3cstop stop-color='%23D45F00' stop-opacity='0' offset='100%25'/%3e %3c/radialGradient%3e %3cradialGradient cx='68.023%25' cy='12.637%25' fx='68.023%25' fy='12.637%25' r='12.093%25' gradientTransform='rotate(11.848 .192 .076) scale(1 .19886)' id='h'%3e %3cstop stop-color='%23D45F00' stop-opacity='.25' offset='0%25'/%3e %3cstop stop-color='%23D45F00' stop-opacity='0' offset='100%25'/%3e %3c/radialGradient%3e %3cradialGradient cx='50.709%25' cy='66.964%25' fx='50.709%25' fy='66.964%25' r='87.22%25' gradientTransform='matrix(0 -.8825 1 0 -.163 1.117)' id='j'%3e %3cstop stop-color='%233B446B' offset='0%25'/%3e %3cstop stop-color='%23202340' offset='68.84%25'/%3e %3c/radialGradient%3e %3cradialGradient cx='49.239%25' cy='66.964%25' fx='49.239%25' fy='66.964%25' r='87.22%25' gradientTransform='matrix(0 -.8825 1 0 -.177 1.104)' id='k'%3e %3cstop stop-color='%233B446B' offset='0%25'/%3e %3cstop stop-color='%23202340' offset='68.84%25'/%3e %3c/radialGradient%3e %3cradialGradient cx='48.317%25' cy='42.726%25' fx='48.317%25' fy='42.726%25' r='29.766%25' gradientTransform='matrix(-.09519 -.96847 1.71516 -1.15488 -.204 1.389)' id='l'%3e %3cstop stop-color='%23E38200' offset='0%25'/%3e %3cstop stop-color='%23CD6700' offset='100%25'/%3e %3c/radialGradient%3e %3cradialGradient cx='50%25' cy='29.807%25' fx='50%25' fy='29.807%25' r='31.377%25' gradientTransform='matrix(.07236 -.9819 2.22613 1.12405 -.2 .454)' id='m'%3e %3cstop stop-color='%23E38200' offset='0%25'/%3e %3cstop stop-color='%23CD6700' offset='100%25'/%3e %3c/radialGradient%3e %3cradialGradient cx='73.646%25' cy='44.274%25' fx='73.646%25' fy='44.274%25' r='29.002%25' gradientTransform='scale(.92955 1) rotate(20.36 .764 .598)' id='p'%3e %3cstop stop-color='%23FF7091' stop-opacity='.7' offset='0%25'/%3e %3cstop stop-color='%23FE6D8E' stop-opacity='0' offset='100%25'/%3e %3c/radialGradient%3e %3cradialGradient cx='26.749%25' cy='29.688%25' fx='26.749%25' fy='29.688%25' r='29.002%25' gradientTransform='scale(.92955 1) rotate(20.36 .278 .353)' id='q'%3e %3cstop stop-color='%23FF7091' stop-opacity='.7' offset='0%25'/%3e %3cstop stop-color='%23FE6D8E' stop-opacity='0' offset='100%25'/%3e %3c/radialGradient%3e %3cradialGradient cx='23.798%25' cy='53.35%25' fx='23.798%25' fy='53.35%25' r='24.89%25' gradientTransform='matrix(-.18738 .97947 -1.25372 -.27758 .951 .449)' id='r'%3e %3cstop stop-color='%239C0600' stop-opacity='.999' offset='0%25'/%3e %3cstop stop-color='%239C0600' stop-opacity='.94' offset='26.692%25'/%3e %3cstop stop-color='%239C0600' stop-opacity='0' offset='100%25'/%3e %3c/radialGradient%3e %3cradialGradient cx='97.063%25' cy='54.555%25' fx='97.063%25' fy='54.555%25' r='15.021%25' gradientTransform='matrix(.8002 .50886 -.59365 1.08039 .518 -.538)' id='s'%3e %3cstop stop-color='%23C71C08' stop-opacity='.75' offset='0%25'/%3e %3cstop stop-color='%23C71C08' stop-opacity='.704' offset='53.056%25'/%3e %3cstop stop-color='%23C71C08' stop-opacity='0' offset='100%25'/%3e %3c/radialGradient%3e %3cradialGradient cx='4.056%25' cy='24.23%25' fx='4.056%25' fy='24.23%25' r='13.05%25' gradientTransform='matrix(.8728 -.3441 .41218 1.20997 -.095 -.037)' id='t'%3e %3cstop stop-color='%239C0600' stop-opacity='.5' offset='0%25'/%3e %3cstop stop-color='%239C0600' stop-opacity='.473' offset='31.614%25'/%3e %3cstop stop-color='%239C0600' stop-opacity='0' offset='100%25'/%3e %3c/radialGradient%3e %3cradialGradient cx='74.586%25' cy='77.013%25' fx='74.586%25' fy='77.013%25' r='17.563%25' gradientTransform='matrix(.77041 .55955 -.56333 .89765 .605 -.339)' id='u'%3e %3cstop stop-color='%239C0600' stop-opacity='.999' offset='0%25'/%3e %3cstop stop-color='%239C0600' stop-opacity='.934' offset='45.7%25'/%3e %3cstop stop-color='%239C0600' stop-opacity='.803' offset='59.211%25'/%3e %3cstop stop-color='%239C0600' stop-opacity='0' offset='100%25'/%3e %3c/radialGradient%3e %3cradialGradient cx='50.001%25' cy='50%25' fx='50.001%25' fy='50%25' r='51.087%25' gradientTransform='matrix(-.3809 .91219 -.97139 -.46943 1.176 .279)' id='v'%3e %3cstop stop-color='%23C71C08' stop-opacity='0' offset='0%25'/%3e %3cstop stop-color='%23C71C08' offset='100%25'/%3e %3c/radialGradient%3e %3cradialGradient cx='2.243%25' cy='4.089%25' fx='2.243%25' fy='4.089%25' r='122.873%25' gradientTransform='scale(.78523 1) rotate(36.406 .025 .05)' id='x'%3e %3cstop stop-color='%23EDA83A' offset='0%25'/%3e %3cstop stop-color='%23FFDC5E' offset='100%25'/%3e %3c/radialGradient%3e %3cradialGradient cx='100%25' cy='7.011%25' fx='100%25' fy='7.011%25' r='105.039%25' gradientTransform='scale(-.90713 -1) rotate(-45.799 -.217 2.489)' id='z'%3e %3cstop stop-color='%23F4B248' offset='0%25'/%3e %3cstop stop-color='%23FFDD5F' offset='100%25'/%3e %3c/radialGradient%3e %3clinearGradient x1='50%25' y1='95.035%25' x2='50%25' y2='7.417%25' id='b'%3e %3cstop stop-color='%23F28A2D' offset='0%25'/%3e %3cstop stop-color='%23FDE86F' offset='100%25'/%3e %3c/linearGradient%3e %3clinearGradient x1='49.985%25' y1='-40.061%25' x2='49.985%25' y2='111.909%25' id='i'%3e %3cstop stop-color='%23482314' offset='0%25'/%3e %3cstop stop-color='%239A4111' offset='100%25'/%3e %3c/linearGradient%3e %3clinearGradient x1='52.727%25' y1='31.334%25' x2='28.964%25' y2='102.251%25' id='o'%3e %3cstop stop-color='%23F34462' offset='0%25'/%3e %3cstop stop-color='%23CC0820' offset='100%25'/%3e %3c/linearGradient%3e %3cpath d='M180 90c0 49.71-40.29 90-90 90S0 139.71 0 90 40.29 0 90 0s90 40.29 90 90z' id='a'/%3e %3cpath d='M108.947 95.828c-23.47-7.285-31.71 8.844-31.71 8.844s2.376-17.954-21.095-25.24c-22.57-7.004-36.253 13.757-37.307 26.812-2.264 28.103 22.134 59.996 31.26 70.86a8.062 8.062 0 008.34 2.584c13.697-3.777 51.904-16.242 66.009-40.667 6.54-11.328 7.06-36.188-15.497-43.193z' id='n'/%3e %3cpath d='M180.642 90c0 49.71-40.289 90-90 90-49.71 0-90-40.29-90-90s40.29-90 90-90c49.711 0 90 40.29 90 90z' id='w'/%3e %3c/defs%3e %3cg fill='none' fill-rule='evenodd'%3e %3cg fill-rule='nonzero'%3e %3cg transform='translate(.005 -.004)'%3e %3cuse fill='url(%23b)' xlink:href='%23a'/%3e %3cuse fill='url(%23c)' xlink:href='%23a'/%3e %3cuse fill='url(%23d)' xlink:href='%23a'/%3e %3cuse fill='url(%23e)' xlink:href='%23a'/%3e %3cuse fill='url(%23f)' xlink:href='%23a'/%3e %3cuse fill='url(%23g)' xlink:href='%23a'/%3e %3cuse fill='url(%23h)' xlink:href='%23a'/%3e %3c/g%3e %3cpath d='M109.013 66.234c-1.14-3.051-36.872-3.051-38.011 0-1.322 3.558 6.806 8.396 19.012 8.255 12.192.14 20.306-4.71 18.999-8.255z' fill='url(%23i)' transform='translate(.005 -.004)'/%3e %3cpath d='M68.006 46.125c.014 7.566-4.823 9.788-11.995 10.702-7.102 1.068-11.883-2.068-11.995-10.702-.099-7.256 3.81-16.116 11.995-16.284 8.17.168 11.981 9.028 11.995 16.284z' fill='url(%23j)' transform='translate(.005 -.004)'/%3e %3cpath d='M54.807 35.054c1.18 1.378.97 3.769-.479 5.358-1.448 1.575-3.571 1.744-4.753.366-1.181-1.378-.97-3.77.478-5.344 1.449-1.59 3.572-1.744 4.754-.38z' fill='%234E506A'/%3e %3cpath d='M112.022 46.125c-.014 7.566 4.823 9.788 11.995 10.702 7.102 1.068 11.883-2.068 11.995-10.702.099-7.256-3.81-16.116-11.995-16.284-8.184.168-11.995 9.028-11.995 16.284z' fill='url(%23k)' transform='translate(.005 -.004)'/%3e %3cpath d='M124.078 34.52c.957 1.547.38 3.881-1.293 5.217-1.674 1.336-3.797 1.181-4.753-.366-.957-1.546-.38-3.88 1.293-5.217 1.66-1.336 3.797-1.181 4.753.366z' fill='%234E506A'/%3e %3cpath d='M37.969 23.344c-2.349 1.983-.45 6.047 3.515 4.19 6.328-2.967 19.899-6.623 31.824-5.287 3.164.351 4.19-.113 3.487-4.022-.689-3.853-4.33-6.37-13.387-5.26-14.035 1.716-23.09 8.396-25.44 10.379z' fill='url(%23l)' transform='translate(.005 -.004)'/%3e %3cpath d='M116.592 12.952c-9.056-1.111-12.698 1.42-13.387 5.259-.703 3.91.323 4.373 3.487 4.022 11.925-1.336 25.481 2.32 31.824 5.287 3.965 1.857 5.864-2.207 3.515-4.19-2.348-1.97-11.404-8.649-25.439-10.378z' fill='url(%23m)' transform='translate(.005 -.004)'/%3e %3c/g%3e %3cg fill-rule='nonzero'%3e %3cuse fill='url(%23o)' xlink:href='%23n'/%3e %3cuse fill='url(%23p)' xlink:href='%23n'/%3e %3cuse fill='url(%23q)' xlink:href='%23n'/%3e %3cuse fill='url(%23r)' xlink:href='%23n'/%3e %3cuse fill='url(%23s)' xlink:href='%23n'/%3e %3cuse fill='url(%23t)' xlink:href='%23n'/%3e %3cuse fill='url(%23u)' xlink:href='%23n'/%3e %3cuse fill-opacity='.5' fill='url(%23v)' xlink:href='%23n'/%3e %3c/g%3e %3cg transform='translate(-.637 -.004)'%3e %3cmask id='y' fill='white'%3e %3cuse xlink:href='%23w'/%3e %3c/mask%3e %3cpath d='M15.52 86.231C9.642 80.508-.708 77.892-1.89 91.153c-.927 10.364 3.93 27.694 16.234 37.763C45.282 154.23 74.742 139.667 75.628 122c.699-13.932-15.502-12.327-20.648-12.045-.352.014-.507-.45-.197-.647a48.147 48.147 0 004.725-3.488c4.036-3.403 1.968-9.31-3.67-7.607-.858.253-14.583 4.359-23.288 1.068-9.872-3.726-11.053-7.214-17.03-13.05z' fill='url(%23x)' fill-rule='nonzero' mask='url(%23y)'/%3e %3cpath d='M161.081 88.2c3.502-6.778 9.066-4.401 12.194-3.359 4.61 1.537 7.353 4.4 7.353 11.572 0 17.001-2.812 32.765-17.002 48.6-25.987 28.982-69.935 25.143-73.675 6.862-3.094-15.16 13.066-16.678 18.34-17.381.365-.042.421-.605.098-.746a46.169 46.169 0 01-5.4-2.896c-5.444-3.403-3.989-10.051 2.405-9.07 6.806 1.012 15.23 2.924 22.486 2.207 21.009-2.11 24.975-19.87 33.201-35.789z' fill='url(%23z)' fill-rule='nonzero' mask='url(%23y)'/%3e %3c/g%3e %3c/g%3e %3c/svg%3e)![](data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%2318AFFF'/%3e%3cstop offset='100%25' stop-color='%230062DF'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0 0 0 0 0 0.299356041 0 0 0 0 0.681187726 0 0 0 0.3495684 0'/%3e%3c/filter%3e%3cpath id='b' d='M8 0a8 8 0 00-8 8 8 8 0 1016 0 8 8 0 00-8-8z'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='white' d='M12.162 7.338c.176.123.338.245.338.674 0 .43-.229.604-.474.725a.73.73 0 01.089.546c-.077.344-.392.611-.672.69.121.194.159.385.015.62-.185.295-.346.407-1.058.407H7.5c-.988 0-1.5-.546-1.5-1V7.665c0-1.23 1.467-2.275 1.467-3.13L7.361 3.47c-.005-.065.008-.224.058-.27.08-.079.301-.2.635-.2.218 0 .363.041.534.123.581.277.732.978.732 1.542 0 .271-.414 1.083-.47 1.364 0 0 .867-.192 1.879-.199 1.061-.006 1.749.19 1.749.842 0 .261-.219.523-.316.666zM3.6 7h.8a.6.6 0 01.6.6v3.8a.6.6 0 01-.6.6h-.8a.6.6 0 01-.6-.6V7.6a.6.6 0 01.6-.6z'/%3e%3c/g%3e%3c/svg%3e)![](data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='10.25%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%23FEEA70'/%3e%3cstop offset='100%25' stop-color='%23F69B30'/%3e%3c/linearGradient%3e%3clinearGradient id='d' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%23472315'/%3e%3cstop offset='100%25' stop-color='%238B3A0E'/%3e%3c/linearGradient%3e%3clinearGradient id='e' x1='50%25' x2='50%25' y1='0%25' y2='81.902%25'%3e%3cstop offset='0%25' stop-color='%23FC607C'/%3e%3cstop offset='100%25' stop-color='%23D91F3A'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0.921365489 0 0 0 0 0.460682745 0 0 0 0 0 0 0 0 0.35 0'/%3e%3c/filter%3e%3cpath id='b' d='M16 8A8 8 0 110 8a8 8 0 0116 0'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='url(%23d)' d='M3 8.008C3 10.023 4.006 14 8 14c3.993 0 5-3.977 5-5.992C13 7.849 11.39 7 8 7c-3.39 0-5 .849-5 1.008'/%3e%3cpath fill='url(%23e)' d='M4.541 12.5c.804.995 1.907 1.5 3.469 1.5 1.563 0 2.655-.505 3.459-1.5-.551-.588-1.599-1.5-3.459-1.5s-2.917.912-3.469 1.5'/%3e%3cpath fill='%232A3755' d='M6.213 4.144c.263.188.502.455.41.788-.071.254-.194.369-.422.371-.78.011-1.708.255-2.506.612-.065.029-.197.088-.332.085-.124-.003-.251-.058-.327-.237-.067-.157-.073-.388.276-.598.545-.33 1.257-.48 1.909-.604a7.077 7.077 0 00-1.315-.768c-.427-.194-.38-.457-.323-.6.127-.317.609-.196 1.078.026a9 9 0 011.552.925zm3.577 0a8.953 8.953 0 011.55-.925c.47-.222.95-.343 1.078-.026.057.143.104.406-.323.6a7.029 7.029 0 00-1.313.768c.65.123 1.363.274 1.907.604.349.21.342.44.276.598-.077.18-.203.234-.327.237-.135.003-.267-.056-.332-.085-.797-.357-1.725-.6-2.504-.612-.228-.002-.351-.117-.422-.37-.091-.333.147-.6.41-.788z'/%3e%3c/g%3e%3c/svg%3e)

11

-    · 
    
    Reply
    
-    · 
    
    Share
    
-    · [1 d](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3642236852497631&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

[Montse T. Martí](https://www.facebook.com/groups/526421244079223/user/739778412/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

[

Chris King

](https://www.facebook.com/groups/526421244079223/user/286500765/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)pardon?! Isn’t this the place where Millennials! throw threads about Digital Aberration, comment on how to emulate classic effects, the latest graphic cards and best GPU’s ?! ![🥴](https://static.xx.fbcdn.net/images/emoji.php/v9/t47/1.5/16/1f974.png) 

![](data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%2318AFFF'/%3e%3cstop offset='100%25' stop-color='%230062DF'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0 0 0 0 0 0.299356041 0 0 0 0 0.681187726 0 0 0 0.3495684 0'/%3e%3c/filter%3e%3cpath id='b' d='M8 0a8 8 0 00-8 8 8 8 0 1016 0 8 8 0 00-8-8z'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='white' d='M12.162 7.338c.176.123.338.245.338.674 0 .43-.229.604-.474.725a.73.73 0 01.089.546c-.077.344-.392.611-.672.69.121.194.159.385.015.62-.185.295-.346.407-1.058.407H7.5c-.988 0-1.5-.546-1.5-1V7.665c0-1.23 1.467-2.275 1.467-3.13L7.361 3.47c-.005-.065.008-.224.058-.27.08-.079.301-.2.635-.2.218 0 .363.041.534.123.581.277.732.978.732 1.542 0 .271-.414 1.083-.47 1.364 0 0 .867-.192 1.879-.199 1.061-.006 1.749.19 1.749.842 0 .261-.219.523-.316.666zM3.6 7h.8a.6.6 0 01.6.6v3.8a.6.6 0 01-.6.6h-.8a.6.6 0 01-.6-.6V7.6a.6.6 0 01.6-.6z'/%3e%3c/g%3e%3c/svg%3e)

1

-   -    · 
        
        Reply
        
    -    · 
        
        Share
        
    -    · [21 h](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3642236852497631&reply_comment_id=3643490682372248&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)
    -    · 
        
        Edited
        
    

[Chris King](https://www.facebook.com/groups/526421244079223/user/286500765/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

![badge icon](https://static.xx.fbcdn.net/rsrc.php/v3/yi/r/oJUaDQAiBQe.png)

[

Montse T. Martí

](https://www.facebook.com/groups/526421244079223/user/739778412/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)remember I am a millennial ![😤](https://static.xx.fbcdn.net/images/emoji.php/v9/t69/1.5/16/1f624.png) [https://www.youtube.com/watch?v=Dc-m9dumEaw](https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DDc-m9dumEaw%26fbclid%3DIwAR0ebqquzvGBPeGlP-zIPPcmObv5bu27p1FJ2s8HwfKrdiCHQ-3dk7fhBjs&h=AT2sYon-lBdsauJ9x1qTmHwTNCUJomI3bvAhfQuUly_KkbDT3hZXGv61WEyfnhSi2JUY0G_osI-LOgHrWx3gKehm5zTRUnTVPn8ZSOmcYzc_ztvWk_76-KOKM5XIA43sLQ&__tn__=R]-R&c[0]=AT1mqXFnzRsoof9Wpx8qGfejHStq5_k5aY4Y05f-gCztERHRAp2gjzakT1KkAj2MDJhiAXGTrHcVpFWnVBWqlUs8EOV_MfcZFyrLTT_LuAfRIAPsTmZDkmBA_2KKwLmveFsSJQAkH84lgEE0MC8Y00b68x7S7nKxWLo)

![Homer: "....Lisa, in this house we obey the laws of thermodynamics !!"](https://external-prg1-1.xx.fbcdn.net/safe_image.php?d=AQHSAQ9pUhhQt4AM&w=98&h=98&url=https%3A%2F%2Fi.ytimg.com%2Fvi%2FDc-m9dumEaw%2Fmaxresdefault.jpg&cfs=1&_nc_cb=1&_nc_hash=AQEbnF95THip1WDV)

YOUTUBE.COM

Homer: "....Lisa, in this house we obey the laws of thermodynamics !!"

[Homer: "....Lisa, in this house we obey the laws of thermodynamics !!"](https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DDc-m9dumEaw%26fbclid%3DIwAR2-NBTZoiRdeb6QRcVg-cfvzFDQIVGuI18XRy0eiXcs6lUHM7tlllU3erg&h=AT1qkrXXU011vha7bKLfZKpG4MN32lZo8HnakOerpRybwZoLvYl9wkzodJ8LYwAwrNQ7ilkNjLG-FSAuW54bdwEypDi67mUk5vPWHdt5H2jfiA6jQLntfYRUHH1_dEFy72SClG6mAaBeLw&__tn__=R]-R&c[0]=AT1mqXFnzRsoof9Wpx8qGfejHStq5_k5aY4Y05f-gCztERHRAp2gjzakT1KkAj2MDJhiAXGTrHcVpFWnVBWqlUs8EOV_MfcZFyrLTT_LuAfRIAPsTmZDkmBA_2KKwLmveFsSJQAkH84lgEE0MC8Y00b68x7S7nKxWLo)

![](data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='10.25%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%23FEEA70'/%3e%3cstop offset='100%25' stop-color='%23F69B30'/%3e%3c/linearGradient%3e%3clinearGradient id='d' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%23472315'/%3e%3cstop offset='100%25' stop-color='%238B3A0E'/%3e%3c/linearGradient%3e%3clinearGradient id='e' x1='50%25' x2='50%25' y1='0%25' y2='81.902%25'%3e%3cstop offset='0%25' stop-color='%23FC607C'/%3e%3cstop offset='100%25' stop-color='%23D91F3A'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0.921365489 0 0 0 0 0.460682745 0 0 0 0 0 0 0 0 0.35 0'/%3e%3c/filter%3e%3cpath id='b' d='M16 8A8 8 0 110 8a8 8 0 0116 0'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='url(%23d)' d='M3 8.008C3 10.023 4.006 14 8 14c3.993 0 5-3.977 5-5.992C13 7.849 11.39 7 8 7c-3.39 0-5 .849-5 1.008'/%3e%3cpath fill='url(%23e)' d='M4.541 12.5c.804.995 1.907 1.5 3.469 1.5 1.563 0 2.655-.505 3.459-1.5-.551-.588-1.599-1.5-3.459-1.5s-2.917.912-3.469 1.5'/%3e%3cpath fill='%232A3755' d='M6.213 4.144c.263.188.502.455.41.788-.071.254-.194.369-.422.371-.78.011-1.708.255-2.506.612-.065.029-.197.088-.332.085-.124-.003-.251-.058-.327-.237-.067-.157-.073-.388.276-.598.545-.33 1.257-.48 1.909-.604a7.077 7.077 0 00-1.315-.768c-.427-.194-.38-.457-.323-.6.127-.317.609-.196 1.078.026a9 9 0 011.552.925zm3.577 0a8.953 8.953 0 011.55-.925c.47-.222.95-.343 1.078-.026.057.143.104.406-.323.6a7.029 7.029 0 00-1.313.768c.65.123 1.363.274 1.907.604.349.21.342.44.276.598-.077.18-.203.234-.327.237-.135.003-.267-.056-.332-.085-.797-.357-1.725-.6-2.504-.612-.228-.002-.351-.117-.422-.37-.091-.333.147-.6.41-.788z'/%3e%3c/g%3e%3c/svg%3e)![](data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%2318AFFF'/%3e%3cstop offset='100%25' stop-color='%230062DF'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0 0 0 0 0 0.299356041 0 0 0 0 0.681187726 0 0 0 0.3495684 0'/%3e%3c/filter%3e%3cpath id='b' d='M8 0a8 8 0 00-8 8 8 8 0 1016 0 8 8 0 00-8-8z'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='white' d='M12.162 7.338c.176.123.338.245.338.674 0 .43-.229.604-.474.725a.73.73 0 01.089.546c-.077.344-.392.611-.672.69.121.194.159.385.015.62-.185.295-.346.407-1.058.407H7.5c-.988 0-1.5-.546-1.5-1V7.665c0-1.23 1.467-2.275 1.467-3.13L7.361 3.47c-.005-.065.008-.224.058-.27.08-.079.301-.2.635-.2.218 0 .363.041.534.123.581.277.732.978.732 1.542 0 .271-.414 1.083-.47 1.364 0 0 .867-.192 1.879-.199 1.061-.006 1.749.19 1.749.842 0 .261-.219.523-.316.666zM3.6 7h.8a.6.6 0 01.6.6v3.8a.6.6 0 01-.6.6h-.8a.6.6 0 01-.6-.6V7.6a.6.6 0 01.6-.6z'/%3e%3c/g%3e%3c/svg%3e)

2

-   -    · 
        
        Reply
        
    -    · 
        
        Share
        
    -    · [21 h](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3642236852497631&reply_comment_id=3643493769038606&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)
    

Write a public reply…

  

[Miles Taylor](https://www.facebook.com/groups/526421244079223/user/1452960088/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

video\_waaaves has a slitscan module

-    · 
    
    Reply
    
-    · 
    
    Share
    
-    · [1 d](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3643086679079315&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

[Miles Taylor](https://www.facebook.com/groups/526421244079223/user/1452960088/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

[https://etsy.me/3qyGtb8](https://l.facebook.com/l.php?u=https%3A%2F%2Fetsy.me%2F3qyGtb8%3Ffbclid%3DIwAR1Ifc03SRSYAf3GSF5NQcA92oSUd3SIuIOE306DkyqC9kWfqnKmAKanRxI&h=AT1AaOeXKQCLVuEHaAvqK_r4ha-tTsiMcwbzOp8eVaxXp0qG4MJ3cql0iui2ceRbZ7HSUhrNxs9DlcC6tStqYVhL9xpXnhXmd4NcARRhRV8biMjVWOrIPSsoBJTwsCOBKg&__tn__=R]-R&c[0]=AT1mqXFnzRsoof9Wpx8qGfejHStq5_k5aY4Y05f-gCztERHRAp2gjzakT1KkAj2MDJhiAXGTrHcVpFWnVBWqlUs8EOV_MfcZFyrLTT_LuAfRIAPsTmZDkmBA_2KKwLmveFsSJQAkH84lgEE0MC8Y00b68x7S7nKxWLo)

![Temporal_Vortex CAPTURE BUNDLE real time slit scan video synthesizer](https://external-prg1-1.xx.fbcdn.net/safe_image.php?d=AQElWWE16DUTOvHd&w=98&h=98&url=https%3A%2F%2Fi.etsystatic.com%2F22109680%2Fr%2Fil%2F72026b%2F2769610703%2Fil_570xN.2769610703_am7g.jpg&cfs=1&_nc_cb=1&_nc_hash=AQHK4tKqW5Q93RY6)

ETSY.COM

Temporal\_Vortex CAPTURE BUNDLE real time slit scan video synthesizer

[Temporal\_Vortex CAPTURE BUNDLE real time slit scan video synthesizer](https://l.facebook.com/l.php?u=https%3A%2F%2Fetsy.me%2F3qyGtb8%3Ffbclid%3DIwAR0Cs7c1p2FEqVM0HHekLVWJ3PLYqr6SdrD2Ao_Z34Bhh62YLXSvIY7UG3w&h=AT3MIMYfLKsVde5wPkNaEJ96ClWbQQiDsEOxMBQulTSjDsvi_pfCpMylUdrGJm0Fm0MhAqUFCGdvsOGToQgp46iONfnBmd6roYuaV1D_mOrIpv3jGMRmgN0oobTGuWwI_GQ0fzEPRtkOSg&__tn__=R]-R&c[0]=AT1mqXFnzRsoof9Wpx8qGfejHStq5_k5aY4Y05f-gCztERHRAp2gjzakT1KkAj2MDJhiAXGTrHcVpFWnVBWqlUs8EOV_MfcZFyrLTT_LuAfRIAPsTmZDkmBA_2KKwLmveFsSJQAkH84lgEE0MC8Y00b68x7S7nKxWLo)

-   -    · 
        
        Reply
        
    -    · 
        
        Share
        
    -    · [1 d](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3643086679079315&reply_comment_id=3643088752412441&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)
    

Write a public reply…

  

[Chris Plant](https://www.facebook.com/groups/526421244079223/user/1005471712/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

I’ve loaded frames into a buffer, that you could push into using a Kinect so where you pushed would go deeper Into the buffer, some thing that?

-   -    · 
        
        Reply
        
    -    · 
        
        Share
        
    -    · [23 h](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3643261359061847&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)
    -    · 
        
        Edited
        
    

[Phoenix Perry](https://www.facebook.com/groups/526421244079223/user/664016137/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

Lol - My eyes just rolled so far back in my head they turned around. Step one, mod a videonics... Step two, hack it. Step three, problem solved. ![😃](https://static.xx.fbcdn.net/images/emoji.php/v9/taa/1.5/16/1f603.png) Exactly. (In all seriousness though, there's that place update that has a machine that'll do it.... ask…

See more

![](data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='10.25%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%23FEEA70'/%3e%3cstop offset='100%25' stop-color='%23F69B30'/%3e%3c/linearGradient%3e%3clinearGradient id='d' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%23472315'/%3e%3cstop offset='100%25' stop-color='%238B3A0E'/%3e%3c/linearGradient%3e%3clinearGradient id='e' x1='50%25' x2='50%25' y1='0%25' y2='81.902%25'%3e%3cstop offset='0%25' stop-color='%23FC607C'/%3e%3cstop offset='100%25' stop-color='%23D91F3A'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0.921365489 0 0 0 0 0.460682745 0 0 0 0 0 0 0 0 0.35 0'/%3e%3c/filter%3e%3cpath id='b' d='M16 8A8 8 0 110 8a8 8 0 0116 0'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='url(%23d)' d='M3 8.008C3 10.023 4.006 14 8 14c3.993 0 5-3.977 5-5.992C13 7.849 11.39 7 8 7c-3.39 0-5 .849-5 1.008'/%3e%3cpath fill='url(%23e)' d='M4.541 12.5c.804.995 1.907 1.5 3.469 1.5 1.563 0 2.655-.505 3.459-1.5-.551-.588-1.599-1.5-3.459-1.5s-2.917.912-3.469 1.5'/%3e%3cpath fill='%232A3755' d='M6.213 4.144c.263.188.502.455.41.788-.071.254-.194.369-.422.371-.78.011-1.708.255-2.506.612-.065.029-.197.088-.332.085-.124-.003-.251-.058-.327-.237-.067-.157-.073-.388.276-.598.545-.33 1.257-.48 1.909-.604a7.077 7.077 0 00-1.315-.768c-.427-.194-.38-.457-.323-.6.127-.317.609-.196 1.078.026a9 9 0 011.552.925zm3.577 0a8.953 8.953 0 011.55-.925c.47-.222.95-.343 1.078-.026.057.143.104.406-.323.6a7.029 7.029 0 00-1.313.768c.65.123 1.363.274 1.907.604.349.21.342.44.276.598-.077.18-.203.234-.327.237-.135.003-.267-.056-.332-.085-.797-.357-1.725-.6-2.504-.612-.228-.002-.351-.117-.422-.37-.091-.333.147-.6.41-.788z'/%3e%3c/g%3e%3c/svg%3e)

1

-   -    · 
        
        Reply
        
    -    · 
        
        Share
        
    -    · [23 h](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3643287182392598&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)
    -    · 
        
        Edited
        
    

[Todd Sines](https://www.facebook.com/groups/526421244079223/user/504820890/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

I want it at the highest resolution as possible ... and[

Matthew Schlanger

](https://www.facebook.com/groups/526421244079223/user/705763048/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)’s gear upstate probably won’t do what I’m looking for ...

[![No photo description available.](https://scontent-prg1-1.xx.fbcdn.net/v/t1.0-0/s526x296/151263786_10159257392700891_4252187453328587892_n.jpg?_nc_cat=106&ccb=3&_nc_sid=dbeb18&_nc_ohc=fxpEOJe1pAUAX_W9PY8&_nc_ht=scontent-prg1-1.xx&tp=7&oh=26a2d3fc6defdb0776e4820a81054cd8&oe=6054FAF4)](https://www.facebook.com/photo.php?fbid=10159257392695891&set=p.10159257392695891&type=3&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

![](data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%2318AFFF'/%3e%3cstop offset='100%25' stop-color='%230062DF'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0 0 0 0 0 0.299356041 0 0 0 0 0.681187726 0 0 0 0.3495684 0'/%3e%3c/filter%3e%3cpath id='b' d='M8 0a8 8 0 00-8 8 8 8 0 1016 0 8 8 0 00-8-8z'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='white' d='M12.162 7.338c.176.123.338.245.338.674 0 .43-.229.604-.474.725a.73.73 0 01.089.546c-.077.344-.392.611-.672.69.121.194.159.385.015.62-.185.295-.346.407-1.058.407H7.5c-.988 0-1.5-.546-1.5-1V7.665c0-1.23 1.467-2.275 1.467-3.13L7.361 3.47c-.005-.065.008-.224.058-.27.08-.079.301-.2.635-.2.218 0 .363.041.534.123.581.277.732.978.732 1.542 0 .271-.414 1.083-.47 1.364 0 0 .867-.192 1.879-.199 1.061-.006 1.749.19 1.749.842 0 .261-.219.523-.316.666zM3.6 7h.8a.6.6 0 01.6.6v3.8a.6.6 0 01-.6.6h-.8a.6.6 0 01-.6-.6V7.6a.6.6 0 01.6-.6z'/%3e%3c/g%3e%3c/svg%3e)

1

-   -    · 
        
        Reply
        
    -    · 
        
        Share
        
    -    · [23 h](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3643294865725163&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)
    

[Todd Sines](https://www.facebook.com/groups/526421244079223/user/504820890/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

[![No photo description available.](https://scontent-prg1-1.xx.fbcdn.net/v/t1.0-0/p160x160/151702809_10159257410935891_2861554919609247974_n.jpg?_nc_cat=100&ccb=3&_nc_sid=dbeb18&_nc_ohc=TgFHU6_yAdwAX9YL3md&_nc_ht=scontent-prg1-1.xx&tp=6&oh=aa5344f2baf8e0261646544f37d034cc&oe=6054D2C0)](https://www.facebook.com/photo.php?fbid=10159257410930891&set=p.10159257410930891&type=3&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

![](data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%2318AFFF'/%3e%3cstop offset='100%25' stop-color='%230062DF'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0 0 0 0 0 0.299356041 0 0 0 0 0.681187726 0 0 0 0.3495684 0'/%3e%3c/filter%3e%3cpath id='b' d='M8 0a8 8 0 00-8 8 8 8 0 1016 0 8 8 0 00-8-8z'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='white' d='M12.162 7.338c.176.123.338.245.338.674 0 .43-.229.604-.474.725a.73.73 0 01.089.546c-.077.344-.392.611-.672.69.121.194.159.385.015.62-.185.295-.346.407-1.058.407H7.5c-.988 0-1.5-.546-1.5-1V7.665c0-1.23 1.467-2.275 1.467-3.13L7.361 3.47c-.005-.065.008-.224.058-.27.08-.079.301-.2.635-.2.218 0 .363.041.534.123.581.277.732.978.732 1.542 0 .271-.414 1.083-.47 1.364 0 0 .867-.192 1.879-.199 1.061-.006 1.749.19 1.749.842 0 .261-.219.523-.316.666zM3.6 7h.8a.6.6 0 01.6.6v3.8a.6.6 0 01-.6.6h-.8a.6.6 0 01-.6-.6V7.6a.6.6 0 01.6-.6z'/%3e%3c/g%3e%3c/svg%3e)

1

-   -    · 
        
        Reply
        
    -    · 
        
        Share
        
    -    · [23 h](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3643310729056910&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)
    

[Todd Sines](https://www.facebook.com/groups/526421244079223/user/504820890/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

[![May be an image of x-ray](https://scontent-prg1-1.xx.fbcdn.net/v/t1.0-9/s851x315/150816221_10159257411035891_6141435091283398990_n.jpg?_nc_cat=110&ccb=3&_nc_sid=dbeb18&_nc_ohc=3CXPA5GhVhIAX_NmfWt&_nc_ht=scontent-prg1-1.xx&tp=7&oh=c429b361026bbc5b54a898f4fdb3040a&oe=60543ED9)](https://www.facebook.com/photo.php?fbid=10159257411030891&set=p.10159257411030891&type=3&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

![](data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%2318AFFF'/%3e%3cstop offset='100%25' stop-color='%230062DF'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0 0 0 0 0 0.299356041 0 0 0 0 0.681187726 0 0 0 0.3495684 0'/%3e%3c/filter%3e%3cpath id='b' d='M8 0a8 8 0 00-8 8 8 8 0 1016 0 8 8 0 00-8-8z'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='white' d='M12.162 7.338c.176.123.338.245.338.674 0 .43-.229.604-.474.725a.73.73 0 01.089.546c-.077.344-.392.611-.672.69.121.194.159.385.015.62-.185.295-.346.407-1.058.407H7.5c-.988 0-1.5-.546-1.5-1V7.665c0-1.23 1.467-2.275 1.467-3.13L7.361 3.47c-.005-.065.008-.224.058-.27.08-.079.301-.2.635-.2.218 0 .363.041.534.123.581.277.732.978.732 1.542 0 .271-.414 1.083-.47 1.364 0 0 .867-.192 1.879-.199 1.061-.006 1.749.19 1.749.842 0 .261-.219.523-.316.666zM3.6 7h.8a.6.6 0 01.6.6v3.8a.6.6 0 01-.6.6h-.8a.6.6 0 01-.6-.6V7.6a.6.6 0 01.6-.6z'/%3e%3c/g%3e%3c/svg%3e)

1

-   -    · 
        
        Reply
        
    -    · 
        
        Share
        
    -    · [23 h](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3643310955723554&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)
    

[Todd Sines](https://www.facebook.com/groups/526421244079223/user/504820890/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

[![May be art](https://scontent-prg1-1.xx.fbcdn.net/v/t1.0-9/s851x315/152285588_10159257411355891_7586597066727266462_n.jpg?_nc_cat=102&ccb=3&_nc_sid=dbeb18&_nc_ohc=kAvmaJUkYFwAX-GFUgq&_nc_ht=scontent-prg1-1.xx&tp=7&oh=9a48cc8fd9002a499ca6e470a2566cb6&oe=60547815)](https://www.facebook.com/photo.php?fbid=10159257411350891&set=p.10159257411350891&type=3&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

![](data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%2318AFFF'/%3e%3cstop offset='100%25' stop-color='%230062DF'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0 0 0 0 0 0.299356041 0 0 0 0 0.681187726 0 0 0 0.3495684 0'/%3e%3c/filter%3e%3cpath id='b' d='M8 0a8 8 0 00-8 8 8 8 0 1016 0 8 8 0 00-8-8z'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='white' d='M12.162 7.338c.176.123.338.245.338.674 0 .43-.229.604-.474.725a.73.73 0 01.089.546c-.077.344-.392.611-.672.69.121.194.159.385.015.62-.185.295-.346.407-1.058.407H7.5c-.988 0-1.5-.546-1.5-1V7.665c0-1.23 1.467-2.275 1.467-3.13L7.361 3.47c-.005-.065.008-.224.058-.27.08-.079.301-.2.635-.2.218 0 .363.041.534.123.581.277.732.978.732 1.542 0 .271-.414 1.083-.47 1.364 0 0 .867-.192 1.879-.199 1.061-.006 1.749.19 1.749.842 0 .261-.219.523-.316.666zM3.6 7h.8a.6.6 0 01.6.6v3.8a.6.6 0 01-.6.6h-.8a.6.6 0 01-.6-.6V7.6a.6.6 0 01.6-.6z'/%3e%3c/g%3e%3c/svg%3e)

1

-   -    · 
        
        Reply
        
    -    · 
        
        Share
        
    -    · [23 h](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3643311149056868&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)
    

[Todd Sines](https://www.facebook.com/groups/526421244079223/user/504820890/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

[![May be art](https://scontent-prg1-1.xx.fbcdn.net/v/t1.0-9/s851x315/151111155_10159257411480891_785631298317937853_n.jpg?_nc_cat=105&ccb=3&_nc_sid=dbeb18&_nc_ohc=ghaKZCZPTzYAX99WCpf&_nc_ht=scontent-prg1-1.xx&tp=7&oh=7b6368c366f64b46cb59c7d860507a62&oe=60546FE4)](https://www.facebook.com/photo.php?fbid=10159257411475891&set=p.10159257411475891&type=3&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

![](data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%2318AFFF'/%3e%3cstop offset='100%25' stop-color='%230062DF'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0 0 0 0 0 0.299356041 0 0 0 0 0.681187726 0 0 0 0.3495684 0'/%3e%3c/filter%3e%3cpath id='b' d='M8 0a8 8 0 00-8 8 8 8 0 1016 0 8 8 0 00-8-8z'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='white' d='M12.162 7.338c.176.123.338.245.338.674 0 .43-.229.604-.474.725a.73.73 0 01.089.546c-.077.344-.392.611-.672.69.121.194.159.385.015.62-.185.295-.346.407-1.058.407H7.5c-.988 0-1.5-.546-1.5-1V7.665c0-1.23 1.467-2.275 1.467-3.13L7.361 3.47c-.005-.065.008-.224.058-.27.08-.079.301-.2.635-.2.218 0 .363.041.534.123.581.277.732.978.732 1.542 0 .271-.414 1.083-.47 1.364 0 0 .867-.192 1.879-.199 1.061-.006 1.749.19 1.749.842 0 .261-.219.523-.316.666zM3.6 7h.8a.6.6 0 01.6.6v3.8a.6.6 0 01-.6.6h-.8a.6.6 0 01-.6-.6V7.6a.6.6 0 01.6-.6z'/%3e%3c/g%3e%3c/svg%3e)

1

-   -    · 
        
        Reply
        
    -    · 
        
        Share
        
    -    · [23 h](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3643311339056849&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)
    

[Todd Sines](https://www.facebook.com/groups/526421244079223/user/504820890/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

[![No photo description available.](https://scontent-prg1-1.xx.fbcdn.net/v/t1.0-9/s851x315/151099693_10159257414705891_1731954050444064954_n.jpg?_nc_cat=104&ccb=3&_nc_sid=dbeb18&_nc_ohc=gqPSEefhdVkAX-5e9Lw&_nc_ht=scontent-prg1-1.xx&tp=7&oh=0188466efa0f040ccf286656b3647135&oe=60542B40)](https://www.facebook.com/photo.php?fbid=10159257414700891&set=p.10159257414700891&type=3&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

![](data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%2318AFFF'/%3e%3cstop offset='100%25' stop-color='%230062DF'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0 0 0 0 0 0.299356041 0 0 0 0 0.681187726 0 0 0 0.3495684 0'/%3e%3c/filter%3e%3cpath id='b' d='M8 0a8 8 0 00-8 8 8 8 0 1016 0 8 8 0 00-8-8z'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='white' d='M12.162 7.338c.176.123.338.245.338.674 0 .43-.229.604-.474.725a.73.73 0 01.089.546c-.077.344-.392.611-.672.69.121.194.159.385.015.62-.185.295-.346.407-1.058.407H7.5c-.988 0-1.5-.546-1.5-1V7.665c0-1.23 1.467-2.275 1.467-3.13L7.361 3.47c-.005-.065.008-.224.058-.27.08-.079.301-.2.635-.2.218 0 .363.041.534.123.581.277.732.978.732 1.542 0 .271-.414 1.083-.47 1.364 0 0 .867-.192 1.879-.199 1.061-.006 1.749.19 1.749.842 0 .261-.219.523-.316.666zM3.6 7h.8a.6.6 0 01.6.6v3.8a.6.6 0 01-.6.6h-.8a.6.6 0 01-.6-.6V7.6a.6.6 0 01.6-.6z'/%3e%3c/g%3e%3c/svg%3e)

2

-    · 
    
    Reply
    
-    · 
    
    Share
    
-    · [23 h](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3643314782389838&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

[Tom Bass](https://www.facebook.com/groups/526421244079223/user/651339872/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

Thanks for talking about this, it inspired me to make an effect ![🙂](https://static.xx.fbcdn.net/images/emoji.php/v9/ta5/1.5/16/1f642.png) 

0:00 / 0:34

![](data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%2318AFFF'/%3e%3cstop offset='100%25' stop-color='%230062DF'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0 0 0 0 0 0.299356041 0 0 0 0 0.681187726 0 0 0 0.3495684 0'/%3e%3c/filter%3e%3cpath id='b' d='M8 0a8 8 0 00-8 8 8 8 0 1016 0 8 8 0 00-8-8z'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='white' d='M12.162 7.338c.176.123.338.245.338.674 0 .43-.229.604-.474.725a.73.73 0 01.089.546c-.077.344-.392.611-.672.69.121.194.159.385.015.62-.185.295-.346.407-1.058.407H7.5c-.988 0-1.5-.546-1.5-1V7.665c0-1.23 1.467-2.275 1.467-3.13L7.361 3.47c-.005-.065.008-.224.058-.27.08-.079.301-.2.635-.2.218 0 .363.041.534.123.581.277.732.978.732 1.542 0 .271-.414 1.083-.47 1.364 0 0 .867-.192 1.879-.199 1.061-.006 1.749.19 1.749.842 0 .261-.219.523-.316.666zM3.6 7h.8a.6.6 0 01.6.6v3.8a.6.6 0 01-.6.6h-.8a.6.6 0 01-.6-.6V7.6a.6.6 0 01.6-.6z'/%3e%3c/g%3e%3c/svg%3e)

5

-   -    · 
        
        Reply
        
    -    · 
        
        Share
        
    -    · [17 h](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3643944398993543&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)
    

[Denise Gallant](https://www.facebook.com/groups/526421244079223/user/1029281582/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

Ask John Whitney Senior (just had to add that).

-   -    · 
        
        Reply
        
    -    · 
        
        Share
        
    -    · [16 h](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3644010355653614&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)
    

[Hugh Cannon](https://www.facebook.com/groups/526421244079223/user/100000939089129/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

Commenting so I can follow this thread.

-   -    · 
        
        Reply
        
    -    · 
        
        Share
        
    -    · [15 h](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3644162488971734&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)
    

[Aaron Lazansky](https://www.facebook.com/groups/526421244079223/user/803349255/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

That would be something

-   -    · 
        
        Reply
        
    -    · 
        
        Share
        
    -    · [12 h](https://www.facebook.com/groups/VIDEOCIRCUITS/permalink/3641441932577123/?comment_id=3644361652285151&__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)
    

[Louis d'Aboville](https://www.facebook.com/groups/526421244079223/user/286303477/?__cft__[0]=AZWpu1rEpvdwyPlBOwWL2jV4ZvebrsSMZmG_WBPpCLGweA59ZX4O4wINCFOe_ecKP2HlSTCjsYB8VfAje9WkNWWlRKityL_O-8yLpvGqIy3ebQNUcrgvdqULiRvZm1wWom_40LkA98s8s3UUDwdUN1Yg&__tn__=R]-R)

TouchDesigner sounds like it would be a great tool for this - Feedback TOP, Displace TOP, and the Time Machine TOP would all be worth checking out.

Also, here's is a nice tutorial on achieving a more traditional slit scan effect

[https://youtu.be/jOcMCGtclBs](https://youtu.be/jOcMCGtclBs?fbclid=IwAR0qHeSSvyaNElLcjlu9cYIb1Tax_0VnR3e8rlE1SzLCZ8vq25-r8ONKl3g)