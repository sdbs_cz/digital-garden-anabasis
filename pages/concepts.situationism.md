# concepts.situationism
#city #media #architecture
# 101 
>Under the controversy of [person–situation debate](https://en.wikipedia.org/wiki/Person%E2%80%93situation_debate "Person–situation debate"), **situationism** is the theory that changes in human behavior are factors of the situation rather than the traits a person possesses.[[1]](https://en.wikipedia.org/wiki/Situationism_(psychology)#cite_note-1) Behavior is believed to be influenced by external, situational factors rather than internal [traits](https://en.wikipedia.org/wiki/Trait_theory "Trait theory") or [motivations](https://en.wikipedia.org/wiki/Motivation "Motivation"). Situationism therefore challenges the positions of trait theorists, such as [Hans Eysenck](https://en.wikipedia.org/wiki/Hans_Eysenck "Hans Eysenck") or [Raymond B. Cattell](https://en.wikipedia.org/wiki/Raymond_B._Cattell "Raymond B. Cattell").[[2]](https://en.wikipedia.org/wiki/Situationism_(psychology)#cite_note-2) This is an ongoing debate that has truth to both sides, psychologists are able to prove each of the view points through human experimentation.
> - https://en.wikipedia.org/wiki/Situationism_(psychology)
> traitblazers meet [[concepts.trailblazers]]

>The **Situationist International** (**SI**) was an [international](https://en.wikipedia.org/wiki/Proletarian_internationalism "Proletarian internationalism") organization of social revolutionaries made up of [avant-garde](https://en.wikipedia.org/wiki/Avant-garde "Avant-garde") artists, intellectuals, and [political theorists](https://en.wikipedia.org/wiki/Political_philosophy "Political philosophy"). It was prominent in Europe from its formation in 1957 to its dissolution in 1972.[[1]](https://en.wikipedia.org/wiki/Situationist_International#cite_note-Plant1992TheMost-1) The intellectual foundations of the Situationist International were derived primarily from [libertarian Marxism](https://en.wikipedia.org/wiki/Libertarian_Marxism "Libertarian Marxism") and the avant-garde [art movements](https://en.wikipedia.org/wiki/Art_movement "Art movement") of the early 20th century, particularly [Dada](https://en.wikipedia.org/wiki/Dada "Dada") and [Surrealism](https://en.wikipedia.org/wiki/Surrealism "Surrealism").[[1]](https://en.wikipedia.org/wiki/Situationist_International#cite_note-Plant1992TheMost-1) Overall, situationist theory represented an attempt to synthesize this diverse field of theoretical disciplines into a modern and comprehensive critique of mid-20th century [advanced capitalism](https://en.wikipedia.org/wiki/Advanced_capitalism "Advanced capitalism").[[1]](https://en.wikipedia.org/wiki/Situationist_International#cite_note-Plant1992TheMost-1) 
> - https://en.wikipedia.org/wiki/Situationist_International
 
## ---> http://www.nothingness.org/
- http://library.nothingness.org/articles/SI/en/display/87
- http://library.nothingness.org/articles/SI/en/display/314

##  games 
- itch.io games #fillin 

### fulldocs
- [[fulldocs.Theory of the Dérive]]
- [[fulldocs.We Go Round and Round in the Night and Are Consumed by Fire]]


## Adjacent
- [[incubation.concept.human geography]]
-  https://en.wikipedia.org/wiki/Psychogeography
------
---
---
---
---
---
---

# #unsorted 
![How did Situationism Influence Art History? | Critical ...](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.0JkZhoWQAK3npu2wgymCSAHaE_%26pid%3DApi&f=1)

![[Pasted image 20211013031922.png]]

![[Pasted image 20211013031833.png]]

![[Pasted image 20211013031858.png]]

![[Pasted image 20211013031845.png]]

![[Pasted image 20211110214931.png]]

![Debord and the Situationist International](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.ORQ40n1GC3Quv1SoeFX6jQHaE3%26pid%3DApi&f=1)

![What is situationism?: a reader](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.qrUnXzuEfj4GoTw-h-7UbwAAAA%26pid%3DApi&f=1)

![Art & design in context The situationist international](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.b4rif790Un8CZ-FhHbefyQHaFj%26pid%3DApi&f=1)

