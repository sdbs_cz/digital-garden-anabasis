# Screen and display
[[incubation.interface]]
[[fulldocs.twitter.zoomable_ui]]

- https://blair-neal.gitbook.io/survey-of-alternative-displays **!!!!**
- A volumetric display for visual, tactile and audio presentation using acoustic trapping https://www.nature.com/articles/s41586-019-1739-5
	- [[incubation.mediamateriality]]
- persistance of vidsion displays - https://hackaday.com/2019/10/29/the-basics-of-persistence-of-vision-projects/


# #techmech 

- https://www.reddit.com/r/software/comments/szfmrf/how_do_i_use_my_old_laptop_as_a_second_screen_to/


## dummy displays

#software
## virtual desktop expansion

### [Deskreen](https://github.com/pavlobu/deskreen)
- turns any device with a web browser into a secondary screen for your computer
### [Barrier](https://github.com/debauchee/barrier)
- [KVM] 
- sharing keyboard and mouse over w\lan
- no monitor share
### [SpaceDesk](https://www.spacedesk.net/)

![[spatial_software_alignment_chart.png]]