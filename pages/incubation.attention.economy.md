
# attention.economy

## 101

> **Attention economics** is an approach to the [management of information](https://en.wikipedia.org/wiki/Information_management "Information management") that treats human [attention](https://en.wikipedia.org/wiki/Attention "Attention") as a scarce [commodity](https://en.wikipedia.org/wiki/Commodity "Commodity") and applies [economic theory](https://en.wikipedia.org/wiki/Economic_theory "Economic theory") to solve various information management problems. According to [Matthew Crawford](https://en.wikipedia.org/wiki/Matthew_Crawford "Matthew Crawford"), "Attention is a [resource](https://en.wikipedia.org/wiki/Resource "Resource")—a person has only so much of it.
>  - https://en.wikipedia.org/wiki/Attention_economy


>In a so-called attention economy, what better way to prove that you’ve paid attention to something than to remix it? Meet the folk music of the twenty-first century.
> - [[people.JaceClayton]]

- [[people.BruceSterling]]
	- https://blog.p2pfoundation.net/daniel-estrada-on-bruce-sterlings-the-caryatids-as-a-model-for-the-attention-economy/2013/10/04

- [[people.JaronLanier]]
	- https://www.quora.com/How-plausible-is-the-proposed-new-economy-that-Jaron-Lanier-proposes-in-his-book-Who-Owns-the-Future?share=1
	- https://www.forbes.com/sites/anthonykosner/2013/07/05/really-could-the-internet-be-shrinking-the-economy-jaron-lanier-on-how-to-fix-it/

- [[people.AndyMatuschak]] note on Programmable attention 
	- https://notes.andymatuschak.org/zJrfPCbY7GcpV9asEc8NTVzXTAV4TvRFMuY6



## Adjacent

- [[concepts.playlisting]]
- [[concepts.memory]]
- [[incubation.mediamateriality]]
- [[concepts.cryptoart]]
- [[incubation.concepts.post-digital]]
- https://en.wikipedia.org/wiki/Parasocial_interaction
- [[fulldocs.BertoltBrech.Questions From a Worker Who Reads.1935]]

-------------------

- Apathy as tool of [[incubation.attention.economy]]
- Server nomadism and patchy junctions

-----------
---------------------------------------

pluralistic@mamot.fr - This is the commercial pressure that turned the esoteric web into the generic web of sensationalism, clickbait and cute animals. It didn't just transform what writers wrote - it also transformed how writers and readers related to one another.

8/

### unclean

utopiah@mastodon.pirateparty.be - Yes ... https://twitter.com/utopiah/status/1554074975862226946 and sadly my still so terribly current (yet raw) notes on Postman's Amusing Ourselves To Death https://fabien.benetou.fr/ReadingNotes/AmusingOurselvesToDeath​

RT @noUpside@twitter.com

Glad people are coming around to this pov. There is a structure to social networks/media, impossible to separate out fully from the substance (content, behavior) it incentivizes. A mutually-shaping system.

https://www.nytimes.com/2022/08/07/opinion/media-message-twitter-instagram.html?smid=nytcore-ios-share https://www.nytimes.com/2022/08/07/opinion/media-message-twitter-instagram.html?referringSource=articleShare​

🐦🔗: https://twitter.com/noUpside/status/1556684435113889792​
![[Pasted image 20220824013443.png]]



> In the camps in north-west Syria, the BBC found that the trend was being facilitated by so-called "TikTok middlemen", who provided families with the phones and equipment to go live.
> - https://www.bbc.com/news/world-63213567

### folders and autechre
> A lot of the recent Autechre output has been a celebration of the big, fat unshackled ponder, the great idea sent boinging down the can for ten minutes and then popping out of the combine. The thirty-five live shows came to us on top of the 2016 “album” _elseq 1-5_ (which lasts four hours and feels like ten) and the 2018 four-volume “album” _NTS Sessions_ (which goes on for eight hours and feels like six). These are folders, not albums.
>  - https://web.archive.org/web/20201229213134/https://substack.sashafrerejones.com/p/autechre-sign

[[autechre]]

Also what about psychic TV and throbbing gristle.

[[incubation.mediamateriality]]