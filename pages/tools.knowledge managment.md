## Tools for [[concepts.knowledge managment]]
>A management information base (MIB) is a database used for managing the entities in a communication network. 
>> https://en.wikipedia.org/wiki/Management_information_base

- [Nice mini essay about tools and notetaking](https://macwright.com/2021/03/16/return-of-fancy-tools.html)
- [How can we develop transformative tools for thought?](https://numinous.productions/ttft/)

### Tools for thought
- notion
- [tools.obsidian](tools.obsidian.md)
- [[tools.dendron]]
- [tiddlywiki](https://tiddlywiki.com/)
- [neuron](https://www.srid.ca/neuron-v1.html)
- [memex](https://getmemex.com/)
	- https://www.steveliu.co/memex
#### To check
- https://edit.place/@philippschmitt/introduction
	- [[incubation.concept.canvas]]
- https://www.litmaps.co/ 

## Adjacent
-[[concepts.digital garden]]

# #tosort
- https://carroftheoverflow.wordpress.com/2016/08/06/dont-let-the-memex-be-a-dreamex/
- https://djon.es/blog/2020/07/06/designing-a-personal-memex-with-foam/
- https://alumni.media.mit.edu/~rhodes/Papers/remembrance.html
- https://zettelkasten.de/posts/reading-web-rss-note-taking/

