>"**Second brain**" can refer to:
>
>-   The [enteric nervous system](https://en.wikipedia.org/wiki/Enteric_nervous_system "Enteric nervous system").
>-   A now-disproven theory that some large dinosaurs may have had [two brains](https://en.wikipedia.org/wiki/Dinosaur_intelligence#"Two_brains"_myth).
> > https://en.wikipedia.org/wiki/Second_brain

- [[concepts.memory]]
- [[concepts.knowledge managment]]