
#unsorted 

- https://linuxmusicians.com/viewtopic.php?t=20773 !!! links collection

	- -->

https://github.com/nodiscc/awesome-linuxaudio
https://loudmax.blogspot.com/
https://github.com/HiFi-LoFi/KlangFalter convolution!
https://kx.studio//Repositories:Plugins
https://www.warmplace.ru/soft/sunvox/
https://home.snafu.de/wahlm/dl8hbs/declick.html
https://github.com/evandelisle/showq cueplayer
https://github.com/alopatindev/sync-audio-tracks/
https://musical-artifacts.com/


https://www.surina.net/soundtouch/

## Souncards and other hardware

### various compatibility lists
- https://wiki.linuxaudio.org/wiki/hardware_support
- https://github.com/torvalds/linux/blob/master/sound/usb/quirks-table.h
- https://www.alsa-project.org/wiki/Matrix:Main
- http://bandshed.net/pdf/LinuxFullySupportedDevice9t2.pdf
- https://interfacinglinux.com/linux-compatible-audio-interfaces/
- https://linuxmusicians.com/viewtopic.php?t=17472&start=30
- arch linux wiki
- https://www.reddit.com/r/linuxaudio/s/xX7VpZAmKn !!!

https://linuxmusicians.com/viewtopic.php?f=18&t=17472&start=30

- http://quicktoots.linux-audio.com/toots/el-cheapo/
	- old skool nerdy way to sync soundcards


### ADAT interfaces
- tascam 208i
- focusrite 18i20 + 18i8 2nd + 3rd gen 
	- https://github.com/geoffreybennett/alsa-scarlett-gui
- motu AVB
	- AVB ultralite
	- [8pre-es](https://motu.com/products/proaudio/8pre-es)
	- https://motu.com/products/avb/lp32
- RME
	- Babyface, Babyface Pro and Babyface Pro FS
	- Fireface UFX is also known to work with Linux, both via Firewire (if you still have it) and via USB in class-compliant mode.
- audient evo16
- presonus 1818VSL

### Specific brands
#### [[linux.audio.steinberg]]
#### [[linux.audio.soundcraft]]

#### Tascam model 16 mix
- https://discourse.ardour.org/t/tascam-model-16/104566
- https://discourse.ardour.org/t/multitrack-recording-with-tascam-model-16/110871
- https://www.reddit.com/r/linuxaudio/comments/1i7k7vq/about_to_switch_back_to_windows_but_this_should/


## Drivers
### Pipewire
#### Pro Audio mode!!!
- https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/FAQ#what-is-the-pro-audio-profile
## Software
### Theory and connections
>Instead, Linux music production can be envisioned as an assembly line where each “worker” (in this case, program) is tasked with a different duty.
> - https://wiki.linuxaudio.org/wiki/composition

- https://kx.studio/Applications
- ...
- https://www.reddit.com/r/linuxaudio/comments/1gli07x/winemusiccreation_an_organization_on_gitlab_and/
- 
-----
-----






### DAWs
- zrythm - https://www.zrythm.org/en/features.html
- non - reintvents DAW - https://non.tuxfamily.org/
	- non-xt ??
- ardour
	- clip launch!
- waveform -https://www.tracktion.com/products/waveform-free-expansions
	- modularly paid
- bitwig - paid
- [[inform.reaper]]

- light linux daw 
	- https://linuxmusicians.com/viewtopic.php?t=27396
#### also see workstations
- audiostellar
- bespoke synth
- din is noise #paid-sometimes 
	- https://dinisnoise.org/?what=screenshots

### MIDI
- machina - finite state automata midi sequencer
	- http://drobilla.net/software/machina.html

### Plugins
- https://www.airwindows.com/ !!!
- recommended DSPs - https://wiki.linuxaudio.org/wiki/plug-ins
	- https://hiphopmakers.com/best-free-linux-vst-plugins

#### ToolSets
- https://calf-studio-gear.org/#filter
- https://lsp-plug.in/
- https://www.airwindows.com/

#### LV2
- https://lv2plug.in/pages/projects.html
- https://drobilla.net/2019/11/11/lv2-the-good-bad-and-ugly.html

#### Effects

- https://github.com/michaelwillis/dragonfly-reverb
- https://lsp-plug.in/ !!!
- http://openavproductions.com/artyfx/ !!
	- https://github.com/openAVproductions/openAV-ArtyFX
- https://code.google.com/archive/p/talentledhack/
	- vocoder?
- https://github.com/jeremysalwen/kn0ck0ut-lv2
	- **spectral substraction**
- http://drobilla.net/software/fomp.html fomp pedals 

##### grains
- https://www.audiopluginsforfree.com/grain-strain/

##### compressor
- https://www.audiopluginsforfree.com/convergence-free/ !!!!

##### Noise reduction
- https://github.com/lucianodato/noise-repellent/wiki
- https://linuxmusicians.com/viewtopic.php?t=18930
- https://www.homebrewaudio.com/9603/reafir-madness-hidden-noise-reduction-tool-in-reaper/
- https://www.neilbickford.com/blog/2020/02/02/a-real-time-jsfx-audio-denoiser-for-reaper/
- https://github.com/micsthepick/REAPERDenoiser/tree/minphase
#### Synths
- sorcer - http://openavproductions.com/sorcer/
	- wavetable


- https://github.com/jeremysalwen/So-synth-LV2

>Openmastering@pixelfed.social - #Vitalium is a fork from the #audioplugin #vital. It's meant to deal with various licencing (I think? @falktx@mastodon.falktx.com ) issues.
Well, it's the same high-quality #wavetable #synth, most probably one of the best synth around at the moment.
>
It's linux only and you'll get it when you install distrho-ports.
>https://github.com/DISTRHO/DISTRHO-Ports​

## Free audio data
- https://wiki.linuxaudio.org/wiki/free_audio_data

## Unsorted / See also
- https://integraudio.com/6-best-noise-reduction-plugins/#The_3_Best_Free_Noise_Reduction_Plugins_2022 [[audio.denoise]]
- https://www.reddit.com/r/vcvrack/comments/zopcjf/simpliciter_by_nysthi_has_amazing_voct_control/ !! ?? [[vcv rack]]
	- Sample oscillator also known as confusingSimpler
- cardinal vs. [[vcv rack]]
	- https://github.com/DISTRHO/Cardinal
- https://github.com/DISTRHO/Ildaeil 
- drones and linux https://linuxmusicians.com/viewtopic.php?t=25136
- https://www.vintagesynth.com/articles/lira8-free-digital-version-soma-labs-lyra-8 !!!
- 






