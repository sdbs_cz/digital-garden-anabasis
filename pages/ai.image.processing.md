# AI and image processing

## Lists and forums
- https://github.com/stars/LinuxBeaver/lists/beaver-s-image-editing-software
- https://www.reddit.com/r/bigsleep/comments/tvw5js/list_of_sitesprogramsprojects_that_use_openais/
- https://blenderartists.org/t/smart-or-ai-upscaling-resize-open-source-solutions/1242386/11
- https://ai.plainenglish.io/top-5-open-source-image-super-resolution-projects-to-boost-your-image-processing-tasks-e6008c978685

## Video
### upscale
#### Upscale-A-Video !!!!
- https://github.com/sczhou/Upscale-A-Video
- https://www.maginative.com/article/upscale-a-video-is-a-new-ai-model-for-real-world-video-super-resolution/
- 
#### topaz - commercial
- gaia CG 
	- VEDE
- artemis 
	- dobra na glitche a textury 
- https://upscale.wiki/wiki/Model_Database 
#### linux & opensource
- [[projects.feedfarm.video2x]]
	- optimized waifu --> dandere
		- https://pythonawesome.com/dandere2x-fast-waifu2x-video-upscaling/​

>LinuxReviews@mstdn.io - RealSR, "Real-World Super-Resolution via Kernel Estimation and Noise Injection", is a amazingly good image up-scaler. Better than #waifu2x. Uses #Vulkan, available on Windows/Linux/macOS.
> https://linuxreviews.org/RealSR​

### slowmo
- dainapp - commercial

## Image

### Unsorted
-  https://github.com/microsoft/Bringing-Old-Photos-Back-to-Life
-  https://github.com/eugenesiow/super-image


- https://github.com/kritiksoman/GIMP-ML [[tools.gimp]]

### upscale
- online
	- https://huggingface.co/spaces/akhaliq/Real-ESRGAN
	- https://unlimited.waifu2x.net/
- offline
	- waifu2x
	- https://github.com/IBM/MAX-Image-Resolution-Enhancer
	- https://www.upscayl.org/

### Expansion
- https://www.reddit.com/r/bigsleep/comments/vtqbzt/colab_notebook_edalle_image_expander_expands_an/

### animeganv2
- https://huggingface.co/spaces/Ebost/animeganv2-self

### face restoration/generation
- https://huggingface.co/spaces/akhaliq/GFPGAN

### dataset processing, but also batch interesting
- https://github.com/albumentations-team/albumentations

### image to motion
- https://www.myheritage.cz/deep-nostalgia

## style transfer
### online
- https://deepai.org/machine-learning-model/fast-style-transfer twoway
- https://tenso.rs/demos/fast-neural-style/


### 

## Adjacent
- https://github.com/dtschump/gmic
- [[ai.image.on-line]]
- [[ai.image.generation]]
- [[ai.narration]]
- [[ai.image.processing]]
- [Visions of Chaos - non-ai generator](https://softology.pro/voc.htm) 
	- mandelbulb