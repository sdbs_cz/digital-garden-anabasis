- visionaries (aka [[concepts.trailblazers]])

[alan kay - tribute to ted nelson](https://www.youtube.com/watch?v=AnrlSqtpOkw) - konec
		- autotranscript [[fulldocs.youtube.alankay.tednelson]] 
		- [[people.AlanKay]] [[people.TedNelson]]
#visionaries
> Visionaries are the most important people we have because it is only by comparing their ideas with our normals that we can gauge how we are doing. Otherwise, as it is for most people, normal becomes their reality and they only measure from it. Toss Ted back into this mix and you've upset the applecart and that's what we need. This allows us to see that normal is only one of many possible constructions and some of them could have been a much better and as the normals in the future could be much better and very different from what is considered reality today. Let's be very thankful that we live in a time in a place where two-eyed people are not burnt at the stake or worse. They were really supported in the 60s and they are tolerated at least today. And let us also be thankful that we have a two-eyed person like Ted Nelson who has been tirelessly energetic about not just having ideas but going out and telling people about them. Not letting them die, not letting them get absorbed into the low-pass filter.
