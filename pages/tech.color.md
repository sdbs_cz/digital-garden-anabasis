# Digital Color
[[tools.blender]]




- [The Hitchhiker's Guide to Digital Colour](https://hg2dc.com/)
- https://ciechanow.ski/color-spaces/
- https://www.lensrentals.com/blog/2021/03/the-lensrentals-podcast-episode-40-what-makes-a-cinematic-image-w-art-adams-from-arri/ 
- https://www.blenderguru.com/tutorials/secret-ingredient-photorealism
- https://docs.blender.org/manual/en/latest/render/color_management.html


----
CG graphics https://chrisbrejon.com/cg-cinematography/ cinematography

https://nofilmschool.com/2018/08/color-grading-sheffield-doc-fest

[# The Secret of Making High-Quality Art (in Blender and Everywhere)]https://www.youtube.com/watch?v=qMH_J_vcoqE

[# Enhancing Photorealism Enhancement](https://www.youtube.com/watch?v=P1IcaBn3ej0)

---
## tech

[FRK\_R23]
Něco o barvách. Možná taky občas řešíte, že když natáhnu render do nějaký aplikace, tak jsou barvy totálně jinde. Typicky jde o aplikace pro motion (ae, fusion atp). V případě že člověk používá OpenEXR, tak je Photoshop totálně v řiti a konverze z linear do srgb je naprosto nepoužitelná.  
  
Takže pro začátek doporučuju stáhnout si EXR-IO plugin do Photoshopu (affinity i krita tohle uměj by default btw): [https://www.exr-io.com/features/](https://www.exr-io.com/features/)  
  
Potom to bude chtít OCIO plugin pro Photoshop: [http://fnordware.blogspot.com/2017/02/opencolorio-for-photoshop.html](http://fnordware.blogspot.com/2017/02/opencolorio-for-photoshop.html)  
  
Tady je k tomu nějaký video: [https://www.youtube.com/watch?v=3J88sSa-20M](https://www.youtube.com/watch?v=3J88sSa-20M)  
  
Protip: Ten plugin neumí načíst config.ocio, kterej se instaluje s blender. Pomohlo mi stažení filmic přímo od Sobotky, ten to bere dobře: [https://sobotka.github.io/filmic-blender/](https://sobotka.github.io/filmic-blender/)  
  
Pro zajímavost ještě příkládám návod na Fusion: [https://www.youtube.com/watch?v=Sfer5VZKe1U](https://www.youtube.com/watch?v=Sfer5VZKe1U)


-------
https://www.color-meanings.com/color-symbolism-chart/