> To his credit Massumi understood that the virtual in Deleuze was explicitly analogical not digital, and the rhizome while nonlinear and “networked” was still nevertheless a form of analogical propagation (asexual rather than sexual, non-symbolic rather than symbolic, and so on).
>
>>> What Is the Analog? | Alexander R. Galloway