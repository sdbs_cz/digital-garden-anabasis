https://discord.com/channels/686053708261228577/710585052769157141/761655649016217620

i use tags as a "state" manager mostly.

#📥️ Seed box | items that i am / will be actively working on
#🌱️ Seedlings | distilled from literature notes
#🌞️ Incubator | items not yet ready for planting or in need of planting
#🌲️ Evergreen | forest notes
#:label: Tag Notes

And finally some for just keeping important things find-able  for me
#✅️ Items that have tasks that i need to complete
#:map: Maps of Content (the emerging organizational structure)
#:gear: General utilities i use in this system
#❗️ IMPORTANT

i take my daily notes
in my private journal (daily's) i put my normal content but i also say what i read/watched/listened to that day as incoming literature/content notes: 
today was a great day, i ate a chocolate muffin while reading [[How To Take Smart Notes]] by [[@Sohnke Ahrens]]

now i have a link to [[How To Take Smart Notes]] & [[@Sohnke Ahrens]]
on my link to the book i will put my literature notes using my media templates and it gets assigned the #📥️ tag as it is literature notes in my inbox to process
then i make my notes, my pure thought notes which are new seedings #🌱️ and when i flesh them out and they are basically done just not densely linked or i have no where to file them yet, or they just need to be added to over some time then they are put into the incubator: #🌞️ until they are ready for planting, when they are densely linked and ready for "planting" in my evergreen forest they get a #🌲️because they are now my evergreen notes.

on the graph now it can show me which notes are #🌱️ and #🌞️ and i can see where they may fit in in the macro view of my ZK graph

my #:label: notes are what many usually use tags for like: #productivity, #business #python, #html, i use notes for this instead. because when the note itself gets a lot of linked references i can see the prevalence of that topic in my ZK, and having that file already makes it easier to set up MOC's like [[Programming]] that have links to [[python]] and [[html]]
this allows me to easily deal with the emerging structure over time

INDEX --> MOC's --> subjects --> jumping off point notes --> the minutia

and with the graph filtering for files that dont exist, or tags it really makes this workflow nice for me as i can easily get down into the weeds of my content and material and get down to linking things up better over time.

and with my #🌲️ notes i can start writing with an outline that could look like this:

- topic
    - [[ADHD hyperfocus is a double edged sword of both deep work and avoidance]]
        - [[With poor executive function even activities you would like to do become difficult to approach]]
    - [[Tasks that are not atomized and cannot be finished in a single step are projects]]
        - [[cloze deletion improves memorization with context rich active recall]]
            - [[Bottom up writing prevents confirmation bias and provide a shorter and iterative feedback loop]]

and makes it so i can just connect thoughts and flesh out the gaps and form those narrative stories in my writing or video scripts. 

p.s. this s*** is so fun