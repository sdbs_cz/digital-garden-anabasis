# Linux light-weight distros

## ?
- https://lilidog.org/

## suace
- https://www.blackdown.org/best-lightweight-linux-distros/
- https://itsubuntu.com/22-best-lightweight-linux-operating-systems-in-2020/
- https://itsubuntu.com/22-best-lightweight-linux-operating-systems-in-2020/
- https://linuxhint.com/best-old-laptop-linux-distributions/

## distros
### ultra-light (<512 ram)
### [Antixlinux](https://antixlinux.com/)
- Min Specs
	- RAM 256/512MB ??

### [Damn Small Linux](http://www.damnsmalllinux.org/)

### [slitaz](https://www.slitaz.org/)

### [knoppix](https://www.knopper.net/knoppix/index-en.html)

### [elive](https://www.elivecd.org/)
- enlightenment

### [porteus](http://www.porteus.org/)

### [bodhi](https://www.bodhilinux.com/) 
- nice and sweet

### lxle
- https://lxle.net/about/
- !!nice and sweet

### Q4OS
- https://q4os.org with trinity
- windows uncanny valley

### light

### [Bunsenlabs](https://www.bunsenlabs.org/) 
fork of Crunchbang



### [Slax](https://www.slax.org)

### psychos linux
- https://psychoslinux.gitlab.io/index_mobile.html



## mid-weight

### [[linux.mxlinux]] 
### avlinux

### arco linux
	- https://www.arcolinux.info/arcolinux-editions/ #arch


## privacy

## Rescue 
### rescue zilla


------


https://www.linuxliteos.com/

https://itsfoss.com/super-lightweight-distros/