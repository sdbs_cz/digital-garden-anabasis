lorenlepton - ok...

so I wanna get into spatial audio. not now, but it would be nice to do it in the future with other mixes & recordings

I *loved* mixing in 5.1 & even 7.1 in studios before. so this is really nice

here are some free tools to do just that

https://leomccormack.github.io/sparta-site/​

https://envelop.us/page/tools​

https://www.blueripplesound.com/pro_audio​

https://plugins.iem.at​

#audio #music #sound #boost #share #audioproduction #spatialaudio #spatial​