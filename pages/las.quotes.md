# [[las]].quotes

>![[las.cloud.obsidian.20210130152324.png]]

## Vilém Flusser 
### Line and Surface

#temporality 

If, then, we call the time involved in reading written lines “historical time,” we ought to call the time involved in reading pictures by a different name, because “history” has the sense of going somewhere, whereas, while reading pictures, we need go nowhere. The proof of this is simple: it takes many more minutes to describe what one has seen in a picture than it does to see it.

#stone #image #video #3d

**However, the stone we have offered as an example is not really typical of our present situation. We can walk right up to a stone, but we can do nothing of the sort with most of the things that determine us at present— either the things that occur in explanations or the things that occurin images. The genetic information or the Vietnam War, or alpha particles, or Miss Bardot’s breasts are all examples. We may have no immediate experience of any of these kinds of things, but we are nonetheless determined by them. **

#fiction #intro #facts #video #vertov

We must therefore ask how the various symbols of the world of fiction relate to their meanings. This shifts our problem to the structure of the media. If we take advantage of what was said in the first paragraph, we may answer the question as follows: Written lines relate their symbols to their meanings point by point (they “conceive” the facts they mean), while surfaces relate their symbols to their meanings by two-dimensional contexts (they “imagine” the facts they mean — if they truly mean facts and are not empty symbols). Thus, our situation provides us with two sorts of fiction: the conceptual and the imaginal; their relation to fact depends on the structure of the medium.

----
#dimension #prophets

(We need no prophets to tell us that the “one-dimensional man” is disappearing.)



----
#dimension

(We do not have a two-dimensional logic comparable in rigor and elaboration to linear Aristotelian logic.)

-----
#dimension #reading #temporality

This gives us the following difference between reading written lines and pictures: we must follow the written text if we want to get at its message, but in pictures we may get the message first, and then try to decompose it. And this points to the difference between the one-dimensional line and the two-dimensional surface: the one aims at getting somewhere; the other is there already, but may reveal how it got there. This difference is one of temporality, and involves the present, the past, and the future. It is obvious that both types of reading involve time—but is it the “same” time?

---
#temporality

On the other hand, would it not be more sensible to say that the times involved in the two processes are different, and that their measurement in minutes fails to reveal this difference? If we accept this last statement, we may say that the reading of pictures takes less time because the moment in which their messages are received is denser; it is more compacted. It also opens up more quickly. If, then, we call the time involved in reading written lines “historical time,” we ought to call the time involved in reading pictures by a different name, because “history” has the sense of going

----
#visionaries #peoplewhohearvoices #film

Visually, films are surfaces, but to the ear they are spatial. We are merged in the ocean of sound and it penetrates us; we are opposed to the world of images, and it merely surrounds us. The term audiovisual obscures this distinction. (It seems that Ortega, like many others, has ignored this difference when speaking of our circunstancia. Visionaries certainly live in a different world from those who hear voices.)

-----

#intro #dimension 

Written lines relate their symbols to their meanings point by point (they “conceive” the facts they mean), while surfaces relate their symbols to their meanings by two-dimensional contexts (they “imagine” the facts they mean—if they truly mean facts and are not empty symbols). 

-----
#intro #fiction #concept

When we translate image into concept, we decompose the image—we analyze it. We throw, so to speak,a conceptual point-net over the image, and capture only such meaning as did not escape through the meshes of the net. Therefore, the meaning of conceptual fiction is much narrower than the meaning of imaginal fiction, although it is far more clear and distinct. Facts are represented more fully by imaginal thought, more clearly by conceptual thought. The messages of imaginal media are richer, and the messages of conceptual media are sharper.

-----
#fiction #elite #society

For the elite, the problem is that the more objective and clearer the linear fiction becomes, the more it is impoverished, because ittends to lose contact with the facts it wants to represent (all meaning). Therefore, the messages of linear fiction can no longer be made satisfactorily adequate to the immediate experience we still have of the world. For the mass culture, the problem is that the more technically perfect the images become, the richer they become and the more completely they substitute themselves for the facts they may have originally represented. Therefore, the facts are no longer needed; the images can stand for themselves, and thus lose all their original meaning. They no longer need tobe made adequate to the immediate experience of the world; that experience is thus abandoned. In other words, the world of linear fiction, the world of the elite, is more and more disclosing its merely conceptual, fictitious character — and the world of surface fiction, the world of themasses, is masking its fictitious

----
#sciences #processthah

The sciences, and other articulations of linear thought such as poetry, literature, and music, are having increasing recourse to imaginal surface thinking; they are able to do so because of the technical advance of surface media. And, in a similar way, these surface media, including painting, graphics, and posters, are having increasing recourse to linear thought, and they can do so because their own technical advance permits it. Although

-------------
-------------
-------------

 ### Codified World
 
#image #reading

The red traffic light means “Stop!” and the obnoxious green of peas means “Buy me!” This explosion of colors means something.

-----

#intro #surface #image #temporality 

The fact that humankind is being programmed by surfaces (images) should not be considered a revolutionary piece of news. On the contrary, it apparently signifies a return to a primitive origin. Before the invention of writing, images were a decisive means of communication. Because most codes are ephemeral, such as the spoken word, gestures, and song, we are dependent on images to decipher the meaning that man has given both his deeds and his suffering from the time of Lascaux to the time of Mesopotamian tiles.

-----

#suns_sequence

The scenic character of codes gives rise to a specific way of life of societies that they program. One can call it the “magical form of being.” An image is a surface whose meaning is suspended in a moment: It “synchronizes” the situations that it represents as a scene. But, after this moment of suspension, the eye has to wander around the image, to receive its meaning as it is. It has to “diachronize synchronicity.” For example: It is clear from the first moment that this scene signifies a situation of the type “walking.” But, only after the diachronization of synchronicity does one realize that what is meant are the sun, and two people and a dog going for a walk.


-----

The difference between old and new magic can be summarized as follows: Prehistoric magic is ritualization of models known as myths; current magic is a ritualization of
models known as programs




## [[people.WalterBenjamin]]

### Arcades
#image #temporality

History decays into images, not into stories.
Walter Benjamin, The Arcades Project, 476.

## [[Vachek]]

"Já si nemyslím, že film má bejt nějakej virtuální svět, ve kterým se ztotožníte s hrdinou a projdete s ním až do konce a tím příběhem se všechno řekne" -#


## [[people.TedNelson]]

### Neologisms
#transclusion #intertwingularity #populitism
https://fangl.es/

### One-liners

![[nelson_intertwingled.png]]

-------
_""Information", referred to as a commodity, is a myth.  Information always comes in packages (media bundles, called "documents" (and sometimes "titles")), and every such package has a point of view.  Even a database has a point of view."_

​—[Ted Nelson​](https://en.wikipedia.org/wiki/Ted_Nelson)

-------

A frying-pan is technology.  All human artifacts are technology.  But beware anybody who uses this term.  Like "maturity" and "reality" and "progress", the word "technology" has an agenda for your behavior: usually what is being referred to as "technology" is **_something that somebody wants you to submit to_**.  "Technology" often implicitly refers to something you are expected to turn over to "the guys who understand it."

-------

HTML is precisely what we were trying to PREVENT— ever-breaking links, links going outward only, quotes you can't follow to their origins, no [version management](https://en.wikipedia.org/wiki/Version_management "Version management"), no rights management

------
**The Myth of "Logical Thinking"** 

> Users are told that learning to use computers is "learning to be logical".  This is nonsense.  You are learning to think like the guy who is conning you, though he probably does not realize it..
> 
> "Logic" (deductive logic) is an intricate game of figuring out what you've already said; it is virtually useless in everyday life, where deduction from explicitly stated premises almost never occurs.
> 
> So when you're praised for "logical thinking", it means you've absorbed a paradigm and can now step through it like the person who is praising you.
>![](https://xanadu.com.au/ted/TN/WRITINGS/TCOMPARADIGM/noLogicalThinking.JPG)


## Aby [[people.AbyWarburg]]
#about_+ #offtopic 

There were other problems that made the work on the Atlas an infinite endeavour. Warburg was a technophile. He was interested in telecommunication, the press and travelling; all these new technologies enabled new forms of travelling, but also prolonged the old idea of migration that connected civilizations from the beginning. _Technology_, for example in the form of printing,was also the direct link between Dürer’s engravings and the 28 telephones in his avant-garde library building. He had already written an article entitled „Airship and submarine in medieval imagination“ that suggested that former societies had anticipated what he called “vehicles of thought” and imagination that we dispose of today. Images were their vehicles.

------

The characterization of the restoration of antiquity as the result of the recent
appearance of a consciousness of historical facts and as carefree artistic
empathy, remains an inadequate descriptive evolutionary theory, unless one
simultaneously dares to descend into the deep human spiritual compulsion and
become enmeshed in the timeless strata of the material. Only then does one
reach the mint that coins the expressive values of pagan emotion which stem
from primal orgiastic experience: thiasotic tragedy.3

------
The acquisition of the feeling of distance between subject and object lis] the
task of so-called cultivation and the criterion of progress of the human race. The
proper object of Cultural History would be the description of the prevalent
state ofreflectivity.6

-------
As Halbwachs
argues:

Every memory, no matter how private it may be, even the memory of events to
which we were the only witness, the memory of thoughts and inexpressible
feelings, is linked to a whole collection of notions that many others possess ...
when we summon LIp a memory ... we connect it to others that surround it: in
truth it is because all around us there are other memories connected to it,
inherent in the objects and beings of the milieu we inhabit, or in us ourselves:
reference points in space and time, conceptions of history, geography,
biography, politics ... 'A'

## The Absopton of Expressive
#language #fix 

However, to the extent that this encouragement proceeded as a mne-
mic function, in other words, had already been reformed once before by art using preexisting forms, the act of restitution remained positioned between impulsive self-release and a conscious and controlled use of forms, in other words, between Dionysus and Apollo, and provided the artistic genius with the psychic space for coining expressions out of his most personal formal language.

----

-------------
-------------
-------------

## Edward O. Wilson
We exist in a bizarre combination of Stone Age emotions, medieval beliefs, and god-like technology.

----
### Consilience 
#### chapter 5

##### Magician to Atom
?? #fillin

##### snakes
#offtopic
Snakes and dream serpents provide an example of how agents of nature can be translated into the symbols of culture. For hundreds of thousands of years, time enough for genetic changes in the brain to program the algorithms of prepared learning, poisonous snakes have been a significant source of injury and death to human beings. The re-sponse to the threat is not simply to avoid it, in the way that certain berries are recognized as poisonous through painful trial and error, but to feel the kind of apprehension and morbid fascination displayed in the presence of snakes by the nonhuman primates. The snake image also attracts many extraneous details that are purely learned, and as a result the intense emotion it evokes enriches cultures around theworld. The tendency of the serpent to appear suddenly in trances and dreams, its sinuous form, and its power and mystery are logical ingredi-ents of myth and religion.

#### Chapter 7

Complex information is thus organized and transmitted by language composed of words. Nodes are almost always linked to other nodes, so that to recall one node is to summon others. This linkage, with all the emotional coloring pulled up with it, is the essence of what we refer to as meaning. The linkage of nodes is assembled as a hierarchy to organize information with more and more meaning. "Hound," "hare" and "chasing" are nodes, each symbolizing collectively a class of more or less similar images. A hound chasing a hare is called a proposition, the next order of complexity in information. The higher order above the proposition is the schema. A typical schema is Ovid's telling of Apollo's courtship of Daphne, like an unstoppable hound in pursuit of an unattainable hare, wherein the dilemma is resolved when Daphne, the hare and a concept, turns into a laurel tree, another concept reached by a proposition.

### oneliners
We are not afraid of predators, we're transfixed by them, prone to weave stories and fables and chatter endlessly about them, because fascination creates preparedness, and preparedness, survival. In a deeply tribal way, we love our monsters.

------

Humanity today is like a waking dreamer, caught between the fantasies of sleep and the chaos of the real world. The mind seeks but cannot find the precise place and hour. We have created a Star Wars civilization, with Stone Age emotions, medieval institutions, and godlike technology. We thrash about. We are terribly confused by the mere fact of our existence, and a danger to ourselves and to the rest of life.

## William S. Burroughs
> Cut word lines Cut music lines Smash the control images Smash the control machine Burn the books Kill the priests Kill! Kill! Kill!
> - William S. Burroughs - The Soft Machine (1961) 

### Electronic Revolution
>The written word is of course a symbol for something and in the case of hieroglyphic language writing like Egyptian it may be a symbol for itself that is a picture of what it represents. This is not true of an alphabet language like English. The word leg has no pictorial resemblance to a leg. It refers to the SPOKEN word leg. so we may forget that a written word IS AN IMAGE and that written words are images in sequence that is to say MOVING PICTURES. So any hieroglyphic sequence gives us an immediate working definition for spoken words. Spoken words are verbal units that refer to this pictorial sequence. And what then is the written word? My basis theory is that the written word was literally a virus that made spoken word possible. The word has not been recognized as a virus because it has achieved a state of stable symbiosis with the host...

## Dziga Vertov
#film

Kino-Eye uses every possible means in montage, comparing and linking all points of the universe in any temporal order, breaking, when necessary, all the laws and conventions of film construction.

----

#film #temporality 

Freed from the boundaries of time and space, I co-ordinate any and all points of the universe, wherever I want them to be. My way leads towards the creation of a fresh perception of the world. Thus I explain in a new way the world unknown to you.

## Eisenstein
#film
For example, in painting the form arises from abstract elements of line and color, while in cinema the material concreteness of the image within the frame presents - as an element - the greatest difficulty in manipulation.

## Andrej Tarkovsky

#patterns
We have forgotten to observe. Instead of observing, we do things according to patterns.

-----
#meaning
If you look for a meaning, you'll miss everything that happens.

## Hans-Georg Gadamer

**The more language is a living operation, the less we are aware of it.** Thus it follows that from the forgetfulness of language that its real being consists in what is said in it. What is said in it constitutes the common world in which we live. … The real being of language is that into which we are taken up when we hear it.
Hans-Georg Gadamer - Man and Language (1966)

------

**What man needs is not just the persistent posing of ultimate questions, but the sense of what is feasible, what is possible, what is correct, here and now.** The philosopher, of all people, must, I think, be aware of the tension between what he claims to achieve and the reality in which he finds himself.
    -   Foreword to the Second Edition, p. xxiv.

------
![[misattributed_gadamer.png]]
#fix

------

## William Gibson
#patterns 

“We have no idea, now, of who or what the inhabitants of our future might be. In that sense, we have no future. Not in the sense that our grandparents had a future, or thought they did. Fully imagined cultural futures were the luxury of another day, one in which 'now' was of some greater duration. For us, of course, things can change so abruptly, so violently, so profoundly, that futures like our grandparents' have insufficient 'now' to stand on. We have no future because our present is too volatile. ... We have only risk management. The spinning of the given moment's scenarios. Pattern recognition”  
― William Gibson, [Pattern Recognition](https://www.goodreads.com/work/quotes/2455062)


## Timothy Leary

_“In the information age, you don't teach philosophy as they did after feudalism. You perform it. If Aristotle were alive today he'd have a talk show.”_

― [Timothy Leary](https://psychology.fas.harvard.edu/people/timothy-leary) (1920-1996)


-------------
-------------
-------------

## Andy Matuschak

https://notes.andymatuschak.org/z8aiVRywvJYDB9gvpCDxa4KUBcKr8R4geNAi

[Prefer associative ontologies to hierarchical taxonomies](https://notes.andymatuschak.org/z29hLZHiVt7W2uss2uMpSZquAX5T6vaeSF6Cy)

-------------
-------------
-------------
