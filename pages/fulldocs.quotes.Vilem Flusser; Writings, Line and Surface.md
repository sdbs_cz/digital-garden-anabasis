---
created: 2021-09-04T23:55:50 (UTC +02:00)
tags: []
source: https://kangyy1.wordpress.com/2014/01/02/vilem-flusser-writings-line-and-surface/
author: 
---

# Vilem Flusser; Writings, Line and Surface | Yiyun Kang: RESEARCH

> ## Excerpt
> Vilem Flusser; Writings, Line and Surface, 1972 Vilem Flusser/Writings; Andreas Strohl, editor; translated by Erik Eisel. 2002 Published by University of Minnesota Press “we must follow the w…

---
**Vilem Flusser; Writings, Line and Surface, 1972**

[![[photo-02-01-2014-12-32-45.jpg]]](https://kangyy1.files.wordpress.com/2014/01/photo-02-01-2014-12-32-45.jpg)

Vilem Flusser/Writings; Andreas Strohl, editor; translated by Erik Eisel.  
2002  
Published by University of Minnesota Press

“we must follow the written text if we want to get its message, but in pictures we may get the message first, and then try to decompose it. And this points to the difference between the one-dimensional line and the two-dimensional surface.” p.23.  
“We all know that a film is a linear sequence of pictures, but while reading or viewing a film, we forget this fact.” p.23.  
“It may be said that films are read as if they were a series of pictures. But these pictures are not identical with the pictures of which the film is physically composed, with the photographs that compose its ribbon. Ther are more like moving pictures of scenes in a play, and this is the reason why the reading of films is often compared to the reading of staged drama, rather than to the reading of pictures. _But this is an error_, because the stage has three dimensions and we can walk into it, while the screen is a 2 dimensional and we can never penetrate it. The theater represents the wolrd of things through _things_, and the film represents the world of things through _projections of things_.” p.24.  
– **그런데 이것이 space와 integrated 되었을 때, projection mapping 을 통해서. 그러면 이제 3dimensioan 획득한다. we can penetrate it.**

“This disclose a central difference: the reading of films goes on in the same “historical time” in which the reading of written lines occurs, but the “historical time” itself occurs, within the reading of films, on a new and different level. ” p.24.

“This reveals the difference between the structure of conceptual and imaginal codes and their respective means of decodification. **Imaginal codes**(films) depend on predetermined viewpointsl the are subjective. And they are based on conventions that need not be consciouly learned; they are unconscious. Conceptual codes(like alphabets) depend on predetermined viewpoints; they are objective. And they are based on conventions that must be consciously learned and accepted; they are conscious.” p.28.

“Therefore, the meaning of conceptual fiction is much narrower than the meaning of imaginal fiction, although it is far more clear and distinct. Facts are represented more fully by imaginal thoughts, more clearly by conceptual thought. The message of imaginal media are richer, and the messages of conceptual media are sharper.” p.28.

“Those of linear fiction(e.g., books, scientific publications, and computer printouts) and those of surface fiction(e.g., filmes, TV pictures, and illustrations). The first may mediate between ourselves and facts in a clear, objective, conscious, or conceptual way, but it is relatively restricted in its message. The second type may mediate between ourselves and facts in an ambivalent, subjective, unconscious, or imaginative way, but it is relatively rich in its message.” p.29.

“It may be that what is happening at present is the attempt to incorporate linear thoughts into surface thought, concept into image, elite media into mass media.”p.29.

“The considerations before us indicate that they may be conjoined in a creative relationship. A new kind of medium may thus emerge, permitting us to rediscover a sense of reality, in this way, we may be able to open up fields for a new type of thinking, with its own logic and its own kind of codified symbols. In short, the synthesis of linear and surface media may result in a new civilization.” p.31.  
–> **굉장한 선견지명으로 보인다. 그래서 탄생한 것이 digital code가 아닐까.**
