# [[fulldocs]].twitter.booksasmaps
- [Tweet](https://twitter.com/i/status/1378550004680249348) by [@andy_matuschak](https://twitter.com/andy_matuschak) on [[April 4th, 2021]]:
- Books as maps! "Today the book is already… an outdated mediation between two different filing systems. For everything that matters is to be found in the card box of the researcher who wrote it, and the scholar studying it assimilates it into his own card index." —Walter Benjamin [pic.twitter.com/6l8zaGver6](https://twitter.com/andy_matuschak/status/1378550004680249348/photo/1)
- (In Reflections, p. 78; via Noah Wardrip-Fruin's interesting foreword to Engelbart's Augmenting Human Intellect in The New Media Reader)
- The odd thing about this framing is that the book doesn't really perform the mediation! It's a sort of serialization of the card box of the researcher who wrote it. The reader has to "come to terms with the author" and then "bring the author to terms" to map to their own.
- Also of course this framing, taking too seriously, ignores the role of narrative—a common issue with hypertext-dreamer interpretations of this kind of comment. Prose is not just a structured sequence of claim-atoms!
- But of course, Benjamin knows this. From the preceding page:
  - "CAUTION: STEPS
  - Work on good prose has three steps: a musical stage when it is composed, an architectonic one when it is built, and a textile one when it is woven."

[[people.WalterBenjamin]]
[[people.AndyMatuschak]]
[[concepts.map]]