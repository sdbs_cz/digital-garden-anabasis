# Playlist
#incubation #chaosstream #radio

## 101
this is place to colect flows and notions around
- playlist
- curatorship
- channeling
- streaming
as gamechangers of culture `consumption`
and also see [[incubation.attention.economy]]

## Flows and notions

> “You didn’t need specialist poets to create this kind of musicalised language, and the diction is very simple, so this was clearly a democratising form of literature. We’re getting an exciting glimpse of a form of oral pop culture that lay under the surface of classical culture.”
>> -  https://www.theguardian.com/books/2021/sep/08/i-dont-care-text-shows-modern-poetry-began-much-earlier-than-believed

>In 1989, British scientist Tim Berners-Lee invented the World Wide Web. By the dawn of the ’90s, the internet had awakened everyone to new technological possibilities. Similarly, music and music listeners were becoming more forward-thinking than ever before. Grunge and heavy metal met in the gauntlet; hip-hop traveled from the underground to the pop charts (N.W.A. out-charted R.E.M. in 1991); pop music became more daring; electronic music began its ascent from small clubs to festival stages. 1991 marked the start of this aggressive reinvention. It was a year that shaped the music we’ve heard for the last three decades. Even today, these 13 albums that were once in rotation in six-disc Sony stereos are responsible for current digital playlists.
> - https://www.spin.com/photos/1991-albums-shaped-future-music/

>https://mamot.fr/@pluralistic/106359487243544156 pluralistic@mamot.fr - Thus, when the internet was demilitarized and the general public started trickling - and then rushing - to use it, there was a widespread hope that we might break free of the tyranny of concentrated, linear programming (in the sense of "what's on," and "what it does to you").
>Much of the excitement over Napster wasn't about getting music for free - it was about the mix-tapification of all music, where your custom playlists would replace the linear album. 
>6/

>Songs circulate online among fans for whom an MP3 player set to Shuffle trumps conventional genre as an organizing principle. Refined tastes intertwine with semi-random surf trails to become indistinguishable from each other. Timelines fray, genealogies wander. These under-the-radar exchanges generally happen outside commercial spheres, adding to the fertile mess. You must sift through a lot of junky MP3s to uncover the great ones, but in the end, all the world's sonic secrets are out there, clumped irregularly across the Internet's flat and mighty sprawl. A catchy genre name or evocative creation myth can make the output of a few friends appear as a bustling scene to outside eyes, and the online hype can turn into a selffulfilling prophecy if global excitement trickles down into actual gigs.
> - [[people.JaceClayton]]  


![[netflix.showplanning.png]]

>I nicked that off Mark Ravenhill who wrote a very good piece which said that if you analyse television now it's a system of guidance - it tells you who is having the Bad Feelings and who is having the Good Feelings. And the person who is having the Bad Feelings is redeemed through a "hugs and kisses" moment at the end. It really is a system not of moral guidance, but of emotional guidance. Morality has been replaced by feeling. That's what all the disorders are about. They are a way of oppressing and measuring whether what you're feeling is the correct feeling. Intellect and morality are intimately related but feeling is now predominant.  
>
It's very difficult to take people out of themselves. Because what you're doing is reinforcing the priority of their own feelings about themselves. The thing about our age is that everyone monitors themselves. It's really fascinating. I did this with the psychological disorders in The Trap. They've become a way of policing yourself. "Am I the right shape? Am I the right emotional construct?" So you edge back to the right emotional shape, or the right physical shape. And vanity in our time - is about pleasing yourself. It's about making yourself feel better about yourself. We live within our selves. We should find a way of escaping it, but the program makers don't have the imagination or the confidence.  
 > 
I grew up at the time of the failure of the optimistic view of changing people. That's what marked out the 20th century - what drove it all was this idea that people can be made better, and fundamentally we can engineer it. That failed. We now have a mirror image of that. There are people who are quite engineer-like - they're almost value-neutral. Every now and again that group meets another group who have a pessimistic view of human nature. But that group, the geeks, think "With that view, we can make the world safe". They're almost engineers of the human soul - we can engineer a better world through pessimistic views of human nature. I think that was true of marketers who saw in Freud's ideas a way of saying, "Look, we can shape how people fulfil their desires". That was also true of the neo-conservatives. They have a dark and pessimistic view of human beings, but they also have an optimistic view of a vision of the world which is that if we create a world that's grand enough, it will contain those dark desires and make them better. A lot of them are ex-Marxists, so it's not surprising.  
  >
By taking an optimistic idea that takes a pessimistic view of human nature through managing it, has increasingly trapped us into a technical process where you manage the feelings of the individual. It's not bad, it's not a conspiracy - it's just an attempt to manage a system. But because it's based on a limited view of human beings, it doesn't quite work. It's the years of stagnation. There's opportunities but no one's grabbing them. It's an extraordinary time of relative peace and relative prosperity, but we are all terrified, anxious, nervous, and we're not making any use of this openness. And it will close down again.  
  - Adam Curtis

---

### Shuffle
- https://www.youtube.com/watch?v=NhAIA-o7f4M

## Links
- [Programming Your Own Channel](https://graceoneill.wordpress.com/2009/05/08/programming-your-own-channel/) 

- [Contextualize Your Listening: The Playlist as Recommendation Engine](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.302.1754&rep=rep1&type=pdf)

- [DJ-boids: emergent collective behavior as multichannel radio station programming](https://dl.acm.org/doi/10.1145/604045.604089)

- https://www.economist.com/europe/2021/03/31/how-netflix-is-creating-a-common-european-culture

- [Aggregators aren't open-ended - by Gordon Brander - Subconscious](https://via.hypothes.is/https://subconscious.substack.com/p/aggregators-arent-open-ended )

- https://www.documentjournal.com/2021/01/the-internet-didnt-kill-counterculture-you-just-wont-find-it-on-instagram/

- The Playlistification of Music - youtube video by Venus Projetct https://www.youtube.com/watch?v=bQDudbp-pag
	- http://www.forgotify.com/

## Adjacent
- [incubation.anarcheology](incubation.anarcheology.md)
- [[incubation.attention.economy]]
- [[concepts.linearity]]
- [[incubation.compression]]
- [[people.JaceClayton]]
- [[concepts.hypertext]]
- [[incubation.mediamateriality]]
- [[areas.stream]]
- [[tool.stream.tech.audio]]
- [[incubation.audio.synthesis.concatenative]]



https://radio.garden/visit/prague/dpbivRM8 **!!**
https://freesound.org/people/frederic.font/sounds/456500/
https://ffont.github.io/freesound-timeline/ 
https://labs.freesound.org/tag/sound-exploration/




--------------
-------------
------------------
------------------

![[meme.spotify.20211027162958.png]]

>**Apple Buys Startup That Makes Music With AI to Fit Your Mood | Time**  
[https://time.com/6146000/apple-ai-music/](https://time.com/6146000/apple-ai-music/)  
> 
>The idea is to generate dynamic soundtracks that change based on user interaction. A song in a video game could change to fit the mood, for instance, or music during a workout could adapt to the user’s intensity.

>Comment from YT:  
>_I remember when I was growing up, where TV's were basic that didn't have all the surround sound speakers, the local FM station would do a simulcast of Star Wars, while it was aired on TV. My dad had our TV hooked up to our stereo system. It was great. Sounded like that you had a mini movie theatre_

Endel app - personalized soundscapes

----



![[Pasted image 20230307201154.png]]


