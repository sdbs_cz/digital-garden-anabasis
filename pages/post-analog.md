


## Side-walk
###  Analogon
>Sartre says that what is required for the imaginary process to occur is an _analogon_—that is, an equivalent of perception. This can be a painting, a photograph, a sketch, or even the [mental image](https://en.wikipedia.org/wiki/Mental_image "Mental image") we conjure when we think of someone or something. Through the imaginary process, the _analogon_ loses its own sense and takes on the sense of the object it represents. Again, we are not deluded. But at some level the photograph of my father ceases being merely colors on paper and instead stands in for my absent father. I then have a tendency to ascribe the feelings I have about my father to the picture of him. Thus, an _analogon_ can take on new qualities based on my own intention toward it.
> - https://en.wikipedia.org/wiki/The_Imaginary_(Sartre)

##### https://analogon.cz/

- https://en.wiktionary.org/wiki/analogon
- 

###  Analogion
> An **analogion** ([Greek](https://en.wikipedia.org/wiki/Greek_language "Greek language"): Ἀναλόγιον) is a [lectern](https://en.wikipedia.org/wiki/Lectern "Lectern") or slanted stand on which [icons](https://en.wikipedia.org/wiki/Icons "Icons") or the [Gospel Book](https://en.wikipedia.org/wiki/Gospel_Book "Gospel Book") are placed for veneration by the faithful in the [Eastern Orthodox Church](https://en.wikipedia.org/wiki/Eastern_Orthodox_Church "Eastern Orthodox Church") and [Eastern Catholic Churches](https://en.wikipedia.org/wiki/Eastern_Catholic_Churches). It may also be used as a [lectern](https://en.wikipedia.org/wiki/Lectern "Lectern") to read from [liturgical books](https://en.wikipedia.org/wiki/Liturgical_book "Liturgical book") during the [divine services](https://en.wikipedia.org/wiki/Divine_Service_(Eastern_Orthodoxy) "Divine Service (Eastern Orthodoxy)").[[1]](https://en.wikipedia.org/wiki/Analogion#cite_note-FOOTNOTEParryMellingBradyGriffith199927-1)
