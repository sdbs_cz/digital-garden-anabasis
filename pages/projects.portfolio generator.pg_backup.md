# Portfolio generators
## Basic prototype 
- [[tools.markdown]] and media files in single file tree aka [[vault]]
- render
	- --> web
	- --> pdf //can be interactive
	- --> printable file //probably pdf
	- --> etc
- media files
	- archive vs. presentation media files 


## Managment
:proposal:
- managment through
	- [areas.filetag](areas.filetag.md)
	- [incubation.annotation.media](incubation.annotation.media.md)
	- ...
		- [[tools.tagspaces]] for [concepts.Digital Asset Managment](concepts.Digital%20Asset%20Managment.md)
		- [tools.obsidian](tools.obsidian.md) or similiar
- how to understand media files and their renders?
	- yaml
	- naming convetions 

## Going ONLINE
### Plan of Action mk. JEKYLL
#### Essentials
- https://jekyllrb.com/
	- uses [[tools.markdown]]
	- CLI / coding heavy, but!
- general templates can be created
	- [ ] how to avoid all portfolios looking the same?
	- additionally, cost of set-up for non-computerandos not too high
- [GitLab CI](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/) can be used to build and deploy (!) websites
	- no previews though
- pre-setup [[anabasis]] machines?

#### Minimal requirements on users
 - [[tools.markdown]] knowledge
 - [[git]] user knowledge (?) 
	 - no reasonable user-friendly GUI :(

#### Features / Workflows
- full page (e.g. about, contact, history...) = single [[tools.markdown]] file
- **OR** [[tools.markdown]] files as separate page components/blocks (i.e. portfolios)
	- annotation with [[yaml metadata]], which allows for:
	- sorting by date, name, project, medium...
	- automatic inclusion into pages (if specified by template)
	- living example: https://t.mldk.cz/tools
		- (each tool a single .md file)
- result = single folder of plain HTML files
- allows for embedding of dynamic content (videos, javascripten, blinkenlichten)


#### Example of a component markdown file
	---
	name: kollagen
	stage: beta
	live-url: /tools/kollagen/
	git: https://gitlab.com/tmladek/kollagen
	tags:
	 - image
	 - experimental
	---
	*kollagen* is a random image mashup/collage generator. Mostly for fun and [kun saxan](https://kunsaxan.sdbs.cz) image feed.


### Plan of action mk. Wordpress (???)
*?**?**?*



