

## Symbols
>The symbols are made up of figures, colours and lines, each illustrating specific areas of the cosmic analyses. Martinus has thus created a physical illustration of spiritual realities that are otherwise inaccessible to our physical sensory perception. This physical visibility makes it easier for us to focus our thoughts on spiritual phenomena and to study them in a systematic and purposeful way, as we are accustomed to doing with physical objects and phenomena.
>
>You can liken the symbols to maps. They provide a symbolic representation of the spiritual and cosmic realities, in the same way as maps provide a symbolic representation of geographical realities.
>-- https://www.martinus.dk/en/martinus-symbols/

### Symbol 33 - `Animal and Human Thought Climates`
![[Pasted image 20220312220410.png]]
#### explanation
>The terrestrial human being is at a transition stage between animal and human being. It has an animal consciousness that is degenerating and a human consciousness that is growing. This transformation is not an act of will but is a matter of maturity and evolution, in the same way as becoming a musical or artistic genius or the like is. Being able to love one’s neighbour as oneself is the same as being a moral genius. The principal, cosmic, organic foundation for the transformation of the living being from animal to human being is its faculty for sympathy with the masculine pole and the feminine pole. The relationship of these poles to one another is the cause of the being’s appearance as a being of the male sex, the female sex or the human sex, the latter being the finished human being in God’s image. The relationship of the poles to one another forms the basis for war as well as peace; the poles are the main organs for the formation of the human being’s fate. They maintain the principle of contrast through which the possibility for all forms of sensory perception exist.

![[Pasted image 20220312220433.png]]
- https://www.martinus.dk/en/martinus-symbols/overview-of-the-symbols/symbol-33a.html
- 
### Vol. 5
>The human body as a stellar system — The human being’s atomic world and the basic energies — The healthy human organism — Illness in the human organism — A microcosmic stellar system. The organic, atomic world  — The universe of instinct. The atom of instinct and the solar system of instinct — The universe of gravity. The atom of gravity and the solar system of gravity — The universe of feeling.  The atom of feeling and the solar system of feeling — The atom of intelligence and the solar system of intelligence — The universe of intuition. The atom of intuition and the solar system of intuition — The universe of memory. The atom of memory and the solar system of memory — Healthy atomic activity in the organism — The explosion of electrons in the human being’s atomic world — Atomic poisoning — The energy of gravity’s penetration of the area of feeling — The energy of feeling’s penetration of the area of gravity — The energy of feeling’s penetration of the area of instinct — The energy of intuition’s penetration of the area of feeling — The energy of intuition’s penetration of the area of intelligence — The energy of gravity’s penetration of the area of intuition — The effect of the atomic catastrophes in the organism — The human being with the perfect thought climate — Animal and vegetarian food — The vibrations of the ideal food — Gastric acid, cooking and food — Animal digestion — Vegetarian digestion — The fate element and talent kernels during discarnation — The fate element and talent kernels during incarnation — The defective organism  — The defective organism and the fate element — The destruction of the human being’s talent kernels — The cosmic spiral cycle 2. The imperfect human being — The cosmic spiral cycle 2. The perfect human being.
----
![[Pasted image 20220312220002.png]]

## Adjacent
- [[concepts.map]]
- [[incubation.areas.diagrammatic]]
- [[people.ClaudeBragdon]]
- [[people.TerenceMcKenna]]