# File system
## From wikipedia
> In [computing](https://en.wikipedia.org/wiki/Computing "Computing"), a **file system** or **filesystem** (often abbreviated to **fs**) is a method and data structure that the operating system uses to control how data is [stored](https://en.wikipedia.org/wiki/Computer%5Fdata%5Fstorage "Computer data storage") and retrieved. [⤴️](https://omnivore.app/me/https-en-wikipedia-org-wiki-file-system-18802899ecc#80132106-6d0f-4193-afe8-43e5241a0fd0)

> Directory structures may be flat (i.e. linear), or allow hierarchies where directories may contain subdirectories. [⤴️](https://omnivore.app/me/https-en-wikipedia-org-wiki-file-system-18802899ecc#75272fd4-3267-4742-9092-c2c7a42d2fc0)

> Other bookkeeping information is typically associated with each file within a file system. The [length](https://en.wikipedia.org/wiki/File%5Fsize "File size") of the data contained in a file may be stored as the number of blocks allocated for the file or as a [byte](https://en.wikipedia.org/wiki/Byte "Byte") count. The [time](https://en.wikipedia.org/wiki/System%5Ftime "System time") that the file was last modified may be stored as the file's timestamp. File systems might store the file creation time, the time it was last accessed, the time the file's [metadata](https://en.wikipedia.org/wiki/Metadata "Metadata") was changed, or the time the file was last backed up. Other information can include the file's [device type](https://en.wikipedia.org/wiki/Device%5Ffile "Device file") (e.g. [block](https://en.wikipedia.org/wiki/Block%5Fdevices "Block devices"), [character](https://en.wikipedia.org/wiki/Character%5Fdevices "Character devices"), [socket](https://en.wikipedia.org/wiki/Internet%5Fsocket "Internet socket"), [subdirectory](https://en.wikipedia.org/wiki/Subdirectory "Subdirectory"), etc.), its owner [user ID](https://en.wikipedia.org/wiki/User%5FID "User ID") and [group ID](https://en.wikipedia.org/wiki/Group%5FID "Group ID"), its [access permissions](https://en.wikipedia.org/wiki/File%5Fsystem%5Fpermissions "File system permissions") and other [file attributes](https://en.wikipedia.org/wiki/File%5Fattribute "File attribute") (e.g. whether the file is read-only, [executable](https://en.wikipedia.org/wiki/Executable "Executable"), etc.). [⤴️](https://omnivore.app/me/https-en-wikipedia-org-wiki-file-system-18802899ecc#22a6cc26-d04a-4bf0-858d-335c6bb1dabb)

> Some file systems allow for different data collections to be associated with one file name. These separate collections may be referred to as _streams_ or _forks_. [⤴️

--------
--------

[making computers better - how we store and collaborate on our work](https://adamwiggins.com/making-computers-better/storage)

## Adjacent
- [[tml.upend]]
- [[incubation.concepts.DatabaseArt]]

