## 2.5Gbps

### endpoints
- https://www.alza.cz/i-tec-usb-c-metal-2-5gbps-d6384104.htm#recenze
- https://www.alza.cz/qnap-qxg-2g1t-i225-d6276557.htm

### middleman?
- https://www.i4wifi.cz/cs/262008-mikrotik-rb5009ug-s-in
- https://www.i4wifi.cz/cs/244171-zyxel-xgs1210-12
- https://www.mironet.cz/zyxel-xgs101012-12port-switch-unmanaged-8x-gbe-2x-25gbe-2x-sfp-uplink-qos+dp441281/
- https://www.mironet.cz/qnap-qsw21042s-6port-switch-2-x-10gbe-sfp-4-x-25gbe-rj45+dp484413/

---
[TP-LINK TL-SG105-M2 / Switch / 5x2500Mbps / QoS | Mironet.cz](https://www.mironet.cz/tplink-tlsg105m2-switch-5x2500mbps-qos+dp467823/ )

---
[ZyXEL MG-105 / Desktop Switch / 5x 2.5Gbps / QoS | Mironet.cz](https://www.mironet.cz/zyxel-mg105-desktop-switch-5x-25gbps-qos+dp482927/ )


------------------


https://www.i4wifi.cz/cs/210989-kleste-krimpovaci-pro-utp-kabely-a-konektory-rj-4p-6p-8p
https://www.i4wifi.cz/cs/182397-keystone-xtendlan-rj-45-cat6
https://www.i4wifi.cz/cs/179886-keystone-xtendlan-rj-45-cat6a