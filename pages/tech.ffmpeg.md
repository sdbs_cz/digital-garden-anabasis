- https://amiaopensource.github.io/ffmprovisr/
- https://github.com/juliojsb/human-friendly-ffmpeg

[[inform.ffmpeg]]

### mp4 --> to wav 
`for i in *.mp4; do ffmpeg -i "$i" "${i%.*}.wav"; done`

### amv video
ffmpeg -i INPUT -ac 1 -ar 22050 -r 15 -block_size 1470 -strict -1 -vf "crop=160:120[:240:0]" OUTPUT