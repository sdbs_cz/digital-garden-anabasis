# Backlinks
kind of a big deal in rome

---

> A core technical difference between a Nelsonian network and what we have become familiar with online is that \[Nelson's\] network links were two-way instead of one-way. In a network with two-way links, each node knows what other nodes are linked to it. ... Two-way linking would preserve context. It's a small simple change in how online information should be stored that couldn't have vaster implications for culture and the economy.[\[16\]](https://en.wikipedia.org/wiki/Ted_Nelson#cite_note-16)
>  - [Jaron Lanier](https://en.wikipedia.org/wiki/Jaron_Lanier "Jaron Lanier") explains the difference between the World Wide Web and Nelson's vision, and the implications:
>  - - [[people.JaronLanier]]

---
---
---


## How do backlinks relate to 
- [[concepts.context]]
- [[concepts.map]]
- [[concepts.bookmarking]]
- [[concepts.parallel textface]]

## #techmech 
[GitHub - andymatuschak/note-link-janitor: Maintains backlink structure among interlinked Markdown notes](https://github.com/andymatuschak/note-link-janitor) #markdown

[[people.TedNelson]]