# Las.quotes.map
### Didi-Huberman
_Atlas. How to Carry the World on One’s Back?_: “The atlas is guided only by changing and provisional principles, the ones that can make new relations appear inexhaustibly - far more numerous than the things themselves - between things and words that nothing seemed to have brought together before