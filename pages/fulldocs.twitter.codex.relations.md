- [Tweet](https://twitter.com/i/status/1418207884110827525) by [@codexeditor](https://twitter.com/codexeditor) on [[July 22nd, 2021]]:
- In a sense you have to literally read between the lines ... shapes, colours, volumes, pitch intervals, optical tones, etc.
  - The "relation" is the thing that is between all those things ... that governs their choice, their ordering, from the macro to the micro... [twitter.com/codexeditor/st…](https://twitter.com/codexeditor/status/1418002079856005121)
- The "relation" is the thing that is between words ... between concepts, entities, attributes.
  - The "relation" is the principle of connectedness: the abstraction with explanatory and predictive power.
  - That is the kind of "relation" I'm seeking with Codex.
- [@jonathanaird](https://twitter.com/jonathanaird) And more here:
  - [twitter.com/codexeditor/st…](https://twitter.com/codexeditor/status/1230634952712060928?s=19)

---------------

- [Tweet](https://twitter.com/i/status/1418002079856005121) by [@codexeditor](https://twitter.com/codexeditor) on [[July 22nd, 2021]]:
- There is your answer for you philosophical, logo-centric types who think that knowledge and wisdom only lies in words.
  - Every painting, sculpture, drawing, symphony, work of architecture, etc., that you have ever ignored is a work of philosophy you have closed your eyes to. [twitter.com/codexeditor/st…](https://twitter.com/codexeditor/status/1418001205884641280)
![Image](https://pbs.twimg.com/media/E63AktUUUAEfFmH?format=png&name=small)

--------------

- [Tweet](https://twitter.com/i/status/1230634952712060928) by [@codexeditor](https://twitter.com/codexeditor) on [[February 20th, 2020]]:
- "The relation between the notes or the total relation of a phrase of music is as much a created thought as is one expressible or explainable in words."
  - Think about the implications of this for philosophy and our logo-centric world view.
  - Really think about.
