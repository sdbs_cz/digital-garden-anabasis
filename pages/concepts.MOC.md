# Map Of  Content
#moc 

# #101

>MOCs are more than just a structure/hub/outline note.
>(1) **MOCs are incubators**. Place notes in there and let them marinate. You can see exactly this use case, upon download of the text files, here: [On the process of forging evergreen notes 214](https://forum.obsidian.md/t/on-the-process-of-forging-evergreen-notes/710)
>I have not seen any examples of hub/outline/structure notes used in this capacity.
>(2) **MOCs are curated workbenches** where ideas go to war for positioning. In an MOC, ideas are encouraged to be organized in very fluid ways: by intuition, by priority, in sequence, alphabetically, et cetera. This shuffling of ideas is like having 20 index cards on a workbench and figuring out all their foundational relationships—yet evolving the content on the note cards at the same time.
>I will provide the exact use case of this awesome power and link to it »here«. And it truly is awesome once you start using it. I have not seen any examples of hub/outline/structure notes used in this capacity.
>(3) **MOCs are summations of thought on the topic**. As MOCs mature, they can evolve into something closer to a more static annotated Table of Contents (TOC). This is the one use case that I’ve seen for hub/structure/outline notes.
>> - https://forum.obsidian.md/t/lyt-kit-now-downloadable/390/11

### ---- praxis ->
https://publish.obsidian.md/lyt-kit/Umami/The+3+Phases+of+MOCs

## Also means `power` in czech

## --> [[areas]] <--
- [[areas.text]]
- 

-----------------------
## Adjacent
- [[concepts.knowledge managment]]
- [[concepts.map]]
- [[fulldocs.tags-tallguyjenks]]
- [[concepts.hierarchy]]
- [[concepts.linearity]]

#fix 