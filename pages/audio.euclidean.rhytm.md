>The **Euclidean rhythm** in music was discovered by [Godfried Toussaint](https://en.wikipedia.org/wiki/Godfried_Toussaint "Godfried Toussaint") in 2004 and is described in a 2005 paper "The [Euclidean Algorithm](https://en.wikipedia.org/wiki/Euclidean_algorithm "Euclidean algorithm") Generates Traditional Musical Rhythms".[[1]](https://en.wikipedia.org/wiki/Euclidean_rhythm#cite_note-gtpdf-1) The [greatest common divisor](https://en.wikipedia.org/wiki/Greatest_common_divisor "Greatest common divisor") of two numbers is used [rhythmically](https://en.wikipedia.org/wiki/Rhythm "Rhythm") giving the number of [beats](https://en.wikipedia.org/wiki/Beat_(music) "Beat (music)") and silences, generating almost all of the most important [world music](https://en.wikipedia.org/wiki/World_music "World music") rhythms,[[2]](https://en.wikipedia.org/wiki/Euclidean_rhythm#cite_note-gtweb-2) except [Indian](https://en.wikipedia.org/wiki/Music_of_India "Music of India").[[3]](https://en.wikipedia.org/wiki/Euclidean_rhythm#cite_note-extv-3) The beats in the resulting rhythms are as equidistant as possible; the same results can be obtained from the [Bresenham](https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm "Bresenham's line algorithm") algorithm.
>
> - https://en.wikipedia.org/wiki/Euclidean_rhythm

## Tools
- Sound-helix - https://www.soundhelix.com/audio-examples
- https://www.hisschemoller.com/blog/2019/music-pattern-generator-v2-1/
- https://bedroomproducersblog.com/2021/02/11/hy-esg-euclidean-gate/

- https://www.hornetplugins.com/hatefish-rhygenerator-euclidean-rhythm-step-sequencer/