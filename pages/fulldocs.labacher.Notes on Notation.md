---
created: 2022-01-23T10:08:49 (UTC +01:00)
tags: []
source: https://www.christophlabacher.com/notes/notes-on-notation
author: 
---

# Notes on Notation — Christoph Labacher

> ## Excerpt
> This is an ongoing collection of notes and quotes on notation systems. Notations allow us to externalize our thinking, making it storable and sharable This means it enables our thoughts to travel through time and space! and susceptible to a process of continuous molding. These properties make them incredibly powerful tools.

---
18\. January 2020

This is an ongoing collection of notes and quotes on notation systems. Notations allow us to externalize our thinking, making it storable and sharable (1)**This means it enables our thoughts to travel through time and space!** and susceptible to a process of continuous molding. These properties make them incredibly powerful tools.

According to Wikipedia “a notation is a collection of related symbols that are each given an arbitrary meaning, created to facilitate structured communication within a domain knowledge or field of study.”

## Notation in Knot Theory

Mathematical [knot theory](https://en.wikipedia.org/wiki/Knot_theory) was revolutionized not once, but twice when new, more powerful notation systems were invented. This is my favorite example to make when trying to explain to people why I am excited by thinking about novel notation systems. Katherine Ye gives a great introduction to this subject in her talk “[Strange Loops: Capturing Knots With Powerful Notations](https://www.youtube.com/watch?v=Wahc9Ocka1g)”.

## Bret Victor on Roman Numerals

> Have you ever tried multiplying roman numerals? It’s incredibly, ridiculously difficult. That’s why, before the 14th century, everyone thought that multiplication was an incredibly difficult concept, and only for the mathematical elite. Then Arabic numerals came along, with their nice place values, and we discovered that even seven-year-olds can handle multiplication just fine. **There was nothing difficult about the concept of multiplication — the problem was that numbers, at the time, had a bad user interface.**
> 
> Bret Victor in [Fast Company — Ex-Apple Designer Creates Teaching UI That “Kills Math” Using Data Viz](https://www.fastcompany.com/1664508/ex-apple-designer-creates-teaching-ui-that-kills-math-using-data-viz), 15.07.11

[Bret Victor](http://worrydream.com/)’s work in the area of interaction design is legendary. He has written and given some great essays and talks on notation, models, and tools for thought.

For a discussion on what we might learn from the “development” of Arabic numerals, refer to the section “[How to invent Hindu-Arabic numerals?](https://numinous.productions/ttft/#how-to-invent-hindu-arabic-numerals)” in the essay “[How can we develop transformative tools for thought?](https://numinous.productions/ttft/#how-to-invent-hindu-arabic-numerals)” by Andy Matuschak and Michael Nielsen.

## Malcolm Gladwell on the Chinese Number System

The way numbers are pronounced in Mandarin, Korean, and Japanese is much more logical than the English way (e.g. ten-one vs. eleven, five-ten-two vs. fifty-two). Because of this, it has been shown that Chinese children can learn to count and calculate much earlier. According to Gladwell “On average, Chinese 4-year-olds can count up to forty. Americans of the same age can only count to fifteen, and don’t reach forty until they’re five.”

> Ask an English seven-year-old to add thirty-seven plus twenty-two, in her head, and she has to convert the words to numbers (37 + 22). Only then can she do the math: 2 plus 7 is nine and 30 and 20 is 50, which makes 59.
> 
> Ask an Asian child to add three-tens-seven and two-tens-two, and then the necessary equation is right there, embedded in the sentence. No number translation is necessary: it’s five-tens-nine.
> 
> Malcolm Gladwell in Outliers

This insight is also mentioned by Aneta Pavlenko in “The Bilingual Mind” (2014) (p. 92), who cites the relevant papers by Miller and associates (2)[Miller & Stigler, 1987](https://psycnet.apa.org/record/1988-32309-001); [Miller et al., 1995](https://psycnet.apa.org/record/1995-36365-001); [Miller et al., 2000](https://psycnet.apa.org/buy/2000-00023-006). She also points to research showing that shorter digit words in a language seem to enable speakers to remember longer number sequences, e.g. when comparing English to Welsh, or Chinese to English, Finnish, Greek, Spanish and Swedish (3)[Ellis & Hennelley, 1980](https://psycnet.apa.org/record/1981-07584-001); [Chincotta & Underwood, 1997](https://psycnet.apa.org/record/1997-06261-004); [Stigler et al., 1986](https://psycnet.apa.org/record/1987-27805-001).

What’s interesting to me about this is that the difference is not in the symbols (11 and 52 are perfectly logical) but in how they are pronounced. Building on top of Bret Victor’s thought on Roman numerals it turns out **there are two ways to optimize notation: Its graphical properties as well as its verbal properties**.

## Jerome S. Bruner on Modes of Representation

Looking at “the techniques or technologies that aid growing human beings to represent in a manageable way the recurrent features of the complex environments in which they live” (p. 1) Jerome S. Bruner (1964) describes three “systems of processing information by which human being construct models of their world: trough action, through imagery, and through language” (p. 1). He developed this classification in order to describe how children can solve more abstract problems as they grow older. While the abilities to use these representations develop after each other, all of them remain “more or less intact throughout life” (p. 2). It is interesting however that the ability to solve complex problems correlates greatly with the ability to use symbolic representation: **Bruner considers “language as an instrument of thought”** (p. 13).

> \[T\]he most important thing about memory is not storage of past experience, but rather the retrieval of what is relevant in some usable form. This depends upon how past experience is coded and processed so that it may indeed be relevant in the present when needed. The end product of such a system of coding is what we may speak of as a representation.
> 
> Jerome S. Bruner: The Course of Cognitive Growth (p. 2)

Bruner describes representations as means by which situations are remembered (and manipulated) in the mind, but they can also be useful when talking about how to represent knowledge in a specific form of information. Each representation enables a specific kind of thinking and makes other kinds more difficult. Because of this, using the most appropriate kind of representation is crucial in order to enable to most productive use of knowledge in a certain situation.

### Enactive Representation

Enactive representation means representing “past events through appropriate motor response” (p. 2). An example of this is remembering a certain movement as “muscle memory”: Riding a bicycle or walking down a stairway are actions that can be easily performed, but are hard to describe because they are only represented as actions in our memory.

### Iconic Representation

Iconic representation means representing events by “the selective organization of percepts and of images, by the spatial, temporal, and qualitative structures of the perceptual field and their transformed images” (p. 2). This form of representation is mostly visual, meaning remembering things as mental pictures, that stands for one or more aspects of a situation.

### Symbolic Representation

Symbolic representation means representing events or things using a system of more or less arbitrary symbols. An example of this is language (both spoken and written), as well as forms of mathematic, musical, or other notation. Symbols can be manipulated, ordered, classified, combined, and so on. Because of this Bruner considers symbolic representation the most powerful one:

> **Translations of experience into symbolic form, with its attendant means of achieving remote reference, transformation, and combination, opens up realms of intellectual possibility that are orders of magnitude beyond the most powerful image forming system.**
> 
> Jerome S. Bruner: The Course of Cognitive Growth (p. 13 – 14)

Reference: Bruner, J. S. (1964). The course of cognitive growth. American Psychologist, 19(1), 1–15.

## Christopher Alexander on why we need a notation system for design problems

These quotes are taken from Christopher Alexander’s early book “Notes on the Synthesis of Form” (1964).

Alexander writes “\[...\] **if we look at the lack of organization and lack of clarity of the forms around us, it is plain that their design has often taxed their designer’s cognitive capacity well beyond the limit.** The idea that the capacity of man’s invention is limited is not so surprising, after all. In other areas it has been shown, and we admit readily enough, that there are bounds to man’s cognitive and creative capacity. \[...\] Similarly, the very frequent failure of individual designers to produce well organized forms suggests strongly that there are limits to the individual designer’s capacity.

“We know that there are similar limits to an individual’s capacity for mental arithmetic. To solve a sticky arithmetical problem, we need a way of setting out the problem which makes it perspicuous. Ordinary arithmetic convention gives us such a way. **Two minutes with a pencil on the back of an envelope lets us solve problems which we could not do in our heads if we tried for a hundred years. But at present we have no corresponding way of simplifying design problems for ourselves.** These notes describe a way of representing design problems which does make them easier to solve. It is a way of reducing the gap between the designer’s small capacity and the great size of his task.” (p. 5 – 6)

“The use of logical structures to represent design problems has an important consequence. It brings with it the loss of innocence. **A logical picture is easier to criticize than a vague picture since the assumptions it is based on are brought out into the open.** Its increased precision gives us the chance to sharpen our conception of what the design process involves. But once what we do intuitively can be described and compared with nonintuitive ways of doing the same things, we cannot go on accepting the intuitive method innocently. Whether we decide to stand for or against pure intuition as a method, we must do so for reasons which can be discussed.” (p. 8)

## Edsger Dijkstra on the Purpose of Abstraction

> The purpose of abstraction is not to be vague, but to create a new semantic level in which one can be absolutely precise.
> 
> [Edsger Dijkstra, “The Humble Programmer”, 1972](https://www.cs.utexas.edu/~EWD/transcriptions/EWD03xx/EWD340.html)

[Edsger W. Dijkstra](https://de.wikipedia.org/wiki/Edsger_W._Dijkstra) was a dutch programmer. “[The Humble Programmer](https://www.cs.utexas.edu/~EWD/transcriptions/EWD03xx/EWD340.html)” was the lecture he gave when he received the 1972 Turing Award.

## Non-Linear Writing Systems

Some interesting reads by [Sai](https://s.ai/):

-   Sai — [On the design of an ideal language](https://s.ai/essays/on_the_design_of_an_ideal_language) (2002)
-   Sai — [Non-Linear Fully Two-Dimensional Writing System Design](https://s.ai/essays/nlf2dws) (2005)
-   Sai — [Unker Non-Linear Writing System](https://s.ai/nlws/)

## Ted Nelson on Documents

> A document is not necessarily a simulation of paper. **In the most general sense, a document is a package of ideas created by human minds and addressed to human minds, intended for the furtherance of those ideas and those minds.** Human ideas manifest as text, connections, diagrams and more: thus how to store them and present them is a crucial issue for civilization.
> 
> Theodor H. Nelson in “[Transliterature: A Humanist Format for Re-Usable Documents and Media](http://transliterature.org/)” \[site seems to be offline\]

## Reading List

Material which I haven’t gotten around to reading yet:

-   Santoro et al., 2021: “[Symbolic Behaviour in Artificial Intelligence](https://arxiv.org/abs/2102.03406)” — “The ability to use symbols is the pinnacle of human intelligence, but has yet to be fully replicated in machines. Here we argue that the path towards symbolically fluent artificial intelligence (AI) begins with a reinterpretation of what symbols are, how they come to exist, and how a system behaves when it uses them. We begin by offering an interpretation of symbols as entities whose meaning is established by convention. But crucially, something is a symbol only for those who demonstrably and actively participate in this convention.”
