>Society can be destroyed when further growth of mass production renders the milieu hostile, when it extinguishes the free use of the natural abilities of society’s members, when it isolates people from each other and locks them into a man-made shell, when it undermines the texture of community by promoting extreme social polarization and splintering specialization, or when cancerous acceleration enforces social change at a rate that rules out legal, cultural, and political precedents as formal guidelines to present behavior. Corporate endeavors which thus threaten society cannot be tolerated. At this point it becomes irrelevant whether an enterprise is nominally owned by individuals, corporations, or the state, because no form of management can make such fundamental destruction serve a social purpose.

> In fact, however, the vision of new possibilities requires only the recognition that scientific discoveries can be used in at least two opposite ways. The first leads to specialization of functions, institutionalization of values and centralization of power and turns people into the accessories of bureaucracies or machines. The second enlarges the range of each person’s competence, control, and initiative, limited only by other individuals’ claims to an equal range of power and freedom.
> 
![](https://hedgedoc.sdbs.cz/uploads/64206809-6db8-4c6f-bbf1-7f1180493962.png)
The Reverend Monsignor

Tools for Conviviality

## Adjacent
- [[people.ClaudeBragdon]]
- [[people.VilemFlusser]]