# [[las]].roadmap
## v0.1
---> https://sdbs.cz/las/
## v0.2
- add content
	- text
		- viz [[las.future]]
	- images 
		- maps and structures
	- jumplinks 
- interaction
	- [x] DDR
	- [x] gamepad
	- [x] joystick

## v0.3
- gallery mode
	- context screen setup vs. space setup
	- context screen setup
		- 4k context beamer
		- HD focus display
		- stand X sit
		- possible
			- adjacent display
				- for [[concepts.hypertext]]
	- space setup
		- interaction and space and chill 
	- technicalia
		- hw specs?	
		- controllers?
		- 4k beamer
- content
	- video
		- historical / academical aproach?
			- archive.org?
- technicalia
	- video playback support  
## v0.5
- [[tml.upend]]