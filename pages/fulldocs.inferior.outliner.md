---
created: 2021-12-28T12:36:30 (UTC +01:00)
tags: [twitter,twitter thread,unroll,threadreader,threads]
source: https://threadreaderapp.com/thread/1474586679646195712.html
author: 
---

# Thread by @syncretizm on Thread Reader App – Thread Reader App

> ## Excerpt
> Thread by @syncretizm: The problem with outliners: Parent - A - B When we want a sequential chain of thought, we nest them as equal level children under the same parent. The alternative would be a very...…

---
The problem with outliners:

Parent  
\- A  
\- B

When we want a sequential chain of thought, we nest them as equal level children under the same parent. The alternative would be a very ugly way:

Parent  
\- A  
\- B  
\- C  
\- ...

This way makes more sense but looks horrible.

The maths doesn't work out either. When use method 1, A/B/C are related via an OR (x) function. In method 2, A/B/C are related via an AND (%) function. We intuitively feel this burden that something is not right. And yet we don't like going into indenting hell.

Part of the reason why I enjoyed writing in prose form in obsidian was because I didn't have to figure out whether I had to indent or write a sibling bullet. Isn't it intuitive? No it isn't, especially when you consider how a bullet is queried in future...

In outliners, querying for a thought doesn't show you a linear progression of thought. It shows you the parent-child relationship, discarding its siblings. The problem is that a thought is multidimensional, whereas an outline only has parent-child... [![[FHbOJtiVkAQXMcw.jpg]]](https://pbs.twimg.com/media/FHbOJtiVkAQXMcw.jpg) 

Axes of thought. An outliner only adequately shows the hierarchy axis, it is 1 dimensional. It also forces the progression axis to work in a parent-child relationship when it doesn't work that way. [![[FHbR1fDUcAQDWeY.jpg]]](https://pbs.twimg.com/media/FHbR1fDUcAQDWeY.jpg) 

A promising approach in [@obsdmd](https://twitter.com/obsdmd) using the breadcrumbs plugin... This could work... VERY well. [![[FHba1oKVgAAX220.jpg]]](https://pbs.twimg.com/media/FHba1oKVgAAX220.jpg) 

@mentions[@obsdmd](https://twitter.com/obsdmd) I'm getting more convinced that outliners are inferior to original Zettelkasten (using cards). Zettelkasten was essentially and originally a thread system made on paper.
