[Rick Prelinger
@footage](https://twitter.com/footage)
https://twitter.com/footage/status/1375893247201964032

THREAD: This is an troubling moment for moving image [#archives](https://twitter.com/hashtag/archives?src=hashtag_click): there's a new, heavily-hyped and market-driven model of digital scarcity ([#NFT](https://twitter.com/hashtag/NFT?src=hashtag_click)), which manufactures uniqueness and aura. At the same time AI-driven tools are turning original images into even prettier fiction. (1/5)

Will [#archives](https://twitter.com/hashtag/archives?src=hashtag_click) junk existing access models and mint [#NFTs](https://twitter.com/hashtag/NFTs?src=hashtag_click) to earn money they desperately need? Or will they use the [#blockchain](https://twitter.com/hashtag/blockchain?src=hashtag_click) to validate archival provenance? Or will we see known-provenance documents sabotaged by fakes whose "authenticity" is blockchain certified? (2/5)

Today, [#copyright](https://twitter.com/hashtag/copyright?src=hashtag_click) trolls claim ownership of many YouTube videos that are either public domain or owned by others. Who's going to prevent them from using the [#blockchain](https://twitter.com/hashtag/blockchain?src=hashtag_click) to claim the entire moving image heritage as their own? Are we in for a new regime of online policing? (3/5)

And finally, will AI-enhanced archival images kill the market for those that are "realer" but not quite as pretty? In sum, I fear [#archives](https://twitter.com/hashtag/archives?src=hashtag_click) are about to have whatever claims on authenticity and provenance they make yanked out from under them. (4/5)

Welcome to a new world of "fake archives" and speculation in archival collectibles. (5/5)

[[concepts.archives.art]]