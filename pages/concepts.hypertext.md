# hypertext

![Hypertext and Hypermedia - Digital competence in the EFL ...](https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fwww3.uah.es%2Ffarmamol%2FPublic%2FCurso_Internet%2FCERN%2FBasicHypertext.jpg&f=1&nofb=1)

## blazers PoV
- [[projects.pile]] >> docs: https://pile.sdbs.cz/tag/19
	- [Web, který nebyl: Xanadu a alternativní pojetí hypertextu](https://pile.sdbs.cz/item/82)
- http://www.eastgate.com/HypertextNow/



### [[people.TedNelson]]

#### - [[concepts.parallel textface]]/[[concepts.knowledge managment]] / [[areas.filetag]]
> A document is not necessarily a simulation of paper. **In the most general sense, a document is a package of ideas created by human minds and addressed to human minds, intended for the furtherance of those ideas and those minds.** Human ideas manifest as text, connections, diagrams and more: thus how to store them and present them is a crucial issue for civilization. 
> -  Theodor H. Nelson in “[Transliterature: A Humanist Format for Re-Usable Documents and Media](http://transliterature.org)” \[site seems to be offline\]

#### about web
> _I DON’T BUY IN_
>
>_[…]_
>
>_The original hypertext project, Xanadu®, has always been about pure document structures where authors and readers don’t have to think about computerish structures of files and hierarchical directories. The Xanadu project has endeavored to implement a pure structure of links and facilitated re-use of content in any amounts and ways, allowing authors to concentrate on what mattered._
>
>_[…]_
>
>_Markup must not be embedded. Hierarchies and files must not be part of the mental structure of documents. Links must go both ways. All these fundamental errors of the Web must be repaired. But the geeks have tried to lock the door behind them to make nothing else possible._

#### ![[concepts.backlinks]]

### etc
- https://www.wired.com/1995/06/xanadu/
- https://ben.balter.com/2015/11/12/why-urls/
- ----> https://subconscious.substack.com/p/hypertext-montage

#### [[people.MaggieAppleton]] summaries about current state
- https://maggieappleton.com/bidirectionals
- https://maggieappleton.com/pattern-languages
- https://maggieappleton.com/xanadu-patterns
- https://maggieappleton.com/neocyborgs
- 

#### Quotes about web

>My internet is substantially quieter than yours, and teaches me new things every day. 
> - [Bruce Sterling](https://www.wired.com/beyond-the-beyond/2020/06/islands-in-the-blog/)

>If someone thinks putting work online or putting subtitles on a video or turning the volume up or having a transcript compromises the experience, then we don’t need to work together. Surprisingly, these are rebuttals I hear mostly from administrators and curators— rarely from artists. I’m very fortunate in that I commission new work almost exclusively from artists, specifically for the digital space, so these are conversations that we’re having at the outset, not after the fact, and even that isn’t enough. I could and need to do more.
>- [What We Mean When We Rant about Digital Art](https://canadianart.ca/interviews/what-we-mean-when-we-rant-about-digital-art/)

>Hosts control URLs. When they delete a URL’s content, intentionally or not, readers find an unreachable website. This often irreversible decay of Web content is commonly known as linkrot. It is similar to the related problem of content drift, or the typically unannounced changes––retractions, additions, replacement––to the content at a particular URL.
>- https://www.cjr.org/analysis/linkrot-content-drift-new-york-times.php

## Adjacent

- [[incubation.annotation]]
- [[concepts.archives.art]]
- [[concepts.trailblazers]]
- [[incubation.concepts.literature.ergodic]]
- [[concepts.map]]
- [[concepts.web.doomed]]


-------------
-------------
-------------



-------------



https://mobile.twitter.com/chatur_shalabh/status/1323496735218442243

https://mobile.twitter.com/mkirschenbaum/status/1301898638994571266?s=20

https://obscuritory.com/multimedia/uncle-buddys-phantom-funhouse/

https://www.francismiller.com/non-linear-reading-routes/ - [[people.FrancisMiller]]