# [[las.quotes]].city theme

cyber -- kybernetes
ambience x ambiance - space and connection and space and context

## Sorted
### Myrto Kostaropoulou - Cities as Meta-Cities
-  https://theccd.org/article/72/cities-as-meta-cities/
  
> Zoli professes: “Each time, a city is inscribed into a long cultural sequence, of which it is the utmost incident”3.
> A city is a condition which more or less efficiently meets its citizens. Also it is a context within which consciousness develops. How would a historian describe the contemporary urban period in about 50 years from now?

- Cities as Meta-Cities  The Centre for Conscious Design
	- sauce it
	
### [[people.WilliamGibson]]
- what is missing?? TVsky?

####  interview about cities
- https://www.scientificamerican.com/article/gibson-interview-cities-in-fact-and-fiction/

#cities #cyber
>  Necessity being one of invention's many mothers, I have a certain faith in our ability to repurpose almost anything, provided it becomes sufficiently necessary. Then again, I suspect we've abandoned cities in the past because they were too thoroughly built to do some specific something that's no longer required.

#cities #cyber
>   The Internet, which I think of as a sort of meta-city, has made it possible for people who don't live in cities to master areas of expertise that previously required residence in a city, but I think it's still a faith in concentrated choice that drives migration to cities.

#### life in the metacity
- https://www.scientificamerican.com/article/life-in-a-meta-city/

** #cities **
> Cities are reduced in choice by Disneylanding themselves. It will become too perfect, there will be no more growth, and there will be a reduction of choice. But the visitors are expecting certain disneyfication in the city.but "of every Disneyland: you can't repurpose a theme park" (88).	

### [[people.JanPatocka]]
#### Polis 
#translate #cities #polis
>The emergence of epic and particularly dramatic poetry, in which individuals observe events with both internal and external perspectives, holds significant importance in the foundations of the historical process. To participate in these events is to surrender oneself to an orgy. History arises as an elevation from decline, as an understanding that life thus far has been a life in decline and that alternative possibilities of living exist. These possibilities transcend the toil for a filled stomach amidst poverty and scarcity, cleverly tamed by human techniques, and extend to the orgiastic moments of private and public life, sexuality, and cult. The Greek polis, epic, tragedy, and philosophy are various facets of the same impulse that signifies a surge from decline.

> Proto má v základech dějinného procesu tak velký význam vznik epické a potom zvláště dramatické poezie, v níž člověk přihlíží vnitřním a pak vnějším okem k dění, jehož se účastnit znamená propadnout orgii; dějiny vznikají jako povznesení z úpadku, jako pochopení,že život dosud byl životem v úpadku a že existuje jiná možnost či možnosti, jak žít, než na jedné straně mozolení za naplněným žaludkem v bídě, nedostatku, důmyslně kroceném lidskými technikami – a na druhé straně orgiastické chvíle soukromé a veřejné, sexualita a kult. Řecká polis, epos, tragédie a filosofie jsou různé stránky téhož elánu, který znamená vzmach z úpadku. 
![[patocka.polis.png.annotations]]/[[patocka.polis.png.annotations]]




### [[people.JaceClayton]]
#temporality #ambience 
> Recorded sound vibrates between history and pleasure. Live Sound exists only in the present. It cannot linger. This is one of the reasons why sound defines public space even more than architecture. Kids jamming that week's hit, neighbours fucking behind a thin wall, the call to prayers divine layer competing with traffic's blare, the loud low boom of something blowing up-and its opposite, hilltop garden quiet.

#architecture #memory
> Architecture real or imagined-prompts memory so efficiently because our brains are hardwired to recall things-in-places and unusual or -disturbing scenes. Memory-quiz champion envision fantastic images that correspond to what they want to remember and "place" each icon in some specific nook of their mental building. These mnemonic aids are called memory palaces. Their effectiveness speaks to how when a place is destroyed or erased, all the personal histories linked to it slip further into the past. #las 


### [[people.VilemFlusser]]
#### City as wave through...
** #cities** ** #topography**
> When it comes to cities, we should learn to think topographically rather than geographically and see the city not as a geographical place, but rather as a ﬂection in a ﬁeld. This is not a comfortable undertaking, as it involves one of those notorious paradigm shifts. When we were forced to see geography as the surface of a globe rather than as the description of a ﬂat area, we ran into problems: Do the occupants of the southern hemisphere stand on their heads?

#cities #connection #polis 
> The three spaces of the city intermingle with one another today like fuzzy sets. The public space pushes into the private thanks to cable (as in the case of television). Private space pushes into public thanks to machines (like cars). The city really no longer contains distinct private and public spaces, and the theoretical space is so thoroughly integrated into both that it is no longer recognizable because it has changed so much. Theory means contemplativeness, and it is sacred because it rises above the ﬂow of commerce. From this we get the weekend, vacation, retirement, and unemployment.

 #cities #human #dimension 
> We are those who are individuals who come together in the city. The old image of the city rests upon this image of humanity. This image of humanity has become unsuitable. Everything is divisible, and there can be no individual. Not only can atoms be split into particles but so can all mental objects; actions become “aktomes,” decisions become “dezidemes,” perceptions become stimulations, representations become pixels.

#cyber
> A striking aspect of this image of the city, when one has mobilized the necessary imagination for it, is its immateriality. In this image there are neither houses nor squares nor temples that are recognizable, rather only a network of wires, a confusion of cables.

#cyber #human 
> We must stop wanting to recognize ourselves and others and instead seek to recognize others and to ﬁnd ourselves in them again. We must break out of the capsule of the self and draw ourselves into concrete intersubjectivity. We must become projects out of subjects. The new city would be a projection of projects among human beings. That sounds utopic, which it certainly is, because the new city is not geographically locatable; on the contrary, it is everywhere where humans open up to one another. But precisely because it sounds utopic it is realistic.

#### [[incubation.flusser.odsubjektukprojektu]]

### [[1000p]] subplot
![[1000p.20211211235108.png]]
	


###  [[concepts.situationism]]


#### [[fulldocs.We Go Round and Round in the Night and Are Consumed by Fire]]

#### [[fulldocs.Theory of the Dérive]]
#ambience #cities 
>One of the basic situationist practices is the _dérive_ \[literally: “drifting”\], a technique of rapid passage through varied ambiances. Dérives involve playful-constructive behavior and awareness of psychogeographical effects, and are thus quite different from the classic notions of journey or stroll.

#cities #temporality
> In a dérive one or more persons during a certain period drop their relations, their work and leisure activities, and all their other usual motives for movement and action, and let themselves be drawn by the attractions of the terrain and the encounters they find there. Chance is a less important factor in this activity than one might think: from a dérive point of view cities have psychogeographical contours, with constant currents, fixed points and vortexes that strongly discourage entry into or exit from certain zones.

>Today the different unities of atmosphere and of dwellings are not precisely marked off, but are surrounded by more or less extended and indistinct bordering regions. The most general change that dérive experience leads to proposing is the constant diminution of these border regions, up to the point of their complete suppression. 
>
> Within architecture itself, the taste for dériving tends to promote all sorts of new forms of labyrinths made possible by modern techniques of construction. Thus in March 1955 the press reported the construction in New York of a building in which one can see the first signs of an opportunity to dérive inside an apartment: 
>
> “The apartments of the helicoidal building will be shaped like slices of cake. One will be able to enlarge or reduce them by shifting movable partitions. The half-floor gradations avoid limiting the number of rooms, since the tenant can request the use of the adjacent section on either upper or lower levels. With this setup three four-room apartments can be transformed into one twelve-room apartment in less than six hours.”

#### Introduction to a Critique of Urban Geography
** #psychogeography ** ** #perspective **
>  The word _psychogeography,_ suggested by an illiterate Kabyle as a general term for the phenomena a few of us were investigating around the summer of 1953, is not too inappropriate. It does not contradict the materialist perspective of the conditioning of life and thought by objective nature. Geography, for example, deals with the determinant action of general natural forces, such as soil composition or climatic conditions, on the economic structures of a society, and thus on the corresponding conception that such a society can have of the world. _Psychogeography_ could set for itself the study of the precise laws and specific effects of the geographical environment, consciously organized or not, on the emotions and behavior of individuals. The adjective _psychogeographical,_ retaining a rather pleasing vagueness, can thus be applied to the findings arrived at by this type of investigation, to their influence on human feelings, and even more generally to any situation or conduct that seems to reflect the same spirit of discovery.

#ambience #cyber
>   But from any standpoint other than that of police control, Haussmann's Paris is a city built by an idiot, full of sound and fury, signifying nothing. Today urbanism's main problem is ensuring the smooth circulation of a rapidly increasing quantity of motor vehicles. We might be justified in thinking that a future urbanism will also apply itself to no less utilitarian projects that will give the greatest consideration to psychogeographical possibilities.

** #cities ** ** #ambience **
>  The revolutionary transformation of the world, of all aspects of the world, will confirm all the dreams of abundance. The sudden change of ambiance in a street within the space of a few meters; the evident division of a city into zones of distinct psychic atmospheres; the path of least resistance which is automatically followed in aimless strolls (and which has no relation to the physical contour of the ground); the appealing or repelling character of certain places--all this seems to be neglected. In any case it is never envisaged as depending on causes that can be uncovered by careful analysis turned to account. People are quite aware that some neighborhoods are sad and others pleasant. But they generally simply assume elegant streets cause a feeling of satisfaction and that poor street are depressing, and let it go at that. In fact, the variety of possible combinations of ambiances, analogous to the blending of pure chemicals in an infinite number of mixtures, gives rise to feelings as differentiated and complex as any other form of spectacle can evoke. The slightest demystified investigation reveals that the qualitatively or quantitatively different influences of diverse urban decors cannot be determined solely on the basis of the era or architectural style, much less on the basis of housing conditions.

** #cities ** #arcades
> Certain of Chirico's paintings, which are clearly provoked by architecturally originated sensations, exert in turn an effect on their objective base to the point of transforming it: they tend themselves to become blueprints or models. Disquieting neighborhoods of arcades could one day carry on and fulfill the allure of these works. 
>
> I scarcely know of anything but those two harbors at dusk painted by Claude Lorrain--which are at the Louvre and which juxtapose extremely dissimilar urban ambiances--that can rival in beauty the Paris metro maps. It will be understood that in speaking here of beauty I don't have in mind plastic beauty--the new beauty can only be beauty of situation--but simply the particularly moving presentation, in both cases, of a _sum of possibilities._

> For example, in the preceding issue of this journal Marien proposed that when global resources have ceased to be squandered on the irrational enterprises that are imposed on us today, all the equestrian statues of all the cities of the world be assembled in a single desert. This would offer to the passersby--the future belongs to them--the spectacle of an artificial cavalry charge, which could even be dedicated to the memory of the greatest massacrers of history, from Tamerlane to Ridgway. Here we see reappear one of the main demands of this generation: educative value. 

#### Formulary for a New Urbanism

** #cities ** ** #perspective **
>  All cities are geological; you cannot take three steps without encountering ghosts bearing all the prestige of their legends. We move within a _closed_ landscape whose landmarks constantly draw us toward the past. Certain _shifting_ angles, certain _receding_ perspectives, allow us to glimpse original conceptions of space, but this vision remains fragmentary. It must be sought in the magical locales of fairy tales and surrealist writings: castles, endless walls, little forgotten bars, mammoth caverns, casino mirrors.

** #cities ** #ambience 
>  Darkness and obscurity are banished by artificial lighting, and the seasons by air conditioning; night and summer are losing their charm and dawn is disappearing. The man of the cities thinks he has escaped from cosmic reality, but there is no corresponding expansion of his dream life. The reason is clear: dreams spring from reality and are realized in it.

** #cities  ** #temporality 
>   Architecture is the simplest means of _articulating_ time and space, of _modulating_ reality, of engendering dreams. It is a matter not only of plastic articulation and modulation expressing an ephemeral beauty, but of a modulation producing influences in accordance with the eternal spectrum of human desires and the progress in realizing them.

### [[people.WalterBenjamin]]	

#### arcade project

** #cities ** ** #arcades  **
> The crowd was the veil from behind which the familiar city as phantasmagoria beckoned to the _flâneur_. In it, the city was now landscape, now a room. And both of these went into the construction of the department store, which made use of _flânerie_ itself in order to sell goods. The department store was the _flâneur's_ final coup. As _flâneurs_, the intelligentsia came into the market place. As they thought, to observe it – but in reality it was already to find a buyer. In this intermediary stage ... they took the form of the _[bohème](https://en.wikipedia.org/wiki/Bohemianism "Bohemianism")_. To the uncertainty of their economic position corresponded the uncertainty of their political function.

>  Drawing on Fournel, and on his analysis of the poetry of Baudelaire, [Walter Benjamin](https://en.wikipedia.org/wiki/Walter_Benjamin "Walter Benjamin") described the _flâneur_ as the essential figure of the modern urban spectator, an amateur detective and investigator of the city. More than this, his _flâneur_ was a sign of the alienation of the city and of capitalism. For Benjamin, the _flâneur_ met his demise with the triumph of [consumer capitalism](https://en.wikipedia.org/wiki/Consumer_capitalism "Consumer capitalism").[\[7\]](https://en.wikipedia.org/wiki/Fl%C3%A2neur#cite_note-benjamin-7)

> Fournel wrote: "The _flâneur_ must not be confused with the _badaud_; a nuance should be observed there .... The simple _flâneur_ is always in full possession of his individuality, whereas the individuality of the _badaud_ disappears. It is absorbed by the outside world ... which intoxicates him to the point where he forgets himself. Under the influence of the spectacle which presents itself to him, the _badaud_ becomes an impersonal creature; he is no longer a human being, he is part of the public, of the crowd."[\[8\]](https://en.wikipedia.org/wiki/Fl%C3%A2neur#cite_note-FOOTNOTEFournel1867270-8)[\[1\]](https://en.wikipedia.org/wiki/Fl%C3%A2neur#cite_note-FOOTNOTEShaya2004-1)

> The real aim of Haussmann’s works was the securing of the city against civil war. He wished to make the erection of barricades in Paris impossible for all time. […] Haussmann intended to put a stop to it in two ways. The breadth of the streets was to make the erection of barricades impossible, and new streets were to provide the shortest route between the barracks and the working-class areas. Contemporaries christened the undertaking: ‘L’embellissement stratégique.’
>  - urban kinesthecish #fix


### [[people.ClaudeBragdon]]
** #cities  #human #ambience  **junction** >> Edward O. Wilson [[people.EdwardOWilson]]
> MAN THE SPACE-EATER
>
> Man has been called the thinking animal. Space-eater would be a more appropriate title, since he so dauntlessly and persistently addresses himself to overcoming the limitations of his space. To realize his success in this, compare, for example, the voyage of Columbus' caravels with that of an ocean liner; or traveling by stage coach with train de luxe. Consider the telephone, the phonograph, the cinematograph, from the standpoint of space-conquest—and wireless telegraphy which sends forth messages in every direction, over sea and land. Most impressive of all are the achievements in the domain of astronomy. One by one the sky has yielded its amazing secrets, till the mind roams free among the stars. The reason why there are to-day so many men braving death in the air is because the conquest of the third dimension is the task to which the Zeit-Geist has for the moment addressed itself, and these intrepid aviators are its chosen instruments—sacrificial pawns in the dimension-gaining game.
>
> All these things are only the outward and visible signs of the angel, incarnate in a world of three dimensions, striving to realize higher spatial, or heavenly, conditions. This spectacle, for example, of a millionaire hurled across a continent in a special train to be present at the bedside of a stricken dear one, may be interpreted as the endeavor of an incarnate soul to achieve, with the aid of human ingenuity applied to space annihilation, that which, discarnate, it could compass without delay or effort. 


### Transmitting Architecture-The Transphysical City]]
- [[fulldocs.Transmitting Architecture-The Transphysical City]]

- also [[Quotes.transmitting.architecture]]

#cities #cyber //follow flusser
>**In this effort** to extend our range and presence to nonlocal realities, architecture has been a bystander, at most housing the equipment that enable us to extend our presence. The technologies that would allow the distribution or transmission of space and place have been unimaginable, until now. Though we learn about much of the world from the media, especially cinema and television, what they provide is only a passive image of place, lacking the inherent freedom of action that characterizes reality, and imposing a single narrative thread upon what is normally an open field of spatial opportunity. However, now that the cinematic image has become habitable and interactive, that boundary has been crossed irrevocably. Not only have we created the conditions for virtual community within a nonlocal electronic public realm, but we are now able to exercise the most radical gesture: distributing space and place, transmitting architecture.

#architecture #cyber //follow flusser
> **Once we cast** architecture into cyberspace, these concerns take on both theoretical and practical urgency. The architect must now take into active interest not only the motion of the user through the environment, but also account for the fact that the environment itself, unencumbered by gravity and other common constraints, may itself change position, attitude, or attribute. This new choreographic consideration is already a profound extension of responsibilities and opportunities, but it still corresponds only to "movement-image". Far more interesting and difficult is the next step, in which the environment is understood not only to move, but also to breathe and transform, to be cast into the wind not like a stone but like a bird. What this requires is the design of mechanisms and algorithms of animation and interactivity for every act of architecture. Mathematically, this means that time must now be added to the long list of parameters of which architecture is a function.

#perception #dimension // follow [[incubation.concepts.post-digital]]
> **We cannot know** the real in its entirety. As much shields as bridges, our senses isolate us from the outside world, even as the cognitive mechanisms that translate raw input into meaningful pattern isolate us from within. In either case, what we do know is known through sampling: continuous reality, if indeed it is continuous, is segmented and reconstituted to fit our understanding.
>  

## R&D neeeded


#### City Walks and Tactile Experience
- (https://contempaesthetics.org/newvolume/pages/article.php?articleID=607&searchstr=warburg)
	- https://www.researchgate.net/publication/298813950_City_Walks_and_Tactile_Experience

#### Urban Kinaesthetics _Tea Lobo_
https://contempaesthetics.org/2020/07/16/urban-kinaesthetics/

>As Alva Noë, one of the main proponents of enactive and embodied cognition, has argued, _all_ objects of perception are too complex to be observed as a whole and all at once. He draws our attention to the simple act of looking at a tomato. From whatever angle you observe, a side of it is always occluded. This is why the body, in its motor function, has such an important role to play in perception. The hidden side of the tomato is experienced as potentially available to perception because it is possible to move the eyes, the head, and the whole body, in order to view this other side. He notes, “My emphasis here is on a special kind of understanding that distinctly overwrites our _perceptual_ access to objects and properties, namely sensorimotor understanding.”[[3]](https://contempaesthetics.org/2020/07/16/urban-kinaesthetics/#_edn3) The proprioceptive sense of the body in motion and locomotion is an inherent component of visual experience, including the experience of the city. While the city as an object of perception might at first appear problematic because we cannot obtain a panoptic perspective of it, it becomes, in fact, a paradigmatic object of perception, if we adopt the view that all perception is necessarily embodied and that it presupposes sensorimotor engagement, that is, it requires some kind of motion (eye or head movement) or locomotion (walking from A to B) to be actualized.

>The 4Es stand for adjectives describing cognition: embodied, embedded, enactive and extended.

#ambience 
>Cognition is necessarily _embedded_ in the environment the organism interacts with, and much of the meaning produced, for instance, in visual perception is only to be understood from complex engagements with the structures of the organism’s surroundings. It is _enactive_ in the sense that meanings have to be teased out in a skillful manner. Also, it is _extended_ in the sense that tools or features of the environment often amplify or enable perception in the first place.

#dimension 
> By contrast, if a robot were to navigate a city, internally representing all possible actions would be impossible, since their number is indefinite:
>
> One can still single out in this “driving space” discrete items, such as wheels and windows, red lights, and other cars. But unlike the world of chessplaying, movement among objects is not a space that can be said to end neatly at some point. Should the robot pay attention to pedestrians or not? Should it take weather conditions into account? Or the country in which the city is located and its unique driving customs? Such a list of questions could go on forever. The driving world does not end at some point; it has the structure of ever-receding levels of detail that blend into a nonspecific background.[[5]](https://contempaesthetics.org/2020/07/16/urban-kinaesthetics/#_edn5)


#### Kyoto and atom bomb
"Kyoto has the advantage of the people being more highly intelligent and hence better able to appreciate the significance of the weapon." 
...
the person deserving credit for savingKyoto from destruction is Henry L. Stimson, the Secretary of War at the time, who had known and admired Kyoto ever since his lovely honeymoon there several decades earlier a nakonec On 30 May, Stimson asked Groves to remove Kyoto from the target list due to its historical, religious and cultural significance, Stimson then approached President Harry S. Truman about the matter. Truman agreed with Stimson, and Kyoto was temporarily removed from the target list.

![[Pasted image 20230520224644.png]]

### <>>?::{{>"?"}}

- [Bertolt Brecht 1935 Questions From a Worker Who Reads](https://www.marxists.org/archive/brecht/works/1935/questions.htm)
- https://www.bbc.com/future/article/20210505-how-cities-will-fossilise
- [[incubation.concepts.cities.invisible.design]]

### [[people.SeanCubitt]] 
- [sound and city](https://www.academia.edu/59847851/Sound_and_the_City)
	- academia.edu links to something completely different
- Online Sound and virtual architecture
	- www.mitpress.mit.edu/leonardo/journal/hplmj.html
	- ISEA96 proceedings
		-   http://www.isea-archives.org/symposia/isea2013/presenters-2013/isea2013-abstract-george-poonkhin-khut-james-brown-theta-lab-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-821/



### Are we human 
- tom wilkinson - article
- Colomina and Wigley - book
- [[incubation.interface]]
- to research
	- Frederick Kietler - Magic architecture
	-  An Unfinished Encyclopedia of Scale - https://www.mos.nyc/news/unfinished-encyclopedia-scale-figures-without-architecture

#human #ambience #fix 
> The twenty-first century is also the age of allergies, the age
of the "environmentally hypersensitive" unable to live in
the modern world. Never at any one time in history have
there been so many people allergic to chemicals, buildings,
electromagnetic fields (f Uf), fragrances. ' . ' Since the
environment is now almost completely man-made, we have
become allergic to ourselves, to our own hyperextended
body in a kind of autoimmune disorder. - colomina wigley

#cities #fix
> We have become sedentary beings, that is our
lot. The house eats away at us in our immobility,
like consumption. We will soon need too many
sanatoria. . . . Engineers are healthy and virile,
active and useful, moral and joyful. Architects are
disenchanted and idle, boastful or morose. That is
because they will soon have nothing to do. We have
no more moneylo pile up historical keepsakes. We
need to cleanse ourselves... .The diagnosis is clear.
Engineers make architecture. . . . People still believe,
here and there, in architects, just as people blindly
believe in doctors. - Le Corbusier






### [[people.McLuhan]] - City as classroom
>teledyn - In "City as a Classroom", they describe an experiment comparing comprehension between a front-projected vs back-projected screens and found very similar results, that back projection triggers a fireplace-stare lull. McLuhan often cited this result in comparing television to cinema. 
#medialiteracy #mcluhan #smartphones #criticalreasoning 
>
> https://www.nature.com/articles/s41598-022-05605-0​

>Let us begin by wondering just what you are doing sitting there at your desk. Here [in the pages that follow] are some questions for you to explore…The questions and experiments you will find in this book are all concerned with important, relatively unexplored areas of our social environment. The research you choose to do will be important and original.

> _What did the designers of traditional schools intend when they put thirty or so desks in rows, facing the front of the room? Why is the blackboard at the front? why is the teacher’s desk at the front?_

>**“_We have to realize that more instruction is going on outside the classroom, many times more every minute of the day than goes on inside the classroom. That is, the amount of information that is embedded in young minds per minute outside the classroom far exceeds anything that happens inside the classroom in just quantitative terms now.”_ _“In the future basic skills will no longer be taught in classrooms.”_ – _McLuhan, M. (1966, April). Electronics & the psychic drop-out. THIS Magazine is about SCHOOLS._  p. 38.**

#### interview
- https://mcluhansnewsciences.com/mcluhan/2018/09/mcluhan-interview-on-the-city-as-classroom/
- 
> A child of three today has been around the world thousands of times with advertisements. He has traveled to every corner of the earth with advertisements and other shows. So that he knows more than Methuselah. Methuselah at the age of nine hundred had known very little about this planet. He had never been around the world. But any infant today of three has been around the world many times in every corner of the world and this incredible situation is not recognized in the schools and not taken advantage of. The phrase ‘grey at three’ comes out of James Joyce’s Finnegans Wake but Finnegans Wake is quite aware that anybody who learns to speak a proper tongue or dialect has acquired vast information and vast skill. A child who at the age of one speaks English or his native tongue has learned more than he will ever learn again in all his life put together.

## Stub
### Mundaneum
> V rámci Světové výstavy EXPO 1910 která se konala v Bruselu vytvořil Otlet s La Fontainem instalaci Mundaneum jako utopickou vizi kosmopolitního „města intelektu.“ Za podpory belgické vlády se Otletův plán začal formovat do podoby enormní kolekce kartiček a dokumentů, tehdy nejmodernější technologie pro skladování informací. S iniciativou byla spojená i vyhledávací služba – uživatelé mohli poštou zaslat dotazy a za poplatek 27 franků na tisíc kartiček obdrželi odpověď. (ibid., s. 188–189)


> [](https://en.wikipedia.org/wiki/Mundaneum#cite_note-6)The Mundaneum was originally housed at the [Palais du Cinquantenaire](https://en.wikipedia.org/wiki/Cinquantenaire_Museum "Cinquantenaire Museum") in [Brussels](https://en.wikipedia.org/wiki/Brussels "Brussels") (Belgium). This was originally renamed Palais Mondial, before the name Mundaneum was adopted. Otlet commissioned architect [Le Corbusier](https://en.wikipedia.org/wiki/Le_Corbusier "Le Corbusier") to design a Mundaneum project to be built in [Geneva, Switzerland](https://en.wikipedia.org/wiki/Geneva,_Switzerland "Geneva, Switzerland") in 1929.[[5]](https://en.wikipedia.org/wiki/Mundaneum#cite_note-:0-5) Although never built, the project triggered the [Mundaneum Affair](https://en.wikipedia.org/w/index.php?title=Mundaneum_Affair&action=edit&redlink=1 "Mundaneum Affair (page does not exist)"), a theoretical argument between Corbusier and Czech critic and architect [Karel Teige](https://en.wikipedia.org/wiki/Karel_Teige "Karel Teige").
>  - https://en.wikipedia.org/wiki/Mundaneum

- http://www.mundaneum.org/en

##### follow
- [[concepts.udc]] # Universal Decimal Classification
- [[concepts.knowledge managment.methodologies]]
- https://artsandculture.google.com/story/0AXRA53tkh4A8A

### [[people.MarkFisher]]
##### flatline constructs

> 72. Dick, Do Androids Dream of Electric Sheep?, 28 Compare this passage from Abstraction and Empathy. “In the Ionic temple and the architectural development ensuing upon it the purely constructional skeleton, which is based solely the laws of matter […] was guided over into the more friendly and agreeable life of the organic, and purely mechanical functions became organic in their effect. The criterion of the organic is always the harmonious, always the balanced, the inwardly calm into whose movement and rhythm we can without difficulty flow with the vital sensation of our organisms. In absolute antithesis to the Greek idea of architecture, we have the, on the other hand, the Egyptian pyramid, which calls a halt to our empathy impulse and presents itself to us as a purely crystalline substance. A third possibility now confronts us in the Gothic cathedral, which indeed operates with abstract values, but nonetheless directs an extremely strong and forcible appeal to our capacity for empathy.
Here, however, constructional relations are not illumined by a feeling for the organic, as is the process in Greek temple building, but purely mechanical relationships of forces are brought to view per se, and in addition these relationships of forces are intensified to the maximum in their tendency to movement and in their content by a power of empathy that extends to the abstract. It is not the life of an organism which we see before us, but that of mechanism. No organic harmony surrounds the feeling of reverence toward the world, but an ever growing and self-intensifying restless striving without deliverance sweeps the inwardly disharmonious psyche away with it in an extravagant ecstasy, into fervent excelsior.” (115


### Unsorted

![[machupicchu.highway.png]]
>  https://www.linkedin.com/pulse/machu-picchu-white-paper-khang-vu-tien/-

>Edent - Book Review: Me++ The Cyborg Self and the Networked City - William J. Mitchell https://shkspr.mobi/blog/2022/07/book-review-me-the-cyborg-self-and-the-networked-city-william-j-mitchell/​

Consider Lewis Mumford:

“Between 1820 and 1900 the destruction and disorder within great cities is like that of a battlefield, proportionate to the very extent of their equipment and the strength of the forces employed. In the new province of city building, one must now keep one’s eyes on the bankers, industrialists, and the mechanical inventors. They were responsible for most of what was good and almost all that was bad. In their own image, they created a new type of city—that which Dickens, in Hard Times, called Coketown. In a greater or lesser degree, every city in the Western World was stamped with the archetypal characteristics of Coketown. Industrialism, the main creative force of the nineteenth century, produced the most degraded urban environment the world had yet seen: for even the quarters of the ruling classes were befouled and overcrowded.” (The City In History, 1961).

------
- city density? information density? parix x new york

-  Moody Attunement in Giorgio de Chirico's Metaphysical City
	- [Dagmar Motycka Weston](https://www.researchgate.net/scientific-contributions/Dagmar-Motycka-Weston-2139503755)
	- > Overly pragmatic conceptions of the city pose a threat to the urban public realm. The Surrealist view offers an alternative. It is explored here by architectural theorist and historian Dagmar Motycka Weston, Honorary Research Fellow at the University of Edinburgh, through the work of the Greek-Italian artist Giorgio de Chirico, which posits other ways to conceive of cities – particularly in relation to the German Romantic notion of ‘moody attunement’ (Stimmung).

- !!! https://tomasp.net/blog/2020/cities-and-programming/

![[Pasted image 20230314210333.png]]

## Adjacent
- [[concepts.map]]
- [[concepts.topology]]
- [[concepts.topography]]
- [[areas.architecture]]
- [[people.ClaudeBragdon]]

-------------------------------

Also https://en.wikipedia.org/wiki/Ponte_City


-----------------------
![[x6JY10t.jpeg]]