# Holon
[[people.ArthurKoestler]]

>Koestler's [materialistic](https://en.wikipedia.org/wiki/Materialism "Materialism") account argues that the personal experience of [duality](https://en.wikipedia.org/wiki/Mind-body_dualism "Mind-body dualism") arises from what Koestler calls a _[holon](https://en.wikipedia.org/wiki/Holon_(philosophy) "Holon (philosophy)")_. The notion of a holon emerges from the observation that everything in the nature is both a whole and a part. It is true for atoms, which are wholes in themselves, but also parts of molecules, of molecules, which can be both wholes and parts of cells, and of cells, which are both autonomous units and parts of organisms. It is also true for human beings, who have an independent life and are part of social systems. Every holon is willing to express two contradictory tendencies: to express itself, and to disappear into something greater. For humans, those tendencies lead to an error in development: we create collective units that are based on the oppression of some individuals and on the inflated egos of others. This is for Koestler an error of transcendence that is reflected in a poor integration of our reptilian brain and cognitive brain.
> -- https://en.wikipedia.org/wiki/The_Ghost_in_the_Machine

## Ghost in the Machine
> Following the holon of humanity down to its roots, the work explains humanity's tendency toward self-destruction in terms of brain structure, philosophies, and its overarching, cyclical political–historical dynamics, reaching the height of its potential in the nuclear arms arena.
>
>One of the book's central concepts is that as the human [triune brain](https://en.wikipedia.org/wiki/Triune_brain "Triune brain") has evolved, it has retained and built upon earlier, more primitive brain structures. The head portion of the "[ghost in the machine](https://en.wikipedia.org/wiki/Ghost_in_the_machine "Ghost in the machine")" has, as a consequence of poor, inadequate connections, a rich potential for conflict. The primitive layers can, and may, together, overpower rational logic's hold. This explains a person's [hate](https://en.wikipedia.org/wiki/Hate "Hate"), [anger](https://en.wikipedia.org/wiki/Anger "Anger") and other such [emotional distress](https://en.wikipedia.org/wiki/Stress_(biology) "Stress (biology)").

### OG
[[people.GilbertRyle]]

>There is a doctrine about the nature and place of the [mind](https://en.wikipedia.org/wiki/Mind "Mind") which is prevalent among [theorists](https://en.wikipedia.org/wiki/Theorist "Theorist"), to which most [philosophers](https://en.wikipedia.org/wiki/Philosopher "Philosopher"), [psychologists](https://en.wikipedia.org/wiki/Psychologist "Psychologist") and religious teachers subscribe with minor reservations. Although they admit certain theoretical difficulties in it, they tend to assume that these can be overcome without serious modifications being made to the architecture of the theory.... [The doctrine states that] with the doubtful exceptions of the mentally-incompetent and infants-in-arms, every human being has both a body and a mind.... The body and the mind are ordinarily harnessed together, but after the death of the body the mind may continue to exist and function.
> -- https://en.wikipedia.org/wiki/Ghost_in_the_machine

## Adjacent
- [[concepts.rhizome]]
- [[people.Martinus]]
