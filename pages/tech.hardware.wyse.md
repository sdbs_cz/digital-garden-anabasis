# Dell Wyse 

## thin client
-

## As tiny-server
- https://www.reddit.com/r/homelab/comments/ihb96s/anyone_else_here_using_dells_wyse_5060_as_a/

### media server
- https://www.reddit.com/r/PleX/comments/md1e04/wyse_5070_is_a_good_silent_plex_server_with_hw/

>[certuna](https://www.reddit.com/user/certuna/)

· [6 mo. ago](https://www.reddit.com/r/selfhosted/comments/orb54q/comment/h6it3pa/?utm_source=reddit&utm_medium=web2x&context=3) · edited 6 mo. ago

The 5010 is too old/slow to do anything useful with IMO, I wouldn’t bother with that anymore.

The 5020 at least has a HD8330E gpu onboard to do hardware encoding, four decent cpu cores and 4 GB RAM. I got it two years ago as a second Plex server and it works very well, but I wouldn’t really advise anything slower.
- https://www.reddit.com/r/selfhosted/comments/orb54q/cheapest_server_to_buy_and_not_rent_for_jellyfin/

## Adjacent
- [[linux.server]]