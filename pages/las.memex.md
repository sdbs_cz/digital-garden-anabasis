- Role of interface
	- Software and readability
	- Hardware and interaction
	- Focus x Context
- Role of information
	- [[people.VilemFlusser]] x [[people.AbyWarburg]] x [[people.TedNelson]]
	- connections
	- [[incubation.attention.economy]]
- And what about that hauntology vibe?


-------------

- [[concepts.knowledge managment]]
- [[incubation.concept.canvas]]
	- [[incubation.infinitecanvas]]
- [[incubation.mediamateriality]]
- [[incubation.interface]]
	- [[incubation.tech.screen]]


-------------------------
- [Immersive mediums replace thought](https://notes.azlen.me/s3husuaw/)
- 

>teledyn - In "City as a Classroom", they describe an experiment comparing comprehension between a front-projected vs back-projected screens and found very similar results, that back projection triggers a fireplace-stare lull. McLuhan often cited this result in comparing television to cinema.  
#medialiteracy #mcluhan #smartphones #criticalreasoning
>
> [https://www.nature.com/articles/s41598-022-05605-0​](https://www.nature.com/articles/s41598-022-05605-0%E2%80%8B)

---------------

![[Pasted image 20230519213304.png]]
![[Pasted image 20230519234532.png]]


![[Pasted image 20230519234833.png]]

-----------

And what



-----------------------
- [[las.letsplay]]