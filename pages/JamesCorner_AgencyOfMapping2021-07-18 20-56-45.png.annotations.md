![[JamesCorner_AgencyOfMapping2021-07-18 20-56-45.png]]

>Mapping is a fantastic cultural project, creating and building the world as
much as measuring and describing it. Long affiliated with the planning
and design of cities, landscapes and buildings, mapping is particularly
instrumental in the construing and constructing of lived space. In this
active sense, the function of mapping is less to mirror reality than to
engender the re-shaping of the worlds in which people live. While there


 - James Corner - Agency of Mapping
-- - https://architecturesofspatialjustice.wordpress.com/2012/10/09/agency_mapping/