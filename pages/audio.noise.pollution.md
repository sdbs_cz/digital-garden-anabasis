>**Cities have become the epicentre of a type of pollution, acoustics,** which, although its invisibility and the fact that coronavirus crisis reduced it until almost yearn it, is severely damaging to human beings. So much so that the European Environment Agency estimates that noise is responsible for 72,000 hospital admissions and 16,600 premature deaths every year in Europe alone.
>... 
>Not all sound is considered noise pollution. **The World Health Organization (WHO) defines noise above 65 decibels (dB) as noise pollution.** To be precise, noise becomes harmful when it exceeds 75 decibels (dB) and is painful above 120 dB. As a consequence, it is recommended noise levels be kept below 65 dB during the day and indicates that restful sleep is impossible with nighttime ambient noise levels in excess of 30 dB.
> - https://www.iberdrola.com/sustainability/what-is-noise-pollution-causes-effects-solutions
> - #cities #noise

https://www.eea.europa.eu/en/advanced-search?q=noise+pollution&size=n_10_n&filters%5B0%5D%5Bfield%5D=readingTime&filters%5B0%5D%5Btype%5D=any&filters%5B0%5D%5Bvalues%5D%5B0%5D%5Bname%5D=All&filters%5B0%5D%5Bvalues%5D%5B0%5D%5BrangeType%5D=fixed&filters%5B1%5D%5Bfield%5D=issued.date&filters%5B1%5D%5Btype%5D=any&filters%5B1%5D%5Bvalues%5D%5B0%5D=Last+5+years&filters%5B2%5D%5Bfield%5D=language&filters%5B2%5D%5Btype%5D=any&filters%5B2%5D%5Bvalues%5D%5B0%5D=en

https://www.sciencedirect.com/topics/earth-and-planetary-sciences/noise-emission ?

## See also
### Academia
- Noise pollution and violent crime
	- https://www.sciencedirect.com/science/article/pii/S0047272722001505
- Noise Pollution: _A Modern Plague_
	- https://www.nonoise.org/library/smj/smj.htm
- Evidence of the impact of noise pollution on biodiversity: a systematic map
	- https://environmentalevidencejournal.biomedcentral.com/articles/10.1186/s13750-020-00202-y

### AI and noise polution
- https://gemmo.ai/ai-noise-pollution/
- https://www.smartcitiesworld.net/news/news/belgian-city-uses-artificial-intelligence-to-tackle-noise-pollution-7243
- https://www.researchgate.net/publication/366764742_Profiling_of_Urban_Noise_Using_Artificial_Intelligence
- https://phys.org/news/2019-03-artificial-intelligence-citizen-perception-events.html

### Others
https://www.nonoise.org/
## Attention
- What about notification sounds
- What about portable speakers
- [[incubation.attention.economy]]

## Adjacent
[[Shluk]]
[[noise.praha]]
