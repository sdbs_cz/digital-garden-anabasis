# parallel textface
[[people.TedNelson]] [[people.VannevarBush]] [[concepts.hypertext]]
>The Parallel Textface that Nelson added to the Xanadu Project in the early 1970's was unique because it allowed a user to create links between documents, even if they were not related. As Nelson notes, "In Bush's trails, the user had no choices to make as he moved through the sequence of items, except at an intersection of trails" (Nelson, 1972, 253). Nelson's system would improve Bush's by giving the user a choice. Additionally, lines that showed the connections between two docuemnts would be displayed on the user's screen. These choices however, would be determined by the user who created the hypertext document.
> - http://www-personal.umich.edu/~mattkaz/history/hypertext2.html


## examples within gardens
+ [[people.AndyMatuschak]] + [[concepts.digital garden]]
	+ https://notes.andymatuschak.org/
+ federated wiki
	+ http://fed.wiki.org/view/welcome-visitors/view/about-federated-wiki/about.fed.wiki/structures

## #techmech
[[tools.obsidian]] - andymatuschak mode plugin

%{"|:?><<>>"%