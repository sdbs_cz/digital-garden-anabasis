# Topology
#chaosstream 

>The definition of topology leads to the following mathematical joke (Renteln and Dundes 2005):
>
>Q: What is a topologist? A: Someone who cannot distinguish between a doughnut and a coffee cup.
>> https://mathworld.wolfram.com/Topology.html

```


# Topology | Definition of Topology by Merriam-Webster

> ## Excerpt
> topographic study of a particular place; specifically : the history of a region as indicated by its topography… See the full definition

---

1 **:** [topographic](https://www.merriam-webster.com/dictionary/topographic) study of a particular place specifically **:** the history of a region as indicated by its [topography](https://www.merriam-webster.com/dictionary/topography)

2a(1) **:** a branch of mathematics concerned with those properties of geometric configurations (such as point sets) which are unaltered by elastic deformations (such as a stretching or a twisting) that are homeomorphisms

(2) **:** the set of all open subsets of a [topological](https://www.merriam-webster.com/dictionary/topological) space

b **:** [configuration](https://www.merriam-webster.com/dictionary/configuration) topology of a molecule topology of a magnetic field

source: https://www.merriam-webster.com/dictionary/topology


```

- https://www.quantamagazine.org/in-topology-when-are-two-shapes-the-same-20210928/

# #chaostream 
- [[concepts.topography]]
- [[fulldocs.mastodon.gis]]

- https://www.lacanonline.com/2015/01/from-the-bridges-of-konigsberg-why-topology-matters-in-psychoanalysis/
	- https://twitter.com/i/status/1225173387624042498 #visual


----------------

# Adjacent
- [[concepts.archives.art]]
- [[people.VilemFlusser]] - Cities
- [[people.WalterBenjamin]] - Arcades

-----------------------------------
-------------------------
------------

##### Topology X Cities?

>![[Pasted image 20210729114352.png]]
>
>>https://appliednetsci.springeropen.com/articles/10.1007/s41109-019-0189-1/figures/5

##### Topology x kama sutra

>![[E_VyPd6WUAk_0uj.jpg]]
>
>>https://twitter.com/PrinceVogel/status/1438181268924010504
