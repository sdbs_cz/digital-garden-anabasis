# interface



## ---> https://t.mldk.cz/notes/6a929cd4-d0d6-4a4c-9bb2-2b672739c00c.html

## #tosort 

https://www.academia.edu/49544506/Interfaces_for_a_Global_Village_Nam_Jun_Paik_Marshall_McLuhan_and_the_Future

[making computers better - how we interact with our tools](https://adamwiggins.com/making-computers-better/interact)

[Are We Human? Interview with Mark Wigley](https://www.youtube.com/watch?v=43lDV6_jJd8)

[Screen As Room: An Architectural Perspective on User Interfaces](https://www.christophlabacher.com/notes/screen-as-room)
- [[fulldocs.labachar.Screen As Room An Architectural Perspective on User Interfaces]]

UI UX...........................

![image.png](blob:vector://vector/9087222b-5414-45aa-b20f-610a36a40995)


![[meme.interface.20210725225803.png]]

>teledyn - In "City as a Classroom", they describe an experiment comparing comprehension between a front-projected vs back-projected screens and found very similar results, that back projection triggers a fireplace-stare lull. McLuhan often cited this result in comparing television to cinema. 
#medialiteracy #mcluhan #smartphones #criticalreasoning 
>
> https://www.nature.com/articles/s41598-022-05605-0​

![[Pasted image 20230519213304.png]]

![[qfW52ej.jpeg]]
## Adjacent
- [[concepts.parallel textface]]

- [[fulldocs.twitter.zoomable_ui]]

- [[incubation.tech.screen]]

- [[people.SeanCubitt]]
- [[people.AlanKay]]
- [[people.JaronLanier]]

- [[incubation.infinitecanvas]]
- [[concepts.map]]
- [[incubation.mediamateriality]]
- [[incubation.concepts.post-digital]]
