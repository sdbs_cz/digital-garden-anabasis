# [[dga]].flat.graph

## `16/07/2022`
![[Pasted image 20220716180237.png]]
![[Pasted image 20220716180109.png]]
![[Pasted image 20220716180442.png]]
![[Pasted image 20220716180629.png]]
## `13/10/2021`
![[graph.20211013002036.png]]
![[graph.desc.20211013002205.png]]
![[graph.zoom.20211013002318.png]]
## `04/08/2021`
### ---> obsidian graph with color groups
![[graph.20210804005638.png]]
![[Pagraph.info.20210804161429.png]]

## `03/08/2021`
### ---> obsidian graph view without tags
![[Screenshot from 2021-08-03 01-59-10.png]]

## `26/07/2021`
### ---> garden server graph view - live --> https://garage.sdbs.cz/!graph
![[graph.20210725230846.png]]
### ---> filetree of syncthinged vault folder
```
Folder PATH listing
Volume serial number is 62CC-8A99
C:.
|   .gitattributes
|   000_start-here.md
|   annotation.md
|   archive.impossible.jpg
|   areas.Algorithmic Editing.md
|   areas.anarcheology.md
|   areas.artificial intelligence.md
|   areas.self-hosting.md
|   areas.stream.jamming.md
|   areas.stream.md
|   areas.stream.tech.audio.md
|   atlas_mnemosyne_desc.png
|   bicameral_idea.png
|   CatalogOFFDigitalDiscomfort.pdf
|   concepts.annotation.image.md
|   concepts.annotation.md
|   concepts.annotation.media.md
|   concepts.annotation.music notation.md
|   concepts.archive.md
|   concepts.archives.art.md
|   concepts.backlinks.md
|   concepts.bookmarking.md
|   concepts.cryptoart.md
|   concepts.digital garden.md
|   concepts.disambiguation.md
|   concepts.filesystem.md
|   concepts.filetag.md
|   concepts.flat-file.md
|   concepts.hypertext.md
|   concepts.knowledge managment.md
|   concepts.markup.md
|   concepts.memory.md
|   concepts.moc.md
|   concepts.parallel textface.md
|   concepts.playlisting.md
|   concepts.reading.md
|   concepts.trailblazers.md
|   danger-of-visual-repre.png
|   database art.md
|   data_wisdom_panel.png
|   defanti-nelson_network.png
|   [[[[[[[[[[[[dga]]]]]]]]]]]].backlinks.fail.md
|   dga.flat.graph.md
|   dga.md
|   dga.terms.md
|   dg_scheme.png
|   digital asset managment.md
|   discuss_sum.png
|   docdrop_screenshot.png
|   EXP_1_stone-japanprometheus.png
|   feedfarm.cards.md
|   folder_scheme.png
|   fulldocs.Attacked from Within  kuro5hin.org.md
|   fulldocs.internet_is_worse.md
|   fulldocs.mastodon.gis.md
|   fulldocs.Speed and Information Cyberspace Alarm.md
|   fulldocs.tags-tallguyjenks.md
|   fulldocs.twitter.amulets-ethereum.md
|   fulldocs.twitter.booksasmaps.md
|   fulldocs.twitter.codex.relations.md
|   fulldocs.twitter.cryptoart.amulets.md
|   fulldocs.twitter.doctorow.appstores.md
|   fulldocs.twitter.prelinger.nft.archives.md
|   fulldocs.twitter.selforganizing-system.md
|   fulldocs.twitter.zoomable_ui.md
|   image_trail_zoom.png
|   incubation.001_projects_schemas.md
|   incubation.archives in art history.md
|   incubation.codes.md
|   incubation.decentralization.md
|   incubation.interface.md
|   incubation.knowledge graph.md
|   incubation.map.md
|   incubation.map.sync-conflict-20210718-213602-ZL4KNMY.md
|   incubation.mediamateriality.md
|   incubation.openAI.md
|   incubation.speednote..md
|   incubation.topography.md
|   incubation.topology.md
|   incubation.wikipedia.md
|   intro_V1b.png
|   lalalala.png
|   las-reconcept-schema.png
|   las-suns-0156.png
|   las.cloud.obsidian.20210130152324.png
|   las.cloud.obsidian.png
|   las.credits.md
|   las.future.md
|   las.future.md.bak
|   las.idea.markmap.md
|   las.inkscape.authoring.md
|   las.inspirations.md
|   las.md
|   las.missing.md
|   las.quotes.cities.md
|   las.quotes.md
|   las.reconcept.anotace.md
|   las.reconcept.canvas.md
|   las.reconcept.frontpage.md
|   las.reconcept.intro.md
|   las.reconcept.md
|   las.reconcept.tasks.md
|   las.roadmap.md
|   linux.rename.md
|   mapping_language.png
|   misattributed_gadamer.png
|   moc.md
|   nelson-hypercomics.png
|   nelson-literarymachines-reading.png
|   nelson-literarymachines-reorganization.png
|   nelson-ttomuchtosay.png
|   nelson-xanadu.png
|   nelson_intertwingled.png
|   news.nft.rndm.20210316203805.png
|   nft.rndm.20210319222605.png
|   opio_viewer.png
|   output.txt
|   panels_using_trails.png
|   Pasted image 20201030141445.png
|   Pasted image 20210423172411.png
|   Pasted image 20210428151102.png
|   Pasted image 20210428151110.png
|   Pasted image 20210512234350.png
|   Pasted image 20210617195358.png
|   Pasted image 20210713135730.png
|   Pasted image 20210725225803.png
|   Pasted image 20210725230846.png
|   pcie - lanes speed.png
|   people.AbyWarburg.md
|   people.AndyMatuschak.md
|   people.DzigaVertov.md
|   people.TedNelson.md
|   people.VannevarBush.md
|   people.WalterBenjamin.md
|   pile.md
|   popcornmaker.png
|   projects.ALEADUB.md
|   projects.feedfarm.hardware.howtos.md
|   projects.feedfarm.hardware.md
|   projects.feedfarm.hardware.now.md
|   projects.feedfarm.md
|   projects.feedfarm.questions.md
|   projects.feedfarm.software.md
|   projects.feedfarm.video2x.md
|   projects.lalar.md
|   projects.portfolio generator.md
|   projects.portfolio generator.pg_backup.md
|   projects.portfolio generator.portfolio cms.md
|   projects.sermon.md
|   projects.upend.md
|   projects.upend.upend_notes_tmp.pdf
|   revisting_zoom.png
|   screen.areas.png
|   Screenshot from 2021-03-28 01-22-25.png
|   Screenshot from 2021-07-18 20-56-45.png
|   spatial_software_alignment_chart.png
|   tech.ffmpeg.md
|   tech.firefox.bandwidth.md
|   tech.grub.issues.md
|   tech.wifi.md
|   tools.archivebox.md
|   tools.blender.svg.md
|   tools.dendron.md
|   tools.EDL.md
|   tools.fediverse.md
|   tools.keyboard.cs.md
|   tools.magenta.md
|   tools.magnet links.md
|   tools.markdown.md
|   tools.mastodon.md
|   tools.mermaid.md
|   tools.obsidian.md
|   tools.raid.md
|   tools.screen.md
|   tools.syncthing.md
|   tools.tagspaces.md
|   tools.text.comparison.md
|   tools.thing.md
|   tools.tts.md
|   xkcd-zooming.png
|   
+---.obsidian
|       app.json
|       appearance.json
|       core-plugins.json
|       hotkeys.json
|       workspace
|       workspace.sync-conflict-20210723-110922-HIYJI7N
|       ~syncthing~workspace.tmp
|       
\---_INFORM
        audio_101.md
        avg.md
        consumer_habits_good.md
        docs.md
        ffmpeg.md
        firefox.md
        ipfs.md
        linux_tips.md
        manuals.md
        ninjam.md
        obs.md
        online_toolbox.md
        prg-wt.md
        reaper.md
        s.m.a.r.t.md
        sdbs_selfhosting.md
        start.md
        supercollider.md
        syncthing.md
        telegram.md
        video_101.md
        ```
		
```

# Adjacent
- [[concepts.flat-file]]
- [[concepts.map]]
- [[incubation.knowledge graph]]
- [[dga]]
