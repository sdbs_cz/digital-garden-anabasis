#!/bin/bash
while :;do
	CHANGED="$(git diff --name-only|sed -e 's/^pages\///'|perl -E 'say join ", ", map {chomp; $_} <>')"
	git pull && \
	git add . && \
	git commit -am "Automatic update, changed: ${CHANGED}" && \
	git push

	sleep 300
done
